# Nohejbal praha

System for the Prague futnet association.

The application consists of 4 distinct parts - 3 clients and an API server
responsible for data manipulating and providing. Detailed information about each
of these parts can be found on the linked pages bellow. In short, these pieces
operate as follows:

**API server**

Backend application built with [Django framework](https://www.djangoproject.com) and its RESTful extension [DRF](https://www.django-rest-framework.org).
API documentation is automatically generated in OpenAPI and [Swagger](https://swagger.io) format by [drf-spectacular](https://github.com/tfranzel/drf-spectacular) package.
In production, we use [PostgreSQL](https://www.postgresql.org) as a database and [NGINX](https://www.nginx.com) as our server.
NGINX and Django application is connected by [uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/) HTTP socket.
All the components are [Dockerized](https://www.docker.com).

**WEB client**

[React](https://reactjs.org/) SPA (Single Page Application) built on top of [react-router-dom](https://www.npmjs.com/package/react-router-dom).
Powered by [redux](https://redux.js.org/) store manipulation library and [axios](https://www.npmjs.com/package/axios)
HTTP client. Styles are implemented with [styled-components](https://styled-components.com/) library and everything is
nicely transpiled and bundled with [babel](https://babeljs.io/) + [webpack](https://webpack.js.org/).

**Android client**

[Android](https://www.android.com/) application written in [Kotlin](https://kotlinlang.org/), designed according to the [MVVM](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) architecture.
The app uses [Retrofit](https://square.github.io/retrofit/) library for API calls and the [Material Design](https://material.io/) library for UI elements.

**iOS client**

[iOS](https://developer.apple.com/ios/) application built with [SwiftUI](https://developer.apple.com/xcode/swiftui/) using [Swift](https://developer.apple.com/swift/) language, designed according to the MVVM using [Combine](https://developer.apple.com/documentation/combine) framework.
For API calls, the app uses [Alamofire](https://github.com/Alamofire/Alamofire) library. 

---

Generated and automatically deployed documentation lives [here](https://nikhec.gitlab.io/nohec).

## Backend
### System requirements
- Python >= 3.9 (preferably in venv)
- sqlite3 ~= 3.32.3 

### Run backend in docker
- go to `/API` and run `docker-compose up`
- it should run at `http://127.0.0.1:8000`
- check our API documentation at `http://127.0.0.1:8000/swagger/`
- you can login into admin at `http://127.0.0.1:8000/admin/` with `admin@admin.cz`, pwd `admin`

### Setup the backend
In the `API/` directory:
 1. install pip requirements: `python -m pip install -r requirements.txt`
 2. run migrations: `python manage.py migrate`
 3. optional - create superuser: `./manage.py createsuperuser`
 
### Running the backend
- `./manage.py runserver` in the `API/` directory.
- optionally, port can be set:
`./manage.py runserver 8080`

### Admin access
- open web browser and go to `http://127.0.0.1:8000/admin/`

### OpenAPI documentation
- Generated and automatically deployed documentation lives [here](https://nikhec.gitlab.io/nohec/API)
- How to generate schema:
`python manage.py spectacular --format openapi-json --file schema.json`

### Dummy accounts
There is an account for each referee and player with the email `<user_id>@nohec.cz` and with `pass` as a password. The user id can be calculated from referee id by adding 1 (`user_id = referee_id + 1`) and from player id by adding 21 (`user_id = player_id + 21`).

---
## Web App
More detailed readme can be found [here](./WEB/README.md)

### Setup the Wep App
In the `/WEB/` directory:
 1. execute `npm install` or `yarn install`
 1. copy `.env.example` > `.env` and set environmental variables accordingly
 1. preferably run `npm run configure` or `yarn configure` to install git hooks

### Documenting
Documentation can be auto-generated from [JSDoc](https://jsdoc.app/) annotations with `npm run docs` command to the `WEB/docs` directory.
Although most of the ugly and/or annoying behaviour is taken care of automatically in the background either by hooking into
docs generating in `WEB/tools/jsdoc-preprocessor.js` or by tweaking final UI with files in `WEB/tools/assets`, certain manual work still needs to be done
in order for all this shady stuff to work:
 - Each `React` component **must** be decorated with `@component`
 - Each `React` component exported with `default` **must** have `@name <Component Name>` decorator.
 - Files named `index.js` are ignored by default. To force including of such a file in the docs, decorate it with `@name`, `@category` and optionally `@module` (if relevant).
 - Docs generator converts markdown directives to appropriate `html` ones so feel free to use **bold**, `code`, *italic*, etc. to increase docs readability.
 - Each `.js` file must either start with file annotation JSDoc or anything other than JSDoc annotation. This is due to preprocessing
   which creates/appends JSDoc with appropriate decorators to the beginning of the file and might thus corrupt other annotations for classes/functions.

---
## Android App
### Build the app
 1. create file `local.properties` in the top level of the ANDROID folder. In the file create a `sdk.dir` property pointing to the Android SDK
 1. from the Android folder run `gradlew assembleDebug` 
 1. in folder `ANDROID/app/build/outputs/apk/debug` will be the generated APK

### Documentation
Generated and automatically deployed documentation lives [here](https://nikhec.gitlab.io/nohec/ANDROID)

Documentation can be auto-generated by [Dokka](https://kotlin.github.io/dokka/). Run the Gradke task `gradlew :app:DocsByDokka` which will generate a Javadoc to `ANDROID/app/build/javadoc`.

---
## iOS App
### Build the app
- Build the app from XCode 

### Documentation
Generated and automatically deployed documentation lives [here](https://nikhec.gitlab.io/nohec/IOS)

Documentation is generated by [SwiftDoc](https://github.com/SwiftDocOrg/swift-doc). Run `swift doc generate` in directory containing .swift files.

