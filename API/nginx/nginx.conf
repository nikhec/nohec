# the upstream component nginx needs to connect to
upstream django {
    server api:8000;
}

# configuration of the server
server {
    listen 80;
    server_name lab.d3s.mff.cuni.cz www.lab.d3s.mff.cuni.cz;

    charset     utf-8;
    # gzip conf
    gzip on;
    gzip_vary on;
    gzip_min_length 10240;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types text/plain text/css text/xml application/javascript text/javascript application/x-javascript application/xml;
    gzip_disable "MSIE [1-6]\.";

    # max upload size
    client_max_body_size 75M;

    location /static/ {
        add_header Access-Control-Allow-Origin '*';
        alias /code/deploy/static/;  # Django project's static files
    }

    location /media/ {
        add_header Access-Control-Allow-Origin '*';
        alias /code/deploy/MEDIA_ROOT/;
    }

    # Finally, send all non-media requests to the Django server.
    location / {
        uwsgi_pass  django;
        include     /etc/nginx/uwsgi_params; # the uwsgi_params file you installed
    }
}
