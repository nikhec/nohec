"""user URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
"""

from django.urls import path

from .views import LoginTokenAPI, LoginSessionAPI, LogoutSessionAPI, LoggedInTestAPI, SignatureAPI

urlpatterns = [
    path('login-token/', LoginTokenAPI.as_view()),
    path('login/', LoginSessionAPI.as_view()),
    path('logout/', LogoutSessionAPI.as_view()),
    path('logged-in-test/', LoggedInTestAPI.as_view()),
    path('signature/', SignatureAPI.as_view()),
]
