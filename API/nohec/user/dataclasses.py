from dataclasses import dataclass


@dataclass
class StatsRow:
    won_matches: int
    draw_matches: int
    lost_matches: int
