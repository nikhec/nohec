from django.contrib.auth import authenticate
from rest_framework import serializers

from nohec.user.models import Player, Referee, User


class UserSessionLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255)
    first_name = serializers.EmailField(max_length=255, read_only=True)
    last_name = serializers.EmailField(max_length=255, read_only=True)
    password = serializers.CharField(max_length=128, write_only=True)

    def validate(self, data):
        email = data.get('email', None)
        password = data.get('password', None)
        user = authenticate(request=self.context.get('request'), email=email, password=password)
        if user is None:
            raise serializers.ValidationError(
                'A user with this email and password is not found.'
            )
        return {
            'user': user,
        }


class EmptySerializer(serializers.Serializer):
    """ To be used in a view, which does not accept or return any data. Used by spectacular to generate docs. """
    pass


class PersonSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    name = serializers.SerializerMethodField()

    def get_name(self, person) -> str:
        return str(person)


class PlayerSerializer(PersonSerializer):
    class Meta:
        model = Player
        fields = ['id', 'name']


class RefereeSerializer(PersonSerializer):
    class Meta:
        model = Referee
        fields = ['id', 'name']


class UserSignatureSerializer(serializers.ModelSerializer):
    signature = serializers.CharField(source='get_signature', read_only=True)

    class Meta:
        model = User
        fields = ['signature']
