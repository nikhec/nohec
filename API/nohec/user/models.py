import base64
import hmac
from datetime import datetime
from typing import List

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.db.models import Q, QuerySet
from django.utils.timezone import now

from nohec import settings
from nohec.user.managers import CustomUserManager


class Individual(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    birth_date = models.DateField()

    def matches(self) -> QuerySet['Match']:
        """
        Returns matches that are relevant to the individual depending on it's roles.
        """
        from nohec.match.models import Match
        f = Q(pk__in=[])  # always False

        if hasattr(self, 'player') and len(current_teams := self.player.current_teams()) > 0:
            f |= (Q(home_team__in=current_teams) | Q(away_team__in=current_teams))

        if hasattr(self, 'referee'):
            f |= Q(referees=self.referee)

        return Match.objects.filter(f)

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class User(AbstractBaseUser, PermissionsMixin):
    username = None
    email = models.EmailField(unique=True)
    individual = models.OneToOneField(Individual, null=True, on_delete=models.SET_NULL, related_name='user')
    date_joined = models.DateTimeField(default=now)
    is_staff = models.BooleanField(default=False)

    # django auth
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def can_modify_match(self, match):
        """
        Returns a flag indicating whether the user can participate in the interactive match editing for the given match.
        """
        return self.individual is not None and match in self.individual.matches()

    def get_signature(self, date: datetime.date = datetime.today()) -> str:
        """
         Generate a Time-based One-time Password (TOTP, RFC6238) signature for the given date.
         This uses a HMAC-based OTP (RFC4226) with days since Unix epoch as a counter.
        """

        # This is not the safest way to obtain a unique secret per user, improve if required
        key = base64.b32decode(settings.SIGNATURE_KEY)  # convert Base32 secret to bytes
        key = hmac.digest(key, self.id.to_bytes(8, 'big', signed=False), 'sha256')  # hash the secret key using user id

        counter = (date - datetime(1970, 1, 1)).days  # count number of days since Unix epoch
        counter = counter.to_bytes(8, 'big', signed=False)  # convert counter to 8 bytes

        mac = hmac.digest(key, counter, 'sha1')  # generate a 20-byte string
        offset = mac[-1] & 0xf  # calculate the dynamic offset

        binary_code = ((mac[offset] & 0x7f) << 24  # extract binary value
                       | (mac[offset + 1] & 0xff) << 16
                       | (mac[offset + 2] & 0xff) << 8
                       | (mac[offset + 3] & 0xff))

        digits = settings.SIGNATURE_DIGITS
        return str(binary_code)[-digits:].zfill(digits)  # adjust to the correct number of digits

    def __str__(self):
        return self.email


class Player(models.Model):
    registration_number = models.CharField(max_length=10, unique=True)
    individual = models.OneToOneField(Individual, on_delete=models.CASCADE, related_name='player')

    def current_teams(self, when: datetime = now()) -> QuerySet['Team']:
        """
        Returns the teams that the player is a member of at the given time.
        By default the current date and time is used.
        """
        return self.teams.filter(Q(teammembership__until__gt=when) | Q(teammembership__until__isnull=True),
                                 teammembership__since__lte=when)

    def get_games_stats(self, matches_ids: List[int]) -> 'PlayerGamesStats':
        """
        Returns a `PlayerGamesStats` for the given matches.
        """
        from nohec.match.models import Game, Match
        from nohec.league.utils import count_stats_for_games
        from nohec.league.dataclasses import PlayerGamesStats

        if not matches_ids:
            raise Exception('`matches_ids` cannot be empty')

        first_match = Match.objects.get(id=matches_ids[0])
        players_team = first_match.home_team if self in first_match.home_players else first_match.away_team

        games = Game.objects \
            .filter(match_id__in=matches_ids) \
            .filter(
                Q(away_players=self) |
                Q(home_players=self) |
                Q(sets__home_substitutions__incoming=self) |
                Q(sets__away_substitutions__incoming=self)) \
            .select_related('match') \
            .prefetch_related('sets') \
            .distinct() \
            .all()

        return PlayerGamesStats(
            count_stats_for_games(games.filter(type=Game.GameType.SINGLE), players_team),
            count_stats_for_games(games.filter(type=Game.GameType.DOUBLE), players_team),
            count_stats_for_games(games.filter(type=Game.GameType.TRIPLE), players_team),
        )

    def __str__(self):
        return str(self.individual)


class Referee(models.Model):
    registration_number = models.CharField(max_length=10, unique=True)
    individual = models.OneToOneField(Individual, on_delete=models.CASCADE, related_name='referee')

    def __str__(self):
        return str(self.individual)
