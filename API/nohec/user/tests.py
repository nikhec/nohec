from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase, APIClient


class UsersManagersTests(TestCase):
    """
    Tests kindly borrowed from https://testdriven.io/blog/django-custom-user-model/
    This is here to make sure we don't make any incompatible changes.
    """

    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(email='user@nohec.cz', password='pass')
        self.assertEqual(user.email, 'user@nohec.cz')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        self.assertIsNone(user.username)
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email='')
        with self.assertRaises(ValueError):
            User.objects.create_user(email='', password='pass')

    def test_create_superuser(self):
        User = get_user_model()
        admin_user = User.objects.create_superuser('super@nohec.cz', 'pass')
        self.assertEqual(admin_user.email, 'super@nohec.cz')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        self.assertIsNone(admin_user.username)
        with self.assertRaises(ValueError):
            User.objects.create_superuser(email='super@nohec.cz', password='pass', is_staff=False)
        with self.assertRaises(ValueError):
            User.objects.create_superuser(email='super@nohec.cz', password='pass', is_superuser=False)


class AuthTest(APITestCase):
    def setUp(self) -> None:
        User = get_user_model()
        User.objects.create_user(email='user@nohec.cz', password='pass')

    def test_login_token_api(self):
        client = APIClient()

        # Invalid username
        response = client.post('/api/users/login-token/', {'username': 'not_a_user@nohec.cz', 'password': 'pass'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Invalid password
        response = client.post('/api/users/login-token/', {'username': 'user@nohec.cz', 'password': 'bad_pass'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Happy case
        response = client.post('/api/users/login-token/', {'username': 'user@nohec.cz', 'password': 'pass'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)
        self.assertTrue('user_id' in response.data)
        self.assertEqual(response.data['email'], 'user@nohec.cz')

        client.credentials(HTTP_AUTHORIZATION='Token ' + response.data['token'])
        response = client.get('/api/users/logged-in-test/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_session_api(self):
        client = APIClient()

        # Happy case
        response = client.post('/api/users/login/', {'email': 'user@nohec.cz', 'password': 'pass'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['email'], 'user@nohec.cz')

        response = client.get('/api/users/logged-in-test/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = client.post('/api/users/logout/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = client.get('/api/users/logged-in-test/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Invalid username
        response = client.post('/api/users/login/', {'email': 'not_a_user@nohec.cz', 'password': 'pass'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Invalid password
        response = client.post('/api/users/login/', {'email': 'user@nohec.cz', 'password': 'bad_pass'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Logout while not logged in
        response = client.post('/api/users/logout/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_signature_api(self):
        client = APIClient()

        # Not logged in
        response = client.get('/api/users/signature/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Happy case
        client.post('/api/users/login/', {'email': 'user@nohec.cz', 'password': 'pass'})
        response = client.get('/api/users/signature/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('signature' in response.data)
