from django.contrib.auth import login, logout
from django.http import HttpResponse
from drf_spectacular.utils import extend_schema, inline_serializer
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.fields import CharField, IntegerField, EmailField
from rest_framework.generics import GenericAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from nohec.user.serializers import UserSessionLoginSerializer, EmptySerializer, UserSignatureSerializer


class LoginTokenAPI(ObtainAuthToken):
    """
    Login endpoint for Token auth.
    """

    @extend_schema(
        responses=inline_serializer('ResponseTokenSerializer',
                                    fields={'token': CharField(), 'user_id': IntegerField(), 'email': EmailField()})
    )
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email
        })


class LoginSessionAPI(GenericAPIView):
    """
    Login endpoint for Session auth.
    """
    serializer_class = UserSessionLoginSerializer

    def post(self, request):
        serializer = UserSessionLoginSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        i = user.individual
        return Response({
            'email': user.email,
            'first_name': i.first_name if i else None,
            'last_name': i.last_name if i else None
        }, 201)


class LogoutSessionAPI(GenericAPIView):
    """
    Logout endpoint for Session auth.
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmptySerializer

    def post(self, request):
        logout(request)
        return HttpResponse(status=200)


class LoggedInTestAPI(GenericAPIView):
    """
    Test whether user's auth is still valid.
    """
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmptySerializer

    def get(self, request):
        return HttpResponse(status=200)


class SignatureAPI(RetrieveAPIView):
    """
    Returns the signature of the current user for today.
    """
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = UserSignatureSerializer

    def get_object(self):
        return self.request.user
