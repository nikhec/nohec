# Register your models here.

from django.contrib import admin

from nohec.user.models import User, Individual, Player, Referee


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    model = User
    list_display = ['email', 'individual', 'is_staff']
    readonly_fields = ['signature']

    def signature(self, obj):
        return obj.get_signature()


@admin.register(Individual)
class IndividualAdmin(admin.ModelAdmin):
    model = Individual
    list_display = ['first_name', 'last_name', 'birth_date']


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    model = Player
    list_display = ['registration_number', 'individual']


@admin.register(Referee)
class RefereeAdmin(admin.ModelAdmin):
    model = Referee
    list_display = ['registration_number', 'individual']
