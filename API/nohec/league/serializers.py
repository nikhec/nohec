from typing import List

from django.db.models import Q
from rest_framework import serializers

from nohec.league.models import Team, Season, League, Club, Court, Player
from nohec.match.models import Match
from nohec.user.serializers import PersonSerializer


class SeasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Season
        fields = '__all__'


class LeagueSerializer(serializers.ModelSerializer):
    class Meta:
        model = League
        exclude = ['has_referee', 'has_single']


class TeamSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField()
    logo = serializers.CharField(source='club.logo', read_only=True)

    class Meta:
        model = Team
        fields = ['id', 'name', 'logo']


class StandingRowSerializer(serializers.Serializer):
    position = serializers.IntegerField()
    team = TeamSerializer()
    matches = serializers.IntegerField()
    wins = serializers.IntegerField()
    draws = serializers.IntegerField()
    losses = serializers.IntegerField()
    sets_won = serializers.IntegerField()
    sets_lost = serializers.IntegerField()
    points = serializers.IntegerField()


class ClubSerializer(serializers.ModelSerializer):
    class Meta:
        model = Club
        fields = ['id', 'name', 'logo']


class CourtSerializer(serializers.ModelSerializer):
    class Meta:
        model = Court
        exclude = ['traveling_instructions']


class ClubDetailTeamSerializer(TeamSerializer):
    court = CourtSerializer()

    class Meta:
        model = Team
        fields = ['id', 'name', 'logo', 'court']


class ClubDetailSerializer(serializers.ModelSerializer):
    teams = ClubDetailTeamSerializer(source='filtered_teams', many=True)

    class Meta:
        model = Club
        fields = ['teams']


class LeagueTeamSerializer(TeamSerializer):
    season_id = serializers.IntegerField(source='league.season.id', read_only=True)

    class Meta:
        model = Team
        fields = ['id', 'name', 'logo', 'league', 'season_id']


class TeamDetailMatchSerializer(serializers.ModelSerializer):
    league = serializers.SlugRelatedField(slug_field='name', read_only=True)
    home_team = TeamSerializer()
    away_team = TeamSerializer()
    score = serializers.ListField(read_only=True, child=serializers.IntegerField(), min_length=2, max_length=2)
    court = CourtSerializer()

    class Meta:
        model = Match
        fields = '__all__'


class GamesStatsSerializer(serializers.Serializer):
    played_games = serializers.IntegerField()
    won_games = serializers.IntegerField()
    lost_games = serializers.IntegerField()


class PlayerGamesStatsSerializer(serializers.Serializer):
    singles = GamesStatsSerializer()
    doubles = GamesStatsSerializer()
    triples = GamesStatsSerializer()


class PlayerWithAllMatchesStatsSerializer(PersonSerializer):
    stats = serializers.SerializerMethodField()

    def get_stats(self, player: Player) -> PlayerGamesStatsSerializer:
        # root serializer must be for team
        team = self.root.instance
        matches_ids: List[int] = Match.objects \
            .filter(state=Match.MatchState.FINISHED) \
            .filter(Q(home_team=team) | Q(away_team=team)) \
            .filter(
                Q(games__away_players=player) |
                Q(games__home_players=player) |
                Q(games__sets__home_substitutions__incoming=player) |
                Q(games__sets__away_substitutions__incoming=player)) \
            .distinct() \
            .values_list('id', flat=True)

        return PlayerGamesStatsSerializer(
            player.get_games_stats(matches_ids)
        ).data

    class Meta:
        model = Player
        fields = ['id', 'name', 'stats']


class TeamDetailSerializer(TeamSerializer):
    current_members = PlayerWithAllMatchesStatsSerializer(many=True)
    home_matches = TeamDetailMatchSerializer(many=True)
    away_matches = TeamDetailMatchSerializer(many=True)

    class Meta:
        model = Team
        fields = ['id', 'name', 'logo', 'leader', 'current_members', 'home_matches', 'away_matches']


class StatsSerializer(serializers.Serializer):
    won_matches = serializers.IntegerField()
    draw_matches = serializers.IntegerField()
    lost_matches = serializers.IntegerField()


class TeamStatisticsSerializer(serializers.ModelSerializer):
    rounds_stats = StatsSerializer(many=True)
    last_five_matches = StatsSerializer()

    class Meta:
        model = Team
        fields = ['rounds_stats', 'last_five_matches']
