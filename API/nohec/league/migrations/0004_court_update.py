# Generated by Django 3.1.7 on 2021-05-18 08:42

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('league', '0003_update_team_membership'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='club',
            name='court',
        ),
        migrations.AddField(
            model_name='court',
            name='traveling_instructions',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AddField(
            model_name='team',
            name='court',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='teams', to='league.court'),
        ),
    ]
