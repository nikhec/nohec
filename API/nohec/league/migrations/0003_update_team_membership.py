# Generated by Django 3.1.7 on 2021-05-08 15:48

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_delete_session'),
        ('league', '0002_add_court'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='players',
            field=models.ManyToManyField(related_name='teams', through='league.TeamMembership', to='user.Player'),
        ),
        migrations.AddField(
            model_name='teammembership',
            name='type',
            field=models.SmallIntegerField(choices=[(1, 'Regular'), (2, 'Junior'), (3, 'Shared')], default=1),
        ),
        migrations.AlterField(
            model_name='teammembership',
            name='player',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user.player'),
        ),
        migrations.AlterField(
            model_name='teammembership',
            name='since',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='teammembership',
            name='team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='league.team'),
        ),
        migrations.AlterField(
            model_name='teammembership',
            name='until',
            field=models.DateTimeField(null=True),
        ),
    ]
