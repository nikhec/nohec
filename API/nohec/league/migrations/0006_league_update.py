# Generated by Django 3.1.7 on 2021-06-29 10:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('league', '0005_club_logo_img'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='season',
            options={'get_latest_by': 'year'},
        ),
        migrations.AddField(
            model_name='league',
            name='has_referee',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='league',
            name='has_single',
            field=models.BooleanField(default=False),
        ),
    ]
