"""league URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
"""

from django.urls import path, include
from django.views.decorators.cache import cache_page

from .views import SeasonAPI, LeagueAPI, StandingsAPI, ClubAPI, ClubDetailAPI, TeamPlayersAPI, TeamAPI, TeamDetailAPI, \
    TeamStatisticsAPI

urlpatterns = [
    path('seasons/', SeasonAPI.as_view()),

    path('leagues/', include([
        path('', LeagueAPI.as_view()),
        path('<int:pk>/standings/', cache_page(60 * 30)(StandingsAPI.as_view())),
    ])),

    path('clubs/', include([
        path('', ClubAPI.as_view()),
        path('<int:pk>/detail/', ClubDetailAPI.as_view()),
    ])),

    path('teams/', include([
        path('', TeamAPI.as_view()),
        path('<int:pk>/players/', TeamPlayersAPI.as_view()),
        path('<int:pk>/detail/', TeamDetailAPI.as_view()),
        path('<int:pk>/statistics/', TeamStatisticsAPI.as_view()),
    ])),
]
