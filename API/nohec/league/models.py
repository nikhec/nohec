from datetime import datetime
from typing import List, Dict

from django.db import models
from django.db.models import Q, QuerySet, Max, Min
from django.utils.timezone import now

from nohec.user.models import Player


class Season(models.Model):
    year = models.PositiveSmallIntegerField(unique=True)

    class Meta:
        get_latest_by = 'year'

    def __str__(self):
        return str(self.year)


class League(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=3)
    season = models.ForeignKey(Season, on_delete=models.CASCADE, related_name='leagues')
    has_referee = models.BooleanField(default=False)
    has_single = models.BooleanField(default=False)

    def get_standings(self) -> List['StandingRow']:
        """
        Returns an ordered list of `StandingRow`s for each team in the league.
        """
        from nohec.league.dataclasses import StandingRow
        from nohec.match.models import Match
        # get all needed data from db in just a few queries
        matches: List[Match] = Match.objects.filter(league=self) \
            .filter(state=Match.MatchState.FINISHED) \
            .select_related('home_team', 'away_team') \
            .prefetch_related('games', 'games__sets')

        # group matches by team
        team_matches: Dict[Team, List[Match]] = {
            t: [] for t in self.teams.all()
        }
        for m in matches:
            team_matches[m.home_team].append(m)
            team_matches[m.away_team].append(m)

        # count total wins, losses etc.
        rows = []
        for team, matches in team_matches.items():
            wins, draws, losses, sets_won, sets_lost, points = [0] * 6
            for m in matches:
                is_home_team = team == m.home_team
                if is_home_team:
                    this_team_points, other_team_points = m.score
                else:
                    other_team_points, this_team_points = m.score

                for g in m.games.all():
                    if is_home_team:
                        this_team_sets, other_team_sets = g.score
                    else:
                        other_team_sets, this_team_sets = g.score
                    sets_won += this_team_sets
                    sets_lost += other_team_sets

                if this_team_points > other_team_points:
                    wins += 1
                    points += 2
                elif this_team_points < other_team_points:
                    losses += 1
                else:
                    draws += 1
                    points += 1

            rows.append(
                StandingRow(
                    position=0,
                    team=team,
                    matches=len(matches),
                    wins=wins,
                    draws=draws,
                    losses=losses,
                    sets_won=sets_won,
                    sets_lost=sets_lost,
                    points=points
                ))

        # sort by points and set position
        rows.sort(key=lambda x: x.points, reverse=True)
        for i, r in enumerate(rows, 1):
            r.position = i

        return rows

    def get_game_types(self) -> List['Game.GameType']:
        """
        Returns the ordered types of games that are played in matches of this league.
        """
        from nohec.match.models import Game
        S, D, T = Game.GameType.SINGLE, Game.GameType.DOUBLE, Game.GameType.TRIPLE

        if self.has_single:
            return [D, D, T, T, D, S, T, T, D, D]
        else:
            return [D, D, T, T, D, T, T, D, D]

    def __str__(self):
        return self.name


class Court(models.Model):
    address = models.CharField(max_length=1000, null=True, blank=False)
    longitude = models.FloatField()
    latitude = models.FloatField()
    traveling_instructions = models.TextField(blank=True, default='')


class Club(models.Model):
    name = models.CharField(max_length=100, unique=True)
    logo_img = models.ImageField(upload_to='logos', null=True)

    @property
    def logo(self) -> str:
        return self.logo_img.url if self.logo_img else '/static/img/logo-placeholder.png'

    def __str__(self):
        return self.name


class Team(models.Model):
    label = models.CharField(max_length=10, null=True, blank=False)
    club = models.ForeignKey(Club, on_delete=models.CASCADE, related_name='teams')
    leader = models.ForeignKey(Player, on_delete=models.DO_NOTHING, related_name='+')
    league = models.ForeignKey(League, on_delete=models.CASCADE, related_name='teams')
    players = models.ManyToManyField(Player, through='TeamMembership', related_name='teams')
    court = models.ForeignKey(Court, on_delete=models.SET_NULL, related_name='teams', null=True)

    @property
    def name(self) -> str:
        """
        Returns the team's name as a concatenation of the club name and the team's label.
        If a club has only a single team then that team has no label and it's name is just the club's name.
        """
        return self.club.name + (' ' + self.label if self.label else '')

    def current_members(self, when: datetime = now()) -> QuerySet[Player]:
        """
        Returns the players that are members of the team at the given time.
        By default the current date and time is used.
        """
        return self.players.filter(Q(teammembership__until__gt=when) | Q(teammembership__until__isnull=True),
                                   teammembership__since__lte=when)

    @property
    def rounds_stats(self) -> List['StatsRow']:
        """
        Returns a list of `StatsRow`s for each round so far.
        """
        from nohec.match.models import Match
        from nohec.league.dataclasses import StatsRow

        matches: QuerySet[Match] = Match.objects \
            .filter(state=Match.MatchState.FINISHED) \
            .filter(Q(home_team=self) | Q(away_team=self)) \
            .select_related('home_team', 'away_team') \
            .prefetch_related('games', 'games__sets')

        max_round = matches.aggregate(Max('round'))['round__max']
        min_round = matches.aggregate(Min('round'))['round__min']

        matches_by_rounds = {
            i: [] for i in range(min_round, max_round + 1)
        }

        for m in matches:
            matches_by_rounds[m.round].append(m)

        return [
            StatsRow(
                won := len([x for x in matches if x.winner == self]),
                draws := len([x for x in matches if x.winner is None]),
                lost := len(matches) - won - draws
            )
            for _, matches in matches_by_rounds.items()
        ]

    @property
    def last_five_matches(self) -> 'StatsRow':
        """
        Returns one `StatsRow` for the last up to five matches.
        """
        from nohec.match.models import Match
        from nohec.league.dataclasses import StatsRow

        matches: QuerySet[Match] = Match.objects \
            .filter(state=Match.MatchState.FINISHED) \
            .filter(Q(home_team=self) | Q(away_team=self)) \
            .select_related('home_team', 'away_team') \
            .prefetch_related('games', 'games__sets') \
            .order_by('-date')[:5]

        return StatsRow(
            won := len([x for x in matches if x.winner == self]),
            draws := len([x for x in matches if x.winner is None]),
            lost := len(matches) - won - draws
        )

    def __str__(self):
        return self.name


class TeamMembership(models.Model):
    class MembershipType(models.IntegerChoices):
        REGULAR = 1
        JUNIOR = 2
        SHARED = 3

    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    type = models.SmallIntegerField(choices=MembershipType.choices, default=MembershipType.REGULAR)
    since = models.DateTimeField()
    until = models.DateTimeField(null=True)
