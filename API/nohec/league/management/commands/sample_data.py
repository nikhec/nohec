import os
import random
import re
from datetime import datetime, timezone, timedelta
from pathlib import Path
from typing import Tuple, List

import openpyxl as xl
from django.contrib.auth.hashers import make_password
from django.core.files import File
from django.core.management import BaseCommand, call_command
from django.db import connection
from django.utils.timezone import now

from nohec.league.models import Season, League, Club, Court, Team, TeamMembership
from nohec.match.models import Match, Game, Set, Substitution
from nohec.user.models import Individual, Player, Referee, User

TEAM_NAME_REGEX = r"^(?P<club>[^\"]*)( \"(?P<label>[A-Z])\")?$"
COORDINATES_REGEX = r"^(?P<lat>[\d.]*)N, (?P<lon>[\d.]*)E$"
COMPLEX_COORDINATES_REGEX = r"^(?P<lat_deg>\d+)°(?P<lat_min>\d+)'(?P<lat_sec>[\d.]+)\"N, (?P<lon_deg>\d+)°(?P<lon_min>\d+)'(?P<lon_sec>[\d.]+)\"E$"

CAPITALIZED_TEAM_WORDS = ['TJ', 'DTJ', 'SK', 'NK', 'CHK', 'AVIA', 'WATTO', 'AB', 'ETC', 'VSK', 'MFF', 'UK']

FIRST_NAMES = ["Jiří", "Jan", "Petr", "Josef", "Pavel", "Martin", "Jaroslav", "Tomáš", "Miroslav", "Zdeněk",
               "František", "Václav", "Michal", "Milan", "Karel", "Jakub", "Lukáš", "David", "Vladimír", "Ladislav",
               "Ondřej", "Roman", "Stanislav", "Marek", "Radek", "Daniel", "Antonín", "Vojtěch", "Filip", "Adam",
               "Miloslav", "Matěj", "Aleš", "Jaromír", "Libor", "Dominik", "Patrik", "Vlastimil", "Jindřich", "Miloš",
               "Oldřich", "Lubomír", "Rudolf", "Ivan", "Luboš", "Robert", "Štěpán", "Radim", "Richard", "Bohumil",
               "Matyáš", "Vít", "Ivo", "Rostislav", "Luděk", "Dušan", "Kamil", "Šimon", "Vladislav", "Zbyněk",
               "Bohuslav", "Michael", "Alois", "Viktor", "Štefan", "Vítězslav", "René", "Jozef", "Ján", "Kryštof",
               "Eduard", "Marcel", "Emil", "Dalibor", "Ludvík", "Radomír", "Tadeáš", "Otakar", "Vilém", "Samuel",
               "Bedřich", "Alexandr", "Denis", "Vratislav", "Leoš", "Radovan", "Břetislav", "Marian", "Přemysl",
               "Robin", "Erik"]

LAST_NAMES = ["Novák", "Svoboda", "Novotný", "Dvořák", "Černý", "Procházka", "Kučera", "Veselý", "Horák", "Němec",
              "Pokorný", "Marek", "Pospíšil", "Hájek", "Jelínek", "Král", "Růžička", "Beneš", "Fiala", "Sedláček",
              "Doležal", "Zeman", "Nguyen", "Kolář", "Krejčí", "Navrátil", "Čermák", "Urban", "Vaněk", "Blažek", "Kříž",
              "Kratochvíl", "Kovář", "Kopecký", "Bartoš", "Vlček", "Musil", "Šimek", "Polák", "Konečný", "Malý", "Čech",
              "Kadlec", "Štěpánek", "Staněk", "Holub", "Dostál", "Soukup", "Šťastný", "Mareš", "Sýkora", "Moravec",
              "Tichý", "Valenta", "Vávra", "Matoušek", "Bláha", "Říha", "Ševčík", "Bureš", "Hruška", "Mašek", "Pavlík",
              "Dušek", "Hrubý", "Havlíček", "Janda", "Mach", "Müller", "Liška"]

HASHED_PASSWORD = make_password('pass')

MATCH_PAIRS = [
    [(0, 1), (2, 3), (4, 5), (6, 7)],
    [(2, 0), (1, 3), (6, 4), (5, 7)],
    [(0, 3), (2, 1), (4, 7), (5, 6)],
    [(4, 0), (1, 5), (2, 6), (3, 7)],
    [(0, 5), (4, 1), (7, 2), (6, 3)],
    [(6, 0), (1, 7), (2, 4), (3, 5)],
    [(0, 7), (6, 1), (5, 2), (4, 3)]
]


class Command(BaseCommand):
    help = 'Fill database with sample data'

    # In SQLite, the ids have to be manually set for bulk_create
    counters = {key: 1 for key in ['individual', 'player', 'user', 'team', 'match', 'game', 'set', 'substitution']}
    counters['user'] += 1  # adjust for admin user

    def add_arguments(self, parser):
        parser.add_argument('--delete', action='store_true', help='First delete previous data')

    def handle(self, *args, **options):
        if options['delete']:
            self._delete_data()
            self.stdout.write(self.style.SUCCESS('Previous data deleted successfully'))

        # Create referees
        individuals = Individual.objects.bulk_create([self.create_individual() for _ in range(20)])
        Referee.objects.bulk_create([Referee(registration_number=10 * i + 1, individual=individuals[i]) for i in range(20)])
        User.objects.bulk_create([self.create_user(individual) for individual in individuals])

        # Setup logos
        logos_dir = Path(__file__).parent / "logos"
        club_logos = {
            img.stem: img
            for img in logos_dir.glob("*.png")
        }

        # Create seasons and leagues
        seasons_dir = Path(__file__).parent / "seasons"
        for season in sorted(seasons_dir.glob("*.xlsx")):
            year = int(season.name[:4])
            workbook = xl.load_workbook(season)

            self._handle_season(year, workbook, club_logos)

        # Update match for testing
        match = Match.objects.filter(date__year=self.this_year(), date__week=self.current_week(), league__code='KP').first()
        match.date = now()
        match.save()

        # since we were using Someting.objects.create(id=<some number>)
        # we need to reset id sequences in some database engines
        reset_sequences_query = call_command(
            'sqlsequencereset', 'league', 'match', 'user', no_color=True, stdout=open(os.devnull, 'w'), verbosity=0
        )
        if reset_sequences_query:
            self.stdout.write('Recomputing sequences')
            self.stdout.write(f'Query: \n{reset_sequences_query}\n\n')
            with connection.cursor() as cursor:
                cursor.execute(reset_sequences_query)
            self.stdout.write('Sequences recomputed.')

        self.stdout.write(self.style.SUCCESS('Sample data loaded successfully'))

    def _delete_data(self):
        for model in [TeamMembership, Set, Game, Match, Referee, Team, Club, Player, Individual, League, Season, Court]:
            model.objects.all().delete()
        User.objects.exclude(email='admin@admin.cz').delete()

    def _handle_season(self, year, workbook, logos):
        self.stdout.write('Season ' + str(year))
        season = Season.objects.create(year=year)

        for sheet in workbook:
            if sheet.sheet_state != sheet.SHEETSTATE_VISIBLE:
                continue  # Skip hidden sheets with irrelevant information

            code = sheet.title[:-3]
            self._handle_league(code, sheet, season, logos)

    def _handle_league(self, code, sheet, season, logos):
        self.stdout.write('League ' + code)
        league_name = 'Krajský přebor' if code == 'KP' else f'{code[2]}. třída'
        league = League.objects.create(name=league_name, code=code, season=season,
                                       has_referee=code in ['KP', 'KS1', 'KS2'], has_single=code == 'KP')

        # Create teams
        teams, team_memberships = [], []

        for i in range(8):
            top = 4 + 5 * i

            if sheet[f'C{top}'].value is None:
                continue  # Skip empty team slots (for leagues with less than 8 teams)

            team, memberships = self._handle_team(sheet, top, league, logos)
            teams.append(team)
            team_memberships += memberships

        teams = Team.objects.bulk_create(teams)
        TeamMembership.objects.bulk_create(team_memberships)

        # Create matches
        matches, games, sets = [], [], []
        game_home_players, game_away_players = [], []
        substitutions, set_home_substitutions, set_away_substitutions = [], [], []

        for round in range(1, 15):
            for home_team_index, away_team_index in MATCH_PAIRS[(round - 1) % 7]:
                if len(teams) <= home_team_index or len(teams) <= away_team_index:
                    continue  # Skip match creation for leagues with less than 8 teams
                if round > 7:
                    home_team_index, away_team_index = away_team_index, home_team_index

                home_team, away_team = teams[home_team_index], teams[away_team_index]
                home_players = list(home_team.players.all())
                away_players = list(away_team.players.all())

                finished = season.year < self.this_year() or round < 8

                match = Match(id=self.get_counter('match'),
                              state=Match.MatchState.FINISHED if finished else Match.MatchState.UPCOMING,
                              round=round,
                              date=datetime.fromisocalendar(season.year, self.current_week() - 8 + round, (home_team.id % 5) + 1)
                                           .astimezone(timezone.utc) + timedelta(hours=15),
                              league=league,
                              home_team=home_team,
                              away_team=away_team,
                              court=home_team.court,
                              home_captain=random.choice(home_players) if finished else None,
                              away_captain=random.choice(away_players) if finished else None)
                matches.append(match)

                if not finished:
                    continue  # For upcoming matches

                # Games
                for i, type in enumerate(league.get_game_types()):
                    game = Game(id=self.get_counter('game'), _order=i, match=match, type=type)
                    games.append(game)

                    random_home_players = random.sample(home_players, type)
                    for player in random_home_players:
                        game_home_players.append(Game.home_players.through(game=game, player=player))
                    random_away_players = random.sample(away_players, type)
                    for player in random_away_players:
                        game_away_players.append(Game.away_players.through(game=game, player=player))

                    # Sets
                    sets += [self.create_set(game, i) for i in range(2)]

                    # Substitutions
                    if type > 1:
                        if random.random() > 0.75:
                            substitution = Substitution(id=self.get_counter('substitution'),
                                                        outgoing=random.choice(random_home_players),
                                                        incoming=random.choice([player for player in home_players if player not in random_home_players]))
                            substitutions.append(substitution)
                            set_home_substitutions.append(Set.home_substitutions.through(set=sets[-random.randint(1, 2)], substitution=substitution))
                        if random.random() > 0.75:
                            substitution = Substitution(id=self.get_counter('substitution'),
                                                        outgoing=random.choice(random_away_players),
                                                        incoming=random.choice([player for player in away_players if player not in random_away_players]))
                            substitutions.append(substitution)
                            set_away_substitutions.append(Set.away_substitutions.through(set=sets[-random.randint(1, 2)], substitution=substitution))

        # Bulk create all the objects
        matches = Match.objects.bulk_create(matches)
        if league.has_referee:
            referees = list(Referee.objects.all())
            Match.referees.through.objects.bulk_create(
                [Match.referees.through(match=match, referee=random.choice(referees)) for match in matches])

        Game.objects.bulk_create(games)
        Game.home_players.through.objects.bulk_create(game_home_players)
        Game.away_players.through.objects.bulk_create(game_away_players)

        Set.objects.bulk_create(sets)
        Substitution.objects.bulk_create(substitutions)
        Set.home_substitutions.through.objects.bulk_create(set_home_substitutions)
        Set.away_substitutions.through.objects.bulk_create(set_away_substitutions)

    def _handle_team(self, sheet, top, league, logos) -> Tuple[Team, List[TeamMembership]]:
        club_name, label = self.parse_team_name(sheet[f'C{top}'].value, sheet[f'C{top + 2}'].value)
        club, created = Club.objects.get_or_create(name=club_name)
        if created:
            # Club logo
            if club_name in logos:
                img = logos[club_name]
                club.logo_img.save(img.name, File(open(img, "rb")))

        # Try to get the teams players from the previous year and create new ones if there aren't any
        try:
            previous_team = club.teams.all() \
                .filter(league__season__year=league.season.year - 1) \
                .filter(label=label) \
                .get()
            players = list(previous_team.players.all())

        except Team.DoesNotExist:
            individuals = Individual.objects.bulk_create([self.create_individual() for _ in range(random.randint(9, 12))])
            players = Player.objects.bulk_create([self.create_player(individual) for individual in individuals])
            User.objects.bulk_create([self.create_user(individual) for individual in individuals])

        # Get or create court
        court_id = int(sheet[f'F{top + 4}'].value)
        court_address = ', '.join([sheet[f'F{top + i}'].value for i in range(3) if sheet[f'F{top + i}'].value is not None])
        latitude, longitude = self.parse_coordinates(sheet[f'G{top + 4}'].value)
        traveling_instructions = '\n'.join([sheet[f'G{top + i}'].value for i in range(4) if sheet[f'G{top + i}'].value is not None])
        court, _ = Court.objects.get_or_create(id=court_id, defaults={
            'address': court_address,
            'latitude': latitude, 'longitude': longitude,
            'traveling_instructions': traveling_instructions
        })

        team = Team(id=self.get_counter('team'), label=label, club=club, leader=players[0], league=league, court=court)
        team_memberships = [self.create_team_membership(team, player, league.season.year) for player in players]

        return team, team_memberships

    @staticmethod
    def parse_team_name(name, suffix) -> Tuple[str, str]:
        name = name.rstrip()

        if not name.endswith('-') and suffix is not None:
            name += ' ' + suffix.strip()

        name = name.removesuffix(' -')

        match = re.match(TEAM_NAME_REGEX, name)

        club_name = match['club']
        label = match['label']

        club_name = ' '.join(word if word in CAPITALIZED_TEAM_WORDS else word.capitalize()
                             for word in club_name.split())

        return club_name, label

    @staticmethod
    def parse_coordinates(value) -> Tuple[float, float]:
        match = re.match(COORDINATES_REGEX, value)
        if match is not None:
            latitude = float(match['lat'])
            longitude = float(match['lon'])
        else:
            match = re.match(COMPLEX_COORDINATES_REGEX, value.strip())
            latitude = int(match['lat_deg']) + int(match['lat_min']) / 60.0 + float(match['lat_sec']) / 3600.0
            longitude = int(match['lon_deg']) + int(match['lon_min']) / 60.0 + float(match['lon_sec']) / 3600.0

        return latitude, longitude

    def create_individual(self) -> Individual:
        return Individual(id=self.get_counter('individual'),
                          first_name=random.choice(FIRST_NAMES),
                          last_name=random.choice(LAST_NAMES),
                          birth_date=datetime(random.randint(1965, 2000),
                                              random.randint(1, 12),
                                              random.randint(1, 28),
                                              tzinfo=timezone.utc))

    def create_player(self, individual: Individual) -> Player:
        id = self.get_counter('player')
        return Player(id=id, registration_number=id * 10, individual=individual)

    def create_user(self, individual: Individual) -> User:
        id = self.get_counter('user')
        return User(id=id, password=HASHED_PASSWORD, individual=individual, email=f'{id}@nohec.cz')

    def create_team_membership(self, team, player, year) -> TeamMembership:
        return TeamMembership(team=team, player=player,
                              since=datetime(year, 1, 1, tzinfo=timezone.utc),
                              until=datetime(year, 12, 31, tzinfo=timezone.utc))

    def create_set(self, game, order) -> Set:
        home_win = random.random() > 0.5
        return Set(id=self.get_counter('set'), game=game, _order=order,
                   home_points=10 if home_win else random.randint(0, 9),
                   away_points=random.randint(0, 9) if home_win else 10,
                   home_timeout=random.randint(0, 1),
                   away_timeout=random.randint(0, 1))

    @staticmethod
    def current_week():
        return datetime.today().isocalendar()[1]

    @staticmethod
    def this_year():
        return datetime.today().year

    def get_counter(self, name):
        value = self.counters[name]
        self.counters[name] += 1
        return value
