# Create your views here.
from django.db.models import Prefetch
from django.shortcuts import get_object_or_404
from django_filters import rest_framework as filters
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiParameter, extend_schema_view
from rest_framework.generics import RetrieveAPIView, ListAPIView

from nohec.league.models import Team, Season, League, Club
from nohec.league.serializers import SeasonSerializer, LeagueSerializer, LeagueTeamSerializer, StandingRowSerializer, \
    ClubSerializer, ClubDetailSerializer, TeamDetailSerializer, TeamStatisticsSerializer
from nohec.user.models import Player
from nohec.user.serializers import PlayerSerializer


class SeasonAPI(ListAPIView):
    """
    Returns a list of all Seasons.
    """
    serializer_class = SeasonSerializer
    queryset = Season.objects


@extend_schema_view(get=extend_schema(
    parameters=[OpenApiParameter('latest_season', OpenApiTypes.BOOL, description='Filter by latest season.')]
))
class LeagueAPI(ListAPIView):
    """
    Returns a list of all Leagues.
    """
    serializer_class = LeagueSerializer

    def get_queryset(self):
        q = League.objects
        if 'latest_season' in self.request.GET and self.request.GET['latest_season'] == 'true':
            q = q.filter(season=Season.objects.latest())
        return q


@extend_schema_view(get=extend_schema(
    parameters=[OpenApiParameter('latest_season', OpenApiTypes.BOOL, description='Filter by latest season.')]
))
class TeamAPI(ListAPIView):
    """
    Returns a list of all Teams.
    """
    serializer_class = LeagueTeamSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('league__season__id',)

    def get_queryset(self):
        q = Team.objects
        GET = self.request.GET
        if (
                'latest_season' in GET
                and GET['latest_season'] == 'true'
                and 'league__season__id' not in GET
        ):
            q = q.filter(league__season=Season.objects.latest())
        return q


class StandingsAPI(ListAPIView):
    """
    Returns a list of scores indicating the relative positions of teams in a league.
    """
    serializer_class = StandingRowSerializer
    queryset = League.objects.none()

    def get_queryset(self):
        league = get_object_or_404(League, id=self.kwargs['pk'])
        return league.get_standings()


class ClubAPI(ListAPIView):
    """
    Returns a list of all Clubs.
    """
    serializer_class = ClubSerializer
    queryset = Club.objects


@extend_schema_view(get=extend_schema(
    parameters=[OpenApiParameter('latest_season', OpenApiTypes.BOOL, description='Filter by latest season.')]
))
class ClubDetailAPI(RetrieveAPIView):
    """
    Returns a detail of a Club.
    """
    serializer_class = ClubDetailSerializer

    def get_queryset(self):
        teams = Team.objects.all()
        if 'latest_season' in self.request.GET and self.request.GET['latest_season'] == 'true':
            teams = teams.filter(league__season=Season.objects.latest())

        return Club.objects.prefetch_related(Prefetch('teams', teams, 'filtered_teams'))


class TeamDetailAPI(RetrieveAPIView):
    """
    Returns a detail of a Team.
    """
    serializer_class = TeamDetailSerializer
    queryset = Team.objects


class TeamStatisticsAPI(RetrieveAPIView):
    serializer_class = TeamStatisticsSerializer
    queryset = Team.objects


class TeamPlayersAPI(ListAPIView):
    """
    Returns a list of current members of a Team.
    The members are determined based on the time and date of the call.
    """
    serializer_class = PlayerSerializer
    queryset = Player.objects.none()

    def get_queryset(self):
        team = get_object_or_404(Team, pk=self.kwargs['pk'])
        return team.current_members()
