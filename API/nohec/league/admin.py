# Register your models here.

from django.contrib import admin

from nohec.league.models import Season, Team, League, Club, TeamMembership, Court


@admin.register(Season)
class SeasonAdmin(admin.ModelAdmin):
    model = Season
    list_display = ['year']


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    model = Team
    list_display = ['name', 'league', 'season']

    def season(self, obj):
        return obj.league.season


@admin.register(League)
class LeagueAdmin(admin.ModelAdmin):
    model = League
    list_display = ['name', 'code', 'season']


@admin.register(Club)
class ClubAdmin(admin.ModelAdmin):
    model = Club
    list_display = ['name']


@admin.register(TeamMembership)
class TeamMembershipAdmin(admin.ModelAdmin):
    model = TeamMembership
    list_display = ['team', 'player', 'since', 'until']


@admin.register(Court)
class CourtAdmin(admin.ModelAdmin):
    model = Court
    list_display = ['address', 'longitude', 'latitude']
