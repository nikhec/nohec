from django.db.models import QuerySet

from nohec.league.dataclasses import GamesStats
from nohec.league.models import Team
from nohec.match.models import Game


def count_stats_for_games(games: QuerySet[Game], team: Team) -> GamesStats:
    ps = GamesStats(games.count())
    for g in games:
        home_game_points, away_game_points = g.score
        is_home = g.match.home_team == team
        if home_game_points > away_game_points:
            if is_home:
                ps.won_games += 1
            else:
                ps.lost_games += 1
        elif away_game_points > home_game_points:
            if is_home:
                ps.lost_games += 1
            else:
                ps.won_games += 1
    return ps
