from datetime import date, timedelta, datetime, timezone

from django.test import TestCase
from django.utils.timezone import now

from nohec.league.dataclasses import StandingRow, StatsRow, PlayerGamesStats, GamesStats
from nohec.league.models import Club, Team, TeamMembership, Season, League
from nohec.match.models import Match, Set, Game
from nohec.user.models import Individual, Player


class TeamMembershipTests(TestCase):
    def setUp(self) -> None:
        individual = Individual.objects.create(first_name='John', last_name='Doe', birth_date=date(1990, 1, 1))
        self.player = Player.objects.create(registration_number='1234567890', individual=individual)

        season = Season.objects.create(year=2021)
        league = League.objects.create(name='Bar', code='XY', season=season)

        club = Club.objects.create(name='Foo')
        self.team = Team.objects.create(label='A', club=club, leader=self.player, league=league)

        TeamMembership.objects.create(since=now() - timedelta(days=100), until=now() - timedelta(days=10),
                                      team=self.team, player=self.player)
        TeamMembership.objects.create(since=now() - timedelta(days=10), until=now() + timedelta(days=10),
                                      team=self.team, player=self.player)
        TeamMembership.objects.create(since=now() + timedelta(days=10), until=None, team=self.team, player=self.player)

    def test_current_members(self):
        current_members = self.team.current_members()

        self.assertEqual(len(current_members), 1)
        self.assertEqual(current_members.get(), self.player)

    def test_current_teams(self):
        current_teams = self.player.current_teams()

        self.assertEqual(len(current_teams), 1)
        self.assertEqual(current_teams.get(), self.team)


class StatsTests(TestCase):
    def setUp(self) -> None:
        season = Season.objects.create(year=2021)
        self.league = League.objects.create(name='1st league', code='FL', season=season)

        home_club = Club.objects.create(name='Foo')
        home_leader_individual = Individual.objects.create(first_name='John', last_name='Doe', birth_date=date(1980, 4, 2))
        self.home_leader = Player.objects.create(registration_number=42, individual=home_leader_individual)
        self.home_team = Team.objects.create(club=home_club, leader=self.home_leader, league=self.league)

        away_club = Club.objects.create(name='Bar')
        away_leader_individual = Individual.objects.create(first_name='Jane', last_name='Doe', birth_date=date(1975, 6, 9))
        away_leader = Player.objects.create(registration_number=123, individual=away_leader_individual)
        self.away_team = Team.objects.create(club=away_club, leader=away_leader, league=self.league)

        match = Match.objects.create(id=1, round=1, date=datetime(2021, 1, 1, 17, tzinfo=timezone.utc),
                                     league=self.league, home_team=self.home_team, away_team=self.away_team,
                                     state=Match.MatchState.FINISHED)

        for type, ((fsh, fsa), (ssh, ssa)) in zip(match.league.get_game_types(),
                                                         [((10, 8), (10, 6)),
                                                          ((7, 10), (10, 9)),
                                                          ((10, 5), (10, 7)),
                                                          ((8, 10), (9, 10)),
                                                          ((7, 10), (10, 4)),
                                                          ((7, 10), (10, 6)),
                                                          ((10, 3), (2, 10)),
                                                          ((9, 10), (8, 10)),
                                                          ((10, 6), (10, 4))]):
            game = Game.objects.create(match=match, type=type)
            game.home_players.set([self.home_leader])
            Set.objects.create(game=game, home_points=fsh, away_points=fsa, home_timeout=0, away_timeout=0)
            Set.objects.create(game=game, home_points=ssh, away_points=ssa, home_timeout=0, away_timeout=0)

        # no games -> draw
        Match.objects.create(id=2, round=2, date=datetime(2021, 1, 1, 17, tzinfo=timezone.utc), league=self.league,
                             home_team=self.home_team, away_team=self.away_team, state=Match.MatchState.FINISHED)

    def test_standings(self):
        st = self.league.get_standings()
        # cast to str to allow easy comparison
        self.assertEqual(str(st), str([
            StandingRow(position=1, team=self.home_team, matches=2, wins=1, draws=1, losses=0, sets_won=10, sets_lost=8, points=3),
            StandingRow(position=2, team=self.away_team, matches=2, wins=0, draws=1, losses=1, sets_won=8, sets_lost=10, points=1)
        ]))

    def test_rounds_stats(self):
        ws = self.home_team.rounds_stats
        self.assertTrue(len(ws) == 2)
        self.assertEqual(str(ws), str(
            [StatsRow(won_matches=1, draw_matches=0, lost_matches=0), StatsRow(won_matches=0, draw_matches=1, lost_matches=0)]
        ))

    def test_last_five_stats(self):
        ltm = self.home_team.last_five_matches
        self.assertEqual(str(ltm), str(StatsRow(won_matches=1, draw_matches=1, lost_matches=0)))

    def test_player_stats(self):
        games_stats_home_player = self.home_leader.get_games_stats([1])
        self.assertEqual(str(games_stats_home_player), str(
            PlayerGamesStats(
                singles=GamesStats(played_games=0, won_games=0, lost_games=0),
                doubles=GamesStats(played_games=5, won_games=2, lost_games=1),
                triples=GamesStats(played_games=4, won_games=1, lost_games=1))))
