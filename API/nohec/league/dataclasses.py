from dataclasses import dataclass


@dataclass
class StandingRow:
    position: int
    team: 'Team'
    matches: int
    wins: int
    draws: int
    losses: int
    sets_won: int
    sets_lost: int
    points: int


@dataclass
class StatsRow:
    won_matches: int
    draw_matches: int
    lost_matches: int


@dataclass
class GamesStats:
    played_games: int
    won_games: int = 0
    lost_games: int = 0


@dataclass
class PlayerGamesStats:
    singles: GamesStats
    doubles: GamesStats
    triples: GamesStats
