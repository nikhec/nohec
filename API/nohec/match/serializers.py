from typing import List, OrderedDict

from rest_framework import serializers

from nohec.league.serializers import TeamSerializer, CourtSerializer, PlayerGamesStatsSerializer
from nohec.match.models import Match, Game, Set, Substitution
from nohec.user.models import Player, User
from nohec.user.serializers import PlayerSerializer, RefereeSerializer, PersonSerializer


class ErrorSerializer(serializers.Serializer):
    """ A general user request error. """
    detail = serializers.CharField()


class SimpleMatchSerializer(serializers.ModelSerializer):
    league = serializers.SlugRelatedField(slug_field='name', read_only=True)
    home_team = TeamSerializer()
    away_team = TeamSerializer()
    score = serializers.ListField(read_only=True, child=serializers.IntegerField(), min_length=2, max_length=2)

    class Meta:
        model = Match
        fields = ['id', 'state', 'date', 'league', 'home_team', 'away_team', 'score']


class MatchSerializer(SimpleMatchSerializer):
    referees = RefereeSerializer(many=True)
    court = CourtSerializer()

    class Meta:
        model = Match
        fields = '__all__'


class SubstitutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Substitution
        exclude = ['id']


class SetSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    home_substitutions = SubstitutionSerializer(many=True)
    away_substitutions = SubstitutionSerializer(many=True)

    class Meta:
        model = Set
        fields = ('id', 'home_points', 'away_points', 'home_timeout', 'away_timeout', 'home_substitutions', 'away_substitutions')


class GameSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    sets = SetSerializer(many=True)

    class Meta:
        model = Game
        fields = ('id', 'type', 'home_players', 'away_players', 'sets')


class PlayerWithMatchStatsSerializer(PersonSerializer):
    stats = serializers.SerializerMethodField()

    def get_stats(self, player: Player) -> PlayerGamesStatsSerializer:
        # root serializer must be for match
        match = self.root.instance
        return PlayerGamesStatsSerializer(
            player.get_games_stats([match.id])
        ).data

    class Meta:
        model = Player
        fields = ['id', 'name', 'stats']


class MatchDetailSerializer(serializers.ModelSerializer):
    games = GameSerializer(many=True)
    home_team = TeamSerializer()
    away_team = TeamSerializer()
    referees = RefereeSerializer(many=True)
    home_players = PlayerWithMatchStatsSerializer(many=True)
    away_players = PlayerWithMatchStatsSerializer(many=True)
    score = serializers.ListField(read_only=True, child=serializers.IntegerField(), min_length=2, max_length=2)
    court = CourtSerializer()
    user_can_modify_match = serializers.SerializerMethodField(method_name='get_user_modify_permission')

    def get_user_modify_permission(self, match) -> bool:
        user = self.context['request'].user
        return isinstance(user, User) and user.can_modify_match(match)

    class Meta:
        model = Match
        fields = ('state', 'date', 'home_captain', 'away_captain', 'home_team', 'away_team', 'referees',
                  'games', 'home_players', 'away_players', 'score', 'court', 'user_can_modify_match')


class EnterMatchSerializer(serializers.ModelSerializer):
    home_team = TeamSerializer()
    away_team = TeamSerializer()
    referees = RefereeSerializer(many=True)
    available_home_players = PlayerSerializer(many=True)
    available_away_players = PlayerSerializer(many=True)
    games = GameSerializer(many=True)
    court = CourtSerializer()

    class Meta:
        model = Match
        fields = ('state', 'home_team', 'away_team', 'home_captain', 'away_captain', 'referees',
                  'available_home_players', 'available_away_players', 'games', 'note', 'court')


class NoteSerializer(serializers.Serializer):
    note = serializers.CharField(allow_blank=True)

    def update(self, instance: Match, validated_data):
        instance.note = validated_data['note']
        instance.save()
        return instance


class CaptainsSerializer(serializers.Serializer):
    home = serializers.PrimaryKeyRelatedField(queryset=Player.objects.all())
    away = serializers.PrimaryKeyRelatedField(queryset=Player.objects.all())

    def validate_home(self, player: Player):
        if player not in self.instance.available_home_players:
            raise serializers.ValidationError('Home captain cannot play for the home team')
        return player

    def validate_away(self, player: Player):
        if player not in self.instance.available_away_players:
            raise serializers.ValidationError('Away captain cannot play for away team')
        return player

    def update(self, instance: Match, validated_data):
        instance.home_captain = validated_data['home']
        instance.away_captain = validated_data['away']
        instance.state = Match.MatchState.IN_PROGRESS
        instance.save()
        return instance


class SaveGameSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    home_players = serializers.PrimaryKeyRelatedField(queryset=Player.objects.all(), many=True)
    away_players = serializers.PrimaryKeyRelatedField(queryset=Player.objects.all(), many=True)

    def validate_home_players(self, players: List[Player]):
        # TODO: validate more player constraints (a player can only play in one game of each type)
        available_players = self.context['match'].available_home_players
        if any([player not in available_players for player in players]):
            raise serializers.ValidationError('Not all home players can play for the home team')
        if len(players) != self.instance.type:
            raise serializers.ValidationError(f'There must be exactly {self.instance.type} home players')
        return players

    def validate_away_players(self, players: List[Player]):
        # TODO: validate more player constraints (a player can only play in one game of each type)
        available_players = self.context['match'].available_away_players
        if any([player not in available_players for player in players]):
            raise serializers.ValidationError('Not all away players can play for the away team')
        if len(players) != self.instance.type:
            raise serializers.ValidationError(f'There must be exactly {self.instance.type} away players')
        return players

    def validate(self, data):
        if len(self.instance.home_players.all()) > 0 and len(self.instance.away_players.all()) > 0:
            raise serializers.ValidationError('This game already has players filled-out')
        return data

    def update(self, instance: Game, validated_data):
        instance.home_players.set(validated_data['home_players'])
        instance.away_players.set(validated_data['away_players'])
        instance.save()
        return instance


class SaveSetSerializer(serializers.ModelSerializer):
    game = serializers.PrimaryKeyRelatedField(queryset=Game.objects.all())
    home_points = serializers.IntegerField(min_value=0, max_value=10)
    away_points = serializers.IntegerField(min_value=0, max_value=10)
    home_timeout = serializers.IntegerField(min_value=0, max_value=1, required=False)
    away_timeout = serializers.IntegerField(min_value=0, max_value=1, required=False)
    home_substitutions = SubstitutionSerializer(many=True, required=False, default=[])
    away_substitutions = SubstitutionSerializer(many=True, required=False, default=[])

    def validate_game(self, game: Game):
        if game.match != self.context['match']:
            raise serializers.ValidationError('This game belongs to a different match')
        if game.sets.count() > 1:
            raise serializers.ValidationError('This game already has enough sets')
        if len(game.home_players.all()) == 0 or len(game.away_players.all()) == 0:
            raise serializers.ValidationError('This game does not have players filled-out')
        return game

    def validate_home_substitutions(self, substitutions: List[OrderedDict]):
        # TODO: validate more player constraints (a player can only play in one game of each type)
        available_players = self.context['match'].available_home_players
        if any([sub['incoming'] not in available_players for sub in substitutions]):
            raise serializers.ValidationError('Not all home players can play for the home team')
        return substitutions

    def validate_away_substitutions(self, substitutions: List[OrderedDict]):
        # TODO: validate more player constraints (a player can only play in one game of each type)
        available_players = self.context['match'].available_away_players
        if any([sub['incoming'] not in available_players for sub in substitutions]):
            raise serializers.ValidationError('Not all away players can play for the away team')
        return substitutions

    def validate(self, data):
        # Only players on the field can be outgoing and players not on the field can be incoming

        current_home_players = set(data['game'].home_players.all())
        current_away_players = set(data['game'].away_players.all())

        for previous_set in data['game'].sets.all():
            for substitution in previous_set.home_substitutions.all():
                current_home_players.remove(substitution.outgoing)
                current_home_players.add(substitution.incoming)
            for substitution in previous_set.away_substitutions.all():
                current_away_players.remove(substitution.outgoing)
                current_away_players.add(substitution.incoming)

        for substitution in data['home_substitutions']:
            if substitution['outgoing'] not in current_home_players:
                raise serializers.ValidationError('A home player not on the field cannot be outgoing')
            if substitution['incoming'] in current_home_players:
                raise serializers.ValidationError('A home player on the field cannot be incoming')
            current_home_players.remove(substitution['outgoing'])
            current_home_players.add(substitution['incoming'])

        for substitution in data['away_substitutions']:
            if substitution['outgoing'] not in current_away_players:
                raise serializers.ValidationError('An away player not on the field cannot be outgoing')
            if substitution['incoming'] in current_away_players:
                raise serializers.ValidationError('An away player on the field cannot be incoming')
            current_away_players.remove(substitution['outgoing'])
            current_away_players.add(substitution['incoming'])

        return data

    def create(self, validated_data):
        home_substitutions = validated_data.pop('home_substitutions')
        away_substitutions = validated_data.pop('away_substitutions')

        set = Set.objects.create(**validated_data)
        set.home_substitutions.set([Substitution.objects.create(**sub) for sub in home_substitutions])
        set.away_substitutions.set([Substitution.objects.create(**sub) for sub in away_substitutions])

        return set

    class Meta:
        model = Set
        fields = ('game', 'home_points', 'away_points', 'home_timeout', 'away_timeout', 'home_substitutions', 'away_substitutions')


class SignatureSerializer(serializers.Serializer):
    home_captain = serializers.CharField(min_length=4, max_length=4)
    away_captain = serializers.CharField(min_length=4, max_length=4)
    referee = serializers.CharField(min_length=4, max_length=4, required=False)

    def validate_home_captain(self, signature):
        home_captain_user = self.context['match'].home_captain.individual.user
        if home_captain_user is None or home_captain_user.get_signature() != signature:
            raise serializers.ValidationError('The signature is invalid')
        return signature

    def validate_away_captain(self, signature):
        away_captain_user = self.context['match'].away_captain.individual.user
        if away_captain_user is None or away_captain_user.get_signature() != signature:
            raise serializers.ValidationError('The signature is invalid')
        return signature

    def validate_referee(self, signature):
        referees = self.context['match'].referees
        if referees.count() == 0:
            raise serializers.ValidationError('There are no referees for this match')
        referee_user = referees.get().individual.user
        if referee_user is None or referee_user.get_signature() != signature:
            raise serializers.ValidationError('The signature is invalid')
        return signature

    def validate(self, data):
        if any([game.sets.count() < 2 for game in self.context['match'].games.all()]):
            raise serializers.ValidationError('Not all games are filled out')
        if 'referee' not in data and self.context['match'].referees.count() > 0:
            raise serializers.ValidationError('A signature for the referee is missing')
        return data
