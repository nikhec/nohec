# Register your models here.

from django.contrib import admin

from nohec.match.models import Match, Game, Set, MatchLock, MatchAudit, Substitution


@admin.register(Match)
class MatchAdmin(admin.ModelAdmin):
    model = Match
    list_display = ['date', 'league', 'round', 'home_team', 'away_team', 'state']


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    model = Game
    list_display = ['match', 'type']


@admin.register(Set)
class SetAdmin(admin.ModelAdmin):
    model = Set
    list_display = ['game', 'home_points', 'away_points']


@admin.register(Substitution)
class SubstitutionAdmin(admin.ModelAdmin):
    model = Substitution
    list_display = ['incoming', 'outgoing']


@admin.register(MatchLock)
class MatchLockAdmin(admin.ModelAdmin):
    model = MatchLock
    list_display = ['match', 'user', 'acquired']


@admin.register(MatchAudit)
class MatchAuditAdmin(admin.ModelAdmin):
    model = MatchAudit
    list_display = ['type', 'match', 'user', 'date']
