"""league URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
"""

from django.urls import path, include, re_path

from .views import MatchAPI, MyMatchAPI, EnterMatchAPI, SaveSetMatchAPI, SignMatchAPI, CaptainsMatchAPI, NoteMatchAPI, \
    SaveGameMatchAPI, MatchGroupByAPI, MyMatchGroupByAPI, MatchDetailAPI, ExitMatchAPI

urlpatterns = [
    path('matches/', MatchAPI.as_view()),
    path('my-matches/', MyMatchAPI.as_view()),

    path('matches/<int:pk>/', include([
        path('enter/', EnterMatchAPI.as_view()),
        path('exit/', ExitMatchAPI.as_view()),
        path('note/', NoteMatchAPI.as_view()),
        path('captains/', CaptainsMatchAPI.as_view()),
        path('game/', SaveGameMatchAPI.as_view()),
        path('set/', SaveSetMatchAPI.as_view()),
        path('sign/', SignMatchAPI.as_view()),
        path('detail/', MatchDetailAPI.as_view()),
    ])),

    re_path(r'^matches-by-(?P<group>date|round)/(?P<order>past|upcoming)/$', MatchGroupByAPI.as_view()),
    re_path(r'^my-matches-by-(?P<group>date|round)/(?P<order>past|upcoming)/$', MyMatchGroupByAPI.as_view()),
    re_path(r'^leagues/(?P<id>[1-9][0-9]*)/matches-by-(?P<group>date|round)/(?P<order>past|upcoming)/$', MatchGroupByAPI.as_view()),
]
