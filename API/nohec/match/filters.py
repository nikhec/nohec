from django_filters import rest_framework as filters

from .models import Match


class MatchFilter(filters.FilterSet):
    class Meta:
        model = Match
        fields = {
            'date': ['lt', 'gt'],  # must be in ISO 8601
            'league__code': ['exact'],
            'league__id': ['exact'],
        }
