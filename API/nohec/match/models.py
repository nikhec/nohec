from typing import Tuple, Optional

from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models import Count, Q, QuerySet, F
from django.utils import timezone
from django.utils.functional import cached_property

from nohec.league.models import Team, League, TeamMembership, Court
from nohec.user.models import Referee, Player, User


class Match(models.Model):
    class MatchState(models.IntegerChoices):
        UPCOMING = 1
        WAITING_FOR_CAPTAINS = 2
        IN_PROGRESS = 3
        FINISHED = 4

    state = models.SmallIntegerField(choices=MatchState.choices, default=MatchState.UPCOMING)
    round = models.PositiveSmallIntegerField()
    date = models.DateTimeField()
    league = models.ForeignKey(League, on_delete=models.CASCADE, related_name='matches')
    home_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='home_matches')
    away_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='away_matches')
    referees = models.ManyToManyField(Referee, related_name='matches')
    home_captain = models.ForeignKey(Player, on_delete=models.SET_NULL, null=True, related_name='+')
    away_captain = models.ForeignKey(Player, on_delete=models.SET_NULL, null=True, related_name='+')
    note = models.TextField(blank=True, default='')
    court = models.ForeignKey(Court, on_delete=models.SET_NULL, related_name='matches', null=True)

    class Meta:
        ordering = ['date']
        verbose_name_plural = 'Matches'
        indexes = [
            models.Index(fields=['date']),
        ]

    @property
    def winner(self) -> Optional[Team]:
        """
        Returns the team that has won the match.
        Is equal to None if it's a draw or the match hasn't yet started.
        """
        home_match_points, away_match_points = self.score
        if home_match_points > away_match_points:
            return self.home_team
        elif away_match_points > home_match_points:
            return self.away_team
        else:
            return None

    @property
    def score(self) -> Tuple[int, int]:
        """
        Returns the score of the match given by the number of won games.
        Is equal to (0, 0) if the match hasn't yet started.
        """
        home_match_points = 0
        away_match_points = 0

        for game in self.games.all():
            home_game_points, away_game_points = game.score

            if (home_game_points == 2) ^ (away_game_points == 2):
                if home_game_points > away_game_points:
                    home_match_points += 1
                else:
                    away_match_points += 1

        return home_match_points, away_match_points

    @cached_property
    def available_home_players(self) -> QuerySet[Player]:
        """
        Returns players of the home team that are eligible to play in this match.
        """
        return self._available_players(self.home_team)

    @cached_property
    def available_away_players(self) -> QuerySet[Player]:
        """
        Returns players of the away team that are eligible to play in this match.
        """
        return self._available_players(self.away_team)

    def _available_players(self, team: Team):
        # Player members via the SHARED team membership type can only play up to a half of all matches in a season
        number_of_matches_in_season = team.home_matches.count() + team.away_matches.count()

        return team.current_members(self.date) \
            .annotate(number_of_home_matches=Count('teammembership__team__home_matches',
                        filter=Q(teammembership__team__home_matches__date__lt=self.date)
                             & Q(Q(teammembership__team__home_matches__games__home_players=F('pk'))
                               | Q(teammembership__team__home_matches__games__sets__home_substitutions__incoming=F('pk'))),
                        distinct=True),
                      number_of_away_matches=Count('teammembership__team__away_matches',
                        filter=Q(teammembership__team__away_matches__date__lt=self.date)
                             & Q(Q(teammembership__team__away_matches__games__away_players=F('pk'))
                               | Q(teammembership__team__away_matches__games__sets__away_substitutions__incoming=F('pk'))),
                        distinct=True)) \
            .annotate(number_of_matches=F('number_of_home_matches') + F('number_of_away_matches')) \
            .exclude(teammembership__type=TeamMembership.MembershipType.SHARED,
                     number_of_matches__gt=number_of_matches_in_season // 2)

    @property
    def home_players(self) -> QuerySet[Player]:
        """
        Returns players of the home team that actually played in this match.
        """
        return Player.objects.filter(
            Q(id__in=self.games.all().values_list('home_players', flat=True))
            | Q(id__in=self.games.all().values_list('sets__home_substitutions__incoming', flat=True))
        )

    @property
    def away_players(self) -> QuerySet[Player]:
        """
        Returns players of the away team that actually played in this match.
        """
        return Player.objects.filter(
            Q(id__in=self.games.all().values_list('away_players', flat=True))
            | Q(id__in=self.games.all().values_list('sets__away_substitutions__incoming', flat=True))
        )


class Game(models.Model):
    class GameType(models.IntegerChoices):
        SINGLE = 1
        DOUBLE = 2
        TRIPLE = 3

    match = models.ForeignKey(Match, on_delete=models.CASCADE, related_name='games')
    type = models.SmallIntegerField(choices=GameType.choices)
    home_players = models.ManyToManyField(Player, related_name='+')
    away_players = models.ManyToManyField(Player, related_name='+')

    class Meta:
        order_with_respect_to = 'match'

    @property
    def score(self) -> Tuple[int, int]:
        """
        Returns the score of the game given by the number of won sets.
        Is equal to (0, 0) if the game hasn't yet started.
        """
        home_game_points = 0
        away_game_points = 0

        for set in self.sets.all():
            if (set.home_points == 10) ^ (set.away_points == 10):
                if set.home_points > set.away_points:
                    home_game_points += 1
                else:
                    away_game_points += 1

        return home_game_points, away_game_points


class Substitution(models.Model):
    incoming = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='+')
    outgoing = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='+')


class Set(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='sets')
    home_points = models.SmallIntegerField()
    away_points = models.SmallIntegerField()
    home_timeout = models.SmallIntegerField(default=0)
    away_timeout = models.SmallIntegerField(default=0)
    home_substitutions = models.ManyToManyField(Substitution, related_name='home')
    away_substitutions = models.ManyToManyField(Substitution, related_name='away')

    class Meta:
        order_with_respect_to = 'game'


class MatchLock(models.Model):
    match = models.OneToOneField(Match, on_delete=models.CASCADE, related_name='+')
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='+')
    acquired = models.DateTimeField()


class MatchAudit(models.Model):
    class AuditType(models.IntegerChoices):
        ENTER_MATCH = 1
        NOTE = 2
        CAPTAINS = 3
        SAVE_GAME = 4
        SAVE_SET = 5
        SIGN = 6
        EXIT_MATCH = 7

    type = models.SmallIntegerField(choices=AuditType.choices)
    match = models.ForeignKey(Match, on_delete=models.CASCADE, related_name='+')
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='+')
    date = models.DateTimeField(default=timezone.now)
    body = models.JSONField(encoder=DjangoJSONEncoder)
