from django.utils.encoding import force_str
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from django.utils.translation import gettext_lazy as _


class LimitOnlyPagination(LimitOffsetPagination):
    """
    Limit that can be used by frontend if it decides to use it.
    """
    limit_query_description = _('Number of results to return.')

    def get_paginated_response(self, data):
        return Response(data)

    def get_paginated_response_schema(self, schema):
        return schema

    def get_schema_operation_parameters(self, view):
        return [
            {
                'name': self.limit_query_param,
                'required': False,
                'in': 'query',
                'description': force_str(self.limit_query_description),
                'schema': {
                    'type': 'integer',
                },
            },
        ]
