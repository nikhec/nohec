# Create your views here.
from datetime import timedelta

from django.contrib.auth.models import AnonymousUser
from django.db import IntegrityError
from django.shortcuts import get_object_or_404
from django.utils.timezone import now
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema_view, extend_schema, OpenApiParameter, inline_serializer, \
    PolymorphicProxySerializer, OpenApiResponse
from rest_framework import generics, status
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.fields import DateField, IntegerField, CharField
from rest_framework.generics import GenericAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.serializers import Serializer

from .exceptions import StatusAPIException
from .filters import MatchFilter
from .models import Match, MatchAudit, MatchLock, Game
from .pagination import LimitOnlyPagination
from .serializers import EnterMatchSerializer, SaveSetSerializer, CaptainsSerializer, NoteSerializer, \
    SaveGameSerializer, MatchSerializer, ErrorSerializer, SimpleMatchSerializer, MatchDetailSerializer, \
    SignatureSerializer
from ..league.models import League, Season
from ..user.models import User
from ..user.serializers import EmptySerializer


class MatchAPI(generics.ListAPIView):
    """
    Returns a list of all Matches.
    """
    serializer_class = MatchSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = MatchFilter
    pagination_class = LimitOnlyPagination

    def get_queryset(self):
        # prefetch related etc to make execution faster
        return Match.objects \
            .select_related('home_team', 'away_team') \
            .prefetch_related('games', 'games__sets')


class MyMatchAPI(MatchAPI):
    """
    Returns a list of all Matches relevant to the current user.
    """
    authentication_classes = [TokenAuthentication, SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = getattr(self.request, 'user', None)

        if user is None or isinstance(user, AnonymousUser) or user.individual is None:
            return Match.objects.none()

        return user.individual.matches() \
            .select_related('home_team', 'away_team') \
            .prefetch_related('games', 'games__sets')


class MatchGroupByAPI(GenericAPIView):
    """
    Returns a list of past or upcoming Matches grouped by their date or round.
    """
    serializer_class = SimpleMatchSerializer

    @extend_schema(
        parameters=[
            OpenApiParameter(name='group', location=OpenApiParameter.PATH, description='Property to group the matches by.', required=True, enum=['date', 'round']),
            OpenApiParameter(name='order', location=OpenApiParameter.PATH, description='A switch for sorting and filtering.', required=True, enum=['past', 'upcoming']),
            OpenApiParameter(name='count', description='Number of results to return.', type=int),
            OpenApiParameter(name='latest_season', description='Filter by latest season.', type=bool)
        ],
        responses=PolymorphicProxySerializer('MetaMatchGroupBy', [
            inline_serializer('MatchGroupByDateSerializer',
                              fields={'date': DateField(), 'matches': MatchSerializer(many=True)}),
            inline_serializer('MatchGroupByRoundSerializer',
                              fields={'round': IntegerField(), 'matches': MatchSerializer(many=True)})
        ], None)
    )
    def get(self, request, group, order, id=None):
        queryset = self.get_queryset()

        if id is not None:
            league = get_object_or_404(League, pk=id)
            queryset = queryset.filter(league=league)
        elif 'latest_season' in request.GET and request.GET['latest_season'] == 'true':
            queryset = queryset.filter(league__season=Season.objects.latest())

        if order == 'past':
            queryset = queryset.filter(state=Match.MatchState.FINISHED).order_by('-date')
        else:  # order == 'upcoming'
            queryset = queryset.exclude(state=Match.MatchState.FINISHED).order_by('date')

        count = self._get_count(request)
        if count is not None:
            queryset = queryset[:count]

        if group == 'date':
            dates = sorted(set(queryset.values_list('date__date', flat=True)), reverse=order == 'past')
            response = [{
                'date': str(date),
                'matches': self.get_serializer([match for match in queryset if match.date.date() == date], many=True).data
            } for date in dates]

        else:  # group == 'round'
            rounds = sorted(set(queryset.values_list('round', flat=True)), reverse=order == 'past')
            response = [{
                'round': round,
                'matches': self.get_serializer([match for match in queryset if match.round == round], many=True).data
            } for round in rounds]

        return Response(response)

    @staticmethod
    def _get_count(request):
        try:
            value = int(request.query_params['count'])
            if value <= 0:
                value = None
        except (KeyError, ValueError):
            value = None
        return value

    def get_queryset(self):
        # prefetch related etc to make execution faster
        return Match.objects \
            .select_related('home_team', 'away_team') \
            .prefetch_related('games', 'games__sets')


class MyMatchGroupByAPI(MatchGroupByAPI):
    """
    Returns a list of past or upcoming Matches relevant to the current user grouped by their date or round.
    """
    authentication_classes = [TokenAuthentication, SessionAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Match.objects.none()

    def get_queryset(self):
        user = getattr(self.request, 'user', None)

        if user is None or user.individual is None:
            return Match.objects.none()
        else:
            # prefetch related etc to make execution faster
            return user.individual.matches()\
                .select_related('home_team', 'away_team') \
                .prefetch_related('games', 'games__sets')


class MatchDetailAPI(RetrieveAPIView):
    """
    Returns the detail of a match.
    """
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    serializer_class = MatchDetailSerializer
    queryset = Match.objects


class InteractiveMatchAPI(GenericAPIView):
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmptySerializer

    audit_type: MatchAudit.AuditType = None

    @staticmethod
    def validate_user_and_match(request, pk) -> tuple[User, Match]:
        """
        Extracts the current user from the request and the requested match from the path primary key.
        Validates that both are valid and that the user is allowed to modify the match.
        """

        match = get_object_or_404(Match, pk=pk)

        user = request.user
        if not user.can_modify_match(match):
            raise StatusAPIException('Current user is not allowed to do this action', status.HTTP_403_FORBIDDEN)

        return user, match

    @staticmethod
    def acquire_lock(match: Match, user: User) -> None:
        """
        Tries to acquire the match lock for the given match and user.
        If the user successfully acquires the lock, it's acquisition time is set to the current time.
        Raises an exception if it is currently held by a different user.
        """

        MatchLock.objects.filter(match=match, acquired__lt=now() - timedelta(minutes=15)).delete()
        try:
            MatchLock.objects.update_or_create(match=match, user=user, defaults={'acquired': now()})
        except IntegrityError:
            raise StatusAPIException('Cannot acquire lock', status.HTTP_409_CONFLICT)

    def upcoming(self, match, user, request):
        raise StatusAPIException('Match is not yet started', status.HTTP_400_BAD_REQUEST)

    def waiting_for_captains(self, match, user, request):
        raise StatusAPIException('Waiting for captains', status.HTTP_400_BAD_REQUEST)

    def in_progress(self, match, user, request):
        return Response(status=status.HTTP_501_NOT_IMPLEMENTED)

    def finished(self, match, user, request):
        raise StatusAPIException('Match is already finished', status.HTTP_410_GONE)

    def post(self, request, pk):
        user, match = self.validate_user_and_match(request, pk)
        self.acquire_lock(match, user)
        MatchAudit.objects.create(type=self.audit_type, match=match, user=user, body=request.data)

        if match.state == Match.MatchState.UPCOMING:
            return self.upcoming(match, user, request)
        elif match.state == Match.MatchState.WAITING_FOR_CAPTAINS:
            return self.waiting_for_captains(match, user, request)
        elif match.state == Match.MatchState.IN_PROGRESS:
            return self.in_progress(match, user, request)
        elif match.state == Match.MatchState.FINISHED:
            return self.finished(match, user, request)
        else:
            raise StatusAPIException('Unknown match state', status.HTTP_500_INTERNAL_SERVER_ERROR)


base_responses = {
    200: EmptySerializer,
    400: OpenApiResponse(ErrorSerializer, description='A general user request error.'),
    403: OpenApiResponse(ErrorSerializer, description='The current user has not provided authentication or is not authorized to edit this match.'),
    404: OpenApiResponse(ErrorSerializer, description='Invalid match id.'),
    409: OpenApiResponse(ErrorSerializer, description='The lock for this match is currently held by a different user.'),
    410: OpenApiResponse(ErrorSerializer, description='Match is already finished and cannot be edited anymore.')
}


def generic_400_response(serializer: Serializer) -> OpenApiResponse:
    return OpenApiResponse(PolymorphicProxySerializer('Meta' + serializer.__class__.__name__ + 'Error', [
        inline_serializer(serializer.__class__.__name__ + 'ErrorSerializer',
                          fields={field_name: CharField(required=False)
                                  for field_name in serializer.fields.keys() | ('non_field_errors',)}),
        ErrorSerializer], None), description='A general user request error or a serializer field error.')


@extend_schema_view(post=extend_schema(request=EmptySerializer, responses=base_responses | {200: EnterMatchSerializer}))
class EnterMatchAPI(InteractiveMatchAPI):
    """
    Returns the current state of the match and starts it if it hasn't been started yet.
    First tries to acquire the lock for the user.
    """
    serializer_class = EnterMatchSerializer
    audit_type = MatchAudit.AuditType.ENTER_MATCH

    def upcoming(self, match, user, request):
        if match.date - timedelta(minutes=15) > now():
            raise StatusAPIException('Too early', status.HTTP_400_BAD_REQUEST)

        match.state = Match.MatchState.WAITING_FOR_CAPTAINS
        match.save()

        for type in match.league.get_game_types():
            Game.objects.create(match=match, type=type)

        serializer = EnterMatchSerializer(match)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def waiting_for_captains(self, match, user, request):
        return self._match_state(match)

    def in_progress(self, match, user, request):
        return self._match_state(match)

    @staticmethod
    def _match_state(match):
        serializer = EnterMatchSerializer(match)
        return Response(serializer.data, status=status.HTTP_200_OK)


@extend_schema_view(post=extend_schema(responses=base_responses))
class ExitMatchAPI(InteractiveMatchAPI):
    """
    Releases the lock held by the current user for the match.
    """
    audit_type = MatchAudit.AuditType.EXIT_MATCH

    def waiting_for_captains(self, match, user, request):
        return self._exit_match(match, user)

    def in_progress(self, match, user, request):
        return self._exit_match(match, user)

    @staticmethod
    def _exit_match(match, user):
        MatchLock.objects.filter(match=match, user=user).delete()
        return Response(status=status.HTTP_200_OK)


@extend_schema_view(post=extend_schema(responses=base_responses | {400: generic_400_response(NoteSerializer())}))
class NoteMatchAPI(InteractiveMatchAPI):
    """
    Updates the match note.
    First tries to acquire the lock for the user.
    """
    serializer_class = NoteSerializer
    audit_type = MatchAudit.AuditType.NOTE

    def waiting_for_captains(self, match, user, request):
        return self._save_note(match, request)

    def in_progress(self, match, user, request):
        return self._save_note(match, request)

    @staticmethod
    def _save_note(match, request):
        serializer = NoteSerializer(match, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@extend_schema_view(post=extend_schema(responses=base_responses | {400: generic_400_response(CaptainsSerializer())}))
class CaptainsMatchAPI(InteractiveMatchAPI):
    """
    Validates and saves the provided captains for the match.
    First tries to acquire the lock for the user.
    """
    serializer_class = CaptainsSerializer
    audit_type = MatchAudit.AuditType.CAPTAINS

    def waiting_for_captains(self, match, user, request):
        serializer = CaptainsSerializer(match, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def in_progress(self, match, user, request):
        raise StatusAPIException('Captains are already selected', status.HTTP_410_GONE)


@extend_schema_view(post=extend_schema(responses=base_responses | {400: generic_400_response(SaveGameSerializer())}))
class SaveGameMatchAPI(InteractiveMatchAPI):
    """
    Validates and saves the provided game.
    First tries to acquire the lock for the user.
    """
    serializer_class = SaveGameSerializer
    audit_type = MatchAudit.AuditType.SAVE_GAME

    def in_progress(self, match, user, request):
        if 'id' not in request.data:
            raise StatusAPIException('Game id is a required field', status.HTTP_400_BAD_REQUEST)

        pk = request.data['id']
        game = get_object_or_404(Game, pk=pk)

        serializer = SaveGameSerializer(game, data=request.data, context={'match': match})
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@extend_schema_view(post=extend_schema(responses=base_responses | {400: generic_400_response(SaveSetSerializer())}))
class SaveSetMatchAPI(InteractiveMatchAPI):
    """
    Validates and saves the provided set.
    First tries to acquire the lock for the user.
    """
    serializer_class = SaveSetSerializer
    audit_type = MatchAudit.AuditType.SAVE_SET

    def in_progress(self, match, user, request):
        serializer = SaveSetSerializer(data=request.data, context={'match': match})
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@extend_schema_view(post=extend_schema(responses=base_responses | {400: generic_400_response(SignatureSerializer())}))
class SignMatchAPI(InteractiveMatchAPI):
    """
    Signs and finishes the match.
    First tries to acquire the lock for the user.
    """
    serializer_class = SignatureSerializer
    audit_type = MatchAudit.AuditType.SIGN

    def in_progress(self, match, user, request):
        serializer = SignatureSerializer(data=request.data, context={'match': match})
        if serializer.is_valid():
            match.state = Match.MatchState.FINISHED
            match.save()

            MatchLock.objects.filter(match=match).delete()

            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
