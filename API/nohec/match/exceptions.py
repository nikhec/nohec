from rest_framework.exceptions import APIException


class StatusAPIException(APIException):
    """
    The purpose of this class is to provide a generic way to raise an exception with a message and a status code using
    it's constructor. Normally in Django this would have to be done by subclassing the APIException for each code.
    """

    def __init__(self, detail, status_code):
        self.detail = detail
        self.status_code = status_code
