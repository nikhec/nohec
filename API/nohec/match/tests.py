from datetime import date, datetime, timedelta
from unittest import TestCase

from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from nohec.league.models import League, Season, Team, Club, TeamMembership
from nohec.match.models import Match, Game, Set
from nohec.user.models import Player, Individual, Referee


class MatchTests(TestCase):
    def test_score(self):
        season = Season.objects.create(year=2021)
        league = League.objects.create(name='1st league', code='FL', season=season)

        home_club = Club.objects.create(name='Foo')
        home_leader_individual = Individual.objects.create(first_name='John', last_name='Doe', birth_date=date(1980, 4, 2))
        home_leader = Player.objects.create(registration_number=42, individual=home_leader_individual)
        home_team = Team.objects.create(club=home_club, leader=home_leader, league=league)

        away_club = Club.objects.create(name='Bar')
        away_leader_individual = Individual.objects.create(first_name='Jane', last_name='Doe', birth_date=date(1975, 6, 9))
        away_leader = Player.objects.create(registration_number=123, individual=away_leader_individual)
        away_team = Team.objects.create(club=away_club, leader=away_leader, league=league)

        match = Match.objects.create(round=1, date=datetime(2021, 1, 1, 17, tzinfo=timezone.utc), league=league, home_team=home_team, away_team=away_team)

        for type, ((fsh, fsa), (ssh, ssa), score) in zip(match.league.get_game_types(),
                                                         [((10, 8), (10, 6), (2, 0)),
                                                          ((7, 10), (10, 9), (1, 1)),
                                                          ((10, 5), (10, 7), (2, 0)),
                                                          ((8, 10), (9, 10), (0, 2)),
                                                          ((7, 10), (10, 4), (1, 1)),
                                                          ((7, 10), (10, 6), (1, 1)),
                                                          ((10, 3), (2, 10), (1, 1)),
                                                          ((9, 10), (8, 10), (0, 2)),
                                                          ((10, 6), (10, 4), (2, 0))]):
            game = Game.objects.create(match=match, type=type)
            Set.objects.create(game=game, home_points=fsh, away_points=fsa, home_timeout=0, away_timeout=0)
            Set.objects.create(game=game, home_points=ssh, away_points=ssa, home_timeout=0, away_timeout=0)

            self.assertEqual(score, game.score)

        self.assertEqual((3, 2), match.score)


class MatchApiTests(APITestCase):
    def setUp(self):
        User = get_user_model()

        i = Individual.objects.create(first_name='Not', last_name='Used', birth_date=date(1958, 9, 7))
        User.objects.create_user(email='not.used@nohec.cz', password='pass', individual=i)
        self.season = Season.objects.create(year=2021)
        self.league = League.objects.create(name='2nd league', code='SL', season=self.season)

        self.home_club = Club.objects.create(name='Foo')
        self.home_leader_individual = Individual.objects.create(first_name='John', last_name='Doe', birth_date=date(1980, 4, 2))
        User.objects.create_user(email='john.doe@nohec.cz', password='pass', individual=self.home_leader_individual)
        self.home_leader = Player.objects.create(registration_number=42, individual=self.home_leader_individual)
        self.home_team = Team.objects.create(club=self.home_club, leader=self.home_leader, league=self.league)
        TeamMembership.objects.create(since=datetime.today().astimezone(tz=timezone.utc) - timedelta(days=10), team=self.home_team, player=self.home_leader)

        self.away_club = Club.objects.create(name='Bar')
        self.away_leader_individual = Individual.objects.create(first_name='Jane', last_name='Doe', birth_date=date(1975, 6, 9))
        User.objects.create_user(email='jane.doe@nohec.cz', password='pass', individual=self.away_leader_individual)
        self.away_leader = Player.objects.create(registration_number=123, individual=self.away_leader_individual)
        self.away_team = Team.objects.create(club=self.away_club, leader=self.away_leader, league=self.league)
        TeamMembership.objects.create(since=datetime.today().astimezone(tz=timezone.utc) - timedelta(days=10), team=self.away_team, player=self.away_leader)

        self.referee_individual = Individual.objects.create(first_name='Jack', last_name='Black', birth_date=date(1990, 5, 5))
        User.objects.create_user(email='jack.black@nohec.cz', password='pass', individual=self.referee_individual)
        self.referee = Referee.objects.create(registration_number=666, individual=self.referee_individual)

        self.match = Match.objects.create(round=1, date=datetime(2021, 1, 1, 17, tzinfo=timezone.utc), league=self.league, home_team=self.home_team, away_team=self.away_team)
        self.match.referees.set([self.referee])

        self.other_individual = Individual.objects.create(first_name='Walter', last_name='White', birth_date=date(1958, 9, 7))
        User.objects.create_user(email='walter.white@nohec.cz', password='pass', individual=self.other_individual)

    def test_my_match_api(self):
        self.assertEqual(self.referee.individual.matches().get(), self.match)
        self.assertEqual(self.home_leader.individual.matches().get(), self.match)
        self.assertEqual(self.away_leader.individual.matches().get(), self.match)
        self.assertEqual(len(self.other_individual.matches()), 0)

        client = APIClient()

        for username in ['john.doe@nohec.cz', 'jane.doe@nohec.cz', 'jack.black@nohec.cz']:
            client.login(username=username, password='pass')
            response = client.get('/api/my-matches/')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data[0]['id'], self.match.pk)
            client.logout()

        client.login(username='walter.white@nohec.cz', password='pass')
        response = client.get('/api/my-matches/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def test_match_detail_api(self):
        client = APIClient()
        # anonymous user
        response = client.get('/api/matches/1/detail/')
        self.assertFalse(response.data['user_can_modify_match'])
        # user without any matches
        client.login(username='not.used@nohec.cz', password='pass')
        response = client.get('/api/matches/1/detail/')
        self.assertFalse(response.data['user_can_modify_match'])
        client.logout()
        # user with this match
        client.login(username='jane.doe@nohec.cz', password='pass')
        response = client.get('/api/matches/1/detail/')
        self.assertTrue(response.data['user_can_modify_match'])
        client.logout()
