
# dump dat:
`./manage.py dumpdata --natural-foreign  --exclude=auth.permission --exclude=contenttypes  --indent=4 > dbdump.json`

# Jak na deploy?
1. Nainstaluj docker, docker-compose
1. Naklonuj repo, spust `init-letsencrypt.sh` - vytvori certifikaty
1. Vytvor `MEDIA_ROOT` v rootu repa
1. `docker-compose up --detach`
