#!/bin/sh -ex

python manage.py migrate &&
python manage.py collectstatic --noinput &&
uwsgi --socket :8000 docker/nohec_uwsgi.ini