package cz.nohejbalpraha.app.matchForm.handler

interface MatchFormFlowHandler {
    /**
     * On next button click listener
     */
    fun next()

    /**
     * Next button label
     */
    fun nextLabel() : String

    /**
     * On match leave listener
     */
    fun leaveMatch()
}