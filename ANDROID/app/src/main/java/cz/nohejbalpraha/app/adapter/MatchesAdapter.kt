package cz.nohejbalpraha.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.databinding.GroupedMatchesBinding
import cz.nohejbalpraha.app.fragment.LeagueMatchesType
import cz.nohejbalpraha.app.handler.MatchClickHandler
import cz.nohejbalpraha.app.model.MatchList

/**
 * RecyclerView adapter for grouped matches
 * @param items grouped matches
 * @param listener match click listener
 * @param matchesType PAST / FUTURE matches
 */
class MatchesAdapter(var items: List<MatchList>, private val listener: MatchClickHandler, private val matchesType: LeagueMatchesType) : RecyclerView.Adapter<MatchesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = GroupedMatchesBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    /**
     * Updates and refreshes the data in the view
     */
    fun updateData(items: List<MatchList>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: GroupedMatchesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: MatchList) {
            binding.pastMatchesAdapter = PastMatchesAdapter(item.getListMatches(), listener)
            binding.futureMatchesAdapter = FutureMatchesAdapter(item.getListMatches(), listener)
            binding.matchesType = matchesType
            binding.description = item.getDescription()
            binding.executePendingBindings()
        }
    }
}