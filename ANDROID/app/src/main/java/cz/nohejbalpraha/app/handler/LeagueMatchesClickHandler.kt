package cz.nohejbalpraha.app.handler

interface LeagueMatchesClickHandler {
    /**
     * Called when show all past matches in league is clicked
     */
    fun pastMatchesClick()

    /**
     * Called when show all future matches in league is clicked
     */
    fun futureMatchesClick()
}