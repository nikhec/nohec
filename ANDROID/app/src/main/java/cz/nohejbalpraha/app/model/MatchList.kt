package cz.nohejbalpraha.app.model

import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.App
import java.time.LocalDate
import java.time.format.DateTimeFormatter

interface MatchList {
    /**
     * Return list of matches
     */
    fun getListMatches() : List<Match>

    /**
     * Returns description for the matches in the list
     */
    fun getDescription() : String
}

class MatchDateList(val date: String) : MatchList {
    var matches: List<Match> = ArrayList()

    override fun getListMatches(): List<Match> {
        return matches
    }

    override fun getDescription(): String {
        val localDate: LocalDate = LocalDate.parse(date, DateTimeFormatter.ISO_DATE)

        val days = arrayOf("", App.context.getString(R.string.monday), App.context.getString(R.string.tuesday), App.context.getString(R.string.wednesday),
            App.context.getString(R.string.thursday), App.context.getString(R.string.friday), App.context.getString(R.string.saturday), App.context.getString(R.string.sunday))

        return days[localDate.dayOfWeek.value] + ", " + DateTimeFormatter.ofPattern("dd.MM.").format(localDate)
    }
}

class MatchRoundList(val round: Int) : MatchList {
    var matches: List<Match> = ArrayList()

    override fun getListMatches(): List<Match> {
        return matches
    }

    override fun getDescription(): String {
        return "$round. " + App.context.getString(R.string.match_round)
    }
}