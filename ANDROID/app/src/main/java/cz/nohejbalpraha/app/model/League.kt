package cz.nohejbalpraha.app.model

data class League(val id: Int, val code: String, val name: String, val season: Int) {
    var teams: List<Team> = ArrayList()
    var matches: List<Match> = ArrayList()
}