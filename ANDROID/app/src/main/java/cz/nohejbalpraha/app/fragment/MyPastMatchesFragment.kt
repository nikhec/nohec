package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.MatchesAdapter
import cz.nohejbalpraha.app.databinding.FragmentMyPastMatchesBinding
import cz.nohejbalpraha.app.handler.MatchClickHandler
import cz.nohejbalpraha.app.viewModel.MyMatchesViewModel

/**
 * Fragment that shows user future matches
 * @param listener click listener
 */
class MyPastMatchesFragment(private val listener: MatchClickHandler) : Fragment() {
    private lateinit var viewModel: MyMatchesViewModel
    private lateinit var adapter: MatchesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity()).get(MyMatchesViewModel::class.java)
        adapter = MatchesAdapter(viewModel.pastMatches.value!!, listener, LeagueMatchesType.PAST)

        viewModel.pastMatches.observe(requireActivity(), adapter::updateData)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : FragmentMyPastMatchesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_past_matches, container, false)
        binding.adapter = adapter
        binding.lifecycleOwner = this

        return binding.root
    }
}