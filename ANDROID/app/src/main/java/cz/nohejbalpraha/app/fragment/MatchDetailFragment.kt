package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.GamesAdapter
import cz.nohejbalpraha.app.databinding.FragmentMatchDetailBinding
import cz.nohejbalpraha.app.handler.GameClickHandler
import cz.nohejbalpraha.app.model.Game
import cz.nohejbalpraha.app.model.Match
import cz.nohejbalpraha.app.viewModel.MatchDetailViewModel
import cz.nohejbalpraha.app.viewModel.MatchDetailViewModelFactory

/**
 * Fragment that shows match detail
 * @param match match
 */
class MatchDetailFragment(private val match: Match) : Fragment(), GameClickHandler {
    private lateinit var viewModel: MatchDetailViewModel
    private lateinit var adapter: GamesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, MatchDetailViewModelFactory(match)).get(MatchDetailViewModel::class.java)
        adapter = GamesAdapter(ArrayList(), this)

        viewModel.match.observe(this, { match ->
            if(match.games != null) {
                adapter.updateData(match.games)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentMatchDetailBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_match_detail, container, false)
        binding.viewModel = viewModel
        binding.adapter = adapter
        binding.lifecycleOwner = this
        binding.executePendingBindings()

        return binding.root
    }

    override fun onClick(game: Game, gameName: String) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, GameDetailFragment(game, gameName, viewModel.match.value!!))
            .addToBackStack(null)
            .commit()
    }
}