package cz.nohejbalpraha.app.model

data class UserSession(val token: String, val user_id: Int, val email: String)
