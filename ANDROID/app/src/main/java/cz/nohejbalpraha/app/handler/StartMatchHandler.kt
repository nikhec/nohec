package cz.nohejbalpraha.app.handler

import cz.nohejbalpraha.app.model.Match

interface StartMatchHandler {
    /**
     * Called when user clicks to start the match. Should open match form.
     * @param match match that should start
     */
    fun startMatch(match: Match)
}