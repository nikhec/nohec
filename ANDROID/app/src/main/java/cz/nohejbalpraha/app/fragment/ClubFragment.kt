package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.TeamsAdapter
import cz.nohejbalpraha.app.databinding.FragmentClubBinding
import cz.nohejbalpraha.app.model.Club
import cz.nohejbalpraha.app.viewModel.ClubViewModel
import cz.nohejbalpraha.app.viewModel.ClubViewModelFactory
import cz.nohejbalpraha.app.viewModel.ViewModelStatus

/**
 * Fragment that shows club detail
 * @param club club
 */
class ClubFragment(private val club: Club) : Fragment() {
    private lateinit var viewModel: ClubViewModel
    private lateinit var teamsAdapter: TeamsAdapter
    private lateinit var mapView: MapView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, ClubViewModelFactory(club)).get(ClubViewModel::class.java)
        teamsAdapter = TeamsAdapter(ArrayList())

        viewModel.clubDetail.observe(this, { club ->
            teamsAdapter.updateData(viewModel.clubDetail.value!!.teams)

            mapView.getMapAsync { map ->
                for (court in viewModel.courts.value!!) {
                    val position = LatLng(
                        court.latitude,
                        court.longitude
                    )
                    map.addMarker(
                        MarkerOptions()
                            .position(position)
                            .title(viewModel.club.name)
                    )
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16F))
                }
            }
        })

        viewModel.status.observe(this, { status ->
            if (status == ViewModelStatus.FAILURE) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, ErrorFragment(requireActivity().getString(R.string.app_error_description)))
                    .commit()
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentClubBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_club, container, false)
        binding.viewModel = viewModel
        binding.adapter = teamsAdapter
        binding.lifecycleOwner = this
        binding.executePendingBindings()

        mapView = binding.root.findViewById(R.id.mapView)
        mapView.onCreate(null)
        mapView.onResume()

        return binding.root
    }
}