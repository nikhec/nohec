package cz.nohejbalpraha.app.matchForm.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.databinding.MatchFormSetBinding
import cz.nohejbalpraha.app.matchForm.MatchFormLayoutManager
import cz.nohejbalpraha.app.matchForm.handler.MatchFormFlowHandler
import cz.nohejbalpraha.app.matchForm.handler.MatchFormSubstitutionHandler
import cz.nohejbalpraha.app.matchForm.handler.MatchFormTimeHandler
import cz.nohejbalpraha.app.matchForm.viewModel.*
import cz.nohejbalpraha.app.model.Substitution

/**
 * Fragment for match form set
 * @param matchID match id
 * @param layoutManager layout manager
 * @param game current game
 * @param set current set
 */
class MatchSetFragment(private val matchID: Int, private val layoutManager: MatchFormLayoutManager, private val game: Int, private val set: Int) : Fragment(), MatchFormFlowHandler, MatchFormTimeHandler, MatchFormSubstitutionHandler {
    private lateinit var viewModel: MatchFormViewModel
    private lateinit var gameViewModel: MatchFormGameViewModel
    private lateinit var setViewModel: MatchFormSetViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(), MatchFormViewModelFactory(matchID)).get(
            MatchFormViewModel::class.java)

        gameViewModel = ViewModelProvider(this, MatchFormGameViewModelFactory(viewModel.match.value!!.home_team, viewModel.match.value!!.away_team, viewModel.match.value!!.games[game]))
            .get(MatchFormGameViewModel::class.java)

        setViewModel = ViewModelProvider(this, MatchFormSetViewModelFactory(matchID, game, set, viewModel.match.value!!.games[viewModel.currentGame.value!!].type))
            .get(MatchFormSetViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: MatchFormSetBinding = DataBindingUtil.inflate(inflater, R.layout.match_form_set, container, false)
        binding.viewModel = viewModel
        binding.setViewModel = setViewModel
        binding.title = viewModel.getGameName(game)
        binding.subtitle = activity?.getString(R.string.match_form_set, set)
        binding.flowHandler = this
        binding.timeHandler = this
        binding.substitutionHandler = this
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun next() {
        if (setViewModel.set.value!!.home_points == 10 && setViewModel.set.value!!.away_points < 10
            || setViewModel.set.value!!.home_points < 10 && setViewModel.set.value!!.away_points == 10) {
            viewModel.recordGameSet(game, setViewModel.set.value!!)
        }
    }

    override fun nextLabel(): String {
        if(set == 1) {
            return activity?.getString(R.string.match_form_set, 2) ?: ""
        }

        return viewModel.getGameName(game + 1)
    }

    override fun leaveMatch() {
        AlertDialog.Builder(activity!!)
            .setMessage(activity?.getString(R.string.match_form_leave_match))
            .setPositiveButton(activity?.getString(R.string.match_form_leave_yes), object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    viewModel.leaveMatch()
                }
            })
            .setNegativeButton(activity?.getString(R.string.match_form_leave_no), null)
            .show()
    }

    override fun startTimer(teamType: TeamType) {
        setViewModel.startTime(teamType)
        layoutManager.openLayout(MatchTimeFragment(if (teamType == TeamType.HOME) viewModel.match.value!!.home_team else viewModel.match.value!!.away_team, this), true)
    }

    override fun endTimer() {
        activity?.onBackPressed()
    }

    override fun openSubstitution(teamType: TeamType) {
        layoutManager.openLayout(MatchSubstitutionFragment(matchID, teamType, this, layoutManager), true)
    }

    override fun substitutionCanceled() {
        activity?.onBackPressed()
    }

    override fun substitutionConfirmed(substitution: Substitution, teamType: TeamType) {
        setViewModel.addSubstitution(substitution, teamType)
        activity?.onBackPressed()
    }
}