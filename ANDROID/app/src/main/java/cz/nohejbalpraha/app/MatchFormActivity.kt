package cz.nohejbalpraha.app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.databinding.ActivityMatchFormBinding
import cz.nohejbalpraha.app.matchForm.MatchFormLayoutManager
import cz.nohejbalpraha.app.matchForm.fragment.*
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModel
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModelFactory
import cz.nohejbalpraha.app.matchForm.viewModel.MatchStatus

const val MATCH_ID = "cz.nohejbalpraha.app.MATCH_ID"

class MatchFormActivity : AppCompatActivity(), MatchFormLayoutManager {
    private var matchID: Int = 0
    private lateinit var viewModel: MatchFormViewModel
    private lateinit var binding: ActivityMatchFormBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        matchID = intent.getIntExtra(MATCH_ID, 0)

        viewModel = ViewModelProvider(this, MatchFormViewModelFactory(matchID)).get(
            MatchFormViewModel::class.java)

        viewModel.matchStatus.observe(this, { status ->
            when (status) {
                MatchStatus.CAPTAINS -> openCaptainSelect()
                MatchStatus.GAME_SQUAD -> openGameSquadForm()
                MatchStatus.GAME_SET -> openGameSetForm()
                MatchStatus.NOTE -> openGameNoteForm()
                MatchStatus.OVERVIEW -> openMatchOverview()
                MatchStatus.SIGN -> openGameSignForm()
                MatchStatus.ERROR -> openError()
                MatchStatus.FINISHED -> leaveMatch()
                MatchStatus.LEAVE -> leaveMatch()
            }
        })

        binding = DataBindingUtil.setContentView(this, R.layout.activity_match_form)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
    }

    private fun openCaptainSelect() {
        openLayout(MatchCaptainsFragment(matchID, this), false)
    }

    private fun openGameSquadForm() {
        openLayout(MatchLineupFragment(matchID, this, viewModel.currentGame.value!!), false)
    }

    private fun openGameSetForm() {
        openLayout(MatchSetFragment(matchID, this, viewModel.currentGame.value!!, viewModel.currentSet.value!!), false)
    }

    private fun openGameNoteForm() {
        openLayout(MatchNoteFragment(matchID, this), false)
    }

    private fun openGameSignForm() {
        openLayout(MatchSignFragment(matchID, this), false)
    }

    private fun openMatchOverview() {
        openLayout(MatchOverviewFragment(matchID), false)
    }

    private fun openError() {
        openLayout(MatchErrorFragment(matchID), false)
    }

    private fun leaveMatch() {
        finish()
    }

    override fun openLayout(fragment: Fragment, fullscreen: Boolean) {
        var replace = R.id.container
        when(fullscreen) {
            true -> replace = R.id.layout
        }

        supportFragmentManager.beginTransaction()
            .replace(replace, fragment)
            .addToBackStack(null)
            .commit()
    }
}