package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.model.League

/**
 * ViewModelFactory for LeagueViewModel
 */
class LeagueViewModelFactory(private val league: League) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LeagueViewModel(league) as T
    }
}

/**
 * ViewModelFactory for LeagueMatchesViewModel
 */
class LeagueMatchesViewModelFactory(private val league: League) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LeagueMatchesViewModel(league) as T
    }

}