package cz.nohejbalpraha.app.matchForm.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.databinding.MatchFormNoteBinding
import cz.nohejbalpraha.app.matchForm.MatchFormLayoutManager
import cz.nohejbalpraha.app.matchForm.handler.MatchFormFlowHandler
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormNoteViewModel
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModel
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModelFactory

/**
 * Fragment for match form note
 * @param matchID match id
 * @param layoutManager layout manager
 */
class MatchNoteFragment(private val matchID: Int, private val layoutManager: MatchFormLayoutManager) : Fragment(), MatchFormFlowHandler {
    private lateinit var viewModel: MatchFormViewModel
    private lateinit var noteViewModel: MatchFormNoteViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(), MatchFormViewModelFactory(matchID)).get(
            MatchFormViewModel::class.java)

        noteViewModel = ViewModelProvider(this).get(MatchFormNoteViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: MatchFormNoteBinding = DataBindingUtil.inflate(inflater, R.layout.match_form_note, container, false)
        binding.flowHandler = this
        binding.viewModel = noteViewModel

        return binding.root
    }

    override fun next() {
        viewModel.recordNote(noteViewModel.note.value!!)
    }

    override fun nextLabel(): String {
        return activity?.getString(R.string.match_form_overview) ?: ""
    }

    override fun leaveMatch() {
        AlertDialog.Builder(activity!!)
            .setMessage(activity?.getString(R.string.match_form_leave_match))
            .setPositiveButton(activity?.getString(R.string.match_form_leave_yes), object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    viewModel.leaveMatch()
                }
            })
            .setNegativeButton(activity?.getString(R.string.match_form_leave_no), null)
            .show()
    }
}