package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.model.Club

/**
 * ViewModelFactory for ClubViewModel
 */
class ClubViewModelFactory(private val club: Club) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ClubViewModel(club) as T
    }
}