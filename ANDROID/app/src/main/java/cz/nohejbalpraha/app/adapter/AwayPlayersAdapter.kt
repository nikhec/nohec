package cz.nohejbalpraha.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.databinding.GameAwayPlayerBinding
import cz.nohejbalpraha.app.model.Player
import cz.nohejbalpraha.app.model.Substitution

/**
 * RecyclerView adapter for away players in game set detail
 * @param items players
 * @param substitutions substitutions that happened during the set
 */
class AwayPlayersAdapter(var items: List<Player>, var substitutions: List<Substitution>) : RecyclerView.Adapter<AwayPlayersAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = GameAwayPlayerBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val binding: GameAwayPlayerBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Player) {
            binding.player = item
            binding.substituted = playerSubstituted(item)
            binding.executePendingBindings()
        }
    }

    private fun playerSubstituted(player: Player) : Boolean {
        for(sub in substitutions) {
            if (sub.incoming == player.id || sub.outgoing == player.id) {
                return true
            }
        }
        return false
    }
}