package cz.nohejbalpraha.app.matchForm.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.databinding.MatchFormLineupBinding
import cz.nohejbalpraha.app.matchForm.MatchFormLayoutManager
import cz.nohejbalpraha.app.matchForm.adapter.SelectPlayersListener
import cz.nohejbalpraha.app.matchForm.handler.MatchFormFlowHandler
import cz.nohejbalpraha.app.matchForm.viewModel.*
import cz.nohejbalpraha.app.model.Player

/**
 * Fragment for squad selection
 * @param matchID match id
 * @param layoutManager layout manager
 * @param game current game
 */
class MatchLineupFragment(private val matchID: Int, private val layoutManager: MatchFormLayoutManager, private val game: Int) : Fragment(),
    SelectPlayersListener, MatchFormFlowHandler {
    private lateinit var viewModel: MatchFormViewModel
    private lateinit var gameViewModel: MatchFormGameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(), MatchFormViewModelFactory(matchID)).get(
            MatchFormViewModel::class.java)

        gameViewModel = ViewModelProvider(this, MatchFormGameViewModelFactory(viewModel.match.value!!.home_team, viewModel.match.value!!.away_team, viewModel.match.value!!.games[game]))
            .get(MatchFormGameViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: MatchFormLineupBinding = DataBindingUtil.inflate(inflater, R.layout.match_form_lineup, container, false)
        binding.viewModel = gameViewModel
        binding.title = viewModel.getGameName(game)
        binding.listener = this
        binding.handler = this
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun openSelection(teamType: TeamType) {
        layoutManager.openLayout(PlayersSelectFragment(matchID, this, teamType, PlayerSelectType.LINEUP), true)
    }

    override fun selectionCanceled() {
        activity?.onBackPressed()
    }

    override fun selectionConfirmed(players: List<Player>, flag: Int) {
        gameViewModel.setSquad(players, if (flag == 0) TeamType.HOME else TeamType.AWAY)
        activity?.onBackPressed()
    }

    override fun next() {
        if (gameViewModel.isSquadSet()) {
            viewModel.recordGameSquad(game, gameViewModel.game.value!!.home_players, gameViewModel.game.value!!.away_players)
        }
    }

    override fun nextLabel(): String {
        return getString(R.string.match_form_set, 1)
    }

    override fun leaveMatch() {
        AlertDialog.Builder(activity!!)
            .setMessage(activity?.getString(R.string.match_form_leave_match))
            .setPositiveButton(activity?.getString(R.string.match_form_leave_yes), object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    viewModel.leaveMatch()
                }
            })
            .setNegativeButton(activity?.getString(R.string.match_form_leave_no), null)
            .show()
    }
}