package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.AccountManager
import cz.nohejbalpraha.app.common.LoginListener
import cz.nohejbalpraha.app.databinding.FragmentMymatchesBinding
import cz.nohejbalpraha.app.handler.MatchClickHandler
import cz.nohejbalpraha.app.handler.UserClickHandler
import cz.nohejbalpraha.app.model.Match
import cz.nohejbalpraha.app.viewModel.MyMatchesViewModel
import cz.nohejbalpraha.app.viewModel.ViewModelStatus

/**
 * Fragment that shows pager for user past and future matches
 */
class MyMatchesFragment : Fragment(), LoginListener, MatchClickHandler, UserClickHandler {
    private lateinit var viewModel: MyMatchesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity()).get(MyMatchesViewModel::class.java)
        viewModel.loadMatches()

        viewModel.status.observe(this, { status ->
            if (status == ViewModelStatus.FAILURE) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, ErrorFragment(requireActivity().getString(R.string.app_error_description)))
                    .commit()
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : FragmentMymatchesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_mymatches, container, false)
        binding.viewModel = viewModel
        binding.userClickHandler = this
        binding.lifecycleOwner = this

        val tabLayout: TabLayout = binding.root.findViewById(R.id.tab_layout)
        val viewPager: ViewPager2 = binding.root.findViewById(R.id.pager)
        viewPager.adapter = PagerAdapter(this)
        val texts = arrayOf(getString(R.string.my_matches_upcoming), getString(R.string.my_matches_past))
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = texts[position]
        }.attach()

        return binding.root
    }

    private inner class PagerAdapter(private val listener: MatchClickHandler) : FragmentStateAdapter(this) {
        override fun getItemCount(): Int = 2

        override fun createFragment(position: Int): Fragment {
            when (position) {
                1 -> return MyPastMatchesFragment(listener)
                else -> return MyFutureMatchesFragment(listener)
            }
        }

    }

    override fun login() {
        // empty
    }

    override fun logout() {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, LoginFragment())
            .commit()
    }

    override fun onResume() {
        super.onResume()
        AccountManager.addListener(this)
    }

    override fun onPause() {
        super.onPause()
        AccountManager.removeListener(this)
    }

    override fun onClick(match: Match) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, MatchDetailFragment(match))
            .addToBackStack(null)
            .commit()
    }

    override fun onClick() {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, UserFragment())
            .addToBackStack(null)
            .commit()
    }
}