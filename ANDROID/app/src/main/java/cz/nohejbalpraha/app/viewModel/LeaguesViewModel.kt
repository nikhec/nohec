package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.model.League
import retrofit2.Call
import retrofit2.Callback;
import retrofit2.Response

/**
 * ViewModel that handles data for leagues list page
 */
class LeaguesViewModel : ViewModel() {
    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData<ViewModelStatus>(ViewModelStatus.LOADING)
    }
    val leagues: MutableLiveData<List<League>> by lazy {
        MutableLiveData<List<League>>(ArrayList())
    }

    init {
        ApiClient.callAPI().getLeagues().enqueue(object : Callback<List<League>> {
            override fun onResponse(call: Call<List<League>>, response: Response<List<League>>) {
                if (response.code() == 200 && response.body() != null) {
                    leagues.value = response.body()!!
                    status.value = ViewModelStatus.SUCCESS
                } else {
                    status.value = ViewModelStatus.FAILURE
                }
            }

            override fun onFailure(call: Call<List<League>>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }
        })
    }
}