package cz.nohejbalpraha.app.model

data class PastMatchOverview(val homeTeam: Team, val awayTeam: Team, val homePoints: Int, val awayPoints: Int)
