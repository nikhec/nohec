package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.AccountManager
import cz.nohejbalpraha.app.common.LoginListener
import cz.nohejbalpraha.app.databinding.FragmentLoginBinding
import cz.nohejbalpraha.app.viewModel.LoginViewModel
import cz.nohejbalpraha.app.viewModel.ViewModelStatus

/**
 * Fragment that shows user login
 */
class LoginFragment : Fragment(), LoginListener {
    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        viewModel.status.observe(this, { status ->
            if (status == ViewModelStatus.FAILURE) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, ErrorFragment(requireActivity().getString(R.string.app_error_description)))
                    .commit()
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        binding.viewModel = viewModel
        binding.executePendingBindings()
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun login() {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, MyMatchesFragment())
            .commit()
    }

    override fun logout() {
        // empty
    }

    override fun onResume() {
        super.onResume()
        AccountManager.addListener(this)
    }

    override fun onPause() {
        super.onPause()
        AccountManager.removeListener(this)
    }
}