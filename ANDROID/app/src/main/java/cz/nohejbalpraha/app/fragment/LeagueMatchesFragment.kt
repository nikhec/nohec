package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.MatchesAdapter
import cz.nohejbalpraha.app.databinding.FragmentLeagueMatchesBinding
import cz.nohejbalpraha.app.handler.MatchClickHandler
import cz.nohejbalpraha.app.model.League
import cz.nohejbalpraha.app.viewModel.LeagueMatchesViewModel
import cz.nohejbalpraha.app.viewModel.LeagueMatchesViewModelFactory
import cz.nohejbalpraha.app.viewModel.ViewModelStatus

/**
 * Fragment that shows matches from a league
 * @param league league
 * @param matches PAST / FUTURE matches
 * @param matchClickListener click listener
 */
class LeagueMatchesFragment(private val league: League, private val matches: LeagueMatchesType, private val matchClickListener: MatchClickHandler) : Fragment() {
    private lateinit var viewModel: LeagueMatchesViewModel
    private lateinit var adapter: MatchesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, LeagueMatchesViewModelFactory(league)).get(LeagueMatchesViewModel::class.java)
        adapter = MatchesAdapter(ArrayList(), matchClickListener, matches)

        when(matches) {
            LeagueMatchesType.FUTURE -> {
                viewModel.futureMatches.observe(this, adapter::updateData)
                viewModel.getFutureMatches()
            }
            else -> {
                viewModel.pastMatches.observe(this, adapter::updateData)
                viewModel.getPastMatches()
            }
        }

        viewModel.status.observe(this, { status ->
            if (status == ViewModelStatus.FAILURE) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, ErrorFragment(requireActivity().getString(R.string.app_error_description)))
                    .commit()
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : FragmentLeagueMatchesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_league_matches, container, false)
        binding.viewModel = viewModel
        binding.adapter = adapter
        binding.lifecycleOwner = this

        return binding.root
    }
}

enum class LeagueMatchesType {
    PAST,
    FUTURE
}