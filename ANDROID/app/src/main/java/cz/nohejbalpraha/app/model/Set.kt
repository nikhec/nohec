package cz.nohejbalpraha.app.model

class Set(val id: Int) {
    var home_points: Int = 0
    var away_points: Int = 0

    var home_timeout: Int = 0
    var away_timeout: Int = 0

    var home_substitutions: List<Substitution> = ArrayList()
    var away_substitutions: List<Substitution> = ArrayList()
}