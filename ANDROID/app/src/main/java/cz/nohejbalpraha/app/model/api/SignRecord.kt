package cz.nohejbalpraha.app.model.api

data class SignRecord(val home_captain: String, val away_captain: String) {
    var referee: String = ""
}
