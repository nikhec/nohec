package cz.nohejbalpraha.app.handler

import cz.nohejbalpraha.app.model.Match

interface MatchClickHandler {
    /**
     * Called when match overview is clicked. Should open match detail page
     */
    fun onClick(match: Match)
}