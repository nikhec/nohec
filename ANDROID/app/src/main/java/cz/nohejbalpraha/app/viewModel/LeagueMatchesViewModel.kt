package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.model.League
import cz.nohejbalpraha.app.model.MatchList
import cz.nohejbalpraha.app.model.MatchRoundList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * ViewModel that handles data for league matches
 */
class LeagueMatchesViewModel(val league: League) : ViewModel() {
    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData(ViewModelStatus.LOADING)
    }
    val pastMatches: MutableLiveData<List<MatchList>> by lazy {
        MutableLiveData(ArrayList())
    }
    val futureMatches: MutableLiveData<List<MatchList>> by lazy {
        MutableLiveData(ArrayList())
    }

    /**
     * Loads past matches for given league from the API
     */
    fun getPastMatches() {
        ApiClient.callAPI().getLeaguePastMatches(leagueID = league.id).enqueue(object:
            Callback<List<MatchRoundList>> {
            override fun onResponse(
                call: Call<List<MatchRoundList>>,
                response: Response<List<MatchRoundList>>
            ) {
                if (response.code() == 200 && response.body() != null) {
                    pastMatches.value = response.body()
                    status.value = ViewModelStatus.SUCCESS
                } else {
                    status.value = ViewModelStatus.FAILURE
                }
            }

            override fun onFailure(call: Call<List<MatchRoundList>>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }

        })
    }

    /**
     * Loads future matches for given league from the API
     */
    fun getFutureMatches() {
        ApiClient.callAPI().getLeagueFutureMatches(leagueID = league.id).enqueue(object : Callback<List<MatchRoundList>> {
            override fun onResponse(
                call: Call<List<MatchRoundList>>,
                response: Response<List<MatchRoundList>>
            ) {
                if (response.code() == 200 && response.body() != null) {
                    futureMatches.value = response.body()
                    status.value = ViewModelStatus.SUCCESS
                }
            }

            override fun onFailure(call: Call<List<MatchRoundList>>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }

        })
    }
}