package cz.nohejbalpraha.app.matchForm.viewModel

import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.App

/**
 * ViewModel that holds data for match form timer
 */
class TimerViewModel : ViewModel() {
    val defaultTime: Long = 30000

    val time: MutableLiveData<String> by lazy {
        MutableLiveData("30")
    }

    init {
        object : CountDownTimer(defaultTime, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                time.value = "${millisUntilFinished / 1000}"
            }

            override fun onFinish() {
                time.value = App.context.getString(R.string.match_form_timer_end)
            }
        }.start()

    }
}