package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.LeaguesAdapter
import cz.nohejbalpraha.app.databinding.FragmentLeaguesBinding
import cz.nohejbalpraha.app.handler.LeagueClickHandler
import cz.nohejbalpraha.app.model.League
import cz.nohejbalpraha.app.viewModel.LeaguesViewModel
import cz.nohejbalpraha.app.viewModel.ViewModelStatus

/**
 * Fragment that shows leagues list
 */
class LeaguesFragment : Fragment(), LeagueClickHandler {
    private lateinit var viewModel: LeaguesViewModel
    private lateinit var adapter: LeaguesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity()).get(LeaguesViewModel::class.java)
        adapter = LeaguesAdapter(viewModel.leagues.value!!, this)

        viewModel.leagues.observe(this, adapter::updateData)

        viewModel.status.observe(this, { status ->
            if (status == ViewModelStatus.FAILURE) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, ErrorFragment(requireActivity().getString(R.string.app_error_description)))
                    .commit()
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding : FragmentLeaguesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_leagues, container, false)
        binding.adapter = adapter
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onClick(league: League) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, LeagueFragment(league))
            .commit()
    }
}