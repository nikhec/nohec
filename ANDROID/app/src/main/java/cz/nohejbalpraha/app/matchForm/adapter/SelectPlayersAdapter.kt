package cz.nohejbalpraha.app.matchForm.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.App
import cz.nohejbalpraha.app.databinding.PlayerInSelectorBinding
import cz.nohejbalpraha.app.model.Player

/**
 * RecyclerView adapter for player select in match form
 * @param title title of the selector
 * @param selectableCount how many players can be selected
 * @param selectedPlayers preselected players
 * @param players players from which can user pick
 * @param listener listener for player selection
 * @param flag optional flag
 */
class SelectPlayersAdapter(val title: String, private val selectableCount: Int, private val selectedPlayers: List<Player>, private val players: List<Player>, val listener: SelectPlayersListener, val flag: Int) : RecyclerView.Adapter<SelectPlayersAdapter.ViewHolder>() {
    val subtitle: String = App.context.getString(R.string.select_help, selectableCount)
    val selected: ArrayList<Int> = ArrayList()
    var error: MutableLiveData<Boolean> = MutableLiveData(false)
    val errorText: String = App.context.getString(R.string.select_error, selectableCount)

    init {
        for (player in selectedPlayers) {
            val index = players.indexOf(player)
            if (index >= 0) {
                selected.add(index)
            }
        }

        error.value = selected.size != selectableCount
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = PlayerInSelectorBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(players[position], position)

    override fun getItemCount(): Int = players.size

    inner class ViewHolder(val binding: PlayerInSelectorBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Player, position: Int) {
            binding.player = item
            binding.position = position
            binding.adapter = this@SelectPlayersAdapter
            binding.executePendingBindings()
        }
    }

    /**
     * Adds player to the selected list.
     * If only one player is selectable it removes any player that was selected before.
     * @param position player index
     */
    fun selectPlayer(position: Int) {
        if(selected.contains(position)) {
            selected.remove(position)
        } else {
            selected.add(position)

            if (selectableCount == 1 && selected.size > 1) {
                selected.removeAt(0)
            }
        }

        error.value = selected.size != selectableCount
        notifyDataSetChanged()
    }

    /**
     * Checks whether player is selected
     * @param position player index
     */
    fun isPlayerSelected(position: Int) : Boolean {
        return selected.contains(position)
    }

    /**
     * Called when user cancels the selection
     */
    fun cancelSelection() {
        listener.selectionCanceled()
    }

    /**
     * Called when user confirms his selection
     */
    fun confirmSelection() {
        if (selected.size == selectableCount) {
            val selectedPlayers = ArrayList<Player>()
            for (index in selected) {
                selectedPlayers.add(players[index])
            }

            listener.selectionConfirmed(selectedPlayers, flag)
        }
    }
}