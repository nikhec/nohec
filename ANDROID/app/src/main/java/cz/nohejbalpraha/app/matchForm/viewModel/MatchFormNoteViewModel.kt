package cz.nohejbalpraha.app.matchForm.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * ViewModel that hold data for note form
 */
class MatchFormNoteViewModel : ViewModel() {
    val note: MutableLiveData<String> by lazy {
        MutableLiveData("")
    }
}