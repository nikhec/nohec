package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.FutureMatchesAdapter
import cz.nohejbalpraha.app.adapter.LeagueStandingAdapter
import cz.nohejbalpraha.app.adapter.PastMatchesAdapter
import cz.nohejbalpraha.app.databinding.FragmentLeagueBinding
import cz.nohejbalpraha.app.handler.LeagueMatchesClickHandler
import cz.nohejbalpraha.app.handler.MatchClickHandler
import cz.nohejbalpraha.app.model.League
import cz.nohejbalpraha.app.model.Match
import cz.nohejbalpraha.app.viewModel.LeagueViewModel
import cz.nohejbalpraha.app.viewModel.LeagueViewModelFactory
import cz.nohejbalpraha.app.viewModel.ViewModelStatus

/**
 * Fragment that shows league detail
 * @param league league
 */
class LeagueFragment(private val league: League) : Fragment(), MatchClickHandler, LeagueMatchesClickHandler {
    private lateinit var viewModel: LeagueViewModel
    private lateinit var standingAdapter: LeagueStandingAdapter
    private lateinit var pastMatchesAdapter: PastMatchesAdapter
    private lateinit var futureMatchesAdapter: FutureMatchesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, LeagueViewModelFactory(league)).get(LeagueViewModel::class.java)
        standingAdapter = LeagueStandingAdapter(viewModel.leagueStanding.value!!)
        pastMatchesAdapter = PastMatchesAdapter(viewModel.pastMatches.value!!, this)
        futureMatchesAdapter = FutureMatchesAdapter(viewModel.futureMatches.value!!, this)

        viewModel.leagueStanding.observe(this, standingAdapter::updateData)

        viewModel.pastMatches.observe(this, pastMatchesAdapter::updateData)

        viewModel.futureMatches.observe(this, futureMatchesAdapter::updateData)

        viewModel.status.observe(this, { status ->
            if (status == ViewModelStatus.FAILURE) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, ErrorFragment(requireActivity().getString(R.string.app_error_description)))
                    .commit()
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding : FragmentLeagueBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_league, container, false)
        binding.leagueStandingAdapter = standingAdapter
        binding.pastMatchesAdapter = pastMatchesAdapter
        binding.futureMatchesAdapter = futureMatchesAdapter
        binding.viewModel = viewModel
        binding.listener = this
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onClick(match: Match) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, MatchDetailFragment(match))
            .addToBackStack(null)
            .commit()
    }

    override fun pastMatchesClick() {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, LeagueMatchesFragment(viewModel.league, LeagueMatchesType.PAST, this))
            .addToBackStack(null)
            .commit()
    }

    override fun futureMatchesClick() {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, LeagueMatchesFragment(viewModel.league, LeagueMatchesType.FUTURE, this))
            .addToBackStack(null)
            .commit()
    }
}