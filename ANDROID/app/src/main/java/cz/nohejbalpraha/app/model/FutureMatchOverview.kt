package cz.nohejbalpraha.app.model

import java.time.LocalDateTime;

data class FutureMatchOverview(val homeTeam: Team, val awayTeam: Team, val date: LocalDateTime)