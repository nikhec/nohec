package cz.nohejbalpraha.app.matchForm.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.databinding.MatchFormTimeBinding
import cz.nohejbalpraha.app.matchForm.handler.MatchFormTimeHandler
import cz.nohejbalpraha.app.matchForm.viewModel.TimerViewModel
import cz.nohejbalpraha.app.model.Team

/**
 * Fragment for match timer
 * @param team team that takes the time
 * @param handler timer handler
 */
class MatchTimeFragment(private val team: Team, private val handler: MatchFormTimeHandler) : Fragment() {
    private lateinit var viewModel: TimerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this).get(TimerViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: MatchFormTimeBinding = DataBindingUtil.inflate(inflater, R.layout.match_form_time, container, false)
        binding.team = team
        binding.handler = handler

        val timerTextView: TextView = binding.root.findViewById(R.id.timerText)
        viewModel.time.observe(viewLifecycleOwner, timerTextView::setText)

        return binding.root
    }
}