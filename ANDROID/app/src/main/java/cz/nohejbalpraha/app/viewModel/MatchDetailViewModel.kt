package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.model.Match
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * ViewModel that handles data for match detail page
 */
class MatchDetailViewModel(val matchOverview: Match) : ViewModel() {
    val match: MutableLiveData<Match> by lazy {
        MutableLiveData<Match>(matchOverview)
    }
    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData<ViewModelStatus>(ViewModelStatus.LOADING)
    }

    init {
        ApiClient.callAPI().getMatchDetail(id = matchOverview.id).enqueue(object : Callback<Match> {
            override fun onResponse(call: Call<Match>, response: Response<Match>) {
                if (response.code() == 200 && response.body() != null) {
                    match.value = response.body()
                    status.value = ViewModelStatus.SUCCESS
                }
            }

            override fun onFailure(call: Call<Match>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }

        })
    }
}