package cz.nohejbalpraha.app.model

class Game(val id: Int, val type: Int) {
    var home_players: List<Int> = ArrayList()
    var away_players: List<Int> = ArrayList()

    var sets: List<Set> = ArrayList()

    var type_label: String = ""

    /**
     * Returns the score for the game
     */
    fun getScore() : String {
        var homePoints = 0
        var awayPoints = 0

        for (set in sets) {
            if (set.home_points == 10) {
                homePoints++
            } else {
                awayPoints++
            }
        }

        return "$homePoints:$awayPoints"
    }
}