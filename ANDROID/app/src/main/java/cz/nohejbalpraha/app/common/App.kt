package cz.nohejbalpraha.app.common

import android.app.Activity
import android.app.Application
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.SHOW_FORCED
import androidx.fragment.app.Fragment

/**
 * Hides keyboard. Available from Fragments
 */
fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

/**
 * Opens keyboard. Available from Fragments
 */
fun Fragment.openKeyboard() {
    view?.let { activity?.openKeyboard() }
}

/**
 * Hides keyboard. Available from Activities
 */
fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

/**
 * Opens keyboard. Available from Activities
 */
fun Activity.openKeaboard() {
    openKeyboard()
}

/**
 * Hides keyboard. Available from Context
 */
fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

/**
 * Opens keyboard. Available from Context
 */
fun Context.openKeyboard() {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInput(SHOW_FORCED, 0)
}

/**
 * Helper class to get context outside of activity
 */
class App : Application() {
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }
}