package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.MatchesAdapter
import cz.nohejbalpraha.app.databinding.FragmentMatchesBinding
import cz.nohejbalpraha.app.handler.MatchClickHandler
import cz.nohejbalpraha.app.model.Match
import cz.nohejbalpraha.app.viewModel.MatchesViewModel
import cz.nohejbalpraha.app.viewModel.ViewModelStatus

class MatchesFragment : Fragment(), MatchClickHandler {
    private lateinit var viewModel: MatchesViewModel
    private lateinit var adapter: MatchesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity()).get(MatchesViewModel::class.java)
        adapter = MatchesAdapter(viewModel.matches.value!!, this, LeagueMatchesType.PAST)

        viewModel.matches.observe(this, adapter::updateData)

        viewModel.status.observe(this, { status ->
            if (status == ViewModelStatus.FAILURE) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, ErrorFragment(requireActivity().getString(R.string.app_error_description)))
                    .commit()
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : FragmentMatchesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_matches, container, false)
        binding.viewModel = viewModel
        binding.adapter = adapter
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onClick(match: Match) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, MatchDetailFragment(match))
            .addToBackStack(null)
            .commit()
    }
}