package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputEditText
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.ClubsAdapter
import cz.nohejbalpraha.app.common.hideKeyboard
import cz.nohejbalpraha.app.common.openKeyboard
import cz.nohejbalpraha.app.databinding.FragmentClubsBinding
import cz.nohejbalpraha.app.handler.ClubsClickHandler
import cz.nohejbalpraha.app.model.Club
import cz.nohejbalpraha.app.viewModel.ClubsViewModel
import cz.nohejbalpraha.app.viewModel.ViewModelStatus

/**
 * Fragment that shows clubs list
 */
class ClubsFragment : Fragment(), ClubsClickHandler {
    private lateinit var viewModel: ClubsViewModel
    private lateinit var adapter: ClubsAdapter
    private lateinit var searchInput: TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity()).get(ClubsViewModel::class.java)
        adapter = ClubsAdapter(viewModel.clubs.value!!, this)

        viewModel.clubs.observe(this, { clubs ->
            adapter.updateData(clubs)
        })

        viewModel.searchKey.observe(this, { key ->
            adapter.filter(key)
        })

        viewModel.status.observe(this, { status ->
            if (status == ViewModelStatus.FAILURE) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, ErrorFragment(requireActivity().getString(R.string.app_error_description)))
                    .commit()
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentClubsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_clubs, container, false)
        binding.viewModel = viewModel
        binding.adapter = adapter
        binding.clickHandler = this
        binding.lifecycleOwner = this
        searchInput = binding.root.findViewById<TextInputEditText>(R.id.search_input)

        return binding.root
    }

    override fun onClubClick(club: Club) {
        hideKeyboard()
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, ClubFragment(club))
            .addToBackStack(null)
            .commit()
    }

    override fun onSearchClick() {
        if (viewModel.searching.value!!) {
            hideKeyboard()
            viewModel.searchKey.postValue("")
        } else {
            searchInput.requestFocus()
            openKeyboard()
        }
        viewModel.searching.postValue(!viewModel.searching.value!!)
    }
}