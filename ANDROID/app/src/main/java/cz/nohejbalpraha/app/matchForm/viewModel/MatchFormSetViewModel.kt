package cz.nohejbalpraha.app.matchForm.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.App
import cz.nohejbalpraha.app.model.GameType
import cz.nohejbalpraha.app.model.Set
import cz.nohejbalpraha.app.model.Substitution

/**
 * ViewModel that holds data for set match form
 * @param matchID match id
 * @param game current game
 * @param setNumber 1st or 2nd set
 */
class MatchFormSetViewModel(private val matchID: Int, private val game: Int, private val setNumber: Int, private val setType: Int) : ViewModel() {
    val set : MutableLiveData<Set> by lazy {
        MutableLiveData(Set(setNumber))
    }
    val homePointsUp : MutableLiveData<Boolean> by lazy {
        MutableLiveData(true)
    }
    val homePointsDown : MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val awayPointsUp : MutableLiveData<Boolean> by lazy {
        MutableLiveData(true)
    }
    val awayPointsDown : MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val homeSubstitution : MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val homeSubstitutionLabel : MutableLiveData<String> by lazy {
        MutableLiveData("")
    }
    val awaySubstitution : MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val awaySubstitutionLabel : MutableLiveData<String> by lazy {
        MutableLiveData("")
    }

    init {
        if(setType == GameType.SINGLE.type) {
            homeSubstitution.value = true
            awaySubstitution.value = true
        }
    }

    /**
     * Adds 1 point to home team
     */
    fun homeTeamPointsUp() {
        set.value!!.home_points++
        set.postValue(set.value)

        if (set.value!!.home_points >= 10) {
            set.value!!.home_points = 10
            homePointsUp.value = false
        }

        homePointsDown.value = true
    }

    /**
     * Removes 1 point from home team
     */
    fun homeTeamPointsDown() {
        set.value!!.home_points--
        set.postValue(set.value)

        if (set.value!!.home_points <= 0) {
            set.value!!.home_points = 0
            homePointsDown.value = false
        }

        homePointsUp.value = true
    }

    /**
     * Adds 1 point to away team
     */
    fun awayTeamPointsUp() {
        set.value!!.away_points++
        set.postValue(set.value)

        if (set.value!!.away_points >= 10) {
            set.value!!.away_points = 10
            awayPointsUp.value = false
        }

        awayPointsDown.value = true
    }

    /**
     * Removes 1 point from away team
     */
    fun awayTeamPointsDown() {
        set.value!!.away_points--
        set.postValue(set.value)

        if (set.value!!.away_points <= 0) {
            set.value!!.away_points = 0
            awayPointsDown.value = false
        }

        awayPointsDown.value = true
    }

    /**
     * Adds a substitution to the set
     * @param substitution substituted players
     * @param teamType HOME / AWAY team
     */
    fun addSubstitution(substitution: Substitution, teamType: TeamType) {
        when(teamType) {
            TeamType.HOME -> {
                (set.value!!.home_substitutions as ArrayList).add(substitution)
                homeSubstitution.value = true
                homeSubstitutionLabel.value = App.context.getString(R.string.match_form_substitution_label)
            }
            TeamType.AWAY -> {
                (set.value!!.away_substitutions as ArrayList).add(substitution)
                awaySubstitution.value = true
                awaySubstitutionLabel.value = App.context.getString(R.string.match_form_substitution_label)
            }
        }
        set.postValue(set.value)
    }

    /**
     * Sets time for given team
     * @param teamType HOME / AWAY team
     */
    fun startTime(teamType: TeamType) {
        when(teamType) {
            TeamType.HOME -> set.value!!.home_timeout = 1
            TeamType.AWAY -> set.value!!.away_timeout = 1
        }

        set.postValue(set.value)
    }
}