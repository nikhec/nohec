package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.model.League
import cz.nohejbalpraha.app.model.Match
import cz.nohejbalpraha.app.model.MatchRoundList
import cz.nohejbalpraha.app.model.TeamLeagueStanding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * ViewModel that handles data for league detail page
 */
class LeagueViewModel(val league: League) : ViewModel() {
    private final val show_matches = 3

    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData<ViewModelStatus>(ViewModelStatus.LOADING)
    }
    val leagueStanding: MutableLiveData<List<TeamLeagueStanding>> by lazy {
        MutableLiveData<List<TeamLeagueStanding>>(ArrayList())
    }
    val pastMatches: MutableLiveData<List<Match>> by lazy {
        MutableLiveData<List<Match>>(ArrayList())
    }
    val futureMatches: MutableLiveData<List<Match>> by lazy {
        MutableLiveData<List<Match>>(ArrayList())
    }

    init {
        loadStandings()
        loadPastMatches()
        loadFutureMatches()
    }

    private fun loadStandings() {
        ApiClient.callAPI().getStandings(league.id).enqueue(object : Callback<List<TeamLeagueStanding>> {
            override fun onResponse(
                call: Call<List<TeamLeagueStanding>>,
                response: Response<List<TeamLeagueStanding>>
            ) {
                if (response.code() == 200 && response.body() != null) {
                    leagueStanding.value = response.body()!!
                    status.value = ViewModelStatus.SUCCESS
                } else {
                    status.value = ViewModelStatus.FAILURE
                }
            }

            override fun onFailure(call: Call<List<TeamLeagueStanding>>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }

        })
    }

    private fun loadPastMatches() {
        ApiClient.callAPI().getPastLeagueMatches(leagueID = league.id, count = show_matches).enqueue(object : Callback<List<MatchRoundList>> {
            override fun onResponse(call: Call<List<MatchRoundList>>, response: Response<List<MatchRoundList>>) {
                if (response.code() == 200 && response.body() != null) {
                    val matches = ArrayList<Match>()

                    for (matchList in response.body()!!) {
                        for (match in matchList.matches) {
                            matches.add(match)
                        }
                    }

                    pastMatches.value = matches
                }
            }

            override fun onFailure(call: Call<List<MatchRoundList>>, t: Throwable) {
                // empty
            }

        })
    }

    private fun loadFutureMatches() {
        ApiClient.callAPI().getFutureLeagueMatches(leagueID = league.id, count = show_matches).enqueue(object : Callback<List<MatchRoundList>> {
            override fun onResponse(
                call: Call<List<MatchRoundList>>,
                response: Response<List<MatchRoundList>>
            ) {
                if (response.code() == 200 && response.body() != null) {
                    val matches = ArrayList<Match>()

                    for (matchList in response.body()!!) {
                        for (match in matchList.matches) {
                            matches.add(match)
                        }
                    }

                    futureMatches.value = matches
                }
            }

            override fun onFailure(call: Call<List<MatchRoundList>>, t: Throwable) {
                // empty
            }
        })
    }
}