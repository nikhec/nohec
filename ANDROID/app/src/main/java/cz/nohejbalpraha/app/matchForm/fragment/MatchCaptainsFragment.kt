package cz.nohejbalpraha.app.matchForm.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.databinding.MatchFormCaptainsBinding
import cz.nohejbalpraha.app.matchForm.MatchFormLayoutManager
import cz.nohejbalpraha.app.matchForm.adapter.SelectPlayersListener
import cz.nohejbalpraha.app.matchForm.handler.MatchFormFlowHandler
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModel
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModelFactory
import cz.nohejbalpraha.app.matchForm.viewModel.TeamType
import cz.nohejbalpraha.app.model.Player

/**
 * Fragment for captain select screen
 * @param matchID match id
 * @param layoutManager layout manager
 */
class MatchCaptainsFragment(private val matchID: Int, private val layoutManager: MatchFormLayoutManager) : Fragment(), SelectPlayersListener, MatchFormFlowHandler {
    private lateinit var viewModel: MatchFormViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(), MatchFormViewModelFactory(matchID)).get(
            MatchFormViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: MatchFormCaptainsBinding = DataBindingUtil.inflate(inflater, R.layout.match_form_captains, container, false)
        binding.viewModel = viewModel
        binding.listener = this
        binding.handler = this
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun openSelection(teamType: TeamType) {
        layoutManager.openLayout(PlayersSelectFragment(matchID, this, teamType, PlayerSelectType.CAPTAIN), true)
    }

    override fun selectionCanceled() {
        activity?.onBackPressed()
    }

    override fun selectionConfirmed(players: List<Player>, flag: Int) {
        when(flag) {
            0 -> viewModel.selectCaptain(players[0], TeamType.HOME)
            1 -> viewModel.selectCaptain(players[0], TeamType.AWAY)
        }

        activity?.onBackPressed()
    }

    override fun next() {
        viewModel.recordCaptains()
    }

    override fun nextLabel(): String {
        return getString(R.string.match_continue_button)
    }

    override fun leaveMatch() {
        AlertDialog.Builder(activity!!)
            .setMessage(activity?.getString(R.string.match_form_leave_match))
            .setPositiveButton(activity?.getString(R.string.match_form_leave_yes), object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    viewModel.leaveMatch()
                }
            })
            .setNegativeButton(activity?.getString(R.string.match_form_leave_no), null)
            .show()
    }
}