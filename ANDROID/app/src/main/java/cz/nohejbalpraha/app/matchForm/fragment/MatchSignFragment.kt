package cz.nohejbalpraha.app.matchForm.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.mukesh.OtpView
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.databinding.MatchFormSignBinding
import cz.nohejbalpraha.app.matchForm.MatchFormLayoutManager
import cz.nohejbalpraha.app.matchForm.handler.MatchFormFlowHandler
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModel
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModelFactory

/**
 * Fragment for match form signing
 * @param matchID match id
 * @param layoutManager layout manager
 */
class MatchSignFragment(private val matchID: Int, private val layoutManager: MatchFormLayoutManager) : Fragment(), MatchFormFlowHandler {
    private val SIGN_LENGTH = 4
    lateinit var viewModel: MatchFormViewModel
    private var refereeSign = ""
    private var homeCaptainSign = ""
    private var awayCaptainSign = ""

    lateinit var refereePin: OtpView
    lateinit var homeCaptainPin: OtpView
    lateinit var awayCaptainPin: OtpView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(), MatchFormViewModelFactory(matchID)).get(
            MatchFormViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: MatchFormSignBinding = DataBindingUtil.inflate(inflater, R.layout.match_form_sign, container, false)
        binding.viewModel = viewModel
        binding.flowHandler = this
        binding.lifecycleOwner = this

        homeCaptainPin = binding.root.findViewById(R.id.homeCaptainSign)
        homeCaptainPin.setText(viewModel.homeCaptainSign)
        homeCaptainPin.setOtpCompletionListener {
            homeCaptainSign = it
            awayCaptainPin.requestFocus()
        }
        homeCaptainPin.requestFocus()
        awayCaptainPin = binding.root.findViewById(R.id.awayCaptainSign)
        awayCaptainPin.setText(viewModel.awayCaptainSign)
        awayCaptainPin.setOtpCompletionListener {
            awayCaptainSign = it
            refereePin.requestFocus()
        }
        refereePin = binding.root.findViewById(R.id.refereeSign)
        refereePin.setText(viewModel.refereeSign)
        refereePin.setOtpCompletionListener {
            refereeSign = it
            homeCaptainPin.requestFocus()
        }

        return binding.root
    }

    override fun next() {
        if (homeCaptainSign.length == SIGN_LENGTH && awayCaptainSign.length == SIGN_LENGTH) {
            viewModel.recordSign(refereeSign, homeCaptainSign, awayCaptainSign)
        }
    }

    override fun nextLabel(): String {
        return activity?.getString(R.string.match_form_sign) ?: ""
    }

    override fun leaveMatch() {
        AlertDialog.Builder(activity!!)
            .setMessage(activity?.getString(R.string.match_form_leave_match))
            .setPositiveButton(activity?.getString(R.string.match_form_leave_yes), object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    viewModel.leaveMatch()
                }
            })
            .setNegativeButton(activity?.getString(R.string.match_form_leave_no), null)
            .show()
    }
}