package cz.nohejbalpraha.app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import cz.nohejbalpraha.app.common.AccountManager
import cz.nohejbalpraha.app.fragment.*

class MainActivity : AppCompatActivity() {
    private val bottomNavigationListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId) {
            R.id.page_me -> {
                if (AccountManager.isLoggedIn()) {
                    openFragment(MyMatchesFragment())
                } else {
                    openFragment(LoginFragment())
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.page_matches -> {
                openFragment(MatchesFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.page_leagues -> {
                openFragment(LeaguesFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.page_clubs -> {
                openFragment(ClubsFragment())
                return@OnNavigationItemSelectedListener true
            }
        }

        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNavigation: BottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigation.setOnNavigationItemSelectedListener(bottomNavigationListener)

        bottomNavigation.selectedItemId = R.id.page_matches
    }

    private fun openFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }
}