package cz.nohejbalpraha.app.handler

interface UserClickHandler {
    /**
     * Should open user profile
     */
    fun onClick()
}