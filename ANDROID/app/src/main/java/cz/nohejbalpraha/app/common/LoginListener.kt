package cz.nohejbalpraha.app.common

interface LoginListener {
    /**
     * Called when user is sign in
     */
    fun login()

    /**
     * Called when user is logged out
     */
    fun logout()
}