package cz.nohejbalpraha.app.model

class Club(val id: Int, val name: String, val logo: String) {
    var detail: ClubDetail = ClubDetail(ArrayList())
}

data class ClubDetail(val teams: List<Team>)