package cz.nohejbalpraha.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.databinding.MatchSetOverviewBinding
import cz.nohejbalpraha.app.model.Set

/**
 * RecyclerView adapter for sets in game detail
 * @param items sets
 */
class SetsAdapter(var items: List<Set>) : RecyclerView.Adapter<SetsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = MatchSetOverviewBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val binding: MatchSetOverviewBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Set) {
            binding.set = item
            binding.executePendingBindings()
        }
    }
}