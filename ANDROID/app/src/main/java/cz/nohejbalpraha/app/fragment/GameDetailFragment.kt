package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.AwayPlayersAdapter
import cz.nohejbalpraha.app.adapter.HomePlayersAdapter
import cz.nohejbalpraha.app.adapter.SetsAdapter
import cz.nohejbalpraha.app.databinding.FragmentGameBinding
import cz.nohejbalpraha.app.model.Game
import cz.nohejbalpraha.app.model.Match
import cz.nohejbalpraha.app.model.Player
import cz.nohejbalpraha.app.model.Substitution

/**
 * Fragment that shows game detail
 * @param game game
 * @param gameName name of the game
 * @param match match that the game is from
 */
class GameDetailFragment(val game: Game, val gameName: String, val match: Match) : Fragment() {
    private lateinit var homePlayersAdapter: HomePlayersAdapter
    private lateinit var awayPlayersAdapter: AwayPlayersAdapter
    private lateinit var setsAdapter: SetsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val playerMap: HashMap<Int, Player> = HashMap()
        for (player in match.home_players) {
            playerMap.set(player.id, player)
        }
        for (player in match.away_players) {
            playerMap.set(player.id, player)
        }

        val homeSubstitutions = ArrayList<Substitution>()
        val awaySubstitutions = ArrayList<Substitution>()
        val homePlayers = ArrayList<Player>()
        val awayPlayers = ArrayList<Player>()
        for (player in game.home_players) {
            if (playerMap.containsKey(player)) {
                homePlayers.add(playerMap.get(player)!!)
            }
        }
        for (player in game.away_players) {
            if (playerMap.containsKey(player)) {
                awayPlayers.add(playerMap.get(player)!!)
            }
        }

        for (set in game.sets) {
            homeSubstitutions.addAll(set.home_substitutions)
            for (sub in set.home_substitutions) {
                if (playerMap.containsKey(sub.incoming)) {
                    homePlayers.add(playerMap.get(sub.incoming)!!)
                }
            }

            awaySubstitutions.addAll(set.away_substitutions)
            for (sub in set.away_substitutions) {
                if (playerMap.containsKey(sub.incoming)) {
                    awayPlayers.add(playerMap.get(sub.incoming)!!)
                }
            }
        }

        homePlayersAdapter = HomePlayersAdapter(homePlayers, homeSubstitutions)
        awayPlayersAdapter = AwayPlayersAdapter(awayPlayers, awaySubstitutions)
        setsAdapter = SetsAdapter(game.sets)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentGameBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_game, container, false)
        binding.game = game
        binding.gameName = gameName
        binding.match = match
        binding.homePlayersAdapter = homePlayersAdapter
        binding.awayPlayersAdapter = awayPlayersAdapter
        binding.setsAdapter = setsAdapter
        binding.executePendingBindings()
        binding.lifecycleOwner = this

        return binding.root
    }
}