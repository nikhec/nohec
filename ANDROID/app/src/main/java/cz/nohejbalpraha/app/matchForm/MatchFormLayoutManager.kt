package cz.nohejbalpraha.app.matchForm

import androidx.fragment.app.Fragment

interface MatchFormLayoutManager {
    /**
     * shows given fragment on screen
     * @param fragment fragment that should be shown
     * @param fullscreen fullscreen or with header
     */
    fun openLayout(fragment: Fragment, fullscreen: Boolean)
}