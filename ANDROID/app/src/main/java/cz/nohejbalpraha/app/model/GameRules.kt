package cz.nohejbalpraha.app.model

enum class GameRules {
    TWO_SETS,
    THREE_SETS
}