package cz.nohejbalpraha.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.databinding.ClubTeamBinding
import cz.nohejbalpraha.app.model.Team

/**
 * RecyclerView adapter for teams in a club detail
 * @param items teams
 */
class TeamsAdapter(private var items: List<Team>) : RecyclerView.Adapter<TeamsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ClubTeamBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    /**
     * Updates and refreshes the data in the view
     */
    fun updateData(items: List<Team>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: ClubTeamBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Team) {
            binding.team = item
            binding.executePendingBindings()
        }
    }
}