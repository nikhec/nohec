package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.model.Club
import cz.nohejbalpraha.app.model.ClubDetail
import cz.nohejbalpraha.app.model.GameCourt
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * ViewModel that handles data for club detail page
 * @param club club
 */
class ClubViewModel(val club: Club) : ViewModel() {
    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData(ViewModelStatus.LOADING)
    }
    val clubDetail: MutableLiveData<ClubDetail> by lazy {
        MutableLiveData<ClubDetail>(ClubDetail(ArrayList()))
    }
    val courts: MutableLiveData<List<GameCourt>> by lazy {
        MutableLiveData(ArrayList())
    }
    val address: MutableLiveData<String> by lazy {
        MutableLiveData("")
    }

    init {
        ApiClient.callAPI().getClub(club.id).enqueue(object : Callback<ClubDetail> {
            override fun onResponse(call: Call<ClubDetail>, response: Response<ClubDetail>) {
                if (response.code() == 200 && response.body() != null) {
                    filterUniqueCourts(response.body()!!)
                    clubDetail.value = response.body()
                    status.value = ViewModelStatus.SUCCESS
                } else {
                    status.value = ViewModelStatus.FAILURE
                }
            }

            override fun onFailure(call: Call<ClubDetail>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }
        })
    }

    private fun filterUniqueCourts(clubDetail: ClubDetail) {
        val courts = ArrayList<GameCourt>()
        var address = ""
        for (team in clubDetail.teams) {
            if (!courts.contains(team.court)) {
                courts.add(team.court)
                address += team.court.address + "\n"
            }
        }

        this.courts.value = courts
        this.address.value = address
    }
}