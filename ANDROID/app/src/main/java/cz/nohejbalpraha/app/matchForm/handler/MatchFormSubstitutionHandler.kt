package cz.nohejbalpraha.app.matchForm.handler

import cz.nohejbalpraha.app.matchForm.viewModel.TeamType
import cz.nohejbalpraha.app.model.Substitution

interface MatchFormSubstitutionHandler {
    /**
     * Should open substitution
     * @param teamType HOME / AWAY team
     */
    fun openSubstitution(teamType: TeamType)

    /**
     * Called when user cancels the substitution
     */
    fun substitutionCanceled()

    /**
     * Called when user confirms the substitution
     * @param substitution substituted players
     * @param teamType HOME / AWAY team
     */
    fun substitutionConfirmed(substitution: Substitution, teamType: TeamType)
}