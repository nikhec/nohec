package cz.nohejbalpraha.app.adapter

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, url: String?) {
    if (!url.isNullOrEmpty()) {
        Glide.with(view)
            .load("http://lab.d3s.mff.cuni.cz:8664$url")
            .centerCrop()
            .into(view)
    }
}

@BindingAdapter("adapter")
fun setRecyclerViewAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>?) {
    view.adapter = adapter
}

class BindingAdapter