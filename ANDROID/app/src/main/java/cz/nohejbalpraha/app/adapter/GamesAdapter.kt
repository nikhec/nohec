package cz.nohejbalpraha.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.App
import cz.nohejbalpraha.app.databinding.MatchGameOverviewBinding
import cz.nohejbalpraha.app.handler.GameClickHandler
import cz.nohejbalpraha.app.model.Game
import cz.nohejbalpraha.app.model.GameType

/**
 * RecyclerView adapter for games in a match
 * @param items games
 * @param listener click listener
 */
class GamesAdapter(var items: List<Game>, private val listener: GameClickHandler) : RecyclerView.Adapter<GamesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = MatchGameOverviewBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    /**
     * Updates and refreshes the data in the view
     */
    fun updateData(items: List<Game>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: MatchGameOverviewBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Game) {
            binding.game = item
            binding.listener = listener
            binding.gameName = getGameName(item)
            binding.executePendingBindings()
        }
    }

    private fun getGameName(item: Game) : String {
        val gameType = item.type
        val position = items.indexOf(item)
        var num = 1

        if (position != 0) {
            for (i in 0..(position - 1)) {
                if (items[i].type == gameType) {
                    num++;
                }
            }
        }

        var gameName = ""
        when(gameType) {
            GameType.SINGLE.type -> gameName = App.context.getString(R.string.game_type_single)
            GameType.DOUBLE.type -> gameName = App.context.getString(R.string.game_type_double)
            GameType.TRIPLE.type -> gameName = App.context.getString(R.string.game_type_triple)
        }

        return "$num. $gameName"
    }
}