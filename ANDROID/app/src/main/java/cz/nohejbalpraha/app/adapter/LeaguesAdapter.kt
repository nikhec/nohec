package cz.nohejbalpraha.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.databinding.LeaguesLeagueBinding
import cz.nohejbalpraha.app.handler.LeagueClickHandler
import cz.nohejbalpraha.app.model.League

/**
 * RecyclerView adapter for leagues in league list
 * @param items leagues
 * @param clickListener click handler
 */
class LeaguesAdapter(private var items: List<League>, private val clickListener: LeagueClickHandler) : RecyclerView.Adapter<LeaguesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LeaguesLeagueBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    /**
     * Updates and refreshes the data in the view
     */
    fun updateData(items: List<League>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: LeaguesLeagueBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: League) {
            binding.league = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }
}