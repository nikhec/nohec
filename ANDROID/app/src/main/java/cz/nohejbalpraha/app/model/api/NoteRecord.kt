package cz.nohejbalpraha.app.model.api

data class NoteRecord(val note: String)