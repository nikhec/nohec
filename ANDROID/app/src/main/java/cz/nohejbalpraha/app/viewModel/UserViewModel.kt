package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.common.AccountManager
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.model.Signature
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserViewModel : ViewModel() {
    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData(ViewModelStatus.LOADING)
    }
    val signature: MutableLiveData<String> by lazy {
        MutableLiveData("")
    }

    init {
        ApiClient.callAPI().getUserSignature().enqueue(object : Callback<Signature> {
            override fun onResponse(call: Call<Signature>, response: Response<Signature>) {
                when(response.code()) {
                    200 -> {
                        signature.value = response.body()?.signature
                        status.value = ViewModelStatus.SUCCESS
                    }
                    else -> {
                        status.value = ViewModelStatus.FAILURE
                    }
                }
            }

            override fun onFailure(call: Call<Signature>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }

        })
    }
}