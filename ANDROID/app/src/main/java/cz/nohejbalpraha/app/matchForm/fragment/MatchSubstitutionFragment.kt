package cz.nohejbalpraha.app.matchForm.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.App
import cz.nohejbalpraha.app.databinding.MatchFormSubstitutionBinding
import cz.nohejbalpraha.app.matchForm.MatchFormLayoutManager
import cz.nohejbalpraha.app.matchForm.adapter.SelectPlayersListener
import cz.nohejbalpraha.app.matchForm.handler.MatchFormSubstitutionHandler
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormSubstitutionViewModel
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModel
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModelFactory
import cz.nohejbalpraha.app.matchForm.viewModel.TeamType
import cz.nohejbalpraha.app.model.Player
import cz.nohejbalpraha.app.model.Substitution
import cz.nohejbalpraha.app.model.Team

/**
 * Fragment for displaying substitution for game set
 * @param matchID match id
 * @param team HOME / AWAY team
 * @param layoutManager layout manager
 */
class MatchSubstitutionFragment(private val matchID: Int, private val team: TeamType, private val handler: MatchFormSubstitutionHandler, private val layoutManager: MatchFormLayoutManager) : Fragment(),
    SelectPlayersListener{
    private lateinit var viewModel: MatchFormViewModel
    private lateinit var substitutionViewModel: MatchFormSubstitutionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(), MatchFormViewModelFactory(matchID)).get(
            MatchFormViewModel::class.java)

        substitutionViewModel = ViewModelProvider(this).get(MatchFormSubstitutionViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: MatchFormSubstitutionBinding = DataBindingUtil.inflate(inflater, R.layout.match_form_substitution, container, false)
        binding.title = App.context.getString(R.string.match_form_substitution_title, if(team == TeamType.HOME) viewModel.match.value!!.home_team.name else viewModel.match.value!!.away_team.name)
        binding.viewModel = substitutionViewModel
        binding.listener = this
        binding.substitutionFragment = this
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun openSelection(teamType: TeamType) {
        layoutManager.openLayout(PlayersSelectFragment(matchID, this, team, if (teamType == TeamType.HOME) PlayerSelectType.SUBSTITUTION_OFF else PlayerSelectType.SUBSTITUTION_ON), true)
    }

    override fun selectionCanceled() {
        activity?.onBackPressed()
    }

    override fun selectionConfirmed(players: List<Player>, flag: Int) {
        when(flag) {
            0 -> substitutionViewModel.substitutePlayerOff(players[0])
            1 -> substitutionViewModel.substitutePlayerOn(players[0])
        }
        activity?.onBackPressed()
    }

    /**
     * Called when user cancels the substitution
     */
    fun cancelSubstitution() {
        handler.substitutionCanceled()
    }

    /**
     * Called when user confirms the substitution
     */
    fun confirmSubstitution() {
        handler.substitutionConfirmed(substitutionViewModel.substitution.value!!, team)
    }
}