package cz.nohejbalpraha.app.common

import android.content.Context
import android.content.SharedPreferences
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.model.UserSession

/**
 * Singleton object that holds information about user login
 */
object AccountManager {
    private val sharedPref: SharedPreferences
    private var session: UserSession? = null
    private var loginListeners: ArrayList<LoginListener>

    init {
        sharedPref = App.context.getSharedPreferences(App.context.getString(R.string.shared_preferences) ,Context.MODE_PRIVATE)

        val userId = sharedPref.getInt(App.context.getString(R.string.user_id_key), 0)
        if (userId != 0) {
            val userEmail = sharedPref.getString(App.context.getString(R.string.user_email_key), "")
            val userToken = sharedPref.getString(App.context.getString(R.string.user_token_key), "")
            session = UserSession(userToken!!, userId, userEmail!!)
        }

        loginListeners = ArrayList()
    }

    /**
     * Logs user in
     * @param userSession object with user details
     */
    fun login(userSession: UserSession) {
        sharedPref
        with(sharedPref.edit()) {
            putInt(App.context.getString(R.string.user_id_key), userSession.user_id)
            putString(App.context.getString(R.string.user_email_key), userSession.email)
            putString(App.context.getString(R.string.user_token_key), userSession.token)
            apply()
        }

        session = userSession

        for (listener in loginListeners) {
            listener.login()
        }
    }

    /**
     * Logs user out
     */
    fun logout() {
        sharedPref
        with(sharedPref.edit()) {
            putInt(App.context.getString(R.string.user_id_key), 0)
            putString(App.context.getString(R.string.user_email_key), "")
            putString(App.context.getString(R.string.user_token_key), "")
            apply()
        }

        session = null

        for (listener in loginListeners) {
            listener.logout()
        }
    }

    /**
     * Returns whether user is logged in
     */
    fun isLoggedIn() : Boolean {
        return session != null
    }

    /**
     * If logged in returns user API token otherwise returns empty string
     */
    fun getToken() : String {
        if (session != null) {
            return session!!.token
        }

        return ""
    }

    /**
     * Returns user session, null if user not logged in
     */
    fun getUserSession() : UserSession? {
        return session
    }

    /**
     * Adds login listener that will be notify on logged in and logged out
     */
    fun addListener(listener: LoginListener) {
        if (!loginListeners.contains(listener)) {
            loginListeners.add(listener)
        }
    }

    /**
     * Removes login listener that will be no longer notified
     */
    fun removeListener(listener: LoginListener) {
        loginListeners.remove(listener)
    }
}