package cz.nohejbalpraha.app.matchForm.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.databinding.MatchFormErrorBinding
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModel
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModelFactory

/**
 * Fragment that shows errors in the match form
 */
class MatchErrorFragment(private val matchID: Int) : Fragment() {
    private lateinit var viewModel: MatchFormViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(), MatchFormViewModelFactory(matchID)).get(
            MatchFormViewModel::class.java
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: MatchFormErrorBinding = DataBindingUtil.inflate(inflater, R.layout.match_form_error, container, false)
        binding.viewModel = viewModel

        return binding.root
    }
}