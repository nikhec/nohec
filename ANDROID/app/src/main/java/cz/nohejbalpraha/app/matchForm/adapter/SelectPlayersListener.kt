package cz.nohejbalpraha.app.matchForm.adapter

import cz.nohejbalpraha.app.matchForm.viewModel.TeamType
import cz.nohejbalpraha.app.model.Player

interface SelectPlayersListener {
    /**
     * Should open player selection
     * @param teamType HOME / AWAY team
     */
    fun openSelection(teamType: TeamType)

    /**
     * Called when selection is canceled
     */
    fun selectionCanceled()

    /**
     * Called when selection is confirmed
     * @param players selected players
     * @param flag optional flag
     */
    fun selectionConfirmed(players: List<Player>, flag: Int)
}