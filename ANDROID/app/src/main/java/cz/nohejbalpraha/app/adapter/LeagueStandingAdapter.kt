package cz.nohejbalpraha.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.databinding.LeagueTeamStandingBinding
import cz.nohejbalpraha.app.model.TeamLeagueStanding

/**
 * RecyclerView adapter for league standing in league detail
 * @param items team standings
 */
class LeagueStandingAdapter(private var items: List<TeamLeagueStanding>) : RecyclerView.Adapter<LeagueStandingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LeagueTeamStandingBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    /**
     * Updates and refreshes the data in the view
     */
    fun updateData(items: List<TeamLeagueStanding>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: LeagueTeamStandingBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TeamLeagueStanding) {
            binding.teamStanding = item
            binding.executePendingBindings()
        }
    }
}