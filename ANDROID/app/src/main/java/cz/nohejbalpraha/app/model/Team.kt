package cz.nohejbalpraha.app.model

class Team(val id: Int, val name: String, val logo: String) {
    var players: List<Player> = ArrayList()
    var league: League = League(1, "", "", 2020)

    var court: GameCourt = GameCourt()
}