package cz.nohejbalpraha.app.model.api

data class CaptainsRecord(val home: Int, val away: Int)