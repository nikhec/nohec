package cz.nohejbalpraha.app.matchForm.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.model.Game
import cz.nohejbalpraha.app.model.Team

/**
 * ViewModelFactory for MatchFormViewModel
 */
class MatchFormViewModelFactory(private val matchID: Int) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MatchFormViewModel(matchID = matchID) as T
    }
}

/**
 * ViewModelFactory for MatchFormGameViewModel
 */
class MatchFormGameViewModelFactory(private val homeTeam: Team, private val awayTeam: Team, private val game: Game) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MatchFormGameViewModel(homeTeam, awayTeam, game) as T
    }
}

/**
 * ViewModelFactory for MatchFormSetViewModel
 */
class MatchFormSetViewModelFactory(private val matchID: Int, private val game: Int, private val setNumber: Int, private val setType: Int) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MatchFormSetViewModel(matchID, game, setNumber, setType) as T
    }
}