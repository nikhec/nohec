package cz.nohejbalpraha.app.common

import cz.nohejbalpraha.app.model.*
import cz.nohejbalpraha.app.model.api.*
import retrofit2.Call
import retrofit2.http.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter

interface APIInterface {
    @POST("users/login-token/")
    fun login(@Body userLogin: UserCredentials) : Call<UserSession>

    @POST("users/logout/")
    fun logout() : Call<Void>

    @GET("matches-by-date/past/")
    fun getPastMatches(@Query("count") count: Int) : Call<List<MatchDateList>>

    @GET("leagues/?latest_season=true")
    fun getLeagues() : Call<List<League>>

    @GET("leagues/{id}/matches-by-round/past/?latest_season=true")
    fun getPastLeagueMatches(@Path("id") leagueID: Int, @Query("count") count: Int) : Call<List<MatchRoundList>>

    @GET("leagues/{id}/matches-by-round/upcoming/?latest_season=true")
    fun getFutureLeagueMatches(@Path("id") leagueID: Int, @Query("count") count: Int) : Call<List<MatchRoundList>>

    @GET("leagues/{id}/standings/")
    fun getStandings(@Path("id") id: Int) : Call<List<TeamLeagueStanding>>

    @GET("clubs/")
    fun getClubs() : Call<List<Club>>

    @GET("clubs/{id}/detail/?latest_season=true")
    fun getClub(@Path("id") id: Int) : Call<ClubDetail>

    @GET("matches/{id}/detail/")
    fun getMatchDetail(@Path("id") id: Int) : Call<Match>

    @GET("my-matches-by-date/upcoming/")
    fun getMyFutureMatches() : Call<List<MatchDateList>>

    @GET("my-matches-by-date/past/")
    fun getMyPastMatches() : Call<List<MatchDateList>>

    @GET("leagues/{id}/matches-by-round/past/")
    fun getLeaguePastMatches(@Path("id") leagueID: Int) : Call<List<MatchRoundList>>

    @GET("leagues/{id}/matches-by-round/upcoming/")
    fun getLeagueFutureMatches(@Path("id") leagueID: Int) : Call<List<MatchRoundList>>

    @POST("matches/{id}/enter/")
    fun enterMatch(@Path("id") matchID: Int) : Call<Match>

    @POST("matches/{id}/captains/")
    fun recordCaptains(@Path("id") matchID: Int, @Body captains: CaptainsRecord) : Call<Void>

    @POST("matches/{id}/game/")
    fun recordGameSquad(@Path("id") matchID: Int, @Body squad: GameSquadRecord) : Call<Void>

    @POST("matches/{id}/set/")
    fun recordSet(@Path("id") matchID: Int, @Body set: SetRecord) : Call<Void>

    @POST("matches/{id}/note/")
    fun recordNote(@Path("id") matchID: Int, @Body note: NoteRecord) : Call<Void>

    @POST("matches/{id}/sign/")
    fun recordSign(@Path("id") matchID: Int, @Body signing: SignRecord) : Call<Void>

    @POST("matches/{id}/exit/")
    fun exitMatch(@Path("id") matchID: Int) : Call<Void>

    @GET("users/signature/")
    fun getUserSignature() : Call<Signature>
}