package cz.nohejbalpraha.app.model

enum class LeagueCategory {
    MEN,
    WOMEN,
    JUNIOR,
    CHILDREN
}