package cz.nohejbalpraha.app.handler

import cz.nohejbalpraha.app.model.League

interface LeagueClickHandler {
    /**
     * Called when a league is clicked. Should open league detail page.
     */
    fun onClick(league: League)
}