package cz.nohejbalpraha.app.matchForm.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.databinding.PlayerSelectorBinding
import cz.nohejbalpraha.app.matchForm.adapter.SelectPlayersAdapter
import cz.nohejbalpraha.app.matchForm.adapter.SelectPlayersListener
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModel
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModelFactory
import cz.nohejbalpraha.app.matchForm.viewModel.TeamType
import cz.nohejbalpraha.app.model.Player

/**
 * Fragment for player selection
 * @param matchID match id
 * @param listener selection listener
 * @param teamType HOME / AWAY team
 * @param playerSelectType CAPTAIN / SQUAD selection
 */
class PlayersSelectFragment(val matchID: Int, val listener: SelectPlayersListener, val teamType: TeamType, private val playerSelectType: PlayerSelectType) : Fragment() {
    private lateinit var viewModel: MatchFormViewModel
    private lateinit var adapter: SelectPlayersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(), MatchFormViewModelFactory(matchID)).get(
            MatchFormViewModel::class.java)

        var flag = 0
        var players : ArrayList<Player> = ArrayList()
        val selected : ArrayList<Player> = ArrayList()
        var teamName = ""
        var playerCount = 1
        if (playerSelectType == PlayerSelectType.LINEUP) {
            playerCount = viewModel.match.value!!.games[viewModel.currentGame.value!!].type
        }

        when(teamType) {
            TeamType.HOME -> {
                flag = 0
                players = viewModel.match.value!!.available_home_players as ArrayList<Player>
                when(playerSelectType) {
                    PlayerSelectType.CAPTAIN -> {
                        if (viewModel.match.value!!.home_captain != -1) {
                            for(player in viewModel.match.value!!.available_home_players) {
                                if (player.id == viewModel.match.value!!.home_captain) {
                                    selected.add(player)
                                }
                            }
                        }
                    }
                    PlayerSelectType.LINEUP -> {
                        if (viewModel.match.value!!.games[viewModel.currentGame.value!!].home_players != null
                            && viewModel.match.value!!.games[viewModel.currentGame.value!!].home_players.isNotEmpty()
                        ) {
                            for (player in viewModel.match.value!!.games[viewModel.currentGame.value!!].home_players) {
                                for (actualPlayer in players) {
                                    if (actualPlayer.id == player) {
                                        selected.add(actualPlayer)
                                        break
                                    }
                                }
                            }
                        }
                    }
                    PlayerSelectType.SUBSTITUTION_OFF -> {
                        flag = 0
                        if (viewModel.match.value!!.games[viewModel.currentGame.value!!].home_players != null
                            && viewModel.match.value!!.games[viewModel.currentGame.value!!].home_players.isNotEmpty()
                        ) {
                            val playingPlayers: ArrayList<Player> = ArrayList()
                            val playersInSquad = viewModel.match.value!!.games[viewModel.currentGame.value!!].home_players.toMutableList()
                            if (viewModel.currentSet.value == 2) {
                                for(substitution in viewModel.match.value!!.games[viewModel.currentGame.value!!].sets[0].home_substitutions) {
                                    playersInSquad.remove(substitution.outgoing)
                                    playersInSquad.add(substitution.incoming)
                                }
                            }
                            for (player in playersInSquad) {
                                if (viewModel.playersMap.containsKey(player)) {
                                    playingPlayers.add(viewModel.playersMap[player]!!)
                                }
                            }
                            players = playingPlayers
                        }
                    }
                    PlayerSelectType.SUBSTITUTION_ON -> {
                        flag = 1
                        players = viewModel.match.value!!.available_home_players.toMutableList() as ArrayList<Player>
                        for (player in viewModel.match.value!!.games[viewModel.currentGame.value!!].home_players) {
                            for (actualPlayer in players) {
                                if (actualPlayer.id == player) {
                                    players.remove(actualPlayer)
                                    break
                                }
                            }
                        }
                    }
                }
                teamName = viewModel.match.value!!.home_team.name
            }
            TeamType.AWAY -> {
                flag = 1
                players = viewModel.match.value!!.available_away_players as ArrayList<Player>
                when(playerSelectType) {
                    PlayerSelectType.CAPTAIN -> {
                        if (viewModel.match.value!!.away_captain != -1) {
                            for (player in viewModel.match.value!!.available_away_players) {
                                if (player.id == viewModel.match.value!!.away_captain) {
                                    selected.add(player)
                                }
                            }
                        }
                    }
                    PlayerSelectType.LINEUP -> {
                        if (viewModel.match.value!!.games[viewModel.currentGame.value!!].away_players != null
                            && viewModel.match.value!!.games[viewModel.currentGame.value!!].away_players.isNotEmpty()
                        ) {
                            for (player in viewModel.match.value!!.games[viewModel.currentGame.value!!].away_players) {
                                for (actualPlayer in players) {
                                    if (actualPlayer.id == player) {
                                        selected.add(actualPlayer)
                                        break
                                    }
                                }
                            }
                        }
                    }
                    PlayerSelectType.SUBSTITUTION_OFF -> {
                        flag = 0
                        if (viewModel.match.value!!.games[viewModel.currentGame.value!!].away_players != null
                            && viewModel.match.value!!.games[viewModel.currentGame.value!!].away_players.isNotEmpty()
                        ) {
                            val playingPlayers: ArrayList<Player> = ArrayList()
                            val playersInSquad = viewModel.match.value!!.games[viewModel.currentGame.value!!].away_players.toMutableList()
                            if (viewModel.currentSet.value == 2) {
                                for(substitution in viewModel.match.value!!.games[viewModel.currentGame.value!!].sets[0].away_substitutions) {
                                    playersInSquad.remove(substitution.outgoing)
                                    playersInSquad.add(substitution.incoming)
                                }
                            }
                            for (player in playersInSquad) {
                                if (viewModel.playersMap.containsKey(player)) {
                                    playingPlayers.add(viewModel.playersMap[player]!!)
                                }
                            }
                            players = playingPlayers
                        }
                    }
                    PlayerSelectType.SUBSTITUTION_ON -> {
                        flag = 1
                        players = viewModel.match.value!!.available_away_players.toMutableList() as ArrayList<Player>
                        for (player in viewModel.match.value!!.games[viewModel.currentGame.value!!].away_players) {
                            for(actualPlayer in players) {
                                if (actualPlayer.id == player) {
                                    players.remove(actualPlayer)
                                    break
                                }
                            }
                        }
                    }
                }
                teamName = viewModel.match.value!!.away_team.name
            }
        }

        val title = when(playerSelectType) {
            PlayerSelectType.CAPTAIN -> getString(R.string.select_captain_title, teamName)
            PlayerSelectType.LINEUP -> getString(R.string.select_squad_title, teamName)
            PlayerSelectType.SUBSTITUTION_ON -> getString(R.string.select_substitution_on_title)
            PlayerSelectType.SUBSTITUTION_OFF -> getString(R.string.select_substitution_off_title)
        }

        adapter = SelectPlayersAdapter(title, playerCount, selected, players, listener, flag)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: PlayerSelectorBinding = DataBindingUtil.inflate(inflater, R.layout.player_selector, container, false)
        binding.adapter = adapter
        binding.lifecycleOwner = this

        return binding.root
    }
}

enum class PlayerSelectType {
    CAPTAIN,
    LINEUP,
    SUBSTITUTION_ON,
    SUBSTITUTION_OFF
}