package cz.nohejbalpraha.app.model

import cz.nohejbalpraha.app.common.AccountManager
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class Match(val id: Int, val home_team: Team, val away_team: Team, val date: String) {
    var score: List<Int> = mutableListOf(0, 0)

    var home_players: List<Player> = ArrayList()
    var away_players: List<Player> = ArrayList()

    var home_captain: Int = -1
    var away_captain: Int = -1

    var games: List<Game> = ArrayList()

    var state: Int = -1
    var state_label: String = ""
    var round: Int = -1

    var note: String = ""

    var referees: List<Referee> = ArrayList()

    var league: String = ""
    var court: GameCourt? = null

    var available_home_players: List<Player> = ArrayList()
    var available_away_players: List<Player> = ArrayList()

    var myMatches: Boolean = false

    /**
     * Returns parsed date of the Match in LocalDateTime object
     */
    fun getDateTime() : LocalDateTime {
        return LocalDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME)
    }

    /**
     * Returns true if current user can start the match at current date and time otherwise return false
     * Requirements to start the match:
     * - User is logged in
     * - Current date and time is passed the start date and time of the match
     * - User is referee of the match or a player from either team
     */
    fun canStart() : Boolean {
        if (AccountManager.isLoggedIn() && LocalDateTime.now().isAfter(getDateTime())) {
            return myMatches
        }

        return false
    }
}