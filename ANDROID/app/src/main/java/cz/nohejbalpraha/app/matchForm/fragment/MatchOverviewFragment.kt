package cz.nohejbalpraha.app.matchForm.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.GamesAdapter
import cz.nohejbalpraha.app.databinding.MatchFormOverviewBinding
import cz.nohejbalpraha.app.handler.GameClickHandler
import cz.nohejbalpraha.app.matchForm.handler.MatchFormFlowHandler
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModel
import cz.nohejbalpraha.app.matchForm.viewModel.MatchFormViewModelFactory
import cz.nohejbalpraha.app.model.Game

/**
 * Fragment for match form overview
 * @param matchID match id
 */
class MatchOverviewFragment(private val matchID: Int) : Fragment(), GameClickHandler, MatchFormFlowHandler {
    private lateinit var viewModel: MatchFormViewModel
    private lateinit var adapter: GamesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(), MatchFormViewModelFactory(matchID)).get(
            MatchFormViewModel::class.java)

        adapter = GamesAdapter(viewModel.match.value!!.games, this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: MatchFormOverviewBinding = DataBindingUtil.inflate(inflater, R.layout.match_form_overview, container, false)
        binding.adapter = adapter
        binding.viewModel = viewModel
        binding.flowHandler = this

        return binding.root
    }

    override fun onClick(game: Game, gameName: String) {
        // empty
    }

    override fun next() {
        viewModel.continueToSign()
    }

    override fun nextLabel(): String {
        return activity?.getString(R.string.match_form_sign) ?: ""
    }

    override fun leaveMatch() {
        AlertDialog.Builder(activity!!)
            .setMessage(activity?.getString(R.string.match_form_leave_match))
            .setPositiveButton(activity?.getString(R.string.match_form_leave_yes), object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    viewModel.leaveMatch()
                }
            })
            .setNegativeButton(activity?.getString(R.string.match_form_leave_no), null)
            .show()
    }
}