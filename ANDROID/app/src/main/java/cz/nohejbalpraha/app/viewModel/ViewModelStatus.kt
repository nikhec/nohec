package cz.nohejbalpraha.app.viewModel

enum class ViewModelStatus {
    LOADING,
    SUCCESS,
    FAILURE
}