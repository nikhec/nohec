package cz.nohejbalpraha.app.matchForm.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.App
import cz.nohejbalpraha.app.model.Player
import cz.nohejbalpraha.app.model.Substitution

class MatchFormSubstitutionViewModel : ViewModel() {
    val substitution: MutableLiveData<Substitution> by lazy {
        MutableLiveData(Substitution(0, 0))
    }
    val playerOffLabel: MutableLiveData<String> by lazy {
        MutableLiveData(App.context.getString(R.string.match_form_select_player))
    }
    val playerOnLabel: MutableLiveData<String> by lazy {
        MutableLiveData(App.context.getString(R.string.match_form_select_player))
    }

    /**
     * Adds player that is substituted
     * @param player outgoing player
     */
    fun substitutePlayerOff(player: Player) {
        playerOffLabel.value = player.name
        substitution.value!!.outgoing = player.id
        substitution.postValue(substitution.value)
    }

    /**
     * Adds player that is coming on the pitch
     * @param player incoming player
     */
    fun substitutePlayerOn(player: Player) {
        playerOnLabel.value = player.name
        substitution.value!!.incoming = player.id
        substitution.postValue(substitution.value)
    }
}