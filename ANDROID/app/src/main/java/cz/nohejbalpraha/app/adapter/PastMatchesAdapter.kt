package cz.nohejbalpraha.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.databinding.PastMatchOverviewBinding
import cz.nohejbalpraha.app.handler.MatchClickHandler
import cz.nohejbalpraha.app.model.Match

/**
 * RecyclerView adapter for past matches
 * @param items matches
 * @param listener click listener
 */
class PastMatchesAdapter(private var items: List<Match>, private val listener: MatchClickHandler) : RecyclerView.Adapter<PastMatchesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = PastMatchOverviewBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    /**
     * Updates and refreshes the data in the view
     */
    fun updateData(items: List<Match>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: PastMatchOverviewBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Match) {
            binding.match = item
            binding.listener = listener
            binding.executePendingBindings()
        }
    }
}