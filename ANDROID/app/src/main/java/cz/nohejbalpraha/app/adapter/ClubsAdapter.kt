package cz.nohejbalpraha.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.databinding.ClubsClubBinding
import cz.nohejbalpraha.app.handler.ClubsClickHandler
import cz.nohejbalpraha.app.model.Club
import java.util.*
import kotlin.collections.ArrayList

/**
 * RecyclerView adapter for clubs in clubs list
 * @param items clubs
 * @param clickListener click listener
 */
class ClubsAdapter(var items: List<Club>, val clickListener: ClubsClickHandler) : RecyclerView.Adapter<ClubsAdapter.ViewHolder>() {
    private var filteredItems: List<Club> = items
    private var filterKey: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ClubsClubBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(filteredItems[position])

    override fun getItemCount(): Int = filteredItems.size

    /**
     * Updates and refreshes the data in the view
     */
    fun updateData(items: List<Club>) {
        this.items = items
        filter(filterKey)
    }

    /**
     * Filters club names by given key. If the key is empty all clubs will be shown
     */
    fun filter(key: String) {
        filterKey = key.toUpperCase(Locale.getDefault())
        filteredItems = ArrayList<Club>()
        for(club in items) {
            if (filterKey.isEmpty() || club.name.toUpperCase(Locale.getDefault()).contains(filterKey)) {
                (filteredItems as ArrayList<Club>).add(club)
            }
        }
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: ClubsClubBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Club) {
            binding.club = item
            binding.clickHandler = clickListener
            binding.executePendingBindings()
        }
    }
}