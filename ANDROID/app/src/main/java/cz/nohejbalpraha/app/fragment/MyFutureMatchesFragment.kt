package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.adapter.FutureMatchesAdapter
import cz.nohejbalpraha.app.databinding.FragmentMyFutureMatchesBinding
import cz.nohejbalpraha.app.handler.MatchClickHandler
import cz.nohejbalpraha.app.viewModel.MyMatchesViewModel

/**
 * Fragment that shows user future matches
 * @param listener click listener
 */
class MyFutureMatchesFragment(private val listener: MatchClickHandler) : Fragment() {
    private lateinit var viewModel: MyMatchesViewModel
    private lateinit var adapter: FutureMatchesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity()).get(MyMatchesViewModel::class.java)
        adapter = FutureMatchesAdapter(viewModel.futureMatches.value!!, listener)

        viewModel.futureMatches.observe(requireActivity(), adapter::updateData)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : FragmentMyFutureMatchesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_future_matches, container, false)
        binding.adapter = adapter
        binding.lifecycleOwner = this

        return binding.root
    }
}