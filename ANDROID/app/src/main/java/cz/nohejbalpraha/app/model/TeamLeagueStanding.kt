package cz.nohejbalpraha.app.model

class TeamLeagueStanding(val team: Team) {
    var position: Int = 0
    var matches: Int = 0

    var wins: Int = 0
    var draws: Int = 0
    var losses: Int = 0

    var sets_won: Int = 0
    var sets_lost: Int = 0

    var points: Int = 0
}