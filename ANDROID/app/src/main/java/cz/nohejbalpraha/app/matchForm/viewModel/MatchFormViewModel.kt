package cz.nohejbalpraha.app.matchForm.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.common.App
import cz.nohejbalpraha.app.model.*
import cz.nohejbalpraha.app.model.Set
import cz.nohejbalpraha.app.model.api.*
import cz.nohejbalpraha.app.viewModel.ViewModelStatus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * ViewModel that holds data for the match form
 * @param matchID match id
 */
class MatchFormViewModel(private val matchID: Int) : ViewModel() {
    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData(ViewModelStatus.LOADING)
    }
    val matchStatus: MutableLiveData<MatchStatus> by lazy {
        MutableLiveData(MatchStatus.LOADING)
    }
    val match: MutableLiveData<Match> by lazy {
        MutableLiveData<Match>()
    }
    val loadingLabel: MutableLiveData<String> by lazy {
        MutableLiveData(App.context.getString(R.string.match_form_loading_match))
    }
    val homeCaptainLabel: MutableLiveData<String> by lazy {
        MutableLiveData(App.context.getString(R.string.match_select_captain))
    }
    val awayCaptainLabel: MutableLiveData<String> by lazy {
        MutableLiveData(App.context.getString(R.string.match_select_captain))
    }
    val currentGame: MutableLiveData<Int> by lazy {
        MutableLiveData(0)
    }
    val currentSet: MutableLiveData<Int> by lazy {
        MutableLiveData(0)
    }
    val hasReferee: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val errorDescription: MutableLiveData<String> by lazy {
        MutableLiveData("")
    }
    var playersMap: HashMap<Int, Player> = HashMap()
    var homeCaptainSign: String = ""
    var awayCaptainSign: String = ""
    var refereeSign: String = ""

    init {
        enterMatch()
    }

    /**
     * Tries to enter the match and sets proper states
     */
    fun enterMatch() {
        loadingLabel.value = App.context.getString(R.string.match_form_loading_match)
        status.value = ViewModelStatus.LOADING
        ApiClient.callAPI().enterMatch(matchID).enqueue(object : Callback<Match> {
            override fun onResponse(call: Call<Match>, response: Response<Match>) {
                when (response.code()) {
                    200, 201 -> {
                        match.value = response.body()

                        prepareMatch()
                        preparePlayers()
                        getMatchScore()
                    }
                    409 -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_lock)
                        matchStatus.value = MatchStatus.ERROR
                    }
                    else -> {
                        matchStatus.value = MatchStatus.LEAVE
                    }
                }
                status.value = ViewModelStatus.SUCCESS
            }

            override fun onFailure(call: Call<Match>, t: Throwable) {
                errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                matchStatus.value = MatchStatus.ERROR

            }

        })
    }

    /**
     * Sets captain for given team
     */
    fun selectCaptain(captain: Player, teamType: TeamType) {
        when(teamType) {
            TeamType.HOME -> {
                homeCaptainLabel.value = captain.name
                match.value?.home_captain = captain.id
            }
            TeamType.AWAY -> {
                awayCaptainLabel.value = captain.name
                match.value?.away_captain = captain.id
            }
        }
    }

    /**
     * Saves captains for the match
     */
    fun recordCaptains() {
        loadingLabel.value = App.context.getString(R.string.match_form_recording_captains)
        status.value = ViewModelStatus.LOADING
        val record = CaptainsRecord(match.value!!.home_captain, match.value!!.away_captain)
        ApiClient.callAPI().recordCaptains(matchID, record).enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                when(response.code()) {
                    200 -> {
                        status.value = ViewModelStatus.SUCCESS
                        matchStatus.value = MatchStatus.GAME_SQUAD
                    }
                    409 -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_lock)
                        matchStatus.value = MatchStatus.ERROR
                    }
                    else -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                        matchStatus.value = MatchStatus.ERROR
                    }
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                matchStatus.value = MatchStatus.ERROR
            }

        })
    }

    /**
     * Saves sqaud for a given game
     * @param game index of the game in the match
     * @param homePlayers list of ids of home team players in the squad
     * @param awayPlayers list of ids of away team players in the squad
     */
    fun recordGameSquad(game: Int, homePlayers: List<Int>, awayPlayers: List<Int>) {
        loadingLabel.value = App.context.getString(R.string.match_form_recording_squad)
        status.value = ViewModelStatus.LOADING
        val record = GameSquadRecord(match.value!!.games[game].id, homePlayers, awayPlayers)
        ApiClient.callAPI().recordGameSquad(matchID, record).enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                when(response.code()) {
                    200 -> {
                        status.value = ViewModelStatus.SUCCESS
                        currentSet.value = currentSet.value!! + 1

                        if (match.value!!.games[currentGame.value!!].sets.isEmpty()) {
                            (match.value!!.games[currentGame.value!!].sets as ArrayList<Set>).add(Set(1))
                            (match.value!!.games[currentGame.value!!].sets as ArrayList<Set>).add(Set(2))
                        }

                        matchStatus.value = MatchStatus.GAME_SET
                    }
                    409 -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_lock)
                        matchStatus.value = MatchStatus.ERROR
                    }
                    else -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                        matchStatus.value = MatchStatus.ERROR
                    }
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                matchStatus.value = MatchStatus.ERROR
            }

        })
    }

    /**
     * Saves set of a game
     * @param game index of the game in the match
     * @param set set to be saved
     */
    fun recordGameSet(game: Int, set: Set) {
        loadingLabel.value = App.context.getString(R.string.match_form_recording_set)
        status.value = ViewModelStatus.LOADING
        val record = SetRecord(match.value!!.games[game].id, set.home_points, set.away_points, set.home_timeout, set.away_timeout, set.home_substitutions, set.away_substitutions)
        ApiClient.callAPI().recordSet(matchID, record).enqueue(object: Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                when(response.code()) {
                    200 -> {
                        status.value = ViewModelStatus.SUCCESS
                        (match.value!!.games[currentGame.value!!].sets as ArrayList)[currentSet.value!! - 1] = set
                        if (currentSet.value == 2) {
                            currentSet.value = 0

                            if(match.value!!.games[currentGame.value!!].sets[0].home_points == 10
                                && set.home_points == 10) {
                                (match.value!!.score as MutableList)[0] = match.value!!.score[0] + 1
                            } else if (match.value!!.games[currentGame.value!!].sets[0].away_points == 10
                                && set.away_points == 10) {
                                (match.value!!.score as MutableList)[1] = match.value!!.score[1] + 1
                            }

                            currentGame.value = currentGame.value!! + 1
                            if (match.value!!.games.size > currentGame.value!!) {
                                matchStatus.value = MatchStatus.GAME_SQUAD
                            } else {
                                matchStatus.value = MatchStatus.NOTE
                            }
                        } else {
                            currentSet.value = currentSet.value!! + 1
                            matchStatus.value = MatchStatus.GAME_SET
                        }

                        match.postValue(match.value)
                    }
                    409 -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_lock)
                        matchStatus.value = MatchStatus.ERROR
                    }
                    else -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                        matchStatus.value = MatchStatus.ERROR
                    }
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                matchStatus.value = MatchStatus.ERROR
            }

        })
    }

    /**
     * Saves note for the match
     * @param note note
     */
    fun recordNote(note: String) {
        loadingLabel.value = App.context.getString(R.string.match_form_recording_note)
        status.value = ViewModelStatus.LOADING
        val record = NoteRecord(note)
        ApiClient.callAPI().recordNote(matchID, record).enqueue(object: Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                when(response.code()) {
                    200 -> {
                        matchStatus.value = MatchStatus.OVERVIEW
                        status.value = ViewModelStatus.SUCCESS
                    }
                    409 -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_lock)
                        matchStatus.value = MatchStatus.ERROR
                    }
                    else -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                        matchStatus.value = MatchStatus.ERROR
                    }
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                matchStatus.value = MatchStatus.ERROR
            }

        })
    }

    /**
     * Changes state from overview to signing
     */
    fun continueToSign() {
        matchStatus.value = MatchStatus.SIGN
    }

    /**
     * Ends editing of the match and releases lock
     */
    fun leaveMatch() {
        matchStatus.value = MatchStatus.LEAVE
        ApiClient.callAPI().exitMatch(matchID).enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                // empty
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                // empty
            }
        })
    }

    /**
     * Saves signings by home and away captains and referee
     * @param referee empty if match doesn't have a referee
     * @param home_captain code for home captain
     * @param away_captain code for away captain
     */
    fun recordSign(referee: String, home_captain: String, away_captain: String) {
        loadingLabel.value = App.context.getString(R.string.match_form_saving_signing)
        status.value = ViewModelStatus.LOADING
        val record = SignRecord(home_captain, away_captain)
        homeCaptainSign = home_captain
        awayCaptainSign = away_captain
        if (referee.isNotEmpty()) {
            record.referee = referee
            refereeSign = referee
        }
        ApiClient.callAPI().recordSign(matchID, record).enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                status.value = ViewModelStatus.SUCCESS
                when(response.code()) {
                    200 -> {
                        matchStatus.value = MatchStatus.FINISHED
                        status.value = ViewModelStatus.SUCCESS
                    }
                    400 -> {
                        if (response.errorBody()!!.string().contains("home_captain")) {
                            homeCaptainSign = ""
                            errorDescription.value = App.context.getString(R.string.match_form_sign_error, App.context.getString(R.string.match_form_sign_home_captain))
                        }
                        if (response.errorBody()!!.string().contains("away_captain")) {
                            awayCaptainSign = ""
                            errorDescription.value = errorDescription.value + App.context.getString(R.string.match_form_sign_error, App.context.getString(R.string.match_form_sign_away_captain))
                        }
                        if (response.errorBody()!!.string().contains("referee")) {
                            refereeSign = ""
                            errorDescription.value = errorDescription.value + App.context.getString(R.string.match_form_sign_error, App.context.getString(R.string.match_form_sign_referee))
                        }
                        matchStatus.value = MatchStatus.ERROR
                    }
                    409 -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_lock)
                        matchStatus.value = MatchStatus.ERROR
                    }
                    else -> {
                        errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                        matchStatus.value = MatchStatus.ERROR
                    }
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                errorDescription.value = App.context.getString(R.string.match_form_error_unspecified)
                matchStatus.value = MatchStatus.ERROR
                status.value = ViewModelStatus.SUCCESS
            }

        })
    }

    /**
     * Returns name of the given game if the given game doesn't exist it returns default 'continue' string
     * @param game position of the game in the match
     */
    fun getGameName(game: Int) : String {
        if (match.value!!.games.size > game) {
            val gameType = match.value!!.games[game].type
            var num = 1

            if (game != 0) {
                for (i in 0..(game - 1)) {
                    if (match.value!!.games[i].type == gameType) {
                        num++;
                    }
                }
            }

            var gameName = ""
            when (gameType) {
                GameType.SINGLE.type -> gameName = App.context.getString(R.string.game_type_single)
                GameType.DOUBLE.type -> gameName = App.context.getString(R.string.game_type_double)
                GameType.TRIPLE.type -> gameName = App.context.getString(R.string.game_type_triple)
            }

            return "$num. $gameName"
        }

        return App.context.getString(R.string.match_form_overview)
    }

    private fun prepareMatch() {
        if (match.value!!.referees != null) {
            hasReferee.value = match.value!!.referees.isNotEmpty()
        }

        if (match.value!!.home_captain <= 0 || match.value!!.away_captain <= 0) { // Captains are not selected
            matchStatus.value = MatchStatus.CAPTAINS
        } else if (!getCurrentGame()) { // All games are not finished
            if (currentSet.value == 0) {
                matchStatus.value = MatchStatus.GAME_SQUAD
            } else {
                matchStatus.value = MatchStatus.GAME_SET
            }
        } else if (match.value!!.state != MatchState.FINISHED.state) { // Match is not signed
            matchStatus.value = MatchStatus.SIGN
        } else { // Match is finished
            matchStatus.value = MatchStatus.FINISHED
        }
    }

    private fun preparePlayers() {
        for (player in match.value!!.available_home_players) {
            playersMap[player.id] = player
        }
        for (player in match.value!!.available_away_players) {
            playersMap[player.id] = player
        }
    }

    private fun getCurrentGame() : Boolean {
        var currGame = 0
        for (game in match.value!!.games) {
            if (game.sets.isEmpty()) {
                (game.sets as ArrayList<Set>).add(Set(1))
                (game.sets as ArrayList<Set>).add(Set(2))

                currentGame.value = currGame
                if (game.home_players.isEmpty() || game.away_players.isEmpty()) {
                    currentSet.value = 0
                } else {
                    currentSet.value = 1
                }

                return false
            } else if (game.sets.size == 1) {
                (game.sets as ArrayList<Set>).add(Set(2))
                currentGame.value = currGame
                currentSet.value = 2

                return false
            }

            currGame++
        }

        return true
    }

    private fun getMatchScore() {
        match.value!!.score = mutableListOf(0, 0)
        for (game in match.value!!.games) {
            if (game.sets != null && game.sets.size == 2) {
                if (game.sets[0].home_points == 10 && game.sets[1].home_points == 10) {
                    (match.value!!.score as MutableList)[0]++
                } else if (game.sets[0].away_points == 10 && game.sets[1].away_points == 10) {
                    (match.value!!.score as MutableList)[1]++
                }
            }
        }

        match.postValue(match.value)
    }
}

enum class TeamType {
    HOME,
    AWAY
}

enum class MatchStatus {
    LOADING,
    CAPTAINS,
    GAME_SQUAD,
    GAME_SET,
    NOTE,
    OVERVIEW,
    SIGN,
    FINISHED,
    ERROR,
    LEAVE
}