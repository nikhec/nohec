package cz.nohejbalpraha.app.common

import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Retrofit client configuration
 */
object ApiClient {
    private val api: APIInterface

    init {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor { chain ->
            val request: Request = chain.request().newBuilder()
                .addHeader("Authorization", "Token " + AccountManager.getToken())
                .build()

            chain.proceed(request)
        }

        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("http://lab.d3s.mff.cuni.cz:8664/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()

        api = retrofit.create(APIInterface::class.java)
    }

    /**
     * Returns Retrofit client to call the API
     */
    fun callAPI() : APIInterface {
        return api
    }
}