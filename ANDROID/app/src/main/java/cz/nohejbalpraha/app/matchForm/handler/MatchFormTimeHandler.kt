package cz.nohejbalpraha.app.matchForm.handler

import cz.nohejbalpraha.app.matchForm.viewModel.TeamType

interface MatchFormTimeHandler {
    /**
     * Should open timer
     * @param teamType time for HOME/AWAY team
     */
    fun startTimer(teamType: TeamType)

    /**
     * Should close timer
     */
    fun endTimer()
}