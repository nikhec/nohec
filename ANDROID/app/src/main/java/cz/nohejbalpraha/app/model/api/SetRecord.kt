package cz.nohejbalpraha.app.model.api

import cz.nohejbalpraha.app.model.Substitution

data class SetRecord(val game: Int, val home_points: Int, val away_points: Int, val home_timeout: Int, val away_timeout: Int, val home_substitutions: List<Substitution>, val away_substitutions: List<Substitution>)