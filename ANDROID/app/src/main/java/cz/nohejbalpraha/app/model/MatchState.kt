package cz.nohejbalpraha.app.model

enum class MatchState(val state: Int) {
    UNDEFINED(-1),
    UPCOMING(1),
    WAITING_FOR_CAPTAINS(2),
    IN_PROGRESS(3),
    FINISHED(4)
}