package cz.nohejbalpraha.app.model.api

data class GameSquadRecord(val id: Int, val home_players: List<Int>, val away_players : List<Int>)