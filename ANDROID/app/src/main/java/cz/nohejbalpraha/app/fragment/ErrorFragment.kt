package cz.nohejbalpraha.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.databinding.FragmentErrorBinding

/**
 * Fragment that shows application error
 * @param errorDescription error description
 */
class ErrorFragment(private val errorDescription: String) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentErrorBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_error, container, false)
        binding.errorDescription = errorDescription

        return binding.root
    }
}