package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.model.MatchDateList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * ViewModel that handles data for matches list page
 */
class MatchesViewModel : ViewModel() {
    private val matches_count = 100

    val matches: MutableLiveData<List<MatchDateList>> by lazy {
        MutableLiveData(ArrayList())
    }
    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData(ViewModelStatus.LOADING)
    }

    init {
        ApiClient.callAPI().getPastMatches(count = matches_count).enqueue(object: Callback<List<MatchDateList>> {
            override fun onResponse(call: Call<List<MatchDateList>>, response: Response<List<MatchDateList>>) {
                if (response.code() == 200 && response.body() != null) {
                    matches.value = response.body()
                    status.value = ViewModelStatus.SUCCESS
                } else {
                    status.value = ViewModelStatus.FAILURE
                }
            }

            override fun onFailure(call: Call<List<MatchDateList>>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }

        })
    }
}