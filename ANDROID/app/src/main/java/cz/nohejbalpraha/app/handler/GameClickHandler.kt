package cz.nohejbalpraha.app.handler

import cz.nohejbalpraha.app.model.Game

interface GameClickHandler {
    /**
     * Called when game overview is clicked. Should open game detail page
     */
    fun onClick(game: Game, gameName: String)
}