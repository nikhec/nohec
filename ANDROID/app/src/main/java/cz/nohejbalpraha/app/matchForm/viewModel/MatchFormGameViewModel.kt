package cz.nohejbalpraha.app.matchForm.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.App
import cz.nohejbalpraha.app.model.Game
import cz.nohejbalpraha.app.model.Player
import cz.nohejbalpraha.app.model.Team

/**
 * ViewModel that holds data for squad selection in match form
 * @param homeTeam home team
 * @param awayTeam away team
 * @param initialGame empty game
 */
class MatchFormGameViewModel(val homeTeam: Team, val awayTeam: Team, private val initialGame: Game) : ViewModel() {
    val status: MutableLiveData<MatchGameStatus>
    val game: MutableLiveData<Game> by lazy {
        MutableLiveData(initialGame)
    }
    val homeLineupLabel: MutableLiveData<String> by lazy {
        MutableLiveData(App.context.getString(R.string.match_form_select_lineup))
    }
    val awayLineupLabel: MutableLiveData<String> by lazy {
        MutableLiveData(App.context.getString(R.string.match_form_select_lineup))
    }

    init {
        status = if (initialGame.home_players.isEmpty()
            || initialGame.away_players.isEmpty()) {
            MutableLiveData(MatchGameStatus.LINEUP)
        } else if (initialGame.sets[0].home_points < 10
            && initialGame.sets[0].away_points < 10) {
            MutableLiveData(MatchGameStatus.FIRST_SET)
        } else if (initialGame.sets[1].home_points < 10
            && initialGame.sets[1].away_points < 10) {
            MutableLiveData(MatchGameStatus.SECOND_SET)
        } else {
            MutableLiveData(MatchGameStatus.FINISHED)
        }
    }

    /**
     * Set squad for given team
     * @param players players that are in the squad
     * @param teamType HOME / AWAY team
     */
    fun setSquad(players: List<Player>, teamType: TeamType) {
        val teamPlayers = ArrayList<Int>()
        var teamLabel = ""
        for (player in players) {
            teamPlayers.add(player.id)
            teamLabel += player.name + ", "
        }

        when(teamType) {
            TeamType.HOME -> {
                homeLineupLabel.value = teamLabel.subSequence(0, teamLabel.length - 2).toString()
                game.value!!.home_players = teamPlayers
            }
            TeamType.AWAY -> {
                awayLineupLabel.value = teamLabel.subSequence(0, teamLabel.length - 2).toString()
                game.value!!.away_players = teamPlayers
            }
        }
        game.postValue(game.value)
    }

    /**
     * Checks whether both squads are set
     */
    fun isSquadSet() : Boolean {
        return game.value!!.home_players.isNotEmpty()
                && game.value!!.away_players.isNotEmpty()
    }
}

enum class MatchGameStatus {
    LINEUP,
    FIRST_SET,
    SECOND_SET,
    FINISHED
}