package cz.nohejbalpraha.app.model

class GameCourt {
    var id: Int = 0
    var address: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0

    override fun equals(other: Any?): Boolean {
        if (other is GameCourt) {
            return this.id == other.id
        }

        return false
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}