package cz.nohejbalpraha.app.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.nohejbalpraha.app.MATCH_ID
import cz.nohejbalpraha.app.MatchFormActivity
import cz.nohejbalpraha.app.common.App
import cz.nohejbalpraha.app.databinding.FutureMatchOverviewBinding
import cz.nohejbalpraha.app.handler.MatchClickHandler
import cz.nohejbalpraha.app.handler.StartMatchHandler
import cz.nohejbalpraha.app.model.Match

/**
 * RecyclerView adapter for future matches
 * @param items matches
 * @param listener click listener
 */
class FutureMatchesAdapter(private var items: List<Match>, private val listener: MatchClickHandler) : RecyclerView.Adapter<FutureMatchesAdapter.ViewHolder>(), StartMatchHandler {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = FutureMatchOverviewBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    /**
     * Updates and refreshes the data in the view
     */
    fun updateData(items: List<Match>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: FutureMatchOverviewBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Match) {
            binding.match = item
            binding.listener = listener
            binding.startMatchHandler = this@FutureMatchesAdapter
            binding.executePendingBindings()
        }
    }

    override fun startMatch(match: Match) {
        val intent = Intent(App.context, MatchFormActivity::class.java).apply {
            putExtra(MATCH_ID, match.id)
            setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        App.context.startActivity(intent)
    }
}