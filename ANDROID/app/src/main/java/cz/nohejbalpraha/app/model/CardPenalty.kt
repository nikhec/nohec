package cz.nohejbalpraha.app.model

class CardPenalty(val player: Player, val reason: String)