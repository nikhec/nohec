package cz.nohejbalpraha.app.handler

import cz.nohejbalpraha.app.model.Club

interface ClubsClickHandler {
    /**
     * Called when a clubs is clicked. Should open club detail page.
     */
    fun onClubClick(club: Club)

    /**
     * Called when the search button is clicked. Should open search input.
     */
    fun onSearchClick()
}