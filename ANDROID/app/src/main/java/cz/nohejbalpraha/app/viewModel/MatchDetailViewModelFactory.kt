package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cz.nohejbalpraha.app.model.Match

/**
 * ViewModelFactory for MatchDetailViewModel
 */
class MatchDetailViewModelFactory(private val match: Match) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MatchDetailViewModel(match) as T
    }

}