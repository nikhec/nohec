package cz.nohejbalpraha.app.model

enum class GameType(val type: Int) {
    SINGLE(1),
    DOUBLE(2),
    TRIPLE(3)
}