package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.model.Club
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * ViewModel that handles data for clubs list page
 */
class ClubsViewModel : ViewModel() {
    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData<ViewModelStatus>(ViewModelStatus.LOADING)
    }
    val clubs: MutableLiveData<List<Club>> by lazy {
        MutableLiveData<List<Club>>(ArrayList())
    }
    val searching: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }
    val searchKey: MutableLiveData<String> by lazy {
        MutableLiveData<String>("")
    }

    init {
        ApiClient.callAPI().getClubs().enqueue(object : Callback<List<Club>> {
            override fun onResponse(call: Call<List<Club>>, response: Response<List<Club>>) {
                if (response.body() != null) {
                    clubs.postValue(response.body())
                    status.value = ViewModelStatus.SUCCESS
                } else {
                    status.value = ViewModelStatus.FAILURE
                }
            }

            override fun onFailure(call: Call<List<Club>>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }
        })
    }
}