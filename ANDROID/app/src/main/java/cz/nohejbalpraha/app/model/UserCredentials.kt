package cz.nohejbalpraha.app.model

data class UserCredentials(val username: String, val password: String)
