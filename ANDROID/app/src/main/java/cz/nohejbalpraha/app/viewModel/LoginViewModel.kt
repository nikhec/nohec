package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.R
import cz.nohejbalpraha.app.common.AccountManager
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.common.App
import cz.nohejbalpraha.app.model.UserCredentials
import cz.nohejbalpraha.app.model.UserSession
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * ViewModel that handles data for login page
 */
class LoginViewModel() : ViewModel()  {
    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData(ViewModelStatus.LOADING)
    }
    val email: MutableLiveData<String> by lazy {
        MutableLiveData<String>("")
    }
    val password: MutableLiveData<String> by lazy {
        MutableLiveData<String>("")
    }
    val loggedIn: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }
    val userSession: MutableLiveData<UserSession> by lazy {
        MutableLiveData<UserSession>(UserSession("", 0, ""))
    }
    val loginError: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loginErrorDescription: MutableLiveData<String> by lazy {
        MutableLiveData("")
    }

    init {
        if (AccountManager.isLoggedIn()) {
            userSession.value = AccountManager.getUserSession()!!
        }
    }

    fun login() {
        val credentials = UserCredentials(email.value!!, password.value!!)
        ApiClient.callAPI().login(credentials).enqueue(object : Callback<UserSession> {
            override fun onResponse(call: Call<UserSession>, response: Response<UserSession>) {
                if (response.code() == 200 && response.body() != null) {
                    AccountManager.login(response.body()!!)
                    if (AccountManager.isLoggedIn()) {
                        loggedIn.value = true
                        userSession.value = AccountManager.getUserSession()!!
                    }
                } else {
                    loginError.value = true
                    loginErrorDescription.value = App.context.getString(R.string.login_error_description)
                }
                status.value = ViewModelStatus.SUCCESS
            }

            override fun onFailure(call: Call<UserSession>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }

        })
    }

    fun logout() {
        AccountManager.logout()
        loggedIn.value = AccountManager.isLoggedIn()
    }
}