package cz.nohejbalpraha.app.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.nohejbalpraha.app.common.AccountManager
import cz.nohejbalpraha.app.common.ApiClient
import cz.nohejbalpraha.app.model.Match
import cz.nohejbalpraha.app.model.MatchDateList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * ViewModel that handles data for user matches page
 */
class MyMatchesViewModel : ViewModel() {
    val futureMatches: MutableLiveData<List<Match>> by lazy {
        MutableLiveData(ArrayList())
    }
    val pastMatches: MutableLiveData<List<MatchDateList>> by lazy {
        MutableLiveData(ArrayList())
    }
    val status: MutableLiveData<ViewModelStatus> by lazy {
        MutableLiveData(ViewModelStatus.LOADING)
    }

    fun loadMatches() {
        ApiClient.callAPI().getMyFutureMatches().enqueue(object : Callback<List<MatchDateList>> {
            override fun onResponse(call: Call<List<MatchDateList>>, response: Response<List<MatchDateList>>) {
                if (response.code() == 200 && response.body() != null) {
                    val matches: ArrayList<Match> = ArrayList()
                    for (matchList in response.body()!!) {
                        for (match in matchList.matches) {
                            match.myMatches = true
                            matches.add(match)
                        }
                    }
                    futureMatches.value = matches
                    status.value = ViewModelStatus.SUCCESS

                    loadPastMatches()
                } else if(response.code() == 401) {
                    AccountManager.logout()
                } else {
                    status.value = ViewModelStatus.FAILURE
                }
            }

            override fun onFailure(call: Call<List<MatchDateList>>, t: Throwable) {
                status.value = ViewModelStatus.FAILURE
            }

        })
    }

    private fun loadPastMatches() {
        ApiClient.callAPI().getMyPastMatches().enqueue(object : Callback<List<MatchDateList>> {
            override fun onResponse(call: Call<List<MatchDateList>>, response: Response<List<MatchDateList>>) {
                if (response.code() == 200 && response.body() != null) {
                    pastMatches.value = response.body()
                }
            }

            override fun onFailure(call: Call<List<MatchDateList>>, t: Throwable) {
                // empty
            }

        })
    }
}