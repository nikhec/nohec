//
//  PlayerData.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 31/05/2021.
//

import Foundation

/// Represents player's data 
class PlayerData: Codable, Identifiable {
    var id: Int?
    var name: String?
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        name = try? container.decode(String.self, forKey: .name)
    }
}

/// Extension for purposes of View previews
extension PlayerData{
    static let example = PlayerData(id: 1, name: "John Doe")
}
