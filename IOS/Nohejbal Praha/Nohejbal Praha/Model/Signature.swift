//
//  Signature.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 02/07/2021.
//

import Foundation

/// Represents singature code
class Signature: Codable, Identifiable {
    var signature: String?
    
    init(signature: String) {
        self.signature = signature
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        signature = try? container.decode(String.self, forKey: .signature)
    }
}

/// Extension for purposes of View previews
extension Signature{
    static let example = Signature(signature: "")
}
