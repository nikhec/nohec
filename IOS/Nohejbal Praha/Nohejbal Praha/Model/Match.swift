//
//  Match.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 23/02/2021.
//

import Foundation

/// Class representng match, contains all necessary basic match information
class Match: Decodable, Identifiable {
    var id: Int?
    var league: String?
    var home_team: TeamResponse?
    var away_team: TeamResponse?
    var score: [Int]?
    var court: GameCourt?
    var state: Int?
    var round: Int?
    var date: String?
    var note: String?
    var home_captain: Int?
    var away_captain: Int?
    var referees: [Int]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case league
        case home_team
        case away_team
        case score
        case court
        case state
        case round
        case date
        case note
        case home_captain
        case away_captain
        case referees
    }
    
    init(id: Int, league: String, home_team:TeamResponse, away_team: TeamResponse, score: [Int], court: GameCourt, state: Int, round: Int, date: String, note:String, home_captain: Int, away_captain: Int, referees: [Int]) {
        self.id = id
        self.league = league
        self.home_team = home_team
        self.away_team = away_team
        self.score = score
        self.court = court
        self.state = state
        self.round = round
        self.date = date
        self.note = note
        self.home_captain = home_captain
        self.away_captain = away_captain
        self.referees = referees
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        league = try? container.decode(String.self, forKey: .league)
        home_team = try? container.decode(TeamResponse.self, forKey: .home_team)
        away_team = try? container.decode(TeamResponse.self, forKey: .away_team)
        score = try? container.decode([Int].self, forKey: .score)
        court = try? container.decode(GameCourt.self, forKey: .court)
        state = try? container.decode(Int.self, forKey: .state)
        round = try? container.decode(Int.self, forKey: .round)
        date = try? container.decode(String.self, forKey: .date)
        note = try? container.decode(String.self, forKey: .note)
        home_captain = try? container.decode(Int.self, forKey: .home_captain)
        away_captain = try? container.decode(Int.self, forKey: .away_captain)
        referees = try? container.decode([Int].self, forKey: .referees)
    }
}

/// Extension for purposes of View previews
extension Match{
    static let example = Match(id: 1, league: "Prva", home_team: TeamResponse.example, away_team: TeamResponse.example, score: [2,2], court: GameCourt.example, state: 4, round: 1, date: "2021-05-14" , note: "nic", home_captain: 1, away_captain: 2, referees: [])
}

/// State of the Match corresponding to backend understanding of possible match states
enum MatchState: Int, CaseIterable, Codable{
    case undefined = -1
    case notStarted = 1
    case waitingForCaptains = 2
    case inProgress = 3
    case finished = 4
}
