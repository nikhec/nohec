//
//  Substitution.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 16/03/2021.
//

import Foundation

/// Represents substitution of two players specified by their ids during game
class Substitution: Codable{
    var incoming: Int?
    var outgoing: Int?
    
    internal init(incoming: Int? = nil, outgoing: Int? = nil) {
        self.incoming = incoming
        self.outgoing = outgoing
    }
    
    enum CodingKeys: String, CodingKey {
        case incoming
        case outgoing
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        incoming = try? container.decode(Int.self, forKey: .incoming)
        outgoing = try? container.decode(Int.self, forKey: .outgoing)
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.incoming, forKey: .incoming)
        try container.encode(self.outgoing, forKey: .outgoing)
    }
    
    func getSendFormat() -> Data {
        let encoder = JSONEncoder()
        let data = try? encoder.encode(self)
        return data!
    }
    
}

/// Extension for purposes of View previews
extension Substitution{
    static let example = Substitution(incoming: 1, outgoing: 2)
}
