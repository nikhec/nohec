//
//  Game.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 02/03/2021.
//

import Foundation

/// Representation of a single game in the match, contains all game details
class Game: Codable, Identifiable {
    var id: Int?
    var type: Int?
    var home_players: [Int]?
    var away_players: [Int]?
    var sets: [GameSet]?
    
    internal init(id: Int? = nil, type: Int? = nil, home_players: [Int]? = nil, away_players: [Int]? = nil, sets: [GameSet]? = nil) {
        self.id = id
        self.type = type
        self.home_players = home_players
        self.away_players = away_players
        self.sets = sets
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        type = try? container.decode(Int.self, forKey: .type)
        home_players = try? container.decode([Int].self, forKey: .home_players)
        away_players = try? container.decode([Int].self, forKey: .away_players)
        sets = try? container.decode([GameSet].self, forKey: .sets)
    }
    
    /// - Returns textual representation of game score, returning sets won by home team concatenated with sets won by away team
    func getScore() -> String {
        var homeScore = 0
        var awayScore = 0
        for set in sets! {
            if set.home_points! > set.away_points! {
                homeScore += 1
            } else {
                awayScore += 1
            }
        }
        return "\(homeScore):\(awayScore)"
    }
    
    /// - Returns numerical value determining winner of current game, +1 if home team wins, 0 in case of draw, -1 if away team wins
    func whoGetsPoint() -> Int {
        var homeScore = 0
        for set in sets! {
            if set.home_points! > set.away_points! {
                homeScore += 1
            }
        }
        return homeScore - 1
    }
}

/// Extension for purposes of View previews
extension Game{
    static let example = Game(id: 1, type: 2, home_players: [], away_players: [], sets: [])
}

/// Possible game types
enum GameType: String, CaseIterable, Codable{
    case SINGLE = "Single"
    case DOUBLE = "Double"
    case TRIPLE = "Triple"
}


