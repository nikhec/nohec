//
//  CardPenalty.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 16/03/2021.
//

import Foundation

/// Represents penalty player can receive during match
class CardPenalty: Codable{
    var player: Int?
    var reason: String?
    
    internal init(player: Int? = nil, reason: String? = nil) {
        self.player = player
        self.reason = reason
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        player = try? container.decode(Int.self, forKey: .player)
        reason = try? container.decode(String.self, forKey: .reason)
    }
}

/// Extension for purposes of View previews
extension CardPenalty{
    static let example = CardPenalty(player: 1, reason: "Insult")
}
