//
//  SigningsRequestObjectNoReferee.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 05/07/2021.
//

import Foundation

/// Represents object containing signings
/// Is used for sending signings request for matches without referee
class SigningsRequestObjectNoReferee: Encodable{
    var home_captain: String?
    var away_captain: String?
    
    internal init(home_captain: String? = nil, away_captain: String? = nil) {
        self.home_captain = home_captain
        self.away_captain = away_captain
    }
    
    enum CodingKeys: String, CodingKey {
        case home_captain
        case away_captain
    }
    
    /// Encoding object
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.home_captain, forKey: .home_captain)
        try container.encode(self.away_captain, forKey: .away_captain)
    }
}

/// Extension for purposes of View previews
extension SigningsRequestObjectNoReferee{
    static let example = SigningsRequestObjectNoReferee()
}
