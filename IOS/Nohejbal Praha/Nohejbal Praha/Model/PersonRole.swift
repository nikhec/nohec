//
//  PersonRole.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 02/03/2021.
//

import Foundation

struct PersonRole: Hashable, Codable {
    var type: PersonRoleType
    var naturalPerson: Person
}

enum PersonRoleType: String, CaseIterable, Codable{
    case PLAYER = "player"
    case REFEREE = "referee"
    case COUCH = "couch"
}
