//
//  ApiLeague.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 13/04/2021.
//

import Foundation

/// Represents league data for purposes of displaying basic information on leagues overview page
class LeagueListItem: Identifiable, Codable{
    var id: Int?
    var code: String?
    var name: String?
    var season: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case code
        case name
        case season
    }
    
    init(id:Int, code: String, name: String, season: Int) {
        self.id = id
        self.code = code
        self.name = name
        self.season = season
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        code = try? container.decode(String.self, forKey: .code)
        name = try? container.decode(String.self, forKey: .name)
        season = try? container.decode(Int.self, forKey: .season)
    }
}

/// Extension for purposes of View previews
extension LeagueListItem{
    static let example = LeagueListItem(id: 1, code: "ABCD", name: "Prva liga", season: 2020)
}
