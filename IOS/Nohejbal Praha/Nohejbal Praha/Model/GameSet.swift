//
//  Set.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 02/03/2021.
//

import Foundation

/// Representing smallest part of the match that is set
class GameSet: Codable, Identifiable {
    var id: Int?
    var home_points: Int?
    var away_points: Int?
    var home_timeout: Int?
    var away_timeout: Int?
    var home_substitutions: [Substitution]?
    var away_substitutions: [Substitution]?
    var homeCards: [CardPenalty]?
    var awayCards: [CardPenalty]?
    
    internal init(id: Int? = nil, homePoints: Int? = nil, awayPoints: Int? = nil, homeTimeouts: Int? = nil, awayTimeouts: Int? = nil, homeSubstitutions: [Substitution]? = nil, awaySubstitutions: [Substitution]? = nil, homeCards: [CardPenalty]? = nil, awayCards: [CardPenalty]? = nil) {
        self.id = id
        self.home_points = homePoints
        self.away_points = awayPoints
        self.home_timeout = homeTimeouts
        self.away_timeout = awayTimeouts
        self.home_substitutions = homeSubstitutions
        self.away_substitutions = awaySubstitutions
        self.homeCards = homeCards
        self.awayCards = awayCards
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        home_points = try? container.decode(Int.self, forKey: .home_points)
        away_points = try? container.decode(Int.self, forKey: .away_points)
        home_timeout = try? container.decode(Int.self, forKey: .home_timeout)
        away_timeout = try? container.decode(Int.self, forKey: .away_timeout)
        home_substitutions = try? container.decode([Substitution].self, forKey: .home_substitutions)
        away_substitutions = try? container.decode([Substitution].self, forKey: .away_substitutions)
        homeCards = try? container.decode([CardPenalty].self, forKey: .homeCards)
        awayCards = try? container.decode([CardPenalty].self, forKey: .awayCards)
    }
}

/// Extension for purposes of View previews
extension GameSet{
    static let example = GameSet(id: 1, homePoints: 10, awayPoints: 9, homeTimeouts: 1, awayTimeouts: 0, homeSubstitutions: [Substitution.example], awaySubstitutions: [Substitution.example], homeCards: [CardPenalty.example], awayCards: [CardPenalty.example])
}
