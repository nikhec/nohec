//
//  SetRequestObject.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 04/07/2021.
//

import Foundation

/// Represents object containing set data
/// Is used for sending set request
class SetRequestObject: Encodable{
    var game: Int?
    var home_points: Int?
    var away_points: Int?
    var home_timeout: Int?
    var away_timeout: Int?
    var home_substitutions : [Substitution]?
    var away_substitutions : [Substitution]?
    
    internal init(game: Int? = nil, home_points: Int? = nil, away_points: Int? = nil, home_timeout: Int? = nil, away_timeout: Int? = nil, home_substitutions: [Substitution]? = nil, away_substitutions: [Substitution]? = nil) {
        self.game = game
        self.home_points = home_points
        self.away_points = away_points
        self.home_timeout = home_timeout
        self.away_timeout = away_timeout
        self.home_substitutions = home_substitutions
        self.away_substitutions = away_substitutions
    }
    
    enum CodingKeys: String, CodingKey {
        case game
        case home_points
        case away_points
        case home_timeout
        case away_timeout
        case home_substitutions
        case away_substitutions
    }
    
    /// Encoding object
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.game, forKey: .game)
        try container.encode(self.home_points, forKey: .home_points)
        try container.encode(self.away_points, forKey: .away_points)
        try container.encode(self.home_timeout, forKey: .home_timeout)
        try container.encode(self.away_timeout, forKey: .away_timeout)
        try container.encode(self.home_substitutions, forKey: .home_substitutions)
        try container.encode(self.away_substitutions, forKey: .away_substitutions)
    }

}

/// Extension for purposes of View previews
extension SetRequestObject{
    static let example = SetRequestObject()
}
