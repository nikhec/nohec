//
//  LeagueItem.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 13/04/2021.
//

import Foundation

/// Represents one season
class Season: Identifiable, Codable{
    var id: Int?
    var year: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case year
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        year = try? container.decode(Int.self, forKey: .year)
    }
}
