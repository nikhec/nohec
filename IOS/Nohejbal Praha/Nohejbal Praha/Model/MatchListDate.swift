//
//  MatchListDate.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 31/05/2021.
//

import Foundation

/// Represents data for a bundle of matches grouped by date or round
class MatchListDate: Decodable{
    var date: String?
    var round: Int?
    var matches: [Match]?
    
    enum CodingKeys: String, CodingKey {
        case date
        case matches
        case round
    }
    
    init(date:String, matches: [Match], round: Int?) {
        self.date = date
        self.matches = matches
        self.round = round
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        date = try? container.decode(String.self, forKey: .date)
        matches = try? container.decode([Match].self, forKey: .matches)
        round = try? container.decode(Int.self, forKey: .round)
    }
}

/// Extension for purposes of View previews
extension MatchListDate{
    static let example = MatchListDate(date: "2021-05-14", matches: [], round: 1)
}
