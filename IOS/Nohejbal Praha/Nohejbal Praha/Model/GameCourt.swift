//
//  GameCourt.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 02/03/2021.
//

import Foundation


/// Representation of playing court object, contains address as well as coordinates of the venue
class GameCourt: Codable, Identifiable {
    var id: Int?
    var address: String?
    var latitude: Double?
    var longitude: Double?
    
    init(id: Int, address: String, latitude: Double, longitude: Double) {
        self.id = id
        self.address = address
        self.latitude = latitude
        self.longitude = longitude
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        address = try? container.decode(String.self, forKey: .address)
        latitude = try? container.decode(Double.self, forKey: .latitude)
        longitude = try? container.decode(Double.self, forKey: .longitude)
    }
}

/// Extension for purposes of View previews
extension GameCourt{
    static let example = GameCourt(id: 1, address: "Vodickova 25", latitude: 50.08095721983656, longitude: 14.424335285462407)
}
