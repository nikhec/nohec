//
//  InteractiveMatch.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 08/06/2021.
//

import Foundation

/// Represent all data required by interactive match operations
class InteractiveMatch: Decodable, Identifiable {
    var state: Int?
    var state_label: String?
    var date: String?
    var home_captain: Int?
    var away_captain: Int?
    var home_team: TeamResponse?
    var away_team: TeamResponse?
    var referees: [Referee]?
    var games: [Game]?
    var available_home_players: [PlayerData]?
    var available_away_players: [PlayerData]?
    var note: String?
    var court: GameCourt?
    
    enum CodingKeys: String, CodingKey {
        case state
        case state_label
        case date
        case home_captain
        case away_captain
        case home_team
        case away_team
        case referees
        case games
        case available_home_players
        case available_away_players
        case note
        case court
    }
    
    internal init(state: Int? = nil, state_label: String? = nil, date: String? = nil, home_captain: Int? = nil, away_captain: Int? = nil, home_team: TeamResponse? = nil, away_team: TeamResponse? = nil, referees: [Referee]? = nil, games: [Game]? = nil, available_home_players: [PlayerData]? = nil, available_away_players: [PlayerData]? = nil, note: String? = nil, court: GameCourt? = nil) {
        self.state = state
        self.state_label = state_label
        self.date = date
        self.home_captain = home_captain
        self.away_captain = away_captain
        self.home_team = home_team
        self.away_team = away_team
        self.referees = referees
        self.games = games
        self.available_home_players = available_home_players
        self.available_away_players = available_away_players
        self.note = note
        self.court = court
    }

    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        state = try? container.decode(Int.self, forKey: .state)
        state_label = try? container.decode(String.self, forKey: .state_label)
        date = try? container.decode(String.self, forKey: .date)
        home_captain = try? container.decode(Int.self, forKey: .home_captain)
        away_captain = try? container.decode(Int.self, forKey: .away_captain)
        home_team = try? container.decode(TeamResponse.self, forKey: .home_team)
        away_team = try? container.decode(TeamResponse.self, forKey: .away_team)
        referees = try? container.decode([Referee].self, forKey: .referees)
        games = try? container.decode([Game].self, forKey: .games)
        available_home_players = try? container.decode([PlayerData].self, forKey: .available_home_players)
        available_away_players = try? container.decode([PlayerData].self, forKey: .available_away_players)
        note = try? container.decode(String.self, forKey: .note)
        court = try? container.decode(GameCourt.self, forKey: .court)
    }
    
    /// - Returns next unfinished game in this match, nil if not existing
    func getNextGame() -> Game? {
        for game in games! {
            if game.sets!.isEmpty || game.sets!.count < 2{
                return game
            }
        }
        return nil
    }
    /// Find current state of the game
    /// - Parameters game: current game
    /// - Returns position of next set to be played, -1 if lineups are not chosen yet
    func getNextSet(game: Game) -> Int {
        if game.home_players!.isEmpty {
            return -1
        } else {
            return game.sets!.count
        }
    }
    
    /// Gets name of the game, more precisely order of this type of game with the type label of the game e.g. "2. Double"
    /// - Parameter gameId : identifier of the game
    /// - Parameter gameType: identifier of type of the game
    /// - Returns textual representation of the game
    func getGameName(gameId: Int, gameType: Int ) -> String{
        var gameLabel = ""
        switch gameType{
        case 1: gameLabel = GameType.SINGLE.rawValue
        case 2: gameLabel = GameType.DOUBLE.rawValue
        case 3: gameLabel = GameType.TRIPLE.rawValue
        default: gameLabel = ""
        }
        var priorGames = 1
        for game in games!{
            if game.id! == gameId {
                return "\(priorGames). \(NSLocalizedString("\(gameLabel)", comment:""))"
            }
            if (game.type! == gameType){
                priorGames += 1
            }
        }
        return ""
    }
    
    /// Get player name from list of players
    /// - Parameter players : list of players to search in
    /// - Parameter id : player identifier
    /// - Returns player name
    func getPlayerById(players: [PlayerData], id: Int) -> String{
        for player in players{
            if player.id == id {
                return player.name!
            }
        }
        return ""
    }
    
    /// - Returns true  if match contains game of type single, false otherwise
    func isWithSingle() -> Bool{
        return games!.contains{ $0.type == 1}
         
    }
    
    /// - Returns position of game in this match
    /// - Parameters gameId - identifier of game
    func getGamePos(gameId: Int) -> Int{
        for i in 0..<games!.count{
            if games![i].id! == gameId{
                return i
            }
        }
        return 0
    }
    
    /// - Returns list of eligible home players for specified game
    /// Rules of eligibility are specified by official rules of association
    /// - Parameter players : list of all players for the team that are to be filtered based on eligibility
    /// - Parameter currentGame : current game
    func getAvailableHomePlayers(players: [PlayerData], currentGame: Game, playersIds: [Int]) -> [PlayerData]{
        let withSingle = isWithSingle()
        var filteredPlayers : [PlayerData] = []
        let gamePos = getGamePos(gameId: currentGame.id!)
        for player in players{
            if playersIds.contains(player.id!){
                continue
            }
            switch gamePos{
            case 1:
                /// Player featured in 1. double and can't feature in 2.double
                if !games![0].home_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case 3:
                /// Player featured in 1. triple and cannot feature in 2.triple
                if !games![2].home_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case 4:
                /// Player featured in 1. or 2. double and cannot feature in 3.double
                if !games![0].home_players!.contains(where: {$0 == player.id}) && !games![1].home_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case (withSingle ? 6 : 5):
                /// Player featured in 1. triple and must feature in 3.triple
                if games![2].home_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case (withSingle ? 7 : 6):
                /// Player featured in 2. triple and must feature in 4.triple
                if games![3].home_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case (withSingle ? 8 : 7):
                /// Player featured in 1. double and must feature in 4.double
                if games![0].home_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case (withSingle ? 9 : 8):
                /// Player featured in 2. double and must feature in 5.double
                if games![1].home_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            default:
                filteredPlayers.append(player)
            }
        }
        return filteredPlayers
    }
    
    /// - Returns list of eligible away players for specified game
    /// Rules of eligibility are specified by official rules of association
    /// - Parameter players : list of all players for the team that are to be filtered based on eligibility
    /// - Parameter currentGame : current game
    /// - Parameter playersIds : ids of players in starting lineup
    func getAvailableAwayPlayers(players: [PlayerData], currentGame: Game, playersIds: [Int]) -> [PlayerData]{
        let withSingle = isWithSingle()
        var filteredPlayers : [PlayerData] = []
        let gamePos = getGamePos(gameId: currentGame.id!)
        for player in players{
            if playersIds.contains(player.id!){
                continue
            }
            switch gamePos{
            case 1:
                if !games![0].away_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case 3:
                if !games![2].away_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case 4:
                if !games![0].away_players!.contains(where: {$0 == player.id}) && !games![1].away_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case (withSingle ? 6 : 5):
                if games![3].away_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case (withSingle ? 7 : 6):
                if games![2].away_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case (withSingle ? 8 : 7):
                if games![1].away_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            case (withSingle ? 9 : 8):
                if games![0].away_players!.contains(where: {$0 == player.id}){
                    filteredPlayers.append(player)
                }
            default:
                filteredPlayers.append(player)
            }
        }
        return filteredPlayers
    }
    
    /// - Returns true if all games were played, false otherwise
    func allGamesPlayed() -> Bool{
        for game in games!{
            if (game.sets!.count < 2){
                return false
            }
        }
        return true
    }
    
    /// - Returns current game score
    func getCurrentScore() -> String{
        var homePoints : Int = 0
        var awayPoints : Int = 0
        for game in games!{
            if game.sets!.count == 2{
                switch game.whoGetsPoint(){
                case 1:
                    homePoints += 1
                case -1:
                    awayPoints += 1
                default:
                    continue
                }
            }
        }
        return "\(homePoints):\(awayPoints)"
    }
}

/// Extension for purposes of View previews
extension InteractiveMatch{
    static let example = InteractiveMatch(state: 1, state_label: "Finished", date: "", home_captain: 1, away_captain: 2, home_team: TeamResponse.example, away_team: TeamResponse.example, referees: [Referee.example], games: [Game.example], note: "", court: GameCourt.example)
}
