//
//  Person.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 08/03/2021.
//

import Foundation

struct Person: Hashable, Codable {
    var firstName: String
    var lastName: String
    var birthDate: Date
}
