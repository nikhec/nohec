//
//  TeamWithCourt.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 01/06/2021.
//

import Foundation

/// Represents response consisting of team data as well as team court information
class TeamWithCourt: Decodable, Identifiable{
    var id: Int?
    var name: String?
    var logo: String?
    var court: GameCourt?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case logo
        case court
    }
    
    init(id: Int, name: String, logo: String, court: GameCourt) {
        self.id = id
        self.name = name
        self.logo = logo
        self.court = court
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        name = try? container.decode(String.self, forKey: .name)
        logo = try? container.decode(String.self, forKey: .logo)
        court = try? container.decode(GameCourt.self, forKey: .court)
    }
}

/// Extension for purposes of View previews
extension TeamWithCourt{
    static let example = TeamWithCourt(id: 1, name: "Muzi", logo: "star", court: GameCourt.example)
}
