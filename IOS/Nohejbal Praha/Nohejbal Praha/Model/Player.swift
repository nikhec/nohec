//
//  Player.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 02/03/2021.
//

import Foundation

struct Player: Hashable, Codable, Identifiable {
    var id: Int
    var registrationNumber: String
    var role: PersonRole
}
