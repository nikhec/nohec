//
//  TeamResponse.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 10/05/2021.
//

import Foundation

/// Team detail API call response 
class TeamResponse: Decodable, Identifiable{
    var id: Int?
    var name: String?
    var logo: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case logo
    }
    
    init(id: Int, name: String, logo: String) {
        self.id = id
        self.name = name
        self.logo = logo
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try? container.decode(Int.self, forKey: .id)
        name = try? container.decode(String.self, forKey: .name)
        logo = try? container.decode(String.self, forKey: .logo)
    }
}

/// Extension for purposes of View previews
extension TeamResponse{
    static let example = TeamResponse(id: 1, name: "Muzi", logo: "star")
}
