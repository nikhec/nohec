//
//  ArrayResponse.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 07/05/2021.
//

import Foundation

/// General API class for response in the form of array, decoding from array
class ArrayResponse<T:Decodable>: Decodable{
    var items: [T]?
    
    /// Decoding and filling array with objects of type T
    required init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        items = []
        while !container.isAtEnd {
            let item = try? container.decode(T.self)
            items!.append(item!)
        }
    }
}
