//
//  ClubResponse.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 07/05/2021.
//

import Foundation

/// Club detail API call response
class ClubResponse: Decodable{
    var teams: [TeamWithCourt]?
    
    enum CodingKeys: String, CodingKey {
        case teams
        case court
    }
    
    init(teams: [TeamWithCourt]) {
        self.teams = teams
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        teams = try? container.decode([TeamWithCourt].self, forKey: .teams)
    }
}

/// Extension for purposes of View previews
extension ClubResponse{
    static let example = ClubResponse(teams: [TeamWithCourt.example, TeamWithCourt.example, TeamWithCourt.example])
}
