//
//  LoginResponse.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 29/03/2021.
//

import Foundation

/// Login API call response
class LoginResponse: Decodable{
    var username: String?
    var password: String?
    var token: String?
    
    enum CodingKeys: String, CodingKey {
        case username
        case password
        case token
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        username = try? container.decode(String.self, forKey: .username)
        password = try? container.decode(String.self, forKey: .password)
        token = try? container.decode(String.self, forKey: .token)
    }
}
