//
//  MatchDetail.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 31/05/2021.
//

import Foundation

/// Represents detailed data for a single Match
class MatchDetailData: Decodable, Identifiable {
    var state: Int?
    var date: String?
    var home_captain: Int?
    var away_captain: Int?
    var home_team: TeamResponse?
    var away_team: TeamResponse?
    var referees: [Referee]?
    var games: [Game]?
    var home_players: [PlayerData]?
    var away_players: [PlayerData]?
    
    enum CodingKeys: String, CodingKey {
        case state
        case date
        case home_captain
        case away_captain
        case home_team
        case away_team
        case referees
        case games
        case home_players
        case away_players
    }
    
    internal init(state: Int? = nil, date: String? = nil, home_captain: Int? = nil, away_captain: Int? = nil, home_team: TeamResponse? = nil, away_team: TeamResponse? = nil, referees: [Referee]? = nil, games: [Game]? = nil, home_players: [PlayerData]? = nil, away_players: [PlayerData]? = nil) {
        self.state = state
        self.date = date
        self.home_captain = home_captain
        self.away_captain = away_captain
        self.home_team = home_team
        self.away_team = away_team
        self.referees = referees
        self.games = games
        self.home_players = home_players
        self.away_players = away_players
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        state = try? container.decode(Int.self, forKey: .state)
        date = try? container.decode(String.self, forKey: .date)
        home_captain = try? container.decode(Int.self, forKey: .home_captain)
        away_captain = try? container.decode(Int.self, forKey: .away_captain)
        home_team = try? container.decode(TeamResponse.self, forKey: .home_team)
        away_team = try? container.decode(TeamResponse.self, forKey: .away_team)
        referees = try? container.decode([Referee].self, forKey: .referees)
        games = try? container.decode([Game].self, forKey: .games)
        home_players = try? container.decode([PlayerData].self, forKey: .home_players)
        away_players = try? container.decode([PlayerData].self, forKey: .away_players)
    }
    
    /// Find player by id from home players
    /// - Parameter playerId : identifier of player to return
    /// - Returns player data
    func getHomePlayer(playerId:Int) -> PlayerData?{
        for player in home_players! {
            if player.id == playerId {
                return player
            }
        }
        return nil
    }
    
    /// Find player by id from away players
    /// - Parameter playerId : identifier of player to return
    /// - Returns player data
    func getAwayPlayer(playerId:Int) -> PlayerData?{
        for player in away_players! {
            if player.id == playerId {
                return player
            }
        }
        return nil
    }
    
    /// Check if player was involved in any substitution during the game
    /// - Parameter playerId : identifier of player to return
    /// - Parameter gameId : identifier of game
    /// - Parameter isHome : flag representing wheter caller is interested in home team
    /// - Returns true if player is involved in substitution (either subbed on or subbed off), false otherwise
    func isPlayerInSubstitution(playerId:Int, gameId: Int, isHome: Bool) -> Bool{
        for game in games!{
            if game.id! == gameId {
                for set in game.sets! {
                    for sub in isHome ? set.home_substitutions! : set.away_substitutions!{
                        if sub.incoming == playerId || sub.outgoing == playerId{
                            return true
                        }
                    }
                }
                return false
            }
        }
        return false
    }
    
    /// Gets name of the game, more precisely order of this type of game with the type label of the game e.g. "2. Double"
    /// - Parameter gameId : identifier of the game
    /// - Parameter gameLabel: type label of the game
    /// - Parameter gameType: identifier of type of the game
    /// - Returns textual representation of the game
    func getGameName(gameId: Int, gameType: Int ) -> String{
        var gameLabel = ""
        switch gameType{
        case 1: gameLabel = GameType.SINGLE.rawValue
        case 2: gameLabel = GameType.DOUBLE.rawValue
        case 3: gameLabel = GameType.TRIPLE.rawValue
        default: gameLabel = ""
        }
        var priorGames = 1
        for game in games!{
            if game.id! == gameId {
                return "\(priorGames). \(NSLocalizedString("\(gameLabel)", comment:""))"
            }
            if (game.type! == gameType){
                priorGames += 1
            }
        }
        return ""
    }
    /// Returns game state label
    func getGameStateLabel() -> String{
        switch state{
        case 1: return "notStarted"
        case 2: return "waitingForCaptains"
        case 3: return "inProgress"
        case 4: return "finished"
        default: return ""
        }
    }
}

/// Extension for purposes of View previews
extension MatchDetailData{
    static let example = MatchDetailData(state: 1, date: "", home_captain: 1, away_captain: 2, home_team: TeamResponse.example, away_team: TeamResponse.example, referees: [Referee.example], games: [Game.example])
}
