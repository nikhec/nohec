//
//  Standing.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 19/02/2021.
//

import Foundation

/// Class representing Team standing object in League detail, for purposes of league table
class Standing: Identifiable, Decodable {
    var id: Int?
    var name:String?
    var logo: String?
    var position:Int?
    var matches:Int?
    var wins:Int?
    var draws:Int?
    var losses:Int?
    var sets_won:Int?
    var sets_lost:Int?
    var points:Int?
    
    enum CodingKeys: String, CodingKey {
        case position
        case matches
        case wins
        case draws
        case losses
        case sets_won
        case sets_lost
        case points
        case team
    }
    
    enum TeamKeys: String, CodingKey{
        case id
        case name
        case logo
    }
    
    init(id: Int, name: String, logo: String, position: Int, matches: Int, wins: Int, draws: Int, losses:Int, sets_won: Int, sets_lost: Int, points: Int) {
        self.id = id
        self.name = name
        self.logo = logo
        self.position = position
        self.matches = matches
        self.wins = wins
        self.draws = draws
        self.losses = losses
        self.sets_won = sets_won
        self.sets_lost = sets_lost
        self.points = points
    }
    
    /// Decoding objects coded by response keys to corresponding fields
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let team = try? container.nestedContainer(keyedBy: TeamKeys.self, forKey: .team)
        id = try? team?.decode(Int.self, forKey: .id)
        name = try? team?.decode(String.self, forKey: .name)
        logo = try? team?.decode(String.self, forKey: .logo)
        position = try? container.decode(Int.self, forKey: .position)
        matches = try? container.decode(Int.self, forKey: .matches)
        wins = try? container.decode(Int.self, forKey: .wins)
        draws = try? container.decode(Int.self, forKey: .draws)
        losses = try? container.decode(Int.self, forKey: .losses)
        sets_won = try? container.decode(Int.self, forKey: .sets_won)
        sets_lost = try? container.decode(Int.self, forKey: .sets_lost)
        points = try? container.decode(Int.self, forKey: .points)
    }
}

/// Extension for purposes of View previews
extension Standing{
    static let example = Standing(id: 1, name: "Slavia", logo: "", position: 1, matches: 5, wins: 4, draws: 1, losses:0, sets_won: 12, sets_lost: 3, points: 8)
}
