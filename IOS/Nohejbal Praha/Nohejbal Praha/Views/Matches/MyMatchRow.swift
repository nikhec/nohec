//
//  MyMatchRow.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 02/07/2021.
//

import SwiftUI

/// Match row for user's match contains match info, and start match button
struct MyMatchRow: View {
    var id: Int
    var home_team : TeamResponse
    var away_team: TeamResponse
    var score: [Int]
    var finished: Bool
    var date: String?
    var match: Match
    @ObservedObject var matchEnterViewModel = MatchEnterViewModel()
    
    init(match: Match) {
        self.id = match.id!
        self.home_team = match.home_team!
        self.away_team = match.away_team!
        self.score = match.score!
        self.finished = match.state == MatchState.finished.rawValue
        self.date = match.date!
        self.match = match
    }
    
    var body: some View {
        VStack{
            GeometryReader{ geometry in
                HStack(){
                    Spacer()
                    Text(home_team.name!)
                        .frame(maxWidth: geometry.size.width * 0.40, alignment: .trailing)
                        .font(.system(size: 13))
                    ScrollView{
                    AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(home_team.logo!)")!)
                        .aspectRatio(contentMode: .fit)
                        .frame(width: geometry.size.width * 0.06, height: geometry.size.width * 0.06)
                    }
                    .frame(width: geometry.size.width * 0.06, height: geometry.size.width * 0.06)
                    if (finished){
                    Text("\(score[0]):\(score[1])" )
                        .frame(maxWidth:geometry.size.width * 0.1, alignment: .center)
                        .font(.system(size: 13))
                        .padding(.top, 3)
                        .padding(.bottom, 3)
                        .background(Color("LightGray"))
                        .cornerRadius(6)
                    } else {
                        VStack{
                            if (date != nil){
                                if Content.base_daytime_formatter.date(from: date!) != nil{
                                Text(Content.time_formatter.string(from: Content.base_daytime_formatter.date(from: date!)!))
                                    .font(.system(size: 12))
                                Text(Content.day_formatter.string(from: Content.base_daytime_formatter.date(from: date!)!))
                                    .font(.system(size: 10))
                                } else {
                                    Text(" : ")
                                        .font(.system(size: 14))
                                }
                            } else {
                                Text(" : ")
                                    .font(.system(size: 14))
                            }
                        }
                        .padding(.leading, 5)
                        .padding(.trailing, 5)
                    }
                    ScrollView{
                        AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(away_team.logo!)")!)
                            .aspectRatio(contentMode: .fit)
                            .frame(width: geometry.size.width * 0.06, height: geometry.size.width * 0.06)
                    }
                    .frame(width: geometry.size.width * 0.06, height: geometry.size.width * 0.06)
                    Text(away_team.name!)
                        .frame(maxWidth: geometry.size.width * 0.40, alignment: .leading)
                        .font(.system(size: 13))
                    Spacer()
                }
            }
                    HStack{
                        Spacer()
                        Text("startInteractive")
                            .font(.system(size: 13))
                            .foregroundColor(.white)
                            .padding(8)
                            .background(Color("Primary"))
                            .cornerRadius(6.0)
                        Spacer()
                    }
        }
        .frame(height:80)
        .foregroundColor(.black)
        .padding(5)
    }
}

struct MyMatchRow_Previews: PreviewProvider {
    static var previews: some View {
        MyMatchRow(match: Match.example)
    }
}
