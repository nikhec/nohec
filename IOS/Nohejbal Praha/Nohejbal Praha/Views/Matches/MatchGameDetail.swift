//
//  MatchDetail.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 25/05/2021.
//

import SwiftUI

/// Page showing detail information of single game of the match
struct MatchGameDetail: View {
    var id: Int
    var game: Game
    var match: MatchDetailData
    var score: [Int]
    var gameScore: String
    var intMatch: InteractiveMatch?
    
    init(id: Int, game: Game, matchDetail: InteractiveMatch, score: [Int], gameScore: String){
        self.id = id
        self.game = game
        self.match = MatchDetailData(state: matchDetail.state, date: matchDetail.date, home_captain: matchDetail.home_captain, away_captain: matchDetail.away_captain, home_team: matchDetail.home_team, away_team: matchDetail.away_team, referees: matchDetail.referees, games: matchDetail.games, home_players: matchDetail.available_home_players, away_players: matchDetail.available_away_players)
        self.score = score
        self.gameScore = gameScore
        self.intMatch = matchDetail
    }
    init(id: Int, game: Game, match: MatchDetailData, score: [Int], gameScore: String){
        self.id = id
        self.game = game
        self.match = match
        self.score = score
        self.gameScore = gameScore
        self.intMatch = nil
    }
    
    var body: some View {
        ScrollView{
            VStack{
                if match.state == MatchState.finished.rawValue{
                    MatchRow(id: id, home_team: match.home_team!, away_team: match.away_team!, score: score, finished: match.state == MatchState.finished.rawValue, date: match.date!)
                    .padding(.bottom, 30)
                    .padding(.top, 30)
                } else {
                    InteractiveMatchRow(match: intMatch!)
                        .padding(.bottom, 30)
                        .padding(.top, 30)
                }
                MatchPartResultRow(matchName: match.getGameName(gameId: game.id!, gameType: game.type!), matchResult: gameScore)
                    .font(.headline)
                    .padding(.bottom, 30)
                HStack{
                    ///
                    /// Home team name with players with substitution indicator if they are included in any substitution
                    VStack(spacing: 0){
                        HStack{
                            AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(match.home_team!.logo!)")!)
                                .aspectRatio(contentMode: .fit)
                                .frame(width:20, height: 20)
                            Text("\(match.home_team!.name!)")
                            Spacer()
                        }
                        .padding(.bottom, 10)
                        ForEach(0..<game.home_players!.count) { i in
                            HStack{
                                Text("\(match.getHomePlayer(playerId: game.home_players![i])!.name!)")
                                    .padding(.bottom, 2)
                                if (match.isPlayerInSubstitution(playerId: game.home_players![i], gameId: game.id!, isHome: true)){
                                    Image("baseline_swap_vert_black_24pt")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(height:20)
                                }
                                Spacer()
                            }
                        }
                        ForEach(game.sets!) { set in
                            ForEach(0..<set.home_substitutions!.count){ i in
                                HStack{
                                    Text("\(match.getHomePlayer(playerId: set.home_substitutions![i].incoming!)!.name!)")
                                        .padding(.bottom, 2)
                                    Image("baseline_swap_vert_black_24pt")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(height:20)
                                    Spacer()
                                }
                            }
                        }
                    }
                    Spacer()
                }
                .padding(.leading, 30)
                HStack{
                    ///
                    /// Away team name with players with substitution indicator if they are included in any substitution
                    Spacer()
                    VStack(spacing: 0){
                        HStack{
                            Spacer()
                            AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(match.away_team!.logo!)")!)
                                .aspectRatio(contentMode: .fit)
                                .frame(width:20, height: 20)
                            Text("\(match.away_team!.name!)")
                        }
                        .padding(.bottom, 10)
                        ForEach(0..<game.away_players!.count) { i in
                            HStack{
                                Spacer()
                                if (match.isPlayerInSubstitution(playerId: game.away_players![i], gameId: game.id!, isHome: false)){
                                    Image("baseline_swap_vert_black_24pt")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(height:20)
                                }
                                Text("\(match.getAwayPlayer(playerId: game.away_players![i])!.name!)")
                                    .padding(.bottom, 2)
                            }
                        }
                        ForEach(game.sets!) { set in
                            ForEach(0..<set.away_substitutions!.count){ i in
                                HStack{
                                    Spacer()
                                    Image("baseline_swap_vert_black_24pt")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(height:20)
                                    Text("\(match.getAwayPlayer(playerId: set.away_substitutions![i].incoming!)!.name!)")
                                        .padding(.bottom, 2)
                                }
                            }
                        }
                    }
                }
                .padding(.trailing, 30)
                Text(gameScore)
                ///
                /// Results of set by set
                ForEach(game.sets!) { set in
                    VStack{
                        Image("outline_sports_soccer_black_24pt")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(height:20)
                        Text("\(set.home_points!):\(set.away_points!)")
                    }
                }
            }
            .navigationBarTitle("matchDetail")
        }
    }
}

struct MatchDetail_Previews: PreviewProvider {
    static var previews: some View {
        MatchGameDetail(id: 1, game: Game.example, match: MatchDetailData.example, score: [1,1], gameScore: "2:0")
    }
}
