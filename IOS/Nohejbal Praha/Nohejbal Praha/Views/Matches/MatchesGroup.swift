//
//  MatchesGroup.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 23/02/2021.
//

import SwiftUI

/// Match group section header with date
struct MatchesGroup: View {
    var title: LocalizedStringKey
    var body: some View {
        VStack(){
            GeometryReader { geometry in
                HStack(spacing: 0){
                    Image("baseline_calendar_today_black_18pt")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(height:18)
                    Text(title)
                        .font(.headline)
                        .padding(.leading, 10)
                        .frame(maxWidth: geometry.size.width * 0.8, alignment: .leading)
                }
            }
            .frame(height: 20, alignment: .center)
            .padding(8)
            .padding(.top, 10)
            .background(Color("LightGray"))
        }
    }
}

struct MatchesGroup_Previews: PreviewProvider {
    static var previews: some View {
        MatchesGroup(title: "2.2.")
    }
}
