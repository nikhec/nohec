//
//  PastMatchesOverview.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 08/06/2021.
//

import SwiftUI

struct MyMatchesOverview: View {
    @ObservedObject var viewModel = MatchesViewModel()
    var count = 100
    var body: some View {
        NavigationView {
            switch viewModel.loadingState {
            case .idle:
                Color.clear.onAppear {
                    viewModel.getMyPastMatchesByDate(count: count)
                }
            case .loading:
                CustomProgressView(){
                    Color.clear
                }
            case .failed(let error):
                ErrorView(errorCode:error)
            case .loaded:
                List{
                    ForEach(0..<viewModel.matches.count) { i in
                        MatchesGroup(title: "\(viewModel.matches[i].date!)", expandAllowed: false)
                            .listRowInsets(EdgeInsets())
                        ForEach(viewModel.matches[i].matches!) { match in
                            NavigationLink(destination: MatchOverview(match: match)) {
                                MatchRow(match: match)
                            }
                            .padding(2)
                        }
                    }
                }
                .navigationBarTitle("myMatchesTitle")
                }
            }
            .onAppear{ viewModel.loadingState = .idle}
    }
}

struct PastMatchesOverview_Previews: PreviewProvider {
    static var previews: some View {
        MyMatchesOverview()
    }
}
