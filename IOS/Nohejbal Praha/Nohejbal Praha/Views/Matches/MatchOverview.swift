//
//  MatchOverview.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 25/05/2021.
//

import SwiftUI

/// Page showing overview of single match
struct MatchOverview: View {
    var match: Match
    var userMatch: Bool
    @ObservedObject var viewModel = MatchDetailViewModel()
    @ObservedObject var matchEnterViewModel = MatchEnterViewModel()
    
    var body: some View {
        ScrollView{
            /// Display content of page based on viewmodel state
            switch viewModel.loadingState {
            case .idle:
            /// Empty page call viewmodel to get content
                Color.clear.onAppear {
                    viewModel.getMatchDetail(matchId: match.id!)
                }
            case .loading:
            /// Loading indicator
                let screenSize = UIScreen.main.bounds.size
                CustomProgressView(){
                    Color.clear
                }
                .frame(width: screenSize.width, height: screenSize.height - 100)
            case .failed(let error):
            /// Error message
                ErrorView(errorCode:error)
            case .loaded:
                /// Content of page showing match information data, and list of all games with results
                    GeometryReader { geometry in
                        VStack{
                        HStack{
                            Spacer()
                                VStack{
                                    AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(match.home_team!.logo!)")!)
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width:80, height: 80)
                                    Text("\(viewModel.match!.home_team!.name!)")
                                        .font(.system(size: 15))
                                }
                                .frame(maxWidth: geometry.size.width * 0.35)
                                Text("\(match.score![0]):\(match.score![1])")
                                    .font(.title)
                                    .frame(maxWidth: geometry.size.width * 0.2)
                                VStack{
                                    AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(match.away_team!.logo!)")!)
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width:80, height: 80)
                                    Text("\(viewModel.match!.away_team!.name!)")
                                        .font(.system(size: 15))
                                }
                                .frame(maxWidth: geometry.size.width * 0.35)
                            Spacer()
                        }
                        /// Match state label
                            TextWithBg(text: viewModel.match!.getGameStateLabel())
                            .padding(.bottom, 20)
                        HStack{
                            if viewModel.match!.state! != MatchState.notStarted.rawValue {
                                Text("matchesTitle")
                                    .padding(.leading, 35)
                                    .font(.system(size: 20, weight: .bold))
                                Spacer()
                            }
                        }
                        /// List of all games
                        ForEach(viewModel.match!.games!) { game in
                            NavigationLink(destination: MatchGameDetail(id: match.id!, game: game, match: viewModel.match!, score: match.score!, gameScore: game.getScore())) {
                                MatchPartResultRow(matchName: viewModel.match!.getGameName(gameId: game.id!, gameType: game.type!), matchResult:game.getScore())
                                .padding(3)
                                    .padding(.bottom, 0)
                                    .padding(.top,0)
                            }
                        }
                    }
                        .padding(.top, 40)
                        .navigationBarTitle("matchDetail", displayMode: .inline)
                    }
                
            }
        }
        .onAppear{
            viewModel.loadingState = .idle
        }
    }
}

struct MatchOverview_Previews: PreviewProvider {
    static var previews: some View {
        MatchOverview(match: Match.example, userMatch: false)
    }
}
