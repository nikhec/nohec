//
//  MatchPartResultRow.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 25/05/2021.
//

import SwiftUI

/// Simple row displaying game name with result
struct MatchPartResultRow: View {
    var matchName: String
    var matchResult: String
    var body: some View {
        HStack{
            Text(matchName)
                .padding(.leading, 30)
                .foregroundColor(.black)
            Spacer()
            Text(matchResult != "0:0" ? matchResult : "")
                .padding(.trailing, 30)
                .foregroundColor(.black)
        }
    }
}

struct MatchPartResultRow_Previews: PreviewProvider {
    static var previews: some View {
        MatchPartResultRow(matchName: "Dvojka", matchResult: "2:0")
    }
}
