//
//  MatchRow.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 23/02/2021.
//

import SwiftUI

/// Component displaying basic row with team names
struct MatchRow: View {
    var id: Int
    var home_team : TeamResponse
    var away_team: TeamResponse
    var score: [Int]
    var finished: Bool
    var date: String?
    @ObservedObject var matchEnterViewModel = MatchEnterViewModel()
    
    init(id: Int, home_team: TeamResponse, away_team: TeamResponse, score: [Int], finished: Bool, date: String) {
        self.id = id
        self.home_team = home_team
        self.away_team = away_team
        self.score = score
        self.finished = finished
        self.date = date
    }
    
    init(match: Match) {
        self.id = match.id!
        self.home_team = match.home_team!
        self.away_team = match.away_team!
        self.score = match.score!
        self.finished = match.state == MatchState.finished.rawValue
        self.date = match.date!
    }
    
    var body: some View {
        VStack{
            GeometryReader{ geometry in
                HStack(){
                    Spacer()
                    /// Home team
                    Text(home_team.name!)
                        .frame(maxWidth: geometry.size.width * 0.40, alignment: .trailing)
                        .font(.system(size: 13))
                    AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(home_team.logo!)")!)
                        .aspectRatio(contentMode: .fit)
                        .frame(width: geometry.size.width * 0.06, height: geometry.size.width * 0.06)
                    ///
                    /// Based on match state show result or date
                    if (finished){
                    Text("\(score[0]):\(score[1])" )
                        .frame(maxWidth:geometry.size.width * 0.1, alignment: .center)
                        .font(.system(size: 13))
                        .padding(.top, 3)
                        .padding(.bottom, 3)
                        .background(Color("LightGray"))
                        .cornerRadius(6)
                    } else {
                        VStack{
                            if (date != nil){
                                if Content.base_daytime_formatter.date(from: date!) != nil{
                                Text(Content.time_formatter.string(from: Content.base_daytime_formatter.date(from: date!)!))
                                    .font(.system(size: 12))
                                Text(Content.day_formatter.string(from: Content.base_daytime_formatter.date(from: date!)!))
                                    .font(.system(size: 10))
                                } else {
                                    Text(" : ")
                                        .font(.system(size: 14))
                                }
                            } else {
                                Text(" : ")
                                    .font(.system(size: 14))
                            }
                        }
                        .frame(maxWidth:geometry.size.width * 0.1, alignment: .center)
                        .padding(.top, 3)
                        .padding(.bottom, 3)
                    }
                    /// Away team
                        AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(away_team.logo!)")!)
                            .aspectRatio(contentMode: .fit)
                            .frame(width: geometry.size.width * 0.06, height: geometry.size.width * 0.06)
                    Text(away_team.name!)
                        .frame(maxWidth: geometry.size.width * 0.40, alignment: .leading)
                        .font(.system(size: 13))
                    Spacer()
                }
            }
        }
        .frame(height:20)
        .foregroundColor(.black)
        .padding(5)
    }
}

struct MatchRow_Previews: PreviewProvider {
    static var previews: some View {
        MatchRow(match: Match.example)
    }
}
