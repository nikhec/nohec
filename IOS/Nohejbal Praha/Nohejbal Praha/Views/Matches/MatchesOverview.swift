//
//  MatchesOverview.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 16/02/2021.
//

import SwiftUI

/// Page showing list of matches grouped by date
struct MatchesOverview: View {
    @ObservedObject var viewModel = MatchesViewModel()
    var count = Content.matchesCount
    var body: some View {
        NavigationView {
            /// Display content of page based on viewmodel state
            switch viewModel.loadingState {
            case .idle:
            /// Empty page call viewmodel to get content
                Color.clear.onAppear {
                    viewModel.getPastMatchesByDate(count: count)
                }
            case .loading:
            /// Loading indicator
                CustomProgressView(){
                    Color.clear
                }
            case .failed(let error):
            /// Error message
                ErrorView(errorCode:error)
            case .loaded:
            /// Content of page showing matches data, grouped by date
            List{
                ForEach(1..<viewModel.matches.count) { i in
                    MatchesGroup(title: "\(Content.date_formatter.string(from: Content.base_date_formatter.date(from: viewModel.matches[i].date!)!))")
                        .listRowInsets(EdgeInsets())
                    ForEach(viewModel.matches[i].matches!) { match in
                        NavigationLink(destination: MatchOverview(match: match, userMatch: false)) {
                            MatchRow(match: match)
                        }
                        .padding(2)
                    }
                }
            }
            .navigationBarTitle("matchesTitle")
            }
        }
        .onAppear{ viewModel.loadingState = .idle}
    }
}

struct MatchesOverview_Previews: PreviewProvider {
    static var previews: some View {
        MatchesOverview()
    }
}
