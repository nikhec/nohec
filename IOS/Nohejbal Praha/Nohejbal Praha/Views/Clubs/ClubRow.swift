//
//  ClubRow.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 01/03/2021.
//

import SwiftUI

/// View consisting of name and logo of a club displayed in club list
struct ClubRow: View {
    var club: ClubListItem
    var body: some View {
        HStack{
            AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(club.logo!)")!)
                .aspectRatio(contentMode: .fit)
                .frame(width: 30, height: 30)
            Text("\(club.name!)")
        }
    }
}

struct ClubRow_Previews: PreviewProvider {
    static var previews: some View {
        ClubRow(club: ClubListItem.example)
    }
}
