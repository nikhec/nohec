//
//  ClubDetail.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 01/03/2021.
//

import SwiftUI

/// Club detail page displaying name, logo, all teams and court information, navigated from clubs list
struct ClubDetail: View {
    var club: ClubListItem
    @ObservedObject var viewModel = ClubViewModel()
    var body: some View {
        ScrollView{
            /// Display content of page based on viewmodel state
            switch viewModel.loadingState {
            case .idle:
            /// Empty page call viewmodel to get content
                Color.clear.onAppear { viewModel.getClub(clubId: club.id!, latest_season: true)}
            case .loading:
            /// Loading indicator
                let screenSize = UIScreen.main.bounds.size
                CustomProgressView(){
                    Color.clear
                }
                .frame(width: screenSize.width, height: screenSize.height - 100, alignment: .center)
            case .failed(let error):
            /// Error message
                ErrorView(errorCode:error)
            case .loaded:
            /// Content of page showing all club data
                VStack{
                    VStack(alignment: .center){
                        AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(club.logo!)")!)
                            .aspectRatio(contentMode: .fit)
                            .frame(width:100, height: 100)
                        Text("\(club.name!)")
                            .font(.title)
                        Divider()
                    }
                    VStack(alignment: .leading){
                        SectionHeader(title: "clubTeams")
                            .font(.headline)
                            .padding(.bottom, 50)
                        ForEach(viewModel.club!.teams!) { team in
                            HStack{
                                AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(club.logo!)")!)
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width:20, height: 20)
                                Text(team.name!)
                                    .font(.system(size: 14))
                            }
                            .padding(2)
                        }
                        .padding(.leading, 8)
                        SectionHeader(title: "clubGround")
                            .font(.headline)
                            .padding(.bottom, 40)
                            .padding(.top, 40)
                        Text("clubAddress")
                            .font(.system(size: 15, weight: .semibold))
                            .padding(.leading, 10)
                            .padding(.bottom, 2)
                        Text("\(viewModel.club!.teams![0].court!.address!)")
                            .padding(.leading, 10)
                            .padding(.bottom, 20)
                            .font(.system(size: 15))
                        
                        if viewModel.club!.teams![0].court != nil {
                            MapView(latitude:viewModel.club!.teams![0].court!.latitude!, longitude: viewModel.club!.teams![0].court!.longitude!)
                                    .frame(height: 250, alignment: .center)
                        }
                    }
                }
            }
        }
        .onAppear{ viewModel.loadingState = .idle}
    }
}

struct ClubDetail_Previews: PreviewProvider {
    static var previews: some View {
        ClubDetail(club: ClubListItem.example)
    }
}
