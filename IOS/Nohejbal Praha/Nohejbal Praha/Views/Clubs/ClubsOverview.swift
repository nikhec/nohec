//
//  ClubsOverview.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 16/02/2021.
//

import SwiftUI

/// Page displaying list of all clubs
struct ClubsOverview: View {
    @ObservedObject var viewModel = ClubsViewModel()
    @State var searchText : String = ""
    var body: some View {
        NavigationView {
            /// Display content of page based on viewmodel state
            switch viewModel.loadingState {
            case .idle:
            /// Empty page call viewmodel to get content
                Color.clear.onAppear(perform: viewModel.getClubs)
            case .loading:
            /// Loading indicator
                CustomProgressView(){
                    Color.clear
                }
            case .failed(let error):
            /// Error message in case of failure
                ErrorView(errorCode:error)
            case .loaded:
            /// Content of page showing list of clubs with additional filtering from search bar
                VStack{
                SearchBar(text: $searchText)
                    .padding(.trailing, 20)
                    .padding(.leading, 20)
                List {
                    ForEach(viewModel.clubs.filter({searchText.isEmpty ? true : $0.name!.lowercased().contains(searchText.lowercased())})) { club in
                    NavigationLink(destination: ClubDetail(club:club)){
                        ClubRow(club: club)
                        }
                    }
                }
                .listStyle(PlainListStyle())
                .navigationBarTitle("clubsTitle")
                }
            }
        }
        .onAppear{ viewModel.loadingState = .idle}
    }
}

struct ClubsOverview_Previews: PreviewProvider {
    static var previews: some View {
        ClubsOverview(searchText: "")
    }
}
