//
//  LeagueMatchesExpanded.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 07/06/2021.
//

import SwiftUI

/// Page to show matches for league, switchable between past and future
struct LeagueMatchesExpanded: View {
    var league: LeagueListItem
    var count = Content.matchesCount
    var selection: Bool
    @ObservedObject var pastMatchesViewModel = MatchesViewModel()
    @ObservedObject var futureMatchesViewModel = MatchesViewModel()
    var body: some View {
        /// Display content of page based on viewmodel state
            switch !selection ? futureMatchesViewModel.loadingState :  pastMatchesViewModel.loadingState {
            case .idle:
            /// Empty page call viewmodel to get content
                Color.clear.onAppear{ !selection ?
                    futureMatchesViewModel.getLeagueFutureMatchesByRound(leagueId: league.id!, count: count) : pastMatchesViewModel.getLeaguePastMatchesByRound(leagueId: league.id!, count: count) }
            case .loading:
            /// Loading indicator
                let screenSize = UIScreen.main.bounds.size
                CustomProgressView(){
                    Color.clear
                }
                .frame(width: screenSize.width, height: 500)
            case .failed(let error):
            /// Error message
                ErrorView(errorCode:error)
            case .loaded:
            /// Content  loaded according to user's selection
                List{
                if selection {
                    /// Past matches
                    ForEach(0..<pastMatchesViewModel.matches.count){ i in
                        MatchesGroup(title: "\(pastMatchesViewModel.matches[i].round!). \(NSLocalizedString("round", comment: ""))")
                                .listRowInsets(EdgeInsets())
                        ForEach(pastMatchesViewModel.matches[i].matches!){ match in
                            NavigationLink(destination: MatchOverview(match: match, userMatch: false)){
                                MatchRow(match: match)
                            }
                            .padding(2)
                        }
                    }
                } else {
                    /// Future matches
                    ForEach(0..<futureMatchesViewModel.matches.count){ i in
                        MatchesGroup(title: "\(futureMatchesViewModel.matches[i].round!).\(NSLocalizedString("round", comment: ""))")
                            .listRowInsets(EdgeInsets())
                        ForEach(futureMatchesViewModel.matches[i].matches!){ match in
                            NavigationLink(destination: MatchOverview(match: match, userMatch: false)) {
                                MatchRow(match: match)
                            }
                            .padding(2)
                        }
                    }
                }
                }
                .navigationBarTitle(league.name!)
        }
        
    }
}

struct LeagueMatchesExpanded_Previews: PreviewProvider {
    static var previews: some View {
        LeagueMatchesExpanded(league: LeagueListItem.example, selection:true)
    }
}
