//
//  LeaguesOverview.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 16/02/2021.
//

import SwiftUI

/// Page showing all leagues list filtered by season
struct LeaguesOverview: View {
    @ObservedObject var viewModel = LeaguesViewModel()
    @ObservedObject var seasonsViewModel = SeasonsViewModel()
    @State var seasonSelected = 0
    @State var seasonId = 0
    @State var seasonYear = 0
    @State var selectSeasonSheet = false
    var body: some View {
        NavigationView {
            /// Display content of page based on viewmodel state
            switch seasonsViewModel.loadingState {
            case .idle:
            /// Empty page call viewmodel to get content
                Color.clear.onAppear(perform: seasonsViewModel.getSeasons)
            case .loading:
            /// Loading indicator
                CustomProgressView(){
                    Color.clear
                }
            case .failed(let error):
            /// Error message
                ErrorView(errorCode:error)
            case .loaded:
            /// Content of page prepared to load data for chosen season
                switch viewModel.loadingState {
                case .idle:
                    Color.clear.onAppear(perform: self.$seasonId.wrappedValue == 0 ? viewModel.getLeaguesLatestSeason : viewModel.getLeagues)
                case .loading:
                    CustomProgressView(){
                        Color.clear
                    }
                case .failed(let error):
                    ErrorView(errorCode:error)
                case .loaded:
                    /// Content of leagues for chosen season loaded
                    VStack{
                        HStack{
                            Spacer()
                            Text(
                                String("\(self.$seasonYear.wrappedValue == 0 ? seasonsViewModel.seasons[seasonsViewModel.seasons.endIndex-1].year! : seasonYear)"))
                                .foregroundColor(.blue)
                                .sheet(isPresented: $selectSeasonSheet, content: {
                                    SeasonSettings(parentSelf: self, seasons: seasonsViewModel.seasons, viewModel: viewModel)
                                })
                                .simultaneousGesture(TapGesture().onEnded{
                                    selectSeasonSheet = true
                                })
                            Image("outline_expand_more_black_24pt")
                                .resizable()
                                .frame(width: 20, height: 20)
                                .foregroundColor(.blue)
                                .padding(.trailing, 20)
                            }
                        List {
                            ForEach(viewModel.leagues) { league in
                                if (self.$seasonId.wrappedValue == 0 || league.season == self.$seasonId.wrappedValue) {
                                    NavigationLink(destination: LeagueDetail(league: league)) {
                                        LeagueRow(league: league)
                                            .listRowInsets(EdgeInsets())
                                            
                                    }
                                }
                            }
                        }
                        .listStyle(PlainListStyle())
                        .navigationBarTitle("leaguesTitle")
                    }
                }
            }
        }
        .onAppear{ viewModel.loadingState = .idle}
    }
    
    /// Season choosing setting
    struct SeasonSettings: View {
        @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
        var parentSelf: LeaguesOverview
        var seasons: [Season]
        var viewModel: LeaguesViewModel
        var body: some View {
            NavigationView {
                /// Show all loaded seasons to pick
                    List {
                        ForEach(seasons) { season in
                            Text(String("\(season.year!)"))
                                .simultaneousGesture(TapGesture().onEnded{
                                    parentSelf.$seasonId.wrappedValue = season.id!
                                    parentSelf.$seasonYear.wrappedValue = season.year!
                                    self.presentationMode.wrappedValue.dismiss()
                                    viewModel.loadingState = .idle
                                })
                        }
                    }
                .navigationBarItems(trailing: Button("hide", action: {
                    self.presentationMode.wrappedValue.dismiss()
                }))
            }
        }
    }
}

struct LeaguesOverview_Previews: PreviewProvider {
    static var previews: some View {
        LeaguesOverview()
    }
}
