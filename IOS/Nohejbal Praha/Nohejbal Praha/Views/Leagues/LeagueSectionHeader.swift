//
//  LeagueSectionHeader.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 07/06/2021.
//

import SwiftUI

/// Section header view component for league
struct LeagueSectionHeader: View {
    var title: String
    var selection: Bool
    var league: LeagueListItem
    var body: some View {
        VStack(){
            GeometryReader { geometry in
                HStack(spacing: 0){
                    Text(NSLocalizedString("\(title)", comment: ""))
                        .font(.headline)
                        .padding(.leading, 10)
                        .frame(maxWidth: geometry.size.width * 0.8, alignment: .leading)
                    NavigationLink(destination: LeagueMatchesExpanded(league: league, selection: selection)){
                            Text("matchesAll")
                    }
                        .frame(maxWidth: geometry.size.width * 0.2)
                }
            }
            .frame(height: 20, alignment: .center)
            .padding(8)
            .background(Color("LightGray"))
        }
    }
}

struct LeagueSectionHeader_Previews: PreviewProvider {
    static var previews: some View {
        LeagueSectionHeader(title: "2.2.", selection: true, league: LeagueListItem.example)
    }
}
