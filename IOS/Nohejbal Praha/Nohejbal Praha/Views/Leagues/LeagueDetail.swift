//
//  LeagueDetail.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 16/02/2021.
//

import SwiftUI

/// Page showing league detail
struct LeagueDetail: View {
    var league: LeagueListItem
    var count = Content.leagueMatchCount
    @ObservedObject var viewModel = StandingsViewModel()
    @ObservedObject var pastMatchesViewModel = MatchesViewModel()
    @ObservedObject var futureMatchesViewModel = MatchesViewModel()
    
    var body: some View {
        ScrollView{
        /// Display content of page based on viewmodel state
        switch viewModel.loadingState {
        case .idle:
        /// Empty page call viewmodel to get content
            Color.clear.onAppear{ viewModel.getStandings(leagueId: league.id!)}
        case .loading:
        /// Loading indicator
            let screenSize = UIScreen.main.bounds.size
            CustomProgressView(){
                Color.clear
            }
            .frame(width: screenSize.width, height: screenSize.height - 100)
        case .failed(let error):
        /// Error message
            ErrorView(errorCode:error)
        case .loaded:
        /// Content of page showing all league detail data
            ScrollView{
                VStack(spacing: 0){
                    ///
                    /// League table
                    SectionHeader(title: "table")
                        .font(.headline)
                        .padding(.bottom, 50)
                        .padding(.top, 10)
                    GeometryReader { geometry in
                        HStack() {
                            Text("standingPlace")
                                .frame(maxWidth: geometry.size.width * 0.05)
                            Text("standingTeam")
                                .frame(maxWidth: geometry.size.width * 0.45)
                            Text("standingPlayed")
                                .frame(maxWidth: geometry.size.width * 0.04)
                            Text("standingWDL")
                                .frame(maxWidth: geometry.size.width * 0.13)
                            Text("standingScore")
                                .frame(maxWidth: geometry.size.width * 0.15)
                            Text("standingPoints")
                                .frame(maxWidth: geometry.size.width * 0.09)
                        }
                        .font(.system(size: 14, weight: .bold))
                        .padding(.top, 10)
                        .padding(.leading, 5)
                        .padding(.trailing, 5)
                    }
                    .padding(.bottom, 30)
                    ForEach(viewModel.standings) { standing in
                        LeagueTableRow(standing:standing)
                    }
                    .padding(.bottom, 15)
                    .listRowInsets(EdgeInsets())
                ///
                /// Load and display past matches
                LeagueSectionHeader(title: "matchesLatest", selection: true, league: league)
                    .listRowInsets(EdgeInsets())
                    .padding(.top, 20)

                switch pastMatchesViewModel.loadingState {
                case .idle:
                    Color.clear.onAppear{ pastMatchesViewModel.getLeaguePastMatchesByRound(leagueId: league.id!, count: count) }
                case .loading:
                    let screenSize = UIScreen.main.bounds.size
                    CustomProgressView(){
                        Color.clear
                    }
                    .frame(width: screenSize.width, height: 500)
                case .failed(let error):
                    ErrorView(errorCode:error)
                case .loaded:
                    ForEach(0..<pastMatchesViewModel.matches.count){ i in
                        VStack{
                            MatchesGroup(title: "\(pastMatchesViewModel.matches[i].round!).kolo")
                                    .listRowInsets(EdgeInsets())
                                    .padding(0)
                            ForEach(pastMatchesViewModel.matches[i].matches!){ match in
                                NavigationLink(destination: MatchOverview(match: match, userMatch: false)) {
                                    MatchRow(match: match)
                                }
                                .padding(2)
                            }
                        }
                        .padding(.bottom, 20)
                    }
                }
                ///
                /// Load and display future matches
                LeagueSectionHeader(title: "matchesNext", selection: false, league: league)
                    .listRowInsets(EdgeInsets())
                    .padding(.top, 10)

                switch futureMatchesViewModel.loadingState{
                case .idle:
                    Color.clear.onAppear{ futureMatchesViewModel.getLeagueFutureMatchesByRound(leagueId: league.id!, count: count)}
                case .loading:
                    let screenSize = UIScreen.main.bounds.size
                    CustomProgressView(){
                        Color.clear
                    }
                    .frame(width: screenSize.width, height: 500)
                case .failed(let error):
                    ErrorView(errorCode:error)
                case .loaded:
                    ForEach(0..<futureMatchesViewModel.matches.count){ i in
                        VStack{
                            MatchesGroup(title: "\(futureMatchesViewModel.matches[i].round!).kolo")
                                .listRowInsets(EdgeInsets())
                                .padding(0)
                            ForEach(futureMatchesViewModel.matches[i].matches!){ match in
                                NavigationLink(destination: MatchOverview(match: match, userMatch: false)) {
                                    MatchRow(match: match)
                                }
                                .padding(2)
                            }
                        }
                        .padding(.bottom, 20)
                    }
                }
        }
            .padding(.bottom, 50)
        .navigationBarTitle("\(league.name!)")
    }
        }
        }
        .onAppear{
            viewModel.loadingState = .idle
            pastMatchesViewModel.loadingState = .idle
            futureMatchesViewModel.loadingState = .idle
        }
    }
}

struct LeagueDetail_Previews: PreviewProvider {
    static var previews: some View {
        LeagueDetail(league: LeagueListItem.example)
    }
}
