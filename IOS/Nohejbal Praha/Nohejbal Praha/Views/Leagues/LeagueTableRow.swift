//
//  LeagueTableRow.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 19/02/2021.
//

import SwiftUI

/// View component representing one line of league table
struct LeagueTableRow: View {
    var standing: Standing
    var body: some View {
        GeometryReader { geometry in
            HStack() {
                Text("\(standing.position!).")
                    .frame(maxWidth: geometry.size.width * 0.05)
                AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(standing.logo!)")!)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: geometry.size.width * 0.05, height: geometry.size.width * 0.05)
                Text(standing.name!)
                    .frame(maxWidth: geometry.size.width * 0.40, alignment: .leading)
                Text("\(standing.matches!)")
                    .frame(maxWidth: geometry.size.width * 0.04)
                Text("\(standing.wins!)/\(standing.draws!)/\(standing.losses!)")
                    .frame(maxWidth: geometry.size.width * 0.13)
                Text("\(standing.sets_won!):\(standing.sets_lost!)")
                    .frame(maxWidth: geometry.size.width * 0.15)
                Text("\(standing.points!)")
                    .frame(maxWidth: geometry.size.width * 0.09)
            }
            .font(.system(size: 14))
            .padding(.leading, 5)
            .padding(.trailing, 5)
        }
        .padding(0)
        .frame(height: 21)
    }
}

struct LeagueTableRow_Previews: PreviewProvider {
    static var previews: some View {
        LeagueTableRow(standing: Standing.example)
    }
}
