//
//  LeagueRow.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 16/02/2021.
//

import SwiftUI

struct LeagueRow: View {
    var league:LeagueListItem
    var body: some View {
        Text(league.name!)
    }
}

struct LeagueRow_Previews: PreviewProvider {
    static var previews: some View {
        LeagueRow(league: LeagueListItem.example)
    }
}
