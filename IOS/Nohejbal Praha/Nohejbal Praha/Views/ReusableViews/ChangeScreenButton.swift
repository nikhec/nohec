//
//  ChangeScreenButton.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 25/06/2021.
//

import SwiftUI

/// Button to navigate between screens, various look based on enabled field
struct ChangeScreenButton: View {
    var text: String
    var enabled: Bool
    var body: some View {
        Button(action: {}, label: {
        Text(NSLocalizedString("\(text)", comment: ""))
            .font(.system(size: 18))
            .foregroundColor(enabled ? Color.blue : Color.white)
            .padding(.top, 10)
            .padding(.bottom, 10)
            .padding(.leading, 50)
            .padding(.trailing, 50)
            .background(enabled ? Color.white : Color("LightGray"))
            .cornerRadius(16)
            .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(Color("LightGray"), lineWidth: 2)
                )
        })
    }
}

struct ChangeScreenButton_Previews: PreviewProvider {
    static var previews: some View {
        ChangeScreenButton(text: "Placeholder", enabled: true)
    }
}
