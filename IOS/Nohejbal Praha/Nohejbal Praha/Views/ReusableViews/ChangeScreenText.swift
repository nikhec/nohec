//
//  ChangeScreenText.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 28/06/2021.
//

import SwiftUI

/// Text to navigate between screens, various look based on enabled field
struct ChangeScreenText: View {
    var text: String
    var enabled: Bool
    var body: some View {
        Text(NSLocalizedString("\(text)", comment: ""))
            .font(.system(size: 18))
            .foregroundColor(enabled ? Color.blue : Color.white)
            .padding(.top, 10)
            .padding(.bottom, 10)
            .padding(.leading, 50)
            .padding(.trailing, 50)
            .background(enabled ? Color.white : Color("LightGray"))
            .cornerRadius(16)
            .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(Color("LightGray"), lineWidth: 2)
                )
    }
}

struct ChangeScreenText_Previews: PreviewProvider {
    static var previews: some View {
        ChangeScreenText(text: "Placeholder", enabled: true)
    }
}
