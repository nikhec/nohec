//
//  TextWithBorder.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 03/05/2021.
//

import SwiftUI

/// Visually customized Text component with background
struct TextWithBorder: View {
    var text: String
    var body: some View {
        Button(action: {}, label: {
        Text(NSLocalizedString("\(text)", comment: ""))
            .font(.system(size: 18))
            .foregroundColor(.white)
            .padding()
            .background(Color("Primary"))
            .cornerRadius(15.0)
        })
    }
}

struct TextWithBorder_Previews: PreviewProvider {
    static var previews: some View {
        TextWithBorder(text: "Placeholder")
    }
}
