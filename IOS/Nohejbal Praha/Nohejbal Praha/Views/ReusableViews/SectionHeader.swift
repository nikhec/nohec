//
//  SectionHeader.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 07/06/2021.
//

import SwiftUI
import Foundation

/// Visual element on top of section, simple text with background
struct SectionHeader: View {
    var title: LocalizedStringKey
    var body: some View {
        GeometryReader { geometry in
            HStack(){
                Text(title)
                    .font(.headline)
                    .padding(.leading, 10)
                    .frame(maxWidth: geometry.size.width, alignment: .leading)
            }
            .frame(height: 20, alignment: .center)
            .padding(8)
            .padding(.top, 10)
            .background(Color("LightGray"))
            }
    }
}

struct SectionHeader_Previews: PreviewProvider {
    static var previews: some View {
        SectionHeader(title: "Section")
    }
}
