//
//  ErrorView.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 18/05/2021.
//

import SwiftUI

/// Error alert view, with message explaining error
struct ErrorView: View {
    var errorCode: String
    var body: some View {
        VStack{}
            .alert(isPresented: .constant(true)) {
                Alert(title: Text(""), message: Text("errorTryAgain"), dismissButton: .default(Text("dissmiss")))
            }
    }
}

struct ErrorView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorView(errorCode: "error")
    }
}
