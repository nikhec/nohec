//
//  TextWithBg.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 07/06/2021.
//

import SwiftUI

/// Customized text with background and rounded corners
struct TextWithBg: View {
    var text : String
    var body: some View {
        Text("\(NSLocalizedString("\(text)", comment: ""))")
            .font(.system(size: 14))
            .foregroundColor(.black)
            .padding(8)
            .background(Color("LightGray"))
            .cornerRadius(15.0)
    }
}

struct TextWithBg_Previews: PreviewProvider {
    static var previews: some View {
        TextWithBg(text: "login")
    }
}
