//
//  AsyncImageView.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 21/06/2021.
//

import SwiftUI

/// Component for displaying asynchronously loaded image
struct AsyncImage: View {
    @ObservedObject private var loader: ImageLoader
        init(url: URL) {
            _loader = ObservedObject(wrappedValue: ImageLoader(url: url))
        }
        var body: some View {
            content
                .onAppear(perform: loader.load)
        }
        private var content: some View {
            Group {
                if loader.image != nil {
                /// Show loaded image
                    Image(uiImage: loader.image!)
                        .resizable()
                } else {
                /// Show placeholder
                    Image(systemName: "photo")
                }
            }
        }
}

struct AsyncImageView_Previews: PreviewProvider {
    static var previews: some View {
        Color.clear
    }
}
