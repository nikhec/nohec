//
//  TextWithBgDisabled.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 25/06/2021.
//

import SwiftUI

/// Text customized to look visually "disabled"
struct TextWithBgDisabled: View {
    var text: String
    var body: some View {
        Text(NSLocalizedString("\(text)", comment: ""))
            .font(.system(size: 20))
            .foregroundColor(.white)
            .padding()
            .background(Color("LightGray"))
            .cornerRadius(6.0)
    }
}

struct TextWithBgDisabled_Previews: PreviewProvider {
    static var previews: some View {
        TextWithBgDisabled(text: "Placeholder")
    }
}
