//
//  LoadingIndicator.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 30/03/2021.
//

import Foundation
import SwiftUI

/// Loading indicator subpart of CustomProgressView
struct LoadingIndicator: UIViewRepresentable {
    typealias UIViewType = UIActivityIndicatorView
    let style: UIActivityIndicatorView.Style
    
    func makeUIView(context: UIViewRepresentableContext<LoadingIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }
    
    func updateUIView(_ view: LoadingIndicator.UIViewType, context: UIViewRepresentableContext<LoadingIndicator>) {
        view.startAnimating()
    }
}
