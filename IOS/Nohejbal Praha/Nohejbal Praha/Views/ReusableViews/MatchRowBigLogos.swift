//
//  MatchRowBigLogos.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 25/06/2021.
//

import SwiftUI

/// Component displaying both teams with large logos and text
struct MatchRowBigLogos: View {
    var home_logo : String
    var away_logo : String
    var home_team : String
    var away_team : String
    var intMatch : InteractiveMatch
    var body: some View {
        VStack{
        GeometryReader { geometry in
            HStack{
                Spacer()
                VStack{
                /// Home team
                    AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(home_logo)")!)
                        .aspectRatio(contentMode: .fit)
                        .frame(width:80, height: 80)
                    Text("\(home_team)")
                        .font(.system(size: 15))
                }
                .frame(maxWidth: geometry.size.width * 0.38)
                VStack{
                /// Score
                    Text(intMatch.getCurrentScore())
                        .font(.system(size: 25, weight: .semibold))
                        .frame(maxWidth: geometry.size.width * 0.15)
                        .padding(.bottom, 5)
                    Text("fullScore")
                        .font(.system(size: 14))
                }
               /// Away team
                VStack{
                    AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(away_logo)")!)
                        .aspectRatio(contentMode: .fit)
                        .frame(width:80, height: 80)
                    Text("\(away_team)")
                        .font(.system(size: 15))
                }
                .frame(maxWidth: geometry.size.width * 0.38)
                Spacer()
            }
        }
        .frame(height: 120)
        }
    }
}

struct MatchRowBigLogos_Previews: PreviewProvider {
    static var previews: some View {
        MatchRowBigLogos(home_logo: "", away_logo: "", home_team: "", away_team: "", intMatch: InteractiveMatch.example)
    }
}
