//
//  CustomProgressView.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 24/05/2021.
//

import SwiftUI

/// Simple loading view
struct CustomProgressView<Content>: View where Content: View {
    var content: () -> Content
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                self.content()
                    .disabled(true)
                    .blur(radius: 3)
                
                VStack {
                    Text("loading")
                    LoadingIndicator(style: .large)
                }
                .frame(width: geometry.size.width/3.0, height: geometry.size.height/6.0)
                .background(Color.secondary.colorInvert())
                .foregroundColor(Color.primary)
                .cornerRadius(20)
                .opacity(1)
            }
        }
    }
}
