//
//  PickerView.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 26/04/2021.
//
import Foundation
import SwiftUI

/// Custom picker component used in interactive match set
struct PickerView: UIViewRepresentable {
    var data: [[String]]
    @Binding var selections: Int
    
    func makeCoordinator() -> PickerView.Coordinator {
        Coordinator(self)
    }

    func makeUIView(context: UIViewRepresentableContext<PickerView>) -> UIPickerView {
        let picker = UIPickerView(frame: .zero)
        picker.dataSource = context.coordinator
        picker.delegate = context.coordinator
        return picker
    }

    func updateUIView(_ view: UIPickerView, context: UIViewRepresentableContext<PickerView>) {
    }

    class Coordinator: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
        var parent: PickerView

        init(_ pickerView: PickerView) {
            self.parent = pickerView
        }

        func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
            return 100
        }

        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return self.parent.data.count
        }

        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return self.parent.data[component].count
        }

        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return self.parent.data[component][row]
        }

        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            print(self.parent.data[0][row])
            self.parent.selections = row
        }
        
        func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
            var pickerLabel: UILabel? = (view as? UILabel)
            if pickerLabel == nil {
                pickerLabel = UILabel()
                pickerLabel?.textAlignment = .center
            }
            pickerLabel?.text = self.parent.data[0][row]
            pickerLabel?.font = UIFont(name: "Arial", size: 50)
            pickerLabel?.textColor = UIColor.black
            pickerLabel?.backgroundColor = UIColor.white
            return pickerLabel!
        }
    }
}
