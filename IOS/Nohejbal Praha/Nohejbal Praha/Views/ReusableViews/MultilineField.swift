//
//  MultilineField.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 03/05/2021.
//

import SwiftUI

/// Custom text field on multiple lines component used in interactive match note page
struct MultilineField: UIViewRepresentable {
    typealias UIViewType = UITextView
        @Binding var text: String
        var onDone: (() -> Void)?
        
        func makeUIView(context: UIViewRepresentableContext<MultilineField>) -> UITextView {
            let textField = UITextView()
            textField.delegate = context.coordinator
            textField.isEditable = true
            textField.font = UIFont.preferredFont(forTextStyle: .body)
            textField.isSelectable = true
            textField.isUserInteractionEnabled = true
            textField.isScrollEnabled = true
            textField.backgroundColor = UIColor.clear
            if nil != onDone {
                textField.returnKeyType = .done
            }

            textField.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
            return textField
        }

        func updateUIView(_ uiView: UITextView, context: UIViewRepresentableContext<MultilineField>) {
            if uiView.text != self.text {
                uiView.text = self.text
            }
            if uiView.window != nil, !uiView.isFirstResponder {
                uiView.becomeFirstResponder()
            }
        }

        func makeCoordinator() -> Coordinator {
            return Coordinator(text: $text, onDone: onDone)
        }

        final class Coordinator: NSObject, UITextViewDelegate {
            var text: Binding<String>
            var onDone: (() -> Void)?

            init(text: Binding<String>, onDone: (() -> Void)? = nil) {
                self.text = text
                self.onDone = onDone
            }

            func textViewDidChange(_ uiView: UITextView) {
                text.wrappedValue = uiView.text
            }

            func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
                if let onDone = self.onDone, text == "\n" {
                    textView.resignFirstResponder()
                    onDone()
                    return false
                }
                return true
            }
        }
}
