//
//  CustomTabs.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 01/06/2021.
//

import SwiftUI

struct CustomTabs: View {
    @State private var selection: Bool = true

    var body: some View {
        HStack{
            Spacer()
            Button {
                selection = true
            } label: {
                Text("Nadchazejici")
                    .foregroundColor(!selection ? .black : .blue)
            }
            Spacer()
            Button {
                selection = false
            } label: {
                Text("Predchozi")
                    .foregroundColor(selection ? .black : .blue)
            }
            Spacer()
        }
    }
}

struct CustomTabs_Previews: PreviewProvider {
    static var previews: some View {
        CustomTabs()
    }
}
