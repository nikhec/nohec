//
//  UserProfileView.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 30/03/2021.
//

import SwiftUI

/// Main user page, content is either user's matches if user is logged in or Login page if user is not currently logged in
struct UserProfileView: View {
    @ObservedObject var viewModel = MatchesViewModel()
    @ObservedObject var loginViewModel = LoginViewModel()
    @ObservedObject var checkLoginViewModel = CheckLoginViewModel()
    @ObservedObject var userSettings = UserSettings()
    @State private var selection: Bool = true
    @State var settings: Bool = false
    var count = Content.matchesCount
    
    /// Visually customized login button
    var loginButton: some View {
                Text("login")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .frame(width: 300, height: 50)
                    .background(Color("Primary"))
                    .cornerRadius(15.0)
                    .shadow(radius: 10.0, x: 20, y: 10)
                    .navigationBarHidden(true)
                    .simultaneousGesture(TapGesture().onEnded{
                        self.loginUser()
                    }).padding(.top, 20)
    }
    
    var body: some View {
        /// Check if user's authentication is still valid
        /// Display content of page based on viewmodel state
        switch checkLoginViewModel.loadingState{
        case .idle:
            /// Empty page call viewmodel to get content
            Color.clear.onAppear {
                checkLoginViewModel.getLoginCheck()
            }
        case .loading:
            /// Loading indicator
            CustomProgressView(){
                Color.clear
            }
        case .failed(let error):
        /// User's authentication is not valid show login page
            NavigationView {
                /// Display content of page based on viewmodel state
                switch loginViewModel.loadingState {
                case .idle:
                        ZStack {
                            LinearGradient(gradient: Gradient(colors: [.white, Color("Primary")]), startPoint: .center, endPoint: .bottom)
                                    .edgesIgnoringSafeArea(.all)
                                VStack(){
                                    Image("nohejbal_logo")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(height:240)
                                        .padding(.bottom, 50)
                                    VStack() {
                                        HStack {
                                            Image("outline_email_black_24pt")
                                                .foregroundColor(.secondary)
                                            TextField("namePlaceholder", text: $loginViewModel.username)
                                                .autocapitalization(.none)
                                                
                                        }
                                        .padding()
                                        .background(Color.white)
                                        .cornerRadius(16.0)
                                        .shadow(radius: 10.0, x: 5, y: -2)
                                                
                                        HStack {
                                            Image("outline_lock_black_24pt")
                                                .foregroundColor(.secondary)
                                            SecureField("passwordPlaceholder", text: $loginViewModel.password)
                                                .foregroundColor(Color.black)
                                        }
                                        .padding()
                                        .background(Color.white)
                                        .cornerRadius(16.0)
                                        .shadow(radius: 10.0, x: 5, y: -2)
                                    }.padding([.leading, .trailing], 27.5)
                                    self.loginButton
                                }
                        }
                    case .loading:
                        CustomProgressView(){
                            Color.clear
                        }
                    case .failed(let error):
                        Color.clear
                            .alert(isPresented: .constant(true)) {
                                Alert(title: Text("Chyba"), message: Text(error), dismissButton: .default(Text("dissmiss")){
                                    loginViewModel.loadingState = .idle
                                }
                                )
                        }
                    /// User logged in succesfully
                    case .loaded:
                            /// Load and display user's matches
                            switch viewModel.loadingState {
                                case .idle:
                                    Color.clear.onAppear {
                                        !selection ? viewModel.getMyPastMatchesByDate(count: count): viewModel.getMyFutureMatchesByDate(count: count)
                                    }
                                case .loading:
                                    CustomProgressView(){
                                        Color.clear
                                    }
                                case .failed(let error):
                                    if UserSettings.init().token == "" {
                                        Color.clear.onAppear{
                                            //self.presentationMode.wrappedValue.dismiss()
                                            loginViewModel.loadingState = .idle
                                        }
                                    } else {
                                        ErrorView(errorCode:error)
                                    }
                                case .loaded:
                                    ScrollView{
                                    VStack{
                                    HStack{
                                        Spacer()
                                        Button {
                                            selection = true
                                            viewModel.loadingState = .idle
                                        } label: {
                                            Text("matchesUpcoming")
                                                .foregroundColor(!selection ? .black : .blue)
                                        }
                                        Spacer()
                                        Button {
                                            selection = false
                                            viewModel.loadingState = .idle
                                        } label: {
                                            Text("matchesPlayed")
                                                .foregroundColor(selection ? .black : .blue)
                                        }
                                        Spacer()
                                    }
                                    .padding(.top, 20)
                                        ForEach(0..<viewModel.matches.count) { i in
                                            MatchesGroup(title: "\(Content.date_formatter.string(from: Content.base_date_formatter.date(from: viewModel.matches[i].date!)!))")
                                                .listRowInsets(EdgeInsets())
                                            ForEach(viewModel.matches[i].matches!) { match in
                                                if canStartMatch(match: match) {
                                                    NavigationLink(destination: InteractiveMatchCaptains(matchId: match.id!, matchBase: match)) {
                                                            MyMatchRow(match: match)
                                                        }
                                                } else {
                                                    NavigationLink(destination: MatchOverview(match: match, userMatch: true)) {
                                                            MatchRow(match: match)
                                                        }
                                                }
                                            }
                                        }
                                    }
                                    .navigationBarTitle("myMatchesTitle")
                                    .navigationBarItems(trailing: Image("outline_more_vert_black_24pt")
                                                            .sheet(isPresented: $settings, content: {
                                                                UserProfileSettings(userSettings: userSettings, parentSelf: self)
                                                            })
                                                            .simultaneousGesture(TapGesture().onEnded{
                                                                settings = true
                                    }))
                                    }
                            }
                                }
                }
                .onAppear{ viewModel.loadingState = .idle}
        case .loaded:
        /// User's auth is still valid display user's matches
                NavigationView {
                    switch viewModel.loadingState {
                    case .idle:
                        Color.clear.onAppear {
                            !selection ? viewModel.getMyPastMatchesByDate(count: count): viewModel.getMyFutureMatchesByDate(count: count)
                        }
                    case .loading:
                        CustomProgressView(){
                            Color.clear
                        }
                    case .failed(let error):
                        if UserSettings.init().token == "" {
                            Color.clear.onAppear{
                                //self.presentationMode.wrappedValue.dismiss()
                                loginViewModel.loadingState = .idle
                            }
                        } else {
                            ErrorView(errorCode:error)
                        }
                    case .loaded:
                        ScrollView{
                        VStack{
                        HStack{
                            Spacer()
                            Button {
                                selection = true
                                viewModel.loadingState = .idle
                            } label: {
                                Text("matchesUpcoming")
                                    .foregroundColor(!selection ? .black : .blue)
                            }
                            Spacer()
                            Button {
                                selection = false
                                viewModel.loadingState = .idle
                            } label: {
                                Text("matchesPlayed")
                                    .foregroundColor(selection ? .black : .blue)
                            }
                            Spacer()
                        }
                        .padding(.top, 20)
                            ForEach(0..<viewModel.matches.count) { i in
                                MatchesGroup(title: "\(Content.date_formatter.string(from: Content.base_date_formatter.date(from: viewModel.matches[i].date!)!))")
                                    .listRowInsets(EdgeInsets())
                                ForEach(viewModel.matches[i].matches!) { match in
                                    if canStartMatch(match: match) {
                                        NavigationLink(destination: InteractiveMatchCaptains(matchId: match.id!, matchBase: match)) {
                                                MyMatchRow(match: match)
                                            }
                                    } else {
                                        NavigationLink(destination: MatchOverview(match: match, userMatch: true)) {
                                                MatchRow(match: match)
                                            }
                                    }
                                }
                            }
                        }
                        .navigationBarTitle("myMatchesTitle")
                        .navigationBarItems(trailing: Image("outline_more_vert_black_24pt")
                                                .sheet(isPresented: $settings, content: {
                                                    UserProfileSettings(userSettings: userSettings, parentSelf: self)
                                                })
                                                .simultaneousGesture(TapGesture().onEnded{
                                                    settings = true
                        }))
                        }
                    }
                    }
                    .onAppear{ viewModel.loadingState = .idle}
            }
        
    }
    
    /// User's profile sheet, contains logout option and signing key
    struct UserProfileSettings: View {
        @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
        @ObservedObject var userSettings: UserSettings
        @ObservedObject var signatureViewModel = SignatureViewModel()
        var parentSelf : UserProfileView
        var body: some View {
            NavigationView {
                switch signatureViewModel.loadingState{
                case .idle:
                    Color.clear.onAppear {
                        signatureViewModel.getSignature()
                    }
                case .loading:
                    CustomProgressView(){
                        Color.clear
                    }
                case .failed(let error):
                    ErrorView(errorCode:error)
                case .loaded:
                    Form{
                        Section(header: Text("userCode")){
                            HStack{
                                Spacer()
                                Text("\(signatureViewModel.signature!.signature!)")
                            }
                        }
                        Section{
                            HStack{
                                Spacer()
                                Text("logout")
                                    .foregroundColor(.blue)
                                    .simultaneousGesture(TapGesture().onEnded{
                                        userSettings.logout()
                                        self.presentationMode.wrappedValue.dismiss()
                                        parentSelf.loginViewModel.loadingState = .idle
                                        self.presentationMode.wrappedValue.dismiss()
                                    })
                                Spacer()
                            }
                        }
                    }
                    .navigationBarItems(trailing: Button("hide", action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }))
                }
            }
            .onAppear{
                signatureViewModel.loadingState = .idle
            }
        }
    }
    
    /// Calls viewmodel to request token
    private func loginUser(){
        loginViewModel.getToken()
    }
    
    /// Check if user's match can be started by user
    /// - Parameter match : match to be started
    /// - Returns true is match can be started
    private func canStartMatch(match: Match) -> Bool {
        var matchTime : Date?
        if match.date != nil{
            if Content.base_daytime_formatter.date(from: match.date!) != nil{
               matchTime =  Content.base_daytime_formatter.date(from: match.date!)
            }
        } else {
            return true
        }
        return ((Date().addingTimeInterval(Content.timeIntervalBeforeMatch*60) > matchTime!) && (match.state != 4))
    }
}

struct UserProfileView_Previews: PreviewProvider {
    static var previews: some View {
        UserProfileView()
    }
}
