//
//  ContentView.swift
//  Nohejbal Praha
//
//  Created by Nik Hec on 15.02.2021.
//

import SwiftUI

struct ContentView: View {
    @State private var selection: Tab = .user
    @ObservedObject var userSettings = UserSettings()
    
    enum Tab{
        case user
        case matches
        case leagues
        case clubs
    }
    
    var body: some View {
        TabView(selection: $selection){
            UserProfileView()
                .tabItem {
                    Image("outline_face_black_24pt")
                    Text("user")
                }
                .tag(Tab.user)
            MatchesOverview()
                .tabItem {
                    Image("outline_sports_soccer_black_24pt")
                    Text("matchesTitle")
                }
                .tag(Tab.matches)
            LeaguesOverview()
                .tabItem {
                    Image("outline_emoji_events_black_24pt")
                    Text("leaguesTitle")
                }
                .tag(Tab.leagues)
            ClubsOverview()
                .tabItem {
                    Image("outline_groups_black_24pt")
                    Text("clubsTitle")
                }
                .tag(Tab.clubs)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
