//
//  InteractiveMatchCaptains.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 20/04/2021.
//

import SwiftUI

/// Page that displays both teams captains picking interface
struct InteractiveMatchCaptains: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var matchId: Int
    var matchBase: Match
    @ObservedObject var viewModel = MatchCaptainsViewModel()
    @ObservedObject var matchEnterViewModel = MatchEnterViewModel()
    @State private var homeSelect = 0
    @State private var awaySelect = 0
    
    var body: some View {
        VStack{
            /// Display content based on current match enter loading state
            switch matchEnterViewModel.loadingState {
            case .idle:
                /// Call viewmodel for data
                Color.clear.onAppear {
                    matchEnterViewModel.postMatchEnter(matchId: matchId)
                }
            case .loading:
                /// Show loading UI
                let screenSize = UIScreen.main.bounds.size
                CustomProgressView(){
                    Color.clear
                }
                .frame(width: screenSize.width, height: screenSize.height - 100)
            case .failed(let error):
                /// Show error message
                Color.clear
                .alert(isPresented: .constant(true)) {
                    Alert(title: Text("Chyba"), message: Text("\(NSLocalizedString("\(error)", comment: ""))"), dismissButton: .default(Text("dissmiss")){
                        presentationMode.wrappedValue.dismiss()
                    }
                    )
                }
            case .loaded:
                /// Based on match progress state, if captains are not yet picked:
                /// - Show loaded page content consisting of match info and captain pickers
                /// Otherwise navigate to match overview page
                switch matchEnterViewModel.match!.state{
                case MatchState.waitingForCaptains.rawValue:
                    InteractiveMatchRow(match: matchEnterViewModel.match!)
                            .padding(.bottom, 10)
                            .padding(.top, 50)
                        Text("chooseCaptains")
                            .font(.system(size: 20, weight: .bold))
                            .padding()
                        Form{
                            Section(header: HStack{
                                AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(matchEnterViewModel.match!.home_team!.logo!)")!)
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width:20, height: 20)
                                Text("\(matchEnterViewModel.match!.home_team!.name!)")
                            } ){
                                Picker(selection: $homeSelect, label:Text("")) {
                                    ForEach(0 ..< matchEnterViewModel.match!.available_home_players!.count) {
                                        Text(matchEnterViewModel.match!.available_home_players![$0].name!)
                                            .foregroundColor(.black)
                                    }
                                }
                            }
                            Section(header: HStack{
                                AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(matchEnterViewModel.match!.away_team!.logo!)")!)
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width:20, height: 20)
                                Text("\(matchEnterViewModel.match!.away_team!.name!)")
                            } ){
                                Picker(selection: $awaySelect, label:Text("")) {
                                    ForEach(0 ..< matchEnterViewModel.match!.available_away_players!.count) {
                                        Text(matchEnterViewModel.match!.available_away_players![$0].name!)
                                            .foregroundColor(.black)
                                    }
                                }
                            }
                        }
                        .frame(height: 200, alignment: .center)
                ///
                /// If captains were picked, user can navigation to match games overview
                NavigationLink(
                    destination: InteractiveMatchGamesOverview(matchId: matchId, match: matchBase), isActive: .constant(viewModel.loadingState == .loaded)){
                        ChangeScreenText(text: "continue", enabled: true)
                            .simultaneousGesture(TapGesture().onEnded{
                                viewModel.postMatchCaptains(matchId: matchId, homeId: matchEnterViewModel.match!.available_home_players![$homeSelect.wrappedValue].id!, awayId: matchEnterViewModel.match!.available_away_players![$awaySelect.wrappedValue].id!)
                            })
                }
                Spacer()
                default:
                ///
                /// Navigation to match games overview
                NavigationLink(
                    destination: InteractiveMatchGamesOverview(matchId: matchId, match: matchBase),
                    isActive: .constant(true)){
                }
                }
            }
        }
        .navigationBarHidden(true)
    }
}

struct InteractiveMatchCaptains_Previews: PreviewProvider {
    static var previews: some View {
        InteractiveMatchCaptains(matchId: 1, matchBase: Match.example)
    }
}
