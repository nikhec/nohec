//
//  InteractiveMatchRecap.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 03/05/2021.
//

import SwiftUI

/// Page with match games recap after all games are finished
struct InteractiveMatchRecap: View {
    var match: Match
    var matchDetail: InteractiveMatch
    var body: some View {
        VStack(spacing: 0){
            InteractiveMatchRow(match: matchDetail)
            Text("recap")
                .font(.system(size: 20, weight: .bold))
                .padding()
            ForEach(matchDetail.games!) { game in
                MatchPartResultRow(matchName: matchDetail.getGameName(gameId: game.id!, gameType: game.type!), matchResult:game.getScore())
                    .padding(5)
            }
            NavigationLink(destination: InteractiveMatchSigning(match: match, intMatch: matchDetail)){
                ChangeScreenText(text:"sign", enabled: true)
            }
            .isDetailLink(false)
            .padding(.top, 30)
            Spacer()
        }
        .padding()
    }
}

struct InteractiveMatchRecap_Previews: PreviewProvider {
    static var previews: some View {
        InteractiveMatchRecap(match: Match.example, matchDetail: InteractiveMatch.example)
    }
}
