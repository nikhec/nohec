//
//  InteractiveMatchSet.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 20/04/2021.
//

import SwiftUI

/// Page that displays sets controls
struct InteractiveMatchSet: View {
    var match : InteractiveMatch
    var game: Game
    var set: Int
    var matchId: Int
    var homeIds: [Int]
    var awayIds: [Int]
    @ObservedObject var viewModel = MatchSetViewModel()
    var homePointsPicker = ["0","1","2","3","4","5","6","7","8","9","10"]
    var awayPointsPicker = ["0","1","2","3","4","5","6","7","8","9","10"]
    @State var homeTimeRemaining = Content.timeoutDuration
    @State var awayTimeRemaining = Content.timeoutDuration
    let homeTimer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    let awayTimer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State private var homeSelect = 0
    @State private var awaySelect = 0
    @State private var homeTimeout = 0
    @State private var awayTimeout = 0
    @State private var showHomeAlert = false
    @State private var showAwayAlert = false
    @Environment(\.presentationMode) var presentationMode
    @State var homeSubs = false
    @State var awaySubs = false
    @State var homeSubsList : [Substitution] = []
    @State var awaySubsList : [Substitution] = []
    @ObservedObject var subSettings = SubSettings()
    var body: some View {
        ScrollView{
        VStack{
            InteractiveMatchRow(match: match)
            HStack{
                Spacer()
                Text("\(match.getGameName(gameId: game.id!, gameType: game.type!))")
                    .font(.system(size: 20))
                    .padding()
                Spacer()
            }
            HStack{
                Spacer()
                Text("\(set).SET")
                    .font(.system(size: 20, weight: .bold))
                Spacer()
            }
            HStack(spacing: 0){
                GeometryReader{ geometry in
                    HStack{
                        HStack{
                            AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(match.home_team!.logo!)")!)
                                .aspectRatio(contentMode: .fit)
                                .frame(width:20, height: 20)
                                .padding(0)
                        Text("\(match.home_team!.name!)")
                            .font(.system(size: 14))
                            .sheet(isPresented: $homeSubs, content: {
                                SubSheet(subSettings: self.subSettings, game: game, match: match, isHome: true, parentSelf: self)
                            })
                        }
                        .frame(maxWidth: geometry.size.width * 0.4)
                        
                        Spacer()
                        HStack{
                        Text("\(match.away_team!.name!)")
                            .font(.system(size: 14))
                            .sheet(isPresented: $awaySubs, content: {
                                SubSheet(subSettings: self.subSettings, game: game, match: match, isHome: false, parentSelf: self)
                            })
                        AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(match.away_team!.logo!)")!)
                            .aspectRatio(contentMode: .fit)
                            .frame(width:20, height: 20)
                            .padding(0)
                        }
                        .frame(maxWidth: geometry.size.width * 0.4)
                    }
                    .padding(.leading, 20)
                    .padding(.trailing, 20)
                }}
                .padding(.bottom, 15)
            HStack{
            ///
            /// Timeouts controls
                if homeTimeout == 0{
                TextWithBorder(text: "timeOut")
                    .padding(.leading, 10)
                    .simultaneousGesture(TapGesture().onEnded{
                        showHomeAlert = true
                        homeTimeRemaining = Content.timeoutDuration
                    })
                    .alert(isPresented: $showHomeAlert) {
                        Alert(title: Text(""), message:  Text("\(homeTimeRemaining)"), dismissButton: .default(Text("dissmiss"), action: {
                            showHomeAlert = false
                            homeTimeRemaining = Content.timeoutDuration
                            homeTimeout = 1
                        }))
                    }
                    if showHomeAlert {
                        Text("")
                            .onReceive(homeTimer) { _ in
                                if homeTimeRemaining > 0 {
                                    homeTimeRemaining -= 1
                                }
                            }
                    }
                } else {
                    TextWithBgDisabled(text: "timeOut")
                        .padding(.leading, 10)
                }
                Spacer()
                if awayTimeout == 0 {
                TextWithBorder(text: "timeOut")
                    .padding(.trailing, 10)
                    .simultaneousGesture(TapGesture().onEnded{
                        showAwayAlert = true
                        awayTimeRemaining = Content.timeoutDuration
                    })
                    .alert(isPresented: $showAwayAlert) {
                        Alert(title: Text(""), message:  Text("\(awayTimeRemaining)"), dismissButton: .default(Text("dissmiss"), action: {
                            showAwayAlert = false
                            awayTimeRemaining = Content.timeoutDuration
                            awayTimeout = 1
                        }))
                    }
                    if showAwayAlert {
                        Text("")
                            .onReceive(awayTimer) { _ in
                                if awayTimeRemaining > 0 {
                                    awayTimeRemaining -= 1
                                }
                            }
                    }
                } else {
                    TextWithBgDisabled(text: "timeOut")
                        .padding(.trailing, 10)
                }
            }
            .padding(5)
            HStack{
            ///
            /// Substitution controls
                TextWithBorder(text: "substitution")
                    .padding(.leading, 10)
                    .simultaneousGesture(TapGesture().onEnded{
                        self.homeSubs = true
                    })
                Spacer()
                TextWithBorder(text: "substitution")
                    .padding(.trailing, 10)
                    .simultaneousGesture(TapGesture().onEnded{
                        self.awaySubs = true
                    })
            }
            HStack{
            ///
            /// Show substitutions
                VStack{
                ForEach(0..<Content.maxSubstitutions){ subIndex in
                    VStack{
                        if self.$homeSubsList.wrappedValue.count > subIndex{
                        HStack{
                        Image("baseline_swap_vert_black_24pt")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(height:20)
                        Text("\(match.getPlayerById(players: match.available_home_players!, id: homeSubsList[subIndex].incoming!))")
                            .padding(.bottom, 2)
                        }
                        Spacer()
                        HStack{
                        Image("baseline_swap_vert_black_24pt")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(height:20)
                            .foregroundColor(Color("DarkerGray"))
                            Text("\(match.getPlayerById(players: match.available_home_players!, id: homeSubsList[subIndex].outgoing!))")
                            .padding(.bottom, 2)
                                .foregroundColor(Color("DarkerGray"))
                        }
                        Spacer()
                    }
                    }
                }
                    Spacer()
                }
                Spacer()
                VStack{
                    ForEach(0..<Content.maxSubstitutions){ subIndex in
                    VStack{
                        if self.$awaySubsList.wrappedValue.count > subIndex{
                        HStack{
                        Spacer()
                        Image("baseline_swap_vert_black_24pt")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(height:20)
                        Text("\(match.getPlayerById(players: match.available_away_players!, id: awaySubsList[subIndex].incoming!))")
                            .padding(.bottom, 2)
                        }
                        HStack{
                        Spacer()
                        Image("baseline_swap_vert_black_24pt")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(height:20)
                            .foregroundColor(Color("DarkerGray"))
                            Text("\(match.getPlayerById(players: match.available_away_players!, id: awaySubsList[subIndex].outgoing!))")
                            .padding(.bottom, 2)
                                .foregroundColor(Color("DarkerGray"))
                        }
                    }
                    }
                }
                    Spacer()
                }
            }
            .padding(5)
            HStack{
            ///
            /// Points picking utilities
                Spacer()
                PickerView(data: [self.homePointsPicker], selections:$homeSelect)
                    .frame(width: 100)
                    .clipped()
                Text(":")
                    .font(.title)
                    .padding(20)
                PickerView(data: [self.awayPointsPicker], selections:$awaySelect)
                    .frame(width: 100)
                    .clipped()
                Spacer()
            }
            if set == 1{
            ///
            /// Navigate to set 2.
            NavigationLink(
                destination: InteractiveMatchSet(match: match, game: game, set:2, matchId: matchId, homeIds: homeIds, awayIds: awayIds), isActive: .constant(viewModel.loadingState == .loaded && ($homeSelect.wrappedValue == 10 || $awaySelect.wrappedValue == 10))){
                ChangeScreenButton(text: set == 1 ? "2.SET" : "continue", enabled: ($homeSelect.wrappedValue == 10 || $awaySelect.wrappedValue == 10))
                        .simultaneousGesture(TapGesture().onEnded{
                            if ($homeSelect.wrappedValue == 10 || $awaySelect.wrappedValue == 10){
                                viewModel.postMatchSet(matchId: matchId,
                                                       gameId: game.id!,
                                                       homePoints: $homeSelect.wrappedValue,
                                                       awayPoints: $awaySelect.wrappedValue,
                                                       homeTimeout: $homeTimeout.wrappedValue,
                                                       awayTimeout: $awayTimeout.wrappedValue,
                                                       homeSubs: $homeSubsList.wrappedValue,
                                                       awaySubs: $awaySubsList.wrappedValue)
                            }
                        })
            }
            .isDetailLink(false)
            } else {
            ///
            /// Navigate back to match games overview
                ChangeScreenButton(text: "continue", enabled: ($homeSelect.wrappedValue == 10 || $awaySelect.wrappedValue == 10))
                        .simultaneousGesture(TapGesture().onEnded{
                            if ($homeSelect.wrappedValue == 10 || $awaySelect.wrappedValue == 10){
                            viewModel.postMatchSet(matchId: matchId,
                                                   gameId: game.id!,
                                                   homePoints: $homeSelect.wrappedValue,
                                                   awayPoints: $awaySelect.wrappedValue,
                                                   homeTimeout: $homeTimeout.wrappedValue,
                                                   awayTimeout: $awayTimeout.wrappedValue,
                                                   homeSubs: $homeSubsList.wrappedValue,
                                                   awaySubs: $awaySubsList.wrappedValue)
                            }
                        })
                switch viewModel.loadingState {
                case .loaded:
                    Color.clear.onAppear {
                        presentationMode.wrappedValue.dismiss()
                    }
                case .loading:
                    let screenSize = UIScreen.main.bounds.size
                    CustomProgressView(){
                        Color.clear
                    }
                    .frame(width: screenSize.width, height: screenSize.height - 100)
                default:
                    Color.clear
                }
            }
        }
        .padding(.top, 30)
        .padding(.bottom, 30)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: buttonNavigateBackCustom)
        }
    }
    
    /// Custom button to navigate back
    var buttonNavigateBackCustom : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) {
            HStack(spacing: 0) {
                Image("outline_arrow_back_ios_black_24pt")
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.blue)
                    .padding(0)
                Text("go_back")
                    .padding(0)
            }
            .padding(.leading, 0)
        }
    }
    
    /// Sheet to perform substitutions
    struct SubSheet: View {
        @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
        @ObservedObject var subSettings: SubSettings
        var game : Game
        var match : InteractiveMatch
        var isHome : Bool
        var parentSelf : InteractiveMatchSet
        var body: some View {
            NavigationView {
                Form {
                    Section(header: Text("outgoing")) {
                        Picker("", selection: $subSettings.chosenOutPlayer, content: {
                            ForEach(0..<(isHome ? parentSelf.homeIds.count : parentSelf.awayIds.count)) { index in
                                Text("\(isHome ? (match.getPlayerById(players: match.available_home_players!, id: parentSelf.homeIds[index])): (match.getPlayerById(players: match.available_away_players!, id: parentSelf.awayIds[index])))")
                                    .tag(index)
                            }
                        })
                    }
                    Section(header: Text("incoming")) {
                        Picker("", selection: $subSettings.chosenInPlayer, content: {
                            ForEach(0..<(isHome ? match.getAvailableHomePlayers(players: match.available_home_players!, currentGame: game, playersIds: parentSelf.homeIds).count : match.getAvailableAwayPlayers(players: match.available_away_players!, currentGame: game, playersIds: parentSelf.awayIds).count)) { index in
                                Text("\(isHome ? (match.getPlayerById(players: getPlayers(home: isHome), id: getPlayers(home: isHome)[index].id!)) : (match.getPlayerById(players: getPlayers(home: isHome), id: getPlayers(home: isHome)[index].id!)))")
                                    .tag(index)
                            }
                        })
                    }
                    Section{
                        HStack{
                            Spacer()
                            Button("confirm", action: {
                                if isHome{
                                    parentSelf.homeSubsList.append(Substitution(incoming: match.getAvailableHomePlayers(players: match.available_home_players!, currentGame: game, playersIds: parentSelf.homeIds)[$subSettings.chosenInPlayer.wrappedValue].id, outgoing: parentSelf.homeIds[$subSettings.chosenOutPlayer.wrappedValue]))
                                } else {
                                    parentSelf.awaySubsList.append(Substitution(incoming: match.getAvailableAwayPlayers(players: match.available_away_players!, currentGame: game, playersIds: parentSelf.awayIds)[$subSettings.chosenInPlayer.wrappedValue].id, outgoing: parentSelf.awayIds[$subSettings.chosenOutPlayer.wrappedValue]))
                                }
                                self.presentationMode.wrappedValue.dismiss()
                            })
                            Spacer()
                        }
                    }
                }
                .navigationBarTitle(Text("\(isHome ? match.home_team!.name! : match.away_team!.name!)"))
                .navigationBarItems(trailing: Button("hide", action: {
                    self.presentationMode.wrappedValue.dismiss()
                }))
            }
        }
        
        /// - Parameter home : flag for home team indication
        /// - Returns list of players
        func getPlayers(home: Bool) -> [PlayerData] {
            return home ? match.getAvailableHomePlayers(players: match.available_home_players!, currentGame: game, playersIds: parentSelf.homeIds) : match.getAvailableAwayPlayers(players: match.available_away_players!, currentGame: game, playersIds: parentSelf.awayIds)
        }
        
        
    }

    /// Represents user's settings of substitution on substitution sheet
    class SubSettings: ObservableObject {
        @Published var showSettings = false
        @Published var chosenOutPlayer = 0
        @Published var chosenInPlayer = 0
    }
}


struct InteractiveMatchSet_Previews: PreviewProvider {
    static var previews: some View {
        InteractiveMatchSet(match: InteractiveMatch.example, game: Game.example, set: 0, matchId: 1, homeIds: [], awayIds: [])
    }
}
