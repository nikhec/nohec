//
//  InteractiveMatchSigning.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 04/05/2021.
//

import SwiftUI

/// Page with match signing fields after all games are finished
struct InteractiveMatchSigning: View {
    var match : Match
    var intMatch : InteractiveMatch
    @ObservedObject var viewModel = MatchSigningViewModel()
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack{
            switch viewModel.loadingState{
            case .idle:
                    InteractiveMatchRow(match: intMatch)
                        .padding(.top, 20)
                        HStack{
                            Spacer()
                            Text("signing")
                                .font(.system(size: 20, weight: .bold))
                                .padding()
                            Spacer()
                        }
                Form{
                    Section(header: Text("referee")
                                .font(.system(size: 16))
                                .foregroundColor(.black)){
                        SecureField("sign_referee", text: $viewModel.referee_sign)
                            .foregroundColor(.black)
                    }
                    Section(header: Text("\(match.home_team!.name!)")
                                .font(.system(size: 16))
                                .foregroundColor(.black)){
                        SecureField("sign_home", text: $viewModel.home_sign)
                            .foregroundColor(.black)
                    }
                    Section(header: Text("\(match.away_team!.name!)")
                                .font(.system(size: 16))
                                .foregroundColor(.black)){
                        SecureField("sign_away", text: $viewModel.away_sign)
                            .foregroundColor(.black)
                    }
                    Section{
                        HStack{
                            Spacer()
                            Text("sign")
                                .simultaneousGesture(TapGesture().onEnded{
                                    viewModel.postMatchSigns(matchId: match.id!, match: match)
                                })
                                .foregroundColor(.blue)
                            Spacer()
                        }
                    }
                }
            case .loading:
                let screenSize = UIScreen.main.bounds.size
                CustomProgressView(){
                    Color.clear
                }
                .frame(width: screenSize.width, height: screenSize.height - 100)
            case .failed(let error):
                ErrorView(errorCode:error)
            case .loaded:
                Color.clear.onAppear {
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
        }
        .onAppear{
            viewModel.loadingState = .idle
        }
    }
}

struct InteractiveMatchSigning_Previews: PreviewProvider {
    static var previews: some View {
        InteractiveMatchSigning(match: Match.example, intMatch: InteractiveMatch.example)
    }
}
