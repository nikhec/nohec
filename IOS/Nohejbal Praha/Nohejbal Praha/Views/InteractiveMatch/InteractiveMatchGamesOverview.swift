//
//  InteractiveMatchGamesOverview.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 15/06/2021.
//

import SwiftUI

/// Page that displays overview data for each game, with general match data
struct InteractiveMatchGamesOverview: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var matchEnterViewModel = MatchEnterViewModel()
    var matchId: Int
    @State var nextClicked : Bool = false
    var match: Match
    var body: some View {
        ScrollView{
            /// Display content based on current match loading state
            switch matchEnterViewModel.loadingState {
            case .idle:
                ///
                /// Call viewmodel for content
                Color.clear.onAppear {
                    matchEnterViewModel.postMatchEnter(matchId: matchId)
                }
            case .loading:
                ///
                /// Show loading UI
                let screenSize = UIScreen.main.bounds.size
                CustomProgressView(){
                    Color.clear
                }
                .frame(width: screenSize.width, height: screenSize.height - 100)
            case .failed(let error):
                ///
                /// Show error message
                ErrorView(errorCode:error)
            case .loaded:
                ///
                /// Show loaded content
                VStack(spacing: 5){
                    MatchRowBigLogos(home_logo: match.home_team!.logo!, away_logo: match.away_team!.logo!, home_team: match.home_team!.name!, away_team: match.away_team!.name!, intMatch: matchEnterViewModel.match!)
                        HStack{
                            Text("matchesTitle")
                                .padding(.leading, 35)
                                .font(.system(size: 20, weight: .bold))
                            Spacer()
                        }
                        .padding(.top, 20)
                        ForEach(matchEnterViewModel.match!.games!) { game in
                        ///
                        /// Foreach game based on the state of the match  we navigate on click to differrent page
                            switch matchEnterViewModel.match!.getNextSet(game: game){
                            case -1:
                            /// Captains were not chosen, navigate to captains pick
                                NavigationLink(destination:InteractiveMatchLineups(match: matchEnterViewModel.match!, game: game, matchId: matchId)){
                                    MatchPartResultRow(matchName: matchEnterViewModel.match!.getGameName(gameId: game.id!, gameType: game.type!), matchResult:game.getScore())
                                        .padding(5)
                                }
                            case 2:
                            /// All sets played, navigate to game detail
                                NavigationLink(destination: MatchGameDetail(id: match.id!, game: game, matchDetail: matchEnterViewModel.match!, score: match.score!, gameScore: game.getScore()))  {
                                    MatchPartResultRow(matchName: matchEnterViewModel.match!.getGameName(gameId: game.id!, gameType: game.type!), matchResult:game.getScore())
                                        .padding(5)
                                }
                                .isDetailLink(false)
                            default:
                            /// Game still in progress navigate to next unfinished set
                                NavigationLink(destination: InteractiveMatchSet(match: matchEnterViewModel.match!, game: game, set: matchEnterViewModel.match!.getNextSet(game: game)+1, matchId: matchId, homeIds: game.home_players!, awayIds: game.away_players!)) {
                                    MatchPartResultRow(matchName: matchEnterViewModel.match!.getGameName(gameId: game.id!, gameType: game.type!), matchResult:game.getScore())
                                        .padding(5)
                                }
                                .isDetailLink(false)
                            }
                        }
                    ///
                    /// Navigate to note page
                    /// Available once all games were played
                    if matchEnterViewModel.match!.state != MatchState.finished.rawValue{
                    NavigationLink(destination: InteractiveMatchNotes(match: match, matchDetail: matchEnterViewModel.match!), isActive: .constant(self.nextClicked)) {
                            HStack{
                                Spacer()
                                ChangeScreenText(text: "continue", enabled: matchEnterViewModel.match!.allGamesPlayed())
                                    .simultaneousGesture(TapGesture().onEnded{
                                        if matchEnterViewModel.match!.allGamesPlayed(){
                                            self.nextClicked = true
                                        }
                                    })
                                Spacer()
                            }
                        }
                        .padding(.top, 20)
                    }
                }
                .padding(.top, 40)
                .padding(.bottom, 40)
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("match_games", displayMode: .inline)
        .navigationBarItems(leading: buttonNavigateBackCustom)
        .onAppear{
            self.nextClicked = false
            matchEnterViewModel.loadingState = .idle
        }
    }
    
    /// Custom button to navigate back
    var buttonNavigateBackCustom : some View { Button(action: {
            self.presentationMode.wrappedValue.dismiss()
            self.presentationMode.wrappedValue.dismiss()
            }) {
        HStack(spacing: 0) {
                    Image("outline_arrow_back_ios_black_24pt")
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(.blue)
                        .padding(0)
                    Text("go_back")
                        .padding(0)
                }
        .padding(.leading, 0)
            }
        }
}

struct InteractiveMatchGamesOverview_Previews: PreviewProvider {
    static var previews: some View {
        InteractiveMatchGamesOverview(matchId: 1, match: Match.example)
    }
}
