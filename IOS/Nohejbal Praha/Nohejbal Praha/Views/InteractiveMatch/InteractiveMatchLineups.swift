//
//  InteractiveMatchLineups.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 20/04/2021.
//

import SwiftUI

/// Page that displays both teams lineups picking interface
struct InteractiveMatchLineups: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var match: InteractiveMatch
    var game: Game
    var matchId: Int
    @ObservedObject var viewModel = MatchLineupsViewModel()
    @State private var homeSelect = [0,1,2]
    @State private var awaySelect = [0,1,2]
    @State private var validPicks = true
    
    init(match: InteractiveMatch, game: Game, matchId: Int){
        self.match = match
        self.game = game
        self.matchId = matchId
    }
    
    var body: some View {
            VStack{
                /// Display content based on current match lineups loading state
                switch viewModel.loadingState{
                case .idle, .loaded:
                    ///
                    /// Show full content of page with info and pickers
                    InteractiveMatchRow(match: match)
                        .padding(.top,20)
                        .padding(.bottom, 10)
                    HStack{
                        Spacer()
                        Text("\(match.getGameName(gameId: game.id!, gameType: game.type!))")
                            .font(.system(size: 20))
                        Spacer()
                    }
                    .padding(10)
                    
                        HStack{
                            Spacer()
                            Text("lineup")
                                .font(.system(size: 20, weight: .bold))
                            Spacer()
                        }
                        .padding(10)
                Form{
                    Section(header: HStack{
                        AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(match.home_team!.logo!)")!)
                            .aspectRatio(contentMode: .fit)
                            .frame(width:20, height: 20)
                        Text("\(match.home_team!.name!)")
                            
                    }
                    .padding(.top, 10)) {
                            ForEach(0..<game.type!) { i in
                                HStack{
                                    Picker(selection: $homeSelect[i], label:Text("\(i+1).")) {
                                        ForEach(0 ..< match.getAvailableHomePlayers(players: match.available_home_players!, currentGame: game, playersIds: game.home_players!).count) {
                                            Text(match.getAvailableHomePlayers(players: match.available_home_players!, currentGame: game, playersIds: game.away_players!)[$0].name!)
                                                .foregroundColor(.black)
                                        }
                                    }
                                }
                            }
                        }
                    Section(header: HStack{
                        ScrollView{
                            AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(match.away_team!.logo!)")!)
                                .aspectRatio(contentMode: .fit)
                                .frame(width:20, height: 20)
                        }
                        Text("\(match.away_team!.name!)")
                    } ){
                    ForEach(0..<game.type!) { i in
                                HStack{
                                    Picker(selection: $awaySelect[i], label:Text("\(i+1).")) {
                                        ForEach(0 ..< match.getAvailableAwayPlayers(players: match.available_away_players!, currentGame: game, playersIds: game.home_players!).count) {
                                            Text(match.getAvailableAwayPlayers(players: match.available_away_players!, currentGame: game, playersIds: game.away_players!)[$0].name!)
                                                .foregroundColor(.black)
                                        }
                                    }
                                }
                            }
                    }
                    Section {
                        HStack{
                            NavigationLink(
                                destination: InteractiveMatchSet(match: match, game: game, set:1, matchId: matchId, homeIds: getHomeIds(), awayIds: getAwayIds()), isActive: .constant(viewModel.loadingState == .loaded)){
                                Spacer()
                                Text("1.SET")
                                    .foregroundColor(validPlayersPicked() ? .blue : Color("LightGray"))
                                    .simultaneousGesture(TapGesture().onEnded{
                                        if validPlayersPicked() && validPicks{
                                            viewModel.postMatchLineups(matchId: matchId, gameId: game.id!, homeIds: getHomeIds(), awayIds: getAwayIds())
                                        }
                                    })
                                Spacer()
                            }
                            .isDetailLink(false)
                            
                        }
                    }
                }
                case .loading:
                    ///
                    /// Show loading UI
                    let screenSize = UIScreen.main.bounds.size
                    CustomProgressView(){
                        Color.clear
                    }
                    .frame(width: screenSize.width, height: screenSize.height - 100)
                case .failed(let error):
                    ///
                    /// Show error message
                    Color.clear
                    .alert(isPresented: .constant(true)) {
                        Alert(title: Text("Chyba"), message: Text(error), dismissButton: .default(Text("dissmiss")){
                            presentationMode.wrappedValue.dismiss()
                        }
                        )
                    }
                }
        }
    }
    /// - Returns ids of picked players from home team
    private func getHomeIds() -> [Int]{
        let subArr = homeSelect[0...game.type!-1]
        return subArr.map {match.getAvailableHomePlayers(players: match.available_home_players!, currentGame: game, playersIds: game.home_players!)[$0].id!}
    }
    
    /// - Returns ids of picked players from home team
    private func getAwayIds() -> [Int]{
        let subArr = awaySelect[0...game.type!-1]
        return subArr.map {match.getAvailableAwayPlayers(players: match.available_away_players!, currentGame: game, playersIds: game.away_players!)[$0].id!}
    }
    
    /// Checks validity of picked players
    private func validPlayersPicked() -> Bool{
        let isValid = Set(homeSelect.prefix(game.type!)).count == homeSelect.prefix(game.type!).count && Set(awaySelect.prefix(game.type!)).count == awaySelect.prefix(game.type!).count
        self.validPicks = isValid
        return isValid
    }
}

struct InteractiveMatchLineups_Previews: PreviewProvider {
    static var previews: some View {
        InteractiveMatchLineups(match: InteractiveMatch.example, game: Game.example, matchId: 1)
    }
}
