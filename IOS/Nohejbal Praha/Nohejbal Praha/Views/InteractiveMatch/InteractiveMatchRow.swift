//
//  InteractiveMatchRow.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 01/07/2021.
//

import SwiftUI

/// View for displaying score of current match in progress, is in top part of most of interactive match progress pages
struct InteractiveMatchRow: View {
    var match : InteractiveMatch
    var body: some View {
        VStack{
            GeometryReader{ geometry in
                HStack(){
                    Spacer()
                    ///
                    /// Home team
                    Text(match.home_team!.name!)
                        .frame(maxWidth: geometry.size.width * 0.40, alignment: .trailing)
                        .font(.system(size: 15))
                    ScrollView{
                        AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(match.home_team!.logo!)")!)
                        .aspectRatio(contentMode: .fit)
                        .frame(width: geometry.size.width * 0.05, height: geometry.size.width * 0.05)
                    }
                    .frame(width: geometry.size.width * 0.05, height: geometry.size.width * 0.05)
                    ///
                    /// Score
                    VStack{
                        Text(match.getCurrentScore())
                            .font(.system(size: 16, weight: .semibold))
                        Text("fullScore")
                            .font(.system(size: 12))
                    }
                    .padding(.leading, 5)
                    .padding(.trailing, 5)
                    ///
                    /// Away team
                    ScrollView{
                        AsyncImage(url: URL(string:"\(Content.baseImgUrl)\(match.away_team!.logo!)")!)
                            .aspectRatio(contentMode: .fit)
                            .frame(width: geometry.size.width * 0.05, height: geometry.size.width * 0.05)
                    }
                    .frame(width: geometry.size.width * 0.05, height: geometry.size.width * 0.05)
                    Text(match.away_team!.name!)
                        .frame(maxWidth: geometry.size.width * 0.40, alignment: .leading)
                        .font(.system(size: 15))
                    Spacer()
                }
            }
        }
        .frame(height:20)
        .foregroundColor(.black)
        .padding(5)
    }
}

struct InteractiveMatchRow_Previews: PreviewProvider {
    static var previews: some View {
        InteractiveMatchRow(match: InteractiveMatch.example)
    }
}
