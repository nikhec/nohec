//
//  InteractiveMatchRecap.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 03/05/2021.
//

import SwiftUI

/// Page with note field after all games are finished
struct InteractiveMatchNotes: View {
    @ObservedObject var viewModel = MatchNotesViewModel()
    var match : Match
    var matchDetail: InteractiveMatch
    static var note:String = ""
    static var noteBinding = Binding<String>(get: { note }, set: { note = $0 } )
    var body: some View {
        VStack{
            InteractiveMatchRow(match: matchDetail)
                .padding(.bottom, 20)
            Text("note")
                .font(.system(size: 20, weight: .bold))
            MultilineTextField(NSLocalizedString("typeHere", comment: ""), text: InteractiveMatchNotes.noteBinding)
                .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray))
            NavigationLink(destination: InteractiveMatchRecap(match: match, matchDetail: matchDetail), isActive:.constant(viewModel.loadingState == .loaded)){
                ChangeScreenText(text: "recap", enabled: true)
                    .simultaneousGesture(TapGesture().onEnded{
                        viewModel.postMatchNotes(matchId: match.id!, note: InteractiveMatchNotes.note)
                    })
            }
            .isDetailLink(false)
            Spacer()
        }
        .padding()
    }
}

struct InteractiveMatchNotes_Previews: PreviewProvider {
    static var previews: some View {
        InteractiveMatchNotes(match: Match.example, matchDetail: InteractiveMatch.example)
    }
}

/// Text field with multiple line option
struct MultilineTextField: View {
    private var placeholder: String
    @State private var viewHeight: CGFloat = 120
    @State private var shouldShowPlaceholder = false
    @Binding private var text: String
    
    /// Text bindings
    private var internalText: Binding<String> {
        Binding<String>(get: { self.text } ) {
            self.text = $0
            self.shouldShowPlaceholder = $0.isEmpty
        }
    }

    var body: some View {
        MultilineField(text: self.internalText)
            .frame(height: 120)
            .background(placeholderView, alignment: .topLeading)
    }
    
    /// Displayed text if text field is initially empty
    var placeholderView: some View {
        Group {
            if shouldShowPlaceholder {
                Text(placeholder).foregroundColor(.gray)
                    .padding(.leading, 4)
                    .padding(.top, 8)
            }
        }
    }
    
    init (_ placeholder: String = "", text: Binding<String>) {
        self.placeholder = placeholder
        self._text = text
        self._shouldShowPlaceholder = State<Bool>(initialValue: self.text.isEmpty)
    }

}

