//
//  UserSettings.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 06/04/2021.
//

import Foundation
import SwiftUI
import Combine

/// Simple object managing user's credentials and token
class UserSettings: ObservableObject {
    @ObservedObject var viewModel = CheckLoginViewModel()
    @Published var username: String? {
        didSet {
            UserDefaults.standard.set(username, forKey: "username")
        }
    }
    @Published var password: String? {
        didSet {
            UserDefaults.standard.set(password, forKey: "password")
        }
    }
    @Published var token: String? {
        didSet {
            UserDefaults.standard.set(token, forKey: "token")
        }
    }
    
    init() {
        self.username = UserDefaults.standard.object(forKey: "username") as? String ?? ""
        self.password = UserDefaults.standard.object(forKey: "password") as? String ?? ""
        self.token = UserDefaults.standard.object(forKey: "token") as? String ?? ""
    }
    
    /// - Returns true if user is logged in
    func userLoggedIn() -> Bool{
        viewModel.getLoginCheck()
        return token != ""
    }
    
    /// Removes user's token from dictionary
    func logout(){
        UserDefaults.standard.removeObject(forKey: "token")
    }
}
