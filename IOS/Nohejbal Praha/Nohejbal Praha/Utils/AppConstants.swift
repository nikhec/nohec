//
//  AppConstants.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 02/03/2021.
//

import Foundation
import UIKit

struct Content {
    static var loginUrl = baseUrl + "api/users/login-token/"
    static var loginTestUrl = baseUrl + "api/users/logged-in-test/"
    static var signatureUrl = baseUrl + "api/users/signature/"
    static var seasonsUrl = baseUrl + "api/seasons/"
    static var leaguesUrl = baseUrl + "api/leagues/"
    static var standingsUrl = baseUrl + "api/leagues/"
    static var clubsUrl = baseUrl + "api/clubs/"
    static var matchesByDate = baseUrl + "api/matches-by-date/"
    static var myMatchesByDate = baseUrl + "api/my-matches-by-date/"
    static var matchesLeagueByRound = "/matches-by-round/"
    static var matches = baseUrl + "api/matches/"
    static var teams = baseUrl + "api/teams/"
    
    static var baseUrl = "http://lab.d3s.mff.cuni.cz:8664/"
    static var baseImgUrl = "http://lab.d3s.mff.cuni.cz:8664"
    
//    Local testing
//    static var baseUrl = "http://127.0.0.1:8000/"
//    static var baseImgUrl = "http://127.0.0.1:8000"
    
    static var standingsSuffix = "/standings/"
    static var detailSuffix = "/detail/"
    static var playersSuffix = "/players/"
    static var pastSuffix = "past/"
    static var futureSuffix = "upcoming/"
    static var enterSuffix = "/enter/"
    static var captainsSuffix = "/captains/"
    static var gameSuffix = "/game/"
    static var setSuffix = "/set/"
    static var notesSuffix = "/note/"
    static var signSuffix = "/sign/"
    
    static let day_formatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM."
        return formatter
    }()
    static let time_formatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter
    }()
    static let base_daytime_formatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter
    }()
    
    static let date_formatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd.MM."
        return formatter
    }()
    
    static let base_date_formatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    static let timeIntervalBeforeMatch = 15.0
    static let timeoutDuration = 30
    static let maxSubstitutions = 5
    static let matchesCount = 100
    static let leagueMatchCount = 4
}
