//
//  LoginHandler.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 29/03/2021.
//

import Alamofire
import Combine
import Foundation
import SwiftUI

/// Handling user login request
class LoginHandler: APIHandler{
    @Published var loginResponse: LoginResponse?
    @ObservedObject var userSettings = UserSettings()
    
    /// Request to get user's token
    /// - Parameter username : user's name
    /// - Parameter password : user's password
    func getToken(username: String, password: String) {
        state = .loading
        AF.request(Content.loginUrl, method: .post, parameters: ["username": username, "password": password]).responseDecodable{
            [weak self] (response: DataResponse<LoginResponse, AFError>) in
            guard let weakSelf = self else { return }
            
            guard let response = weakSelf.handleResponse(response) as? LoginResponse else {
                return
            }
            weakSelf.loginResponse = response
            if response.token != nil {
                weakSelf.userSettings.username = response.username
                weakSelf.userSettings.password = response.password
                weakSelf.userSettings.token = response.token
                weakSelf.state = .loaded
            } else {
                weakSelf.state = .failed("")
            }
            
        }
    }
}
