//
//  BaseArrayHandler.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 31/05/2021.
//

import Foundation
import Alamofire

/// Handles array response
class BaseArrayHandler<T: Decodable>: APIHandler{
    @Published var response: ArrayResponse<T>?
    
    /// Request to get response in form of array
    /// - Parameter url : url path for request
    /// - Parameter method : http method (get/post)
    func getData(url: String, method: HTTPMethod){
        state = .loading
        AF.request(url, method: method).responseDecodable{
            [weak self] (response: DataResponse<ArrayResponse<T>, AFError>) in
            guard let weakSelf = self else { return }
            debugPrint(response)
            
            guard let response = weakSelf.handleResponse(response) as? ArrayResponse<T> else {
                return
            }
            weakSelf.response = response
            weakSelf.state = LoadingState.loaded
        }
    
    }
    
    /// Request with headers to get response in form of array
    /// - Parameter url : url path for request
    /// - Parameter method : http method (get/post)
    /// - Parameter headers : request http headers
    func getDataHeaders(url: String, method: HTTPMethod, headers: HTTPHeaders){
        state = .loading
        AF.request(url, method: method, headers: headers).responseDecodable{
            [weak self] (response: DataResponse<ArrayResponse<T>, AFError>) in
            guard let weakSelf = self else { return }
            debugPrint(response)
            
            guard let response = weakSelf.handleResponse(response) as? ArrayResponse<T> else {
                return
            }
            weakSelf.response = response
            weakSelf.state = LoadingState.loaded
        }
    
    }
}

