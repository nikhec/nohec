//
//  EmptyResponseHandler.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 11/06/2021.
//

import Foundation
import Alamofire

/// Handles empty response 
class EmptyResponseHandler<T:Decodable>{
    @Published var response: T?
    @Published var state = LoadingState.idle
    
    /// Request expecting empty response, nonempty response signals error
    /// - Parameter url : url path for request
    /// - Parameter method : request http method
    /// - Parameter headers : request http headers
    /// - Parameter parameters : request http parameters
    func getDataHeaders(url: String, method: HTTPMethod, headers: HTTPHeaders, parameters: Parameters?){
        state = .loading
        AF.request(url, method: method, parameters: parameters, encoding: URLEncoding(arrayEncoding: .noBrackets), headers: headers).responseDecodable{
            [weak self] (response: DataResponse<T, AFError>) in
            guard let weakSelf = self else { return }
            
            if response.response != nil && response.response!.statusCode == 200 {
                weakSelf.state = LoadingState.loaded
            } else {
                debugPrint(response)
                weakSelf.state = LoadingState.failed("")
            }
        }
    }
    
    /// Request expecting empty response, nonempty response signals error
    /// - Parameter url : url path for request
    /// - Parameter method : request http method
    /// - Parameter headers : request http headers
    /// - Parameter parameters : request Set data object
    func getDataHeadersJsonSet(url: String, method: HTTPMethod, headers: HTTPHeaders, parameters: SetRequestObject){
        state = .loading
        AF.request(url, method: method, parameters: parameters, encoder: JSONParameterEncoder.default, headers: headers).responseDecodable{
            [weak self] (response: DataResponse<T, AFError>) in
            guard let weakSelf = self else { return }
            
            if response.response != nil && response.response!.statusCode == 200 {
                weakSelf.state = LoadingState.loaded
            } else {
                debugPrint(response)
                weakSelf.state = LoadingState.failed("")
            }
        }
    }
    
    /// Request expecting empty response, nonempty response signals error
    /// - Parameter url : url path for request
    /// - Parameter method : request http method
    /// - Parameter headers : request http headers
    /// - Parameter parameters : request Singings data object
    func getDataHeadersJsonSignings(url: String, method: HTTPMethod, headers: HTTPHeaders, parameters: SigningsRequestObject){
        state = .loading
        AF.request(url, method: method, parameters: parameters, encoder: JSONParameterEncoder.default, headers: headers).responseDecodable{
            [weak self] (response: DataResponse<T, AFError>) in
            guard let weakSelf = self else { return }
            
            if response.response != nil && response.response!.statusCode == 200 {
                weakSelf.state = LoadingState.loaded
            } else {
                debugPrint(response)
                weakSelf.state = LoadingState.failed("")
            }
        }
    }
    
    /// Request expecting empty response, nonempty response signals error
    /// - Parameter url : url path for request
    /// - Parameter method : request http method
    /// - Parameter headers : request http headers
    /// - Parameter parameters : request Singings data object
    func getDataHeadersJsonSigningsNoReferee(url: String, method: HTTPMethod, headers: HTTPHeaders, parameters: SigningsRequestObjectNoReferee){
        state = .loading
        AF.request(url, method: method, parameters: parameters, encoder: JSONParameterEncoder.default, headers: headers).responseDecodable{
            [weak self] (response: DataResponse<T, AFError>) in
            guard let weakSelf = self else { return }
            
            if response.response != nil && response.response!.statusCode == 200 {
                weakSelf.state = LoadingState.loaded
            } else {
                debugPrint(response)
                weakSelf.state = LoadingState.failed("")
            }
        }
    }
}
