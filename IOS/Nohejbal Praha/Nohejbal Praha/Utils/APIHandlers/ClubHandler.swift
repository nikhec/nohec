//
//  ClubHandler.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 07/05/2021.
//

import Foundation
import Alamofire

/// Club response handler 
class ClubHandler<T:Decodable>: APIHandler{
    @Published var clubResponse: T?
    
    /// Request to get club's data
    /// - Parameter url : url path for request
    func getClub(url: String){
        state = .loading
        AF.request(url, method: .get).responseDecodable{
            [weak self] (response: DataResponse<T, AFError>) in
            guard let weakSelf = self else { return }
            
            guard let response = weakSelf.handleResponse(response) as? T else {
                return
            }
            weakSelf.clubResponse = response
            weakSelf.state = LoadingState.loaded
        }
    }
}
