//
//  BaseSimpleHandler.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 31/05/2021.
//

import Foundation
import Alamofire

/// Handles single element type response
class BaseSimpleHandler<T:Decodable>: APIHandler{
    @Published var response: T?
    
    /// Request to get response in form of single element
    /// - Parameter url : url path for request
    func getData(url: String){
        state = .loading
        AF.request(url, method: .get).responseDecodable{
            [weak self] (response: DataResponse<T, AFError>) in
            guard let weakSelf = self else { return }
            
            debugPrint(response)
            guard let response = weakSelf.handleResponse(response) as? T else {
                return
            }
            
            weakSelf.response = response
            weakSelf.state = LoadingState.loaded
        }
    }
    
    /// Request to get response in form of single element
    /// - Parameter url : url path for request
    /// - Parameter method : request http method
    /// - Parameter headers : request http headers
    /// - Parameter parameters : request http parameters
    func getDataHeaders(url: String, method: HTTPMethod, headers: HTTPHeaders, parameters: Parameters?){
        state = .loading
        AF.request(url, method: method, parameters: parameters, headers: headers).responseDecodable{
            [weak self] (response: DataResponse<T, AFError>) in
            guard let weakSelf = self else { return }
            
            switch response.response!.statusCode {
            case 200, 201:
                debugPrint(response)
                guard let response = weakSelf.handleResponse(response) as? T else {
                    return
                }
                weakSelf.response = response
                weakSelf.state = LoadingState.loaded
            case 403:
                weakSelf.state = .failed("")
                return
            case 409:
                weakSelf.state = .failed("lockHeld")
                return
            case 410:
                weakSelf.state = .failed("matchAlreadyFinished")
                return
            default:
                debugPrint(response.response!.statusCode)
                weakSelf.state = .failed("")
                return
            }
            
        }

    }
}
