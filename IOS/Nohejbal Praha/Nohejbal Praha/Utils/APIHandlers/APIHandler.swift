//
//  APIHandler.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 29/03/2021.
//

import Foundation
import Alamofire
import Combine

/// Base API response handler
class APIHandler{
    @Published var state = LoadingState.idle
    
    /// Parses response received
    /// T represents parsing target type
    /// - Parameter response : request response
    /// - Returns parsed value, nil in case of failure
    func handleResponse<T: Decodable>(_ response: DataResponse<T, AFError>) -> Any?{
        switch response.result {
        case .success:
            return response.value
        case .failure(let err):
            state = LoadingState.failed(err.localizedDescription)
            return nil
        }
    }
}
