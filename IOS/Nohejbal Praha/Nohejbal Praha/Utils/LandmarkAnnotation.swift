//
//  LandmarkAnnotation.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 01/03/2021.
//

import Foundation
import SwiftUI
import MapKit

/// Represents annotation for map view
class LandmarkAnnotation: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String?,
         subtitle: String?,
         coordinate: CLLocationCoordinate2D) {
            self.title = title
            self.subtitle = subtitle
            self.coordinate = coordinate
    }
}
