//
//  ImageLoader.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 21/06/2021.
//

import Foundation
import SwiftUI
import Combine

/// Image loading handler
class ImageLoader: ObservableObject {
    @Published var image: UIImage?
    private let url: URL
    private var cancellable: AnyCancellable?
    private static let imageProcessingQueue = DispatchQueue(label: "image-processing")
    private(set) var isLoading = false
        
    
    init(url: URL) {
        self.url = url
    }

    deinit {
        cancel()
    }
    
    func load() {
        /// Return if previous loading operation is not done
        guard !isLoading else {
            return
        }
        /// Url request
        /// reacts to events by modifing loading state
        cancellable = URLSession.shared.dataTaskPublisher(for: url)
                    .subscribe(on: Self.imageProcessingQueue)
                    .map { UIImage(data: $0.data) }
                    .handleEvents(receiveSubscription: { [weak self] _ in self?.onStart() },
                                      receiveCompletion: { [weak self] _ in self?.onFinish() },
                                      receiveCancel: { [weak self] in self?.onFinish() })
                    .replaceError(with: nil)
                    .receive(on: DispatchQueue.main)
                    .sink { [weak self] in self?.image = $0 }
    }

    func cancel() {}
    
    private func onStart() {
        isLoading = true
    }
       
    private func onFinish() {
        isLoading = false
    }
}
