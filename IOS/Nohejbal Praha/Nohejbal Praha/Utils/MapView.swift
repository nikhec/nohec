//
//  MapView.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 01/03/2021.
//
//
import SwiftUI
import MapKit

/// Basic map view component
struct MapView: UIViewRepresentable {
    var latitude: Double;
    var longitude: Double;
    let zoom: Double = 0.01
    func makeUIView(context: Context) -> MKMapView{
             MKMapView(frame: .zero)
        }
    
    /// Sets zoom and annotaion
    func updateUIView(_ view: MKMapView, context: Context){
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let span = MKCoordinateSpan(latitudeDelta: zoom, longitudeDelta: zoom)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        view.setRegion(region, animated: true)
        view.addAnnotation(LandmarkAnnotation(title: "", subtitle: "", coordinate: coordinate))
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(latitude: 50.08095721983656, longitude: 14.424335285462407)
    }
}

