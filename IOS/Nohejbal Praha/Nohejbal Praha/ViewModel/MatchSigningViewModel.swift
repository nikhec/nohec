//
//  MatchSigningViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 21/06/2021.
//

import Foundation
import SwiftUI
import Combine
import Alamofire

/// Viewmodel handling interactive match signings
class MatchSigningViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    @Published var home_sign = ""
    @Published var away_sign = ""
    @Published var referee_sign = ""
    var signHandler = EmptyResponseHandler<String?>()
    var signsValid : String?
    @Published var loadingState = LoadingState.idle
    
    /// Publishing from response
    private var areSignsValidPublisher: AnyPublisher<String?, Never> {
        signHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return nil
                }
            
                return response
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        signHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        areSignsValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.signsValid, on: self)
            .store(in: &disposables)
    }
    
    /// Calls handler to send signings for match identified by matchId
    /// - Parameter matchId : match identifier
    /// - Parameter match : match
    func postMatchSigns(matchId: Int, match: Match){
        if match.referees != nil && !match.referees!.isEmpty{
        signHandler.getDataHeadersJsonSignings(url: Content.matches + "\(matchId)" + Content.signSuffix, method: .post, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]), parameters: SigningsRequestObject(home_captain: home_sign, away_captain: away_sign, referee: referee_sign))
        } else {
            signHandler.getDataHeadersJsonSigningsNoReferee(url: Content.matches + "\(matchId)" + Content.signSuffix, method: .post, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]), parameters: SigningsRequestObjectNoReferee(home_captain: home_sign, away_captain: away_sign))
        }
    }

    
}
