//
//  LoginViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 30/03/2021.
//

import Foundation
import SwiftUI
import Combine

/// Viewmodel handling user's login page process
class LoginViewModel: ObservableObject, Identifiable {
    @Published var username = ""
    @Published var password = ""
    @Published var loadingState = LoadingState.idle
    private var disposables: Set<AnyCancellable> = []
    var loginHandler = LoginHandler()
    @Published var token = ""
    
    /// Publisher of loading state changes
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        loginHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    /// Publisher of user token
    private var isAuthenticatedPublisher: AnyPublisher<String, Never> {
        loginHandler.$loginResponse
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return ""
                }
                return response.token ?? ""
            }
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        isAuthenticatedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.token, on:self)
            .store(in: &disposables)
    }
    
    /// This function calls handler to query token for user credentials
    func getToken(){
        loginHandler.getToken(username: username, password: password)
    }
    
    
}
