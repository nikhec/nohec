//
//  ClubsViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 07/05/2021.
//

import Foundation
import SwiftUI
import Combine

/// Viewmodel handling clubs overview
class ClubsViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var clubsHandler = BaseArrayHandler<ClubListItem>()
    @Published var clubs : [ClubListItem] = []
    @Published var loadingState = LoadingState.idle
    
    /// Publishing loaded clubs from response
    private var areClubsLoadedPublisher: AnyPublisher<[ClubListItem], Never> {
        clubsHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return []
                }
            
                return response.items ?? []
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        clubsHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init(){
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)

        areClubsLoadedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.clubs, on: self)
            .store(in: &disposables)
    }
    
    /// This function calls handler to query data for all clubs
    func getClubs(){
        clubsHandler.getData(url: Content.clubsUrl, method: .get)
    }
    
    
}


