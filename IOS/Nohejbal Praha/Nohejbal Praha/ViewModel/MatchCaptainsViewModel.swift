//
//  MatchCaptainsViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 11/06/2021.
//

import Foundation
import SwiftUI
import Combine
import Alamofire

/// Viewmodel handling interactive match captains
class MatchCaptainsViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var captainsHandler = EmptyResponseHandler<String?>()
    var captainsValid : String?
    @Published var loadingState = LoadingState.idle
    
    /// Publishing from response
    private var areCaptainsValidPublisher: AnyPublisher<String?, Never> {
        captainsHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return nil
                }
            
                return response
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        captainsHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        areCaptainsValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.captainsValid, on: self)
            .store(in: &disposables)
    }
    
    /// Calls handler to send id of captain for each team participating in this match identified by id
    /// - Parameter matchId : match identifier
    /// - Parameter homeId : home captain id
    /// - Parameter awayId : away captain id
    func postMatchCaptains(matchId: Int, homeId: Int, awayId: Int){
        captainsHandler.getDataHeaders(url: Content.matches + "\(matchId)" + Content.captainsSuffix, method: .post, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]), parameters: ["home": homeId, "away": awayId])
    }
    
}
