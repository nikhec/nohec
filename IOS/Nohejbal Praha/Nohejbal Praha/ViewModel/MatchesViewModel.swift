//
//  MatchesViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 28/05/2021.
//

import Foundation
import SwiftUI
import Combine
import Alamofire

/// Viewmodel handling matches overview
class MatchesViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var matchesHandler = BaseArrayHandler<MatchListDate>()
    @Published var matches : [MatchListDate] = []
    @Published var loadingState = LoadingState.idle
    
    /// Publishing loaded matches from response
    private var areMatchesLoadedPublisher: AnyPublisher<[MatchListDate], Never> {
        matchesHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return []
                }
                return response.items ?? []
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        matchesHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        areMatchesLoadedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.matches, on: self)
            .store(in: &disposables)
        
    }
    /// Calls handler to query data for past matches by date
    /// - Parameter count : number of matches to retrieve
    func getPastMatchesByDate(count: Int){
        matchesHandler.getData(url: Content.matchesByDate + Content.pastSuffix + "?count=\(count)", method: .get)
    }
    
    /// Calls handler to query data for past matches from league identified by id grouped by round
    /// - Parameter leagueId : league identifier
    /// - Parameter count : number of matches to retrieve
    func getLeaguePastMatchesByRound(leagueId: Int, count: Int){
        matchesHandler.getData(url: Content.leaguesUrl + "\(leagueId)" + Content.matchesLeagueByRound + Content.pastSuffix + "?count=\(count)", method: .get)
    }
    
    /// Calls handler to query data for future matches from league identified by id grouped by round
    /// - Parameter leagueId : league identifier
    /// - Parameter count : number of matches to retrieve
    func getLeagueFutureMatchesByRound(leagueId: Int, count: Int){
        matchesHandler.getData(url: Content.leaguesUrl + "\(leagueId)" + Content.matchesLeagueByRound + Content.futureSuffix + "?count=\(count)", method: .get)
    }
    
    /// Calls handler to query data for future matches relevant to logged in user, grouped by date
    /// - Parameter count : number of matches to retrieve
    func getMyFutureMatchesByDate(count: Int){
        matchesHandler.getDataHeaders(url: Content.myMatchesByDate + Content.futureSuffix + "?count=\(count)", method: .get, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]))

    }
    
    /// Calls handler to query data for past matches relevant to logged in user, grouped by date
    /// - Parameter count : number of matches to retrieve
    func getMyPastMatchesByDate(count: Int){
        matchesHandler.getDataHeaders(url: Content.myMatchesByDate + Content.pastSuffix + "?count=\(count)", method: .get, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]))
    }

}
