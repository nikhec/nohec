//
//  ClubViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 07/05/2021.
//

import Foundation
import SwiftUI
import Combine

/// Viewmodel handling club detail data
class ClubViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var clubHandler = BaseSimpleHandler<ClubResponse>()
    @Published var club : ClubResponse?
    @Published var loadingState = LoadingState.idle
    
    /// Publishing loaded club from response
    private var isClubLoadedPublisher: AnyPublisher<ClubResponse?, Never> {
        clubHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return nil
                }
            
                return response
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        clubHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        isClubLoadedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.club, on: self)
            .store(in: &disposables)
    }
    
    /// This function calls handler to query data for current club
    /// - Parameter clubId : identifier of club
    /// - Parameter latest_season : flag wheter data only from latest season should be displayed
    func getClub(clubId: Int, latest_season: Bool){
        clubHandler.getData(url: Content.clubsUrl + "\(clubId)" + Content.detailSuffix + "?latest_season=\(latest_season)")
    }
    
}
