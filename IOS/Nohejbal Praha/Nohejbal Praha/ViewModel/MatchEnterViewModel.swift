//
//  MatchEnterViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 08/06/2021.
//

import Foundation
import SwiftUI
import Combine
import Alamofire

/// Viewmodel handling interactive match current state
class MatchEnterViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var matchHandler = BaseSimpleHandler<InteractiveMatch>()
    @Published var match : InteractiveMatch?
    @Published var loadingState = LoadingState.idle
    
    /// Publishing loaded interactive match state from response
    private var isMatchEnterStateLoadedPublisher: AnyPublisher<InteractiveMatch?, Never> {
        matchHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return nil
                }
            
                return response
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        matchHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        isMatchEnterStateLoadedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.match, on: self)
            .store(in: &disposables)
    }
    
    /// Calls handler to query data for user's match identified by id, if match has not started yet, it is started by this call..
    /// - Parameter matchId : match identifier
    func postMatchEnter(matchId: Int){
        matchHandler.getDataHeaders(url: Content.matches + "\(matchId)" + Content.enterSuffix, method: .post, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]), parameters: nil)
    }
    
}
