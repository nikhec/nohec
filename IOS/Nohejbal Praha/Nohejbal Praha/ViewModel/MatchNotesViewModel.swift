//
//  MatchNotesViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 18/06/2021.
//

import Foundation
import SwiftUI
import Combine
import Alamofire

/// Viewmodel handling interactive match notes
class MatchNotesViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var notesHandler = EmptyResponseHandler<String?>()
    var notesValid : String?
    @Published var loadingState = LoadingState.idle
    
    /// Publishing from response
    private var areNotesValidPublisher: AnyPublisher<String?, Never> {
        notesHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return nil
                }
            
                return response
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        notesHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        areNotesValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.notesValid, on: self)
            .store(in: &disposables)
    }

    /// Calls handler to send note for match identified by matchId
    /// - Parameter matchId : match identifier
    /// - Parameter note : text of note
    func postMatchNotes(matchId: Int, note: String){
        notesHandler.getDataHeaders(url: Content.matches + "\(matchId)" + Content.notesSuffix, method: .post, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]), parameters: ["note": note])
    }
    
}
