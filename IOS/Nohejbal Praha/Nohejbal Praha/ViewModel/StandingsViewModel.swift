//
//  StandingsViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 19/04/2021.
//

import Foundation
import SwiftUI
import Combine

/// Viewmodel handling standings data for league
class StandingsViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var standingsHandler = BaseArrayHandler<Standing>()
    @Published var standings : [Standing] = []
    @Published var loadingState = LoadingState.idle
    
    /// Publishing loaded standings from response
    private var areStandingsLoadedPublisher: AnyPublisher<[Standing], Never> {
        standingsHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return []
                }
            
                return response.items ?? []
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        standingsHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        areStandingsLoadedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.standings, on: self)
            .store(in: &disposables)
    }
    
    /// This function calls handler to query data for all standings in current league
    /// - Parameter leagueId : Id of league
    func getStandings(leagueId: Int){
        standingsHandler.getData(url: Content.standingsUrl + "\(leagueId)" + Content.standingsSuffix, method: .get)
    }
    
    
}


