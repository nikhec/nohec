//
//  LeaguesViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 13/04/2021.
//

import Foundation
import SwiftUI
import Combine

/// Viewmodel handling leagues overview
class LeaguesViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var leaguesHandler = BaseArrayHandler<LeagueListItem>()
    @Published var leagues : [LeagueListItem] = []
    @Published var loadingState = LoadingState.idle
    
    /// Publisher of leagues data
    private var areLeaguesLoadedPublisher: AnyPublisher<[LeagueListItem], Never> {
        leaguesHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return []
                }
                return response.items ?? []
            }

            .eraseToAnyPublisher()
    }
    /// Publisher of loading state changes
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        leaguesHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        areLeaguesLoadedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.leagues, on: self)
            .store(in: &disposables)

        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
    }
    /// This function calls handler to query data for leagues from all seasons
    func getLeagues(){
        leaguesHandler.getData(url: Content.leaguesUrl, method: .get)
    }
    
    /// This function calls handler to query data for leagues in latest season
    func getLeaguesLatestSeason(){
        leaguesHandler.getData(url: Content.leaguesUrl + "?latest_season=\(true)", method: .get)
    }
    
    
}

