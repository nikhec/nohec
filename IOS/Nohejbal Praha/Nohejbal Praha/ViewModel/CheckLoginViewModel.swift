//
//  CheckLoginViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 14/06/2021.
//

import Foundation
import SwiftUI
import Combine
import Alamofire

/// Viewmodel handling checking of user's login validity
class CheckLoginViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var loginHandler = EmptyResponseHandler<String?>()
    var loginValid : String?
    @Published var loadingState = LoadingState.idle
    
    /// Publishing  from response
    private var isLoginCheckedPublisher: AnyPublisher<String?, Never> {
        loginHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return nil
                }
            
                return response
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        loginHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        isLoginCheckedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loginValid, on: self)
            .store(in: &disposables)
    }
    
    /// Calls handler to check if user's auth is still valid.
    func getLoginCheck(){
        loginHandler.getDataHeaders(url: Content.loginTestUrl, method: .get, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]), parameters: nil)
    }
    
}

