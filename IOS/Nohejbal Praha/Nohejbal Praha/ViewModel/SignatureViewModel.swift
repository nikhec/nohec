//
//  SignatureViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 02/07/2021.
//

import Foundation
import SwiftUI
import Combine
import Alamofire

/// Viewmodel handling user's signature key
class SignatureViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var signatureHandler = BaseSimpleHandler<Signature>()
    @Published var signature : Signature?
    @Published var loadingState = LoadingState.idle
    
    /// Publishing from response
    private var isSignatureLoadedPublisher: AnyPublisher<Signature?, Never> {
        signatureHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return nil
                }
            
                return response
            }
            .eraseToAnyPublisher()
    }
    
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        signatureHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        isSignatureLoadedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.signature, on: self)
            .store(in: &disposables)
    }
    
    /// This function calls handler to query data for user's signature
    func getSignature(){
        signatureHandler.getDataHeaders(url: Content.signatureUrl, method: .get, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]), parameters: nil)
    }
    
}
