//
//  MatchDetailViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 31/05/2021.
//

import Foundation
import SwiftUI
import Combine

/// Viewmodel handling match detail
class MatchDetailViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var matchHandler = BaseSimpleHandler<MatchDetailData>()
    @Published var match : MatchDetailData?
    @Published var loadingState = LoadingState.idle
    
    /// Publishing loaded matchDetail from response
    private var isMatchDetailLoadedPublisher: AnyPublisher<MatchDetailData?, Never> {
        matchHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return nil
                }
            
                return response
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        matchHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        isMatchDetailLoadedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.match, on: self)
            .store(in: &disposables)
    }
    
    /// Calls handler to query for detail data of match identified by id
    /// - Parameter matchId : match identifier
    func getMatchDetail(matchId: Int){
        matchHandler.getData(url: Content.matches + "\(matchId)" + Content.detailSuffix)
    }
    
}
