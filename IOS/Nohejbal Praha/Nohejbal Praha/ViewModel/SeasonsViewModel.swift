//
//  LeaguesViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 12/04/2021.
//

import Foundation
import SwiftUI
import Combine

/// Viewmodel handling loading seasons
class SeasonsViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var seasonsHandler = BaseArrayHandler<Season>()
    @Published var seasons : [Season] = []
    @Published var loadingState = LoadingState.idle
    
    /// Publisher of loading state changes
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        seasonsHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    /// Publisher of seasons data
    private var areSeasonsLoadedPublisher: AnyPublisher<[Season], Never> {
        seasonsHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return []
                }
            
                return response.items ?? []
            }
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        areSeasonsLoadedPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.seasons, on: self)
            .store(in: &disposables)
    }
    
    /// This function calls handler to query data for seasons
    func getSeasons(){
        seasonsHandler.getData(url: Content.seasonsUrl, method: .get)
    }
    
    
}
