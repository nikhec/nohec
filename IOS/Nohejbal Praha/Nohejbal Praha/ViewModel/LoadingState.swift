//
//  LoadingState.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 18/05/2021.
//

import Foundation
import Combine

/// General viewmodel state
enum LoadingState : Equatable{
    case idle
    case loading
    case failed(String)
    case loaded
}
