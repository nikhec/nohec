//
//  MatchLineupsViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 14/06/2021.
//

import Foundation
import SwiftUI
import Combine
import Alamofire

/// Viewmodel handling interactive match lineups
class MatchLineupsViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var lineupsHandler = EmptyResponseHandler<String?>()
    var lineupsValid : String?
    @Published var loadingState = LoadingState.idle
    
    /// Publishing from response
    private var areLineupsValidPublisher: AnyPublisher<String?, Never> {
        lineupsHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return nil
                }
            
                return response
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        lineupsHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        areLineupsValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.lineupsValid, on: self)
            .store(in: &disposables)
    }
    
    /// Calls handler to send ids of players for each team participating in this game identified by gameId of the match identified by matchId
    /// - Parameter matchId : match identifier
    /// - Parameter gameId : game identifier
    /// - Parameter homeIds : home players ids
    /// - Parameter awayIds : away players ids
    func postMatchLineups(matchId: Int, gameId: Int, homeIds: [Int], awayIds: [Int]){
        lineupsHandler.getDataHeaders(url: Content.matches + "\(matchId)" + Content.gameSuffix, method: .post, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]), parameters: ["id": gameId, "home_players": homeIds, "away_players": awayIds])
    }
    
}

