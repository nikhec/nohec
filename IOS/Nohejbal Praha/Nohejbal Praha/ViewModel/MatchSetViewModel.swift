//
//  MatchSetViewModel.swift
//  Nohejbal Praha
//
//  Created by Richard Savčinský on 14/06/2021.
//

import Foundation
import SwiftUI
import Combine
import Alamofire

/// Viewmodel handling interactive match set
class MatchSetViewModel: ObservableObject {
    private var disposables: Set<AnyCancellable> = []
    var setHandler = EmptyResponseHandler<String?>()
    var setValid : String?
    @Published var loadingState = LoadingState.idle
    
    /// Publishing from response
    private var isSetValidPublisher: AnyPublisher<String?, Never> {
        setHandler.$response
            .receive(on: RunLoop.main)
            .map { response in
                guard let response = response else {
                    return nil
                }
            
                return response
            }
            .eraseToAnyPublisher()
    }
    
    /// Publishing loading state
    private var loadingStatePublisher: AnyPublisher<LoadingState, Never> {
        setHandler.$state
            .receive(on: RunLoop.main)
            .map {$0}
            .eraseToAnyPublisher()
    }
    
    init() {
        loadingStatePublisher
            .receive(on: RunLoop.main)
            .assign(to: \.loadingState, on: self)
            .store(in: &disposables)
        
        isSetValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.setValid, on: self)
            .store(in: &disposables)
    }
    
    /// Calls handler to send data of current set in the game identified by gameId of the match identified by matchId
    /// - Parameter matchId : match identifier
    /// - Parameter gameId : game identifier
    /// - Parameter homePoints : number of points scored by home team
    /// - Parameter awayPoints : number of points scored by away team
    /// - Parameter homeTimeout : number of timeouts used by home team
    /// - Parameter awayTimeout : number of timeouts used by away team
    /// - Parameter homeSubs : list of home team substitutions
    /// - Parameter awaySubs : list of away team substitutions
    func postMatchSet(matchId: Int, gameId: Int, homePoints: Int, awayPoints: Int, homeTimeout: Int, awayTimeout: Int, homeSubs: [Substitution], awaySubs: [Substitution]){
        setHandler.getDataHeadersJsonSet(url: Content.matches + "\(matchId)" + Content.setSuffix, method: .post, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Token " + UserSettings.init().token!)]), parameters: SetRequestObject(game: gameId, home_points: homePoints, away_points: awayPoints, home_timeout: homeTimeout, away_timeout: awayTimeout, home_substitutions: homeSubs, away_substitutions: awaySubs))
    }
    
    
    
}
