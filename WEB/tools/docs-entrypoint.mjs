/**
 * Generates entrypoint for documentation with general information and links to documentation parts.
 *
 * @module
 * @category Tools
 * @name Docs Entrypoint
 */

import { readFileSync, writeFileSync } from 'fs'
import { join, dirname } from 'path'
import { fileURLToPath } from 'url'
import showdown from 'showdown'

const dir = dirname(fileURLToPath(import.meta.url))
const README_FILE = join(dir, '..', '..', 'README.md')
const TARGET_FILE = join(dir, '..', 'docs', 'index.html')

/**
 * Creates markdown template for documentation entrypoint file.
 * It is combination of intro parsed out from repository's root README.md and ToC with links to documentation parts.
 *
 * @function
 * @returns {string} - Created markdown string
 */
const createMarkdownEntrypoint = () => {
  const readmeContent = readFileSync(README_FILE, { encoding: 'utf8' })
  const firstBlockEnd = readmeContent.indexOf('---')
  const entrypointContent = readmeContent.substring(0, firstBlockEnd)
    + '\n## Continue to'
    + '\n - **[API documentation](./API/index.html)**'
    + '\n - **[ANDROID documentation](./ANDROID/index.html)**'
    + '\n - **[IOS documentation](./IOS/index.html)**'
    + '\n - **[WEB documentation](./WEB/index.html)**'

  writeFileSync(TARGET_FILE, entrypointContent)
  return entrypointContent
}

/**
 * Converts previously created markdown to html and places it into defined template.
 * The template uses styling and icon of WEB/ documentation.
 *
 * @function
 * @param {string} markdown
 * @returns {string} - Complete html string
 */
const convertMarkdownToHtml = (markdown) => {
  const converter = new showdown.Converter()
  const html = converter.makeHtml(markdown)

  return `
    <html lang="en">
      <head>
        <title>Nohec documentation</title>
        <link rel="icon" href="WEB/icon.png">
        <link type="text/css" rel="stylesheet" href="WEB/styles/app.min.css">
      </head>
      <body>
        <div id="main" style="padding-top: 0 !important">
          <div class="core" id="main-content-wrapper">
            <div class="content">
              ${html}
            </div>
          </div>
        </div>
      </body>
    </html>
  `
}

const markdown = createMarkdownEntrypoint()
const html = convertMarkdownToHtml(markdown)
writeFileSync(TARGET_FILE, html)
