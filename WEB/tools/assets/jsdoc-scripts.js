/**
 * @module
 * @category Tools
 * @name JSDoc Scripts
 */

const OMIT_SUB_CATEGORIES = ['MODULES', 'COMPONENTS']

/**
 * Truncates subcategory names in the left navbar on window's load event.
 *
 * @function
 */
const truncNames = () => {
  const subCategories = document.getElementById('sidebarNav').getElementsByTagName('h3')

  Array.from(subCategories).forEach(subCategory => {
    const nameParts = subCategory.innerText.split(' / ')

    if (OMIT_SUB_CATEGORIES.includes(nameParts[0]?.toUpperCase())) {
      const newName = nameParts.slice(1).join(' / ')
      if (newName === '') {
        subCategory.remove()
      } else {
        subCategory.innerText = newName
      }
    }
  })
}

/**
 * Alphabetically sorts categories and subcategories within the left navbar.
 *
 * @function
 */
const sortCategories = () => {
  const categories = document.getElementById('sidebarNav').getElementsByClassName('category')

  Array.from(categories).forEach(category => {
    const newChildren = []
    const children = Array.from(category.children)

    children.forEach((child, index) => {
      if (child.tagName !== 'UL' || children[index - 1]?.tagName !== 'H3') {
        if (child.tagName === 'H3') {
          const newWrapper = document.createElement('div')
          newWrapper.appendChild(child)

          const nextChild = children[index + 1]

          if (nextChild?.tagName === 'UL') {
            const newList = document.createElement('ul')
            Array.from(nextChild.getElementsByTagName('li'))
              .sort((a, b) => a.textContent.localeCompare(b.textContent))
              .forEach(li => newList.appendChild(li))

            newWrapper.appendChild(newList)
          }
          newChildren.push(newWrapper)
        } else {
          newChildren.push(child)
        }
      }
    })

    category.innerHTML = ''
    newChildren
      .sort((a, b) => {
        if (a.tagName === b.tagName) {
          if (a.tagName === 'H2') {
            return a.textContent.localeCompare(b.textContent)
          } else if (a.tagName === 'DIV') {
            const aHeader = a.getElementsByTagName('H3')[0]?.textContent
            const bHeader = b.getElementsByTagName('H3')[0]?.textContent

            return aHeader && bHeader
              ? aHeader.localeCompare(bHeader)
              : -1
          }
          return -1
        } else if (a.tagName === 'H2' || (a.tagName === 'UL' && b.tagName !== 'H2')) {
          return -1
        } else if (b.tagName === 'H2' || (b.tagName === 'UL' && a.tagName !== 'H2')) {
          return 1
        }
        return 0
      })
      .forEach(child => category.appendChild(child))
  })
}

window.onload = function () {
  truncNames()
  sortCategories()
}
