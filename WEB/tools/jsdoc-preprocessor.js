/**
 * @module
 * @category Tools
 * @name JSDoc Preprocessor
 */

/**
 * Hooks into JSDoc preprocessing to automatically add annotations to the `source` string.
 *
 *  - Each `.jsx` file is categorized under `Components` and gets assigned `subcategory` based on its path.
 *  - Each `.js` file inside `/src/styles` directory handled the same way components are.
 *  - `.js` files inside `/tools` directory and `index.js` files are not processed in any way.
 *  - Other `.js` files are categorized under `Modules` and gets assigned `name` and `subcategory` based on its path.
 *
 *  @param {Object} event
 *  @param {string} event.filename - The name of the file
 *  @param {string} event.source - The contents of the file
 */
const beforeParse = event => {
  const { filename, source } = event

  if (filename.endsWith('index.js') || filename.match(/(\/|\\)tools(\/|\\).+\.js$/)) {
    return
  }
  const matches = filename.match(/src(\/|\\)styles(\/|\\).*(\/|\\)([^/\\]+)\.js$/)
  if (matches) {
    const fileName = matches[4]
    const fileDoc = [
      ` * @module ${fileName}`,
      ' * @category Components',
      ' * @subcategory Styled',
    ].join('\n')

    if (source.startsWith('/**')) {
      event.source = source.replace(/ \*\//g, ' * @styled-component\n */')
        .replace(' * @styled-component', fileDoc)
    } else {
      event.source = ['/**', fileDoc, ' */'].join('\n')
        + source.replace(/ \*\//g, ' * @styled-component\n */')
    }
  } else if (filename.endsWith('.jsx')) {
    const extraDoc = [' * @category Components']

    if (filename.includes('components')) {
      const subcategory = filename
        .replace(/^.*(\/|\\)components(\/|\\)((.*)(\/|\\))?[^/\\]+\.jsx$/, '$4')
        .replace(/(\/|\\)/, ' / ')

      extraDoc.push(` * @subcategory ${subcategory}`)
    } else {
      extraDoc.push(' * @subcategory src')
    }

    event.source = source.replace(/( \* @component)/, `$1\n${extraDoc.join('\n')}`)
  } else if (filename.endsWith('.js')) {
    const [subcategory, name] = filename
      .replace(/^.*(\/|\\)src(\/|\\)(.*)(\/|\\)([^/\\]+)\.js$/, '$3|$5')
      .replace(/(\/|\\)/, ' / ')
      .split('|')

    const extraDoc = [
      ' * @module',
      ` * @name ${name}`,
      ' * @category Modules',
      ` * @subcategory ${subcategory}`,
    ].join('\n')

    if (source.startsWith('/**')) {
      event.source = source.replace(' */', `${extraDoc}\n */`)
    } else {
      event.source = ['/**', extraDoc, ' */'].join('\n') + source
    }
  }
}

/**
 * Here, we modify name of each created `Doclet` object of styled components to look like React component.
 * It has to be done this way because `better-docs`'s `@component` decorator does not work for styled components.
 *
 * @param {Object} event
 */
const newDoclet = ({ doclet }) => {
  if (doclet.styledComponent) {
    delete doclet.scope
    doclet.kind = 'member'
    doclet.name = `&lt;${doclet.name} ${doclet.properties
      ? doclet.properties.map(({ name }) => `<i>${name}</i> `).join('')
      : ''
    }/>`
  }
}

/**
 * Here, we define custom tag `@styled-component` that we automatically append to all exported members of `src/styled` modules.
 * Later, we modify displayed `{ComponentName}` of each such styled component to `<{ComponentName} {...props} />`
 *
 * @param {Object} dictionary
 */
exports.defineTags = dictionary => {
  dictionary.defineTag('styled-component', {
    mustHaveValue: false,
    mustNotHaveDescription: false,
    canHaveType: false,
    canHaveName: false,
    onTagged: doclet => {
      doclet.styledComponent = true
    },
  })
}

exports.handlers = {
  beforeParse,
  newDoclet,
}
