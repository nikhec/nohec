/**
 * Helper functions that reshape fetched left panel data for later storing inside states/store.
 */

/**
 * Maps received API leagues to Leagues structure grouping leagues by season.
 *
 * @function
 * @param apiLeagues - Fetched API leagues fetched
 * @returns {Record<number, League[]>} - Parsed structured leagues, grouped by seasons
 */
export const selectLeagues = apiLeagues => apiLeagues.reduce((acc, { season, ...rest }) => {
  if (!acc[season]) {
    acc[season] = []
  }
  acc[season].push(rest)

  return acc
}, {})
