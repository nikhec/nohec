/**
 * Helper functions that reshape data received from API for storing to the stores and later usage by React components.
 */
import { MATCH_RECORD_STATE } from 'src/constants'
import { MEDIA_URL } from 'src/api-client/config'

import { selectMatchGames } from './matchDetailSelectors'

/**
 * Parses match record as returned from API into structure reflecting currently played match state.
 *
 * @function
 * @param {ApiMatchRecord} matchRecord - Single match record
 * @returns {MatchRecord} - Parsed object mirroring match record state
 */
export const selectMatchRecord = matchRecord => {
  const state = selectMatchRecordState(matchRecord)

  const { id, games, home_captain: homeCaptain, away_captain: awayCaptain, note } = matchRecord
  const selectedGames = selectMatchGames(games)

  return {
    id,
    state,
    score: selectMatchScore(selectedGames),
    games: selectedGames,
    homeTeam: {
      ...matchRecord.home_team,
      logo: `${MEDIA_URL}${matchRecord.home_team.logo}`,
      players: matchRecord.available_home_players,
    },
    homeCaptain,
    awayTeam: {
      ...matchRecord.away_team,
      logo: `${MEDIA_URL}${matchRecord.away_team.logo}`,
      players: matchRecord.available_away_players,
    },
    awayCaptain,
    note: note !== '' ? note : null,
  }
}

/**
 * Selects match scores from given games.
 *
 * @function
 * @param {MatchDetailGame[]} games - List of games played within the match
 * @returns {number[]} - Tuple of overall scores for given match
 */
export const selectMatchScore = games =>
  games.reduce((acc, { sets }) => {
    if (sets.length !== 2) {
      return acc
    }

    if (sets[0].homePoints === sets[1].homePoints && sets[0].homePoints === 10) {
      return [acc[0] + 1, acc[1]]
    } else if (sets[0].awayPoints === sets[1].awayPoints && sets[0].awayPoints === 10) {
      return [acc[0], acc[1] + 1]
    }

    return acc
  }, [0, 0])

/**
 * Selects state of given match record.
 *
 * @function
 * @inner
 * @param {ApiMatchRecord} matchRecord - Single match record
 * @returns {string} - Parsed match state identifier
 */
const selectMatchRecordState = matchRecord => {
  if (matchRecord.state !== 2 && matchRecord.state !== 3) {
    // Match record not in progress is considered finished for simplicity.
    return MATCH_RECORD_STATE.FINISHED
  } else if (!matchRecord.home_captain || !matchRecord.away_captain) {
    // Match record in progress without associated captains renders captain selection screen.
    return MATCH_RECORD_STATE.CAPTAIN_SELECTION
  } else {
    const compositionInProgress = matchRecord.games.findIndex(game => !game.away_players.length || !game.home_players.length)
    const gameInProgress = matchRecord.games.findIndex(game => game.sets.length !== 2)

    // Match record that has all games filled can render either on comment or on overview screens.
    if (gameInProgress === -1) {
      if (!matchRecord.note) {
        return MATCH_RECORD_STATE.SUMMATION
      } else {
        // In case we have everything we display overview to allow checking before signing on refresh.
        return MATCH_RECORD_STATE.OVERVIEW
      }
    } else if (compositionInProgress !== -1 && compositionInProgress <= gameInProgress) {
      // Match record in progress with game without associated players renders player selection screen
      return MATCH_RECORD_STATE.PLAYER_SELECTION
    } else {
      // Match record in progress with associated players but without complete set score of some game renders game screen.
      return MATCH_RECORD_STATE.GAME
    }
  }
}
