/**
 * Matches selectors are helper functions that reshape general matches data for later storing inside states/store.
 */

import { MEDIA_URL } from 'src/api-client/config'

/**
 * Turns an array of matches  to structure indexed by years then months and then days.
 *
 * @function
 * @param {Match[]} matches - Array of matches stored in state.
 * @returns {Record<string, Record<string, Record<string, Match>>>} reshapedMatches - Matches in nested structure indexed by year, month and day
 */
export const selectMatchesByDate = matches =>
  matches.reduce((acc, match) => {
    const matchDate = new Date(match.dateTime)
    const [day, month, year] = [matchDate.getDate(), matchDate.getMonth(), matchDate.getFullYear()]

    if (acc[year] == null) {
      acc[year] = {
        [month]: {
          [day]: [],
        },
      }
    } else if (acc[year][month] == null) {
      acc[year][month] = {
        [day]: [],
      }
    } else if (acc[year][month][day] == null) {
      acc[year][month][day] = []
    }

    acc[year][month][day].push(match)
    return acc
  }, {})

/**
 * Turns an array of matches returned from API to match object used by the web application, sorted by date.
 *
 * @function
 * @param {ApiMatch[]} matches - Array of matches fetched from db.
 * @returns {Match[]} reshapedMatches - Matches reshaped for the use on the FE within state
 */
export const selectMatches = matches => matches
  .map(match => selectMatch(match))
  .sort((matchA, matchB) => matchB.dateTime.localeCompare(matchA.dateTime))

/**
 * Turns an array of matches returned from API to league matches structure and merges them with previous state.
 *
 * @function
 * @param {ApiMatch[]} matches - Array of matches fetched from db
 * @param {LeagueMatches} prevMatches - League matches structure
 * @param {number} leagueId - Unique identifier of the league fetched matches belong to
 * @returns {LeagueMatches} - Matches reshaped for the use on the FE within state
 */
export const selectLeagueMatches = (matches, prevMatches, leagueId) => {
  const selectedMatches = selectMatches(matches)

  const newMatches = { ...prevMatches }
  newMatches[leagueId] = selectedMatches.reduce((acc, match) => {
    if (match.isFinished) {
      acc.prev.push(match)
    } else {
      acc.next.push(match)
    }

    return acc
  }, { prev: [], next: [] })

  newMatches[leagueId].next.sort((matchA, matchB) => matchA.dateTime.localeCompare(matchB.dateTime))
  return newMatches
}

/**
 * Turns ApiMatch to match object used by web application.
 *
 * @function
 * @param {ApiMatch} match - Single match fetched from db
 * @returns {Match} - Reshaped match for use on FE
 */
export const selectMatch = ({ id, date, league, court, score, round, state, ...rest }) => {
  const matchDate = new Date(date)

  return {
    id,
    league,
    dateTime: date,
    place: court?.address.split(',')[0],
    round,
    isFinished: state === 4,
    score: score?.length
      ? score
      : null,
    date: `${matchDate.getDate()}.${matchDate.getMonth() + 1}.${matchDate.getFullYear()}`,
    time: `${`0${matchDate.getHours()}`.slice(-2)}:${`0${matchDate.getMinutes()}`.slice(-2)}`,
    homeTeam: {
      ...rest.home_team,
      logo: `${MEDIA_URL}${rest.home_team.logo}`,
    },
    awayTeam: {
      ...rest.away_team,
      logo: `${MEDIA_URL}${rest.away_team.logo}`,
    },
  }
}

/**
 * Finds closest match to be played. Returns currently played match if it started sooner than an hour back.
 *
 * @function
 * @param {Match[]} matches - Array of freshly fetched matches
 * @param {Match | null} nextMatch - Match currently selected as the Next Match
 * @returns {Match} - Next Match
 */
export const selectNextMatch = (matches, nextMatch) => {
  const time = new Date()
  time.setHours(Math.max(time.getHours() - 1, 0))
  const firstDateTime = time.toISOString()

  return matches.reduce((acc, match) =>
    match.dateTime > firstDateTime && (!acc || match.dateTime < acc.dateTime)
      ? match
      : acc,
    nextMatch?.dateTime > firstDateTime ? nextMatch : null)
}
