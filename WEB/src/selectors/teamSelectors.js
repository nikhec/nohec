/**
 * Helper functions that reshape fetched clubs data for later storing inside states/store.
 */

import { MEDIA_URL } from 'src/api-client/config'
import { selectMatches } from 'src/selectors/matchSelectors'

/**
 * Maps received API team objects to TeamList structure grouping team by season and league, and merges it with previous state.
 *
 * @function
 * @param apiTeams - new API team fetched for currently selected seasonId
 * @param teamList - previous team list
 * @returns {Record<number, Record<number, ApiTeam[]>>} - parsed structured team list grouping team by season
 */
export const selectTeamList = (apiTeams, teamList) =>
  apiTeams.reduce((acc, { league, season_id: seasonId, logo, ...rest }) => {
    if (!acc[seasonId]) {
      acc[seasonId] = {}
    }
    if (!acc[seasonId][league]) {
      acc[seasonId][league] = []
    }

    acc[seasonId][league] = acc[seasonId][league].filter(({ id }) => id !== rest.id)
    acc[seasonId][league].push({
      ...rest,
      logo: `${MEDIA_URL}${logo}`,
    })
    return acc
  }, { ...teamList })

/**
 * Maps received API team detail object to TeamDetail structure.
 *
 * @function
 * @param {ApiTeamDetail} apiTeamDetail - team detail info as fetched from API
 * @returns {TeamDetail} - parsed structured team detail info
 */
export const selectTeamDetail = ({
  current_members: currentMembers,
  home_matches: homeMatches,
  away_matches: awayMatches,
  logo,
  ...rest
}) => ({
  ...rest,
  logo: `${MEDIA_URL}${logo}`,
  currentMembers: currentMembers.map(member => ({
    ...member,
    stats: selectDetailPlayerStats(member.stats),
  })),
  matches: selectMatches([...homeMatches, ...awayMatches])
    .sort((matchA, matchB) => matchB.round - matchA.round)
    .filter(({ score }) => Boolean(score)),
})

/**
 * Maps received API player statistics object to PlayerDetailStats structure.
 *
 * @function
 * @param {ApiPlayerDetailStats} apiPlayerStats - Statistics Object for a single player as returned from the API
 * @returns {PlayerDetailStats} - Parsed structured detail player statistics
 */
export const selectDetailPlayerStats = ({ singles, doubles, triples }) => {
  const stats = {
    singles: selectGameTypePlayerStats(singles),
    doubles: selectGameTypePlayerStats(doubles),
    triples: selectGameTypePlayerStats(triples),
  }

  return {
    games: {
      wins: stats.singles.wins + stats.doubles.wins + stats.triples.wins,
      losses: stats.singles.losses + stats.doubles.losses + stats.triples.losses,
      draws: stats.singles.draws + stats.doubles.draws + stats.triples.draws,
    },
    ...stats,
  }
}

/**
 * Maps received API player's statistics for a single game type to PlayerDetailGameTypeStats structure.
 *
 * @function
 * @inner
 * @param {ApiPlayerDetailGameTypeStats} apiGameStats - Statistics object for a single game type as returned from the API
 * @returns {PlayerDetailGameTypeStats} - Parsed structured team detail player' statistics for a single game type
 */
const selectGameTypePlayerStats = apiGameStats => ({
  wins: apiGameStats.won_games,
  losses: apiGameStats.lost_games,
  draws: apiGameStats.played_games - apiGameStats.won_games - apiGameStats.lost_games,
})

/**
 * Turns an array of standings returned from API to league standings structure and merges them with previous state.
 *
 * @function
 * @param {ApiStanding[]} standings - Array of standings team objects fetched from db
 * @param {Record<number, Standing[]>} prevStandings - League standings structure
 * @param {number} leagueId - Unique identifier of the league fetched matches belong to
 * @returns {Record<number, Standing[]>} - Matches reshaped for the use on the FE within state
 */
export const selectLeagueStandings = (standings, prevStandings, leagueId) => {
  const newStandings = { ...prevStandings }
  newStandings[leagueId] = standings.map(standing => ({
    ...standing,
    team: {
      ...standing.team,
      logo: `${MEDIA_URL}${standing.team.logo}`,
    },
    setsWon: standing.sets_won,
    setsLost: standing.sets_lost,
  }))

  return newStandings
}

/**
 * Turns API team statistics object into shape used by the client.
 *
 * @function
 * @param {ApiTeamStatistics} statistics - API statistics as returned from the API
 * @returns {TeamStatistics} - Reshaped statistics object used by the client
 */
export const selectTeamStatistics = statistics => ({
  roundStats: statistics.rounds_stats.map(stats => ({
    wins: stats.won_matches,
    draws: stats.draw_matches,
    losses: stats.lost_matches,
  })),
  lastFiveMatches: ({
    wins: statistics.last_five_matches.won_matches,
    draws: statistics.last_five_matches.draw_matches,
    losses: statistics.last_five_matches.lost_matches,
  }),
})
