/**
 * Match Detail selectors are helper functions that reshape detailed match data for later storing inside states/store.
 */
import { MEDIA_URL } from 'src/api-client/config'
import { selectDetailPlayerStats } from 'src/selectors/teamSelectors'

import { selectMatch } from './matchSelectors'

/**
 * Selects order of the game within games of the same type.
 *
 * @function
 * @inner
 * @param {ApiMatchDetailGame[]} games - List of all games within the match
 * @param {number} id - Id of inspected game
 * @param {number} index - Index of inspected game within the collection of match's games
 * @param {number} type - Type of inspected game
 * @returns {number} - Order of the game within games of the same type
 */
const selectMatchGameTypeOrder = (games, id, index, type) =>
  games.filter((game, i) => game.type === type && i < index).length + 1

/**
 * Maps Api game structure to the shape used by the web application.
 *
 * @function
 * @param {ApiMatchDetailGame[]} games - API match games
 * @returns {MatchDetailGame[]} - Reshaped match games
 */
export const selectMatchGames = games => games.map(({
  home_players: homePlayers,
  away_players: awayPlayers,
  id,
  sets,
  type,
}, index) => ({
  id,
  type,
  typeOrder: selectMatchGameTypeOrder(games, id, index, type),
  homePlayers,
  awayPlayers,
  sets: sets.map(set => ({
    homePoints: set.home_points,
    homeSubstitutions: set.home_substitutions.map(subs => [subs.outgoing, subs.incoming]),
    awayPoints: set.away_points,
    awaySubstitutions: set.away_substitutions.map(subs => [subs.outgoing, subs.incoming]),
  })),
})).sort((gameA, gameB) => gameA.id - gameB.id)

/**
 * Maps Api team structure to the shape used by the web application.
 *
 * @function
 * @inner
 * @param {ApiMatchDetailTeam} team - Team object
 * @param {ApiDetailPlayer[]} players - Team players
 * @param {number | undefined} captainId - Id of the team captain chosen for the match
 * @returns {MatchDetailTeam} - Mapped team for use by the web application
 */
const selectTeamPlayers = ({ id, name, logo }, players, captainId) => ({
  id,
  name,
  logo: `${MEDIA_URL}${logo}`,
  players: players.map(player => ({
    ...player,
    isCaptain: player.id === captainId,
    stats: selectDetailPlayerStats(player.stats),
  })),
})

/**
 * Turns API match detail to match detail object used by web application.
 *
 * @function
 * @param {ApiMatchDetail} match - Single match detail fetched from the API
 * @returns {MatchDetail} - Reshaped match detail for use by the web application
 */
export const selectMatchDetail = match => ({
  ...selectMatch(match),
  place: match.court,
  canModify: match.user_can_modify_match,
  referees: match.referees.map(({ id }) => id),
  homeTeam: selectTeamPlayers(match.home_team, match.home_players, match.home_captain),
  awayTeam: selectTeamPlayers(match.away_team, match.away_players, match.away_captain),
  games: selectMatchGames(match.games),
})

/**
 * Turns API match detail to number of referees assigned to the match.
 *
 * @function
 * @param {ApiMatchDetail} match - Single match detail fetched from the API
 * @returns {number} - Number of referees assigned to the match
 */
export const selectMatchRefereeCount = match => match.referees.length ?? 0
