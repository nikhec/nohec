/**
 * @ignore
 */

/**
 * @global
 * @typedef {Object} MatchRecordGameState
 * @property {function} setGameTimeouts
 * @property {function} setGameScore
 * @property {function} setGameSubstitutions
 * @property {number[]} gameScore
 * @property {number[]} gameTimeouts
 * @property {Object} gameSubstitutions
 * @property {number[]} gameSubstitutions.away
 * @property {number[]} gameSubstitutions.home
 */

// ---------------------------------------
// MATCH DETAIL TYPES
// ---------------------------------------

/**
 * @global
 * @typedef {Object} ApiMatchDetailTeam - Detailed team playing given match as returned from the API
 * @property {number} id - Unique id of the team
 * @property {string} name - Name of the team
 * @property {string} logo - Team logo
 */

/**
 * @global
 * @typedef {Object} ApiSubstitution - Single substitution action record
 * @property {number} outgoing - unique identifier of the outgoing player
 * @property {number} incoming - unique identifier of the incoming player
 */

/**
 * @global
 * @typedef {Object} ApiMatchDetailSet - Single set object within an ApiMatchRecord's game as returned from the API
 * @property {number} home_points - points the home team gathered during the set, 0-10
 * @property {ApiSubstitution[]} home_substitutions - home team substitutions
 * @property {number} away_points - points the away team gathered during the set, 0-10
 * @property {ApiSubstitution[]} away_substitutions - away team substitutions
 */

/**
 * @global
 * @typedef {Object} ApiMatchDetailGame - Partial game the match as returned from the API
 * @property {number} id - associated id of the game
 * @property {number} type - associated type of the game, [1=Single, 2=Double, 3=Triple]
 * @property {number[]} home_players - ids of home players assigned to given game
 * @property {number[]} away_players - ids of away players assigned to given game
 * @property {ApiMatchDetailSet[]} sets - sets played within given game
 */

/**
 * @global
 * @typedef {Object} ApiMatchDetail - Single match object as returned from the API
 * @property {number} id - Unique match identifier
 * @property {string} date - ISO Date string the match takes place on
 * @property {ApiMatchCourt} court - Place the match is played on
 * @property {number[] | null | undefined} score - Match result
 * @property {boolean} user_can_modify_match - Indicates current user's permissions towards the match
 * @property {Object} referees - List of referees associated with the match
 * @property {number} referees.id - Unique identifier of the referee user
 * @property {string} referees.name - Name of the referee user
 * @property {ApiDetailPlayer[]} home_players - List of players associated with the home team at the time of match
 * @property {ApiDetailPlayer[]} away_players - List of players associated with the away team at the time of match
 * @property {number} home_captain - Id of the captain of home team chosen for the given match
 * @property {number} away_captain - Id of the captain of away team chosen for the given match
 * @property {ApiMatchDetailTeam} home_team - Home team
 * @property {ApiMatchDetailTeam} away_team - Visitor team
 * @property {ApiMatchDetailGame[]} games - list of partial games the match consists of
 */

/**
 * @global
 * @typedef {Object} MatchDetailSet - Single set object within a MatchDetail's game
 * @property {number} homePoints - points the home team gathered during the set, 0-10
 * @property {number[][]} homeSubstitutions - home team substitutions
 * @property {number} awayPoints - points the away team gathered during the set, 0-10
 * @property {number[][]} awaySubstitutions - away team substitutions
 */

/**
 * @global
 * @typedef {Object} MatchDetailGame - Single partial game that the match consists of
 * @property {number} id - associated id of the game
 * @property {number} type - associated type of the game, either DOUBLE, TRIPLE or SINGLE
 * @property {number} typeOrder - order of game within games of its type
 * @property {number[]} homePlayers - ids of home players assigned to given game
 * @property {number[]} awayPlayers - ids away players assigned to given game
 * @property {MatchDetailSet[]} sets - sets played within given game
 */

/**
 * @global
 * @typedef {Object} MatchDetailPlayer - Single player as returned from the API
 * @property {number} id - Unique id of the player
 * @property {string} name - Name of the player
 * @property {boolean} isCaptain - indicates whether the user was a captain in this match
 * @property {PlayerDetailStats} stats - Statistics associated with the player based on the matches he played
 */

/**
 * @global
 * @typedef {Object} MatchDetailTeam - Team playing given match
 * @property {number} id - Unique id of the team
 * @property {string} name - Name of the team
 * @property {string} logo - Team logo
 * @property {MatchDetailPlayer[]} players - List of players associated with the team at the time of match
 */

/**
 * @global
 * @typedef {Object} MatchDetail - Parsed object representing match detail
 * @property {number} id - Unique match identifier
 * @property {string} dateTime - ISO Date string the match takes place on
 * @property {string} date - Readable string representing displayed date of match in day.month.year format
 * @property {string} time - Readable string representing displayed time of match in hours:minutes format
 * @property {ApiMatchCourt} place - Place the match is played on
 * @property {number[] | null} score - Match result
 * @property {boolean} canModify - Indicates current user's permissions towards the match
 * @property {number[]} referees - Unique identifiers of referee users
 * @property {MatchDetailTeam} homeTeam - Home team
 * @property {MatchDetailTeam} awayTeam - Visitor team
 * @property {MatchDetailGame[]} games - list of partial games the match consists of
 */

// ---------------------------------------
// MATCH TYPES
// ---------------------------------------

/**
 * @global
 * @typedef {Object} ApiTeam - Team playing given match as returned from the API
 * @property {number} id - Unique id of the team
 * @property {string} name - Name of the team
 * @property {string} logo - Team logo
 */

/**
 * @global
 * @typedef {Object} ApiMatchCourt - Place associated with the match
 * @property {number} id - Unique match place identifier
 * @property {string} address - Address of the place
 * @property {number} longitude - Longitude of the place
 * @property {number} latitude - Latitude of the place
 */

/**
 * @global
 * @typedef {Object} ApiMatch - Single match object as returned from the API
 * @property {number} id - Unique match identifier
 * @property {number} round - Number of the round the match was played in
 * @property {string} date - ISO Date string the match takes place on
 * @property {string} league - League the match belongs to
 * @property {ApiMatchCourt} court - Place the match is played on
 * @property {number} state - Identifier of the match state
 * @property {number[] | null | undefined} score - Match result
 * @property {ApiTeam} home_team - Home team
 * @property {ApiTeam} away_team - Visitor team
 */

/**
 * @global
 * @typedef {Object} Match - Single match object reshaped for the use on the FE within state
 * @property {number} id - Unique match identifier
 * @property {number} round - Number of the round the match was played in
 * @property {string} dateTime - ISO Date string the match takes place on
 * @property {string} date - Readable string representing displayed date of match in day.month.year format
 * @property {string} time - Readable string representing displayed time of match in hours:minutes format
 * @property {string} league - League the match belongs to
 * @property {string} place - Place the match is played on
 * @property {boolean} isFinished - Indicates whether is the match finished or not
 * @property {number[] | null} score - Match result
 * @property {ApiTeam} homeTeam - Home team
 * @property {ApiTeam} awayTeam - Visitor team
 */

/**
 * @global
 * @typedef {Record<number, { prev: Match[], next: Match[] }>} LeagueMatches - Structured object holding league matches grouped by leagueId, and separated into previous and next matches.
 */

// ---------------------------------------
// MATCH RECORD TYPES
// ---------------------------------------

/**
 * @global
 * @typedef {Object} ApiMatchRecord - Single match record returned from the API
 * @property {number} id - Unique identifier of the match
 * @property {number} state - Match state, [1=Upcoming, 2=Waiting For Captains, 3=In Progress, 4=Finished]
 * @property {Object} available_home_players - Players currently available to the home team
 * @property {number} home_captain - Id of the captain of the home team
 * @property {ApiMatchDetailTeam} home_team - Home team
 * @property {Object} available_away_players - Players currently available to the away team
 * @property {number} away_captain - Id of the captain of the away team
 * @property {ApiMatchDetailTeam} away_team - Away team
 * @property {string} note - Match note submitted during match finalization
 * @property {ApiMatchDetailGame[]} games - List of partial games the match consists of
 */

/**
 * @global
 * @typedef {Object} MatchRecord - Parsed object representing current state of the match
 * @property {number} id - Unique identifier of the match
 * @property {string} state - State of the match, decides which screen is to be rendered
 * @property {number[]} score - Overall point score of the team
 * @property {MatchDetailTeam} homeTeam - Home team
 * @property {number} homeCaptain - Id of the captain of the home team
 * @property {MatchDetailTeam} awayTeam - Away team
 * @property {number} awayCaptain - Id of the captain of the away team
 * @property {string | null} note - Provided note for the match
 * @property {MatchDetailGame[]} games - List of partial games the match consists of
 */

// ---------------------------------------
// TEAM TYPES
// ---------------------------------------

/**
 * @global
 * @typedef {Object} ApiPlayerDetailGameTypeStats - Statistics object for a single game type as returned from the API
 * @property {number} played_games - Games of this type the player participated in
 * @property {number} won_games - Games of this type the player won
 * @property {number} lost_games - Games of this type the player lost
 */

/**
 * @global
 * @typedef {Object} ApiPlayerDetailStats - Statistics Object for a single player as returned from the API
 * @property {ApiPlayerDetailGameTypeStats} singles - Actual statistics of the player from singles games
 * @property {ApiPlayerDetailGameTypeStats} doubles - Actual statistics of the player from doubles games
 * @property {ApiPlayerDetailGameTypeStats} triples - Actual statistics of the player from triples games
 */

/**
 * @global
 * @typedef {Object} ApiDetailPlayer - A single player object as returned from the API
 * @property {number} id - Unique id of the player
 * @property {string} name - Name of the player
 * @property {ApiPlayerDetailStats} stats - Statistics associated with the player based on the matches he played
 */

/**
 * @global
 * @typedef {Object} ApiTeamDetail - Team detail as returned from the API
 * @property {number} id - Unique id of the team
 * @property {string} name - Name of the team
 * @property {string} logo - Logo of the team
 * @property {number} leader - Unique id of the team leader
 * @property {ApiMatch[]} away_matches - List of matches the team played away
 * @property {ApiMatch[]} home_matches - List of matches the team played home
 * @property {ApiDetailPlayer[]} current_members - List of players currently associated with the team
 */

/**
 * @global
 * @typedef {Object} PlayerDetailGameTypeStats - Statistics object for a single game type as returned from the API
 * @property {number} wins - Number of won matches of the team within the league
 * @property {number} draws - Number of tied matches of the team within the league
 * @property {number} losses - Number of lost matches of the team within the league
 */

/**
 * @global
 * @typedef {Object} PlayerDetailStats - Statistics Object for a single player
 * @property {PlayerDetailGameTypeStats} games - Actual statistics of the player from all games
 * @property {PlayerDetailGameTypeStats} singles - Actual statistics of the player from singles games
 * @property {PlayerDetailGameTypeStats} doubles - Actual statistics of the player from doubles games
 * @property {PlayerDetailGameTypeStats} triples - Actual statistics of the player from triples games
 */

/**
 * @global
 * @typedef {Object} TeamDetailPlayer - A single player object
 * @property {number} id - Unique id of the player
 * @property {string} name - Name of the player
 * @property {PlayerDetailStats} stats - Statistics associated with the player based on the matches he played
 */

/**
 * @global
 * @typedef {Object} TeamDetail - Reshaped team detail structure
 * @property {number} id - Unique id of the team
 * @property {string} name - Name of the team
 * @property {string} logo - Logo of the team
 * @property {number} leader - Unique id of the team leader
 * @property {Match[]} matches - List of matches played by the team
 * @property {TeamDetailPlayer[]} currentMembers - List of players currently associated with the team
 */

/**
 * @global
 * @typedef {Object} ApiStanding - Standing object of a single team as returned from the API
 * @property {number} position - Position of the team within the league's ladder
 * @property {number} matches - Number of matches so far played by the team within the league
 * @property {number} wins - Number of won matches of the team within the league
 * @property {number} draws - Number of tied matches of the team within the league
 * @property {number} losses - Number of lost matches of the team within the league
 * @property {number} sets_won - Number of won sets of the team within the league
 * @property {number} sets_lost - Number of lost sets of the team within the league
 * @property {number} points - Number of points of the team within the league
 * @property {ApiTeam} team - Team object
 */

/**
 * @global
 * @typedef {Object} Standing - Reshaped standing object
 * @property {number} position - Position of the team within the league's ladder
 * @property {number} matches - Number of matches so far played by the team within the league
 * @property {number} wins - Number of won matches of the team within the league
 * @property {number} draws - Number of tied matches of the team within the league
 * @property {number} losses - Number of lost matches of the team within the league
 * @property {number} setsWon - Number of won sets of the team within the league
 * @property {number} setsLost - Number of lost sets of the team within the league
 * @property {number} points - Number of points of the team within the league
 * @property {ApiTeam} team - Team object
 */

/**
 * @global
 * @typedef {Object} ApiMatchStatisticsGroup - Group of match statistics for some period of time
 * @property {number} won_matches - Number of won matches
 * @property {number} draw_matches - Number of tied matches
 * @property {number} lost_matches - Number of lost matches
 */

/**
 * @global
 * @typedef {Object} ApiTeamStatistics - Team statistics object as returned from the API
 * @property {ApiMatchStatisticsGroup[]} rounds_stats - Statistics grouped by rounds
 * @property {ApiMatchStatisticsGroup} last_five_matches - Statistics from the last ten matches
 */

/**
 * @global
 * @typedef {Object} MatchStatisticsGroup - Group of match statistics for some period of time
 * @property {number} wins - Number of won matches
 * @property {number} ties - Number of tied matches
 * @property {number} losses - Number of lost matches
 */

/**
 * @global
 * @typedef {Object} TeamStatistics - Team statistics object
 * @property {MatchStatisticsGroup[]} roundStats - Statistics grouped by rounds
 * @property {MatchStatisticsGroup} lastFiveMatches - Statistics from the last ten matches
 */

// ---------------------------------------
// PANEL TYPES
// ---------------------------------------

/**
 * @global
 * @typedef {Object} ApiLeague - Single league within a league list as returned from the API.
 * @property {number} id - Unique id of the league
 * @property {string} name - Name of the league
 * @property {string} code - Code of the league
 * @property {number} season - Unique id of the season associated with the league
 */

/**
 * @global
 * @typedef {Object} League - Single league within a league list as returned from the API.
 * @property {number} id - Unique id of the league
 * @property {string} name - Name of the league
 * @property {string} code - Code of the league
 */
