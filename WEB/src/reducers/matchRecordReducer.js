import { createReducer } from '@reduxjs/toolkit'

import { MatchRecordActions } from 'src/actions'
import { selectMatchRecord, selectMatchScore } from 'src/selectors/matchRecordSelectors'
import { MATCH_RECORD_STATE } from 'src/constants'

const initialState = {
  state: null,
  error: null,
}

/**
 * Marks captains as selected.
 *
 * @param {Object} prevState - Previous state
 * @param {Object} action - Dispatched action object
 * @param {Object} action.payload - New captains to be set
 * @returns {Object} - New state
 */
const captainsSelected = (prevState, { payload: { home, away } }) => ({
  ...prevState,
  state: {
    ...prevState.state,
    state: MATCH_RECORD_STATE.PLAYER_SELECTION,
    homeCaptain: home,
    awayCaptain: away,
  },
})

/**
 * Loads given data as new match recording state.
 *
 * @param {Object} prevState - Previous state
 * @param {Object} action - Dispatched action object
 * @param {Object} action.payload - New match record state
 * @returns {Object} - New state
 */
const recordStateLoadingSuccess = (prevState, { payload }) => ({
  ...prevState,
  state: selectMatchRecord(payload),
  error: null,
})

/**
 * Stores new match record fetching error.
 *
 * @param {Object} prevState - Previous state
 * @param {Object} action - Dispatched action object
 * @param {string} action.payload - Error received on new match record state loading
 * @returns {Object} - New state
 */
const recordStateLoadingError = (prevState, { payload: error }) => ({
  ...prevState,
  state: {},
  error,
})

/**
 * Stores selected players for given game.
 *
 * @param {Object} prevState - Previous state
 * @param {Object} action - Dispatched action object
 * @param {Object} action.payload - Object holding id of the game and lists of player ids to be set
 * @returns {Object} - New state
 */
const playersSelected = ({ state, ...rest }, { payload: { gameId, homePlayers, awayPlayers } }) => ({
  ...rest,
  state: {
    ...state,
    state: MATCH_RECORD_STATE.GAME,
    games: [
      ...state.games.filter(({ id }) => id !== gameId),
      {
        ...state.games.find(({ id }) => id === gameId),
        homePlayers,
        awayPlayers,
      },
    ].sort((gameA, gameB) => gameA.id - gameB.id),
  },
})

/**
 * Stores set result and changes match record state accordingly.
 *
 * @param {Object} prevState - Previous state
 * @param {Object} action - Dispatched action object
 * @param {Object} action.payload - Object holding id of the game and a result of the set
 * @returns {Object} - New state
 */
const setSubmitted = ({ state, ...rest }, { payload: { gameId, result } }) => {
  const gameToUpdate = state.games.find(({ id }) => id === gameId)
  const updatedGames = [
    ...state.games.filter(({ id }) => id !== gameId),
    {
      ...gameToUpdate,
      sets: [...gameToUpdate.sets, result],
    },
  ].sort((gameA, gameB) => gameA.id - gameB.id)

  const isLastGame = updatedGames.every(game => game.sets.length === 2)

  return ({
    ...rest,
    state: {
      ...state,
      // We need to update match recording state accordingly either to SUMMATION, PLAYER_SELECTION or keep it GAME
      state: gameToUpdate.sets.length + 1 !== 2
        ? MATCH_RECORD_STATE.GAME
        : isLastGame
          ? MATCH_RECORD_STATE.SUMMATION
          : MATCH_RECORD_STATE.PLAYER_SELECTION,
      // We map each value of result to either 0 or 1 and sum it with previous value of score on given index
      score: selectMatchScore(updatedGames),
      games: updatedGames,
    },
  })
}

/**
 * Stores match's note and changes match record state accordingly.
 *
 * @param {Object} prevState - Previous state
 * @param {Object} action - Dispatched action object
 * @param {Object} action.payload - Object holding filled match note
 * @returns {Object} - New state
 */
const noteSubmitted = ({ state, ...rest }, { payload: { note } }) => ({
  ...rest,
  state: {
    ...state,
    state: MATCH_RECORD_STATE.OVERVIEW,
    note,
  },
})

/**
 * Changes the state after the user has confirmed the match record on the overview page to the final signing.
 *
 * @param {Object} prevState - Previous state
 * @returns {Object} - New state
 */
const overviewConfirmed = ({ state, ...rest }) => ({
  ...rest,
  state: {
    ...state,
    state: MATCH_RECORD_STATE.SIGN,
  },
})

/**
 * Match record reducer that maps actions emitted during match recording to state changes.
 *
 * @name matchRecordReducer
 * @static
 */
export default createReducer(initialState, {
  [MatchRecordActions.captainsSelected.toString()]: captainsSelected,
  [MatchRecordActions.playersSelected.toString()]: playersSelected,
  [MatchRecordActions.setSubmitted.toString()]: setSubmitted,
  [MatchRecordActions.noteSubmitted.toString()]: noteSubmitted,
  [MatchRecordActions.overviewConfirmed.toString()]: overviewConfirmed,
  [MatchRecordActions.recordStateLoadingSuccess.toString()]: recordStateLoadingSuccess,
  [MatchRecordActions.recordStateLoadingError.toString()]: recordStateLoadingError,
})
