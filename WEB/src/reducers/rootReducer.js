import { createReducer, combineReducers } from '@reduxjs/toolkit'
import { connectRouter } from 'connected-react-router'

import { RootActions, AuthActions } from 'src/actions'

import customizationReducer from './customizationReducer'
import matchRecordReducer from './matchRecordReducer'

const initialState = {
  intl: null,
  user: null,
}

/**
 * Loads new Intl internationalization object for current locale into the state.
 *
 * @param {Object} state - Previous state
 * @param {Object} action - Dispatched action object
 * @param {Object} action.payload - New intl object
 * @returns {Object} - New state
 */
const loadInternationalization = (state, { payload: intl }) => ({
  ...state,
  intl,
})

/**
 * Marks user as logged in.
 *
 * @param {Object} state - Previous state
 * @param {Object} action - Dispatched action object
 * @param {string} action.payload - Logged user's email and name
 * @returns {Object} - New state
 */
const logInSuccess = (state, { payload: user }) => ({
  ...state,
  user,
})

/**
 * Marks user as logged out.
 *
 * @param {Object} state - Previous state
 * @returns {Object} - New state
 */
const logOutSuccess = state => ({
  ...state,
  user: null,
})

/**
 * Root reducer that maps root actions and actions emitted during authentication to state changes.
 *
 * @name RootReducer
 */
const rootReducer = createReducer(initialState, {
  [RootActions.loadInternationalization.toString()]: loadInternationalization,
  [AuthActions.logInSuccess.toString()]: logInSuccess,
  [AuthActions.logOutSuccess.toString()]: logOutSuccess,
})

/**
 * Creates final Reducer by hooking partial reducers under appropriate keys.
 * Accepts history object that is then stored in the `app` state of the Redux store.
 *
 * @name createRootReducer
 * @function
 * @static
 * @param {Object} history - BrowserHistory object
 * @returns {Reducer}
 */
export default history => combineReducers({
  router: connectRouter(history),
  app: rootReducer,
  customization: customizationReducer,
  matchRecord: matchRecordReducer,
})
