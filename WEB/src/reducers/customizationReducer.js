import { createReducer } from '@reduxjs/toolkit'

import { CustomizationActions } from 'src/actions'
import { STORAGE, LOCALES } from 'src/constants'

const storageLocale = localStorage.getItem(STORAGE.LOCALE)

const initialState = {
  locale: Object.values(LOCALES).indexOf(storageLocale) !== -1 ? storageLocale : LOCALES.CS,
  localeChanging: false,
  theme: localStorage.getItem(STORAGE.THEME) === 'dark' ? 'dark' : 'light',
  themeChanging: false,
}

/**
 * Triggers locale changing and starts changing animation.
 *
 * @param {Object} state - Previous state
 * @param {Object} action - Dispatched action object
 * @param {string} action.payload - New locale to be selected
 * @returns {Object} - New state
 */
const changeLocale = (state, { payload: locale }) => ({
  ...state,
  locale,
})

/**
 * Sets locale changing animation state.
 *
 * @param {Object} state - Previous state
 * @param {Object} action - Dispatched action object
 * @param {string} action.payload - New locale animation state to be set
 * @returns {Object} - New state
 */
const setLocaleChanging = (state, { payload: localeChanging }) => ({
  ...state,
  localeChanging,
})

/**
 * Triggers theme changing and starts changing animation.
 *
 * @param {Object} state - Previous state
 * @param {Object} action - Dispatched action object
 * @param {string} action.payload - New theme to be selected
 * @returns {Object} - New state
 */
const changeTheme = (state, { payload: theme }) => ({
  ...state,
  theme,
})

/**
 * Sets theme changing animation state.
 *
 * @param {Object} state - Previous state
 * @param {Object} action - Dispatched action object
 * @param {string} action.payload - New theme animation state to be set
 * @returns {Object} - New state
 */
const setThemeChanging = (state, { payload: themeChanging }) => ({
  ...state,
  themeChanging,
})

/**
 * Customization reducer that maps customization actions to state changes.
 *
 * @name customizationReducer
 * @static
 */
export default createReducer(initialState, {
  [CustomizationActions.changeLocale.toString()]: changeLocale,
  [CustomizationActions.setLocaleChanging.toString()]: setLocaleChanging,
  [CustomizationActions.changeTheme.toString()]: changeTheme,
  [CustomizationActions.setThemeChanging.toString()]: setThemeChanging,
})
