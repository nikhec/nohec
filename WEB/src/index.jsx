/**
 * React application entry point.
 * Hooks the whole React `<Application>` to the DOM tree under an element with `root` id.
 * Is used as an application entry point during webpack bundling.
 *
 * **Note that this module does not provide any exported `<Index>` component!**
 *
 * @name Index
 * @component
 */
import React from 'react'
import ReactDOM from 'react-dom'

import './styles/index.css'
import './styles/themes.css'
import Application from './Application'

ReactDOM.render(<Application />, document.getElementById('root'))
