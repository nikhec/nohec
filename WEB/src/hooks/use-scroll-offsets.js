import { useLayoutEffect, useState } from 'react'

import { SCROLL_ROOT } from 'src/constants'

/**
 * Subscribes to the value of current scroll offset and returns it live.
 *
 * @function
 * @param {String} [scrollRootId=SCROLL_ROOT] - Id of the element scrolling takes place in
 * @returns {number}
 */
export const useScrollOffset = (scrollRootId = SCROLL_ROOT) => {
  // scrolled is set to null initially to distinguish first value from future that are directly computed
  // null is falsy value so it can be seamlessly converted to false anyway if needed
  const [scrollOffset, setScrollOffset] = useState(null)

  useLayoutEffect(() => {
    const element = document.getElementById(scrollRootId)

    if (element) {
      const handleScroll = () => {
        const { scrollTop } = element
        setScrollOffset(scrollTop - element.offsetTop)
      }

      element.addEventListener('scroll', handleScroll)
      return () => element.removeEventListener('scroll', handleScroll)
    }

    console.error(`Cannot append scroll event listener to element with id ${scrollRootId} - no such element`)
    return undefined
  }, [])

  return scrollOffset
}

/**
 * Returns `true`/`false` based on whether current view is scrolled within given scroll root element or not.
 *
 * @function
 * @param {string} [scrollRootId=SCROLL_ROOT] - Id of the element scrolling takes place in
 * @returns {boolean}
 */
export const useIsScrolled = (scrollRootId = SCROLL_ROOT) => {
  const scrollOffset = useScrollOffset(scrollRootId)

  return Boolean(scrollOffset && scrollOffset > 0)
}
