import { useCallback, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'

/**
 * @global
 * @typedef {Object} FetchedState
 * @property {boolean} isFetching - Indicates whether is the fetching still in progress
 * @property {any} state - Fetched data
 * @property {null | string} error - Parsed error message in case of failure
 */

/**
 * Returns by default static immutable state object created from dispatching given fetching action with provided payload.
 *
 * @function
 * @param {PayloadActionCreator<void, string>} fetchingAction - Action to dispatch in order to fetch data
 * @param {Object} actionPayload - Additional action payload
 * @param {function} dataTransformFunc - Transformation function applied to fetched data in onSuccess callback. Receives previous state as second argument
 * @param {array} deps - Dependencies controlling state refreshing
 * @param {any} defaultState - Default state value
 * @returns {FetchedState}
 */
export const useFetchedState = (
  fetchingAction,
  actionPayload = {},
  dataTransformFunc = data => data,
  deps = [],
  defaultState = {}
) => {
  const [isFetching, setIsFetching] = useState(true)
  const [fetchedState, setFetchedState] = useState({
    state: defaultState,
    error: null,
  })

  const onSuccess = useCallback(data => {
    setFetchedState(prevState => ({
      state: dataTransformFunc(data, prevState.state),
      error: null,
    }))
    setIsFetching(false)
  }, [setFetchedState, setIsFetching, ...deps])

  const onError = useCallback(error => {
    setFetchedState(prevState => ({
      state: prevState.state,
      error,
    }))
    setIsFetching(false)
  }, [setFetchedState, setIsFetching, ...deps])

  const dispatch = useDispatch()
  useEffect(() => {
    setIsFetching(true)
    dispatch(fetchingAction({
      onSuccess,
      onError,
      ...actionPayload,
    }))
  }, deps)

  return {
    isFetching,
    ...fetchedState,
  }
}

/**
 * Returns by default static immutable state array created from dispatching given fetching action with provided payload.
 *
 * @function
 * @param {PayloadActionCreator<void, string>} fetchingAction - Action to dispatch in order to fetch data.
 * @param {Object} actionPayload - Additional action payload.
 * @param {function} dataTransformFunc - Transformation function applied to fetched data in onSuccess callback. Receives previous state as second argument.
 * @param {array} deps - Dependencies controlling state refreshing.
 * @returns {FetchedState}
 */
export const useFetchedArrayState = (
  fetchingAction,
  actionPayload = {},
  dataTransformFunc = data => data,
  deps = []
) => useFetchedState(fetchingAction, actionPayload, dataTransformFunc, deps, [])

/**
 * Returns by default static immutable state array created from dispatching given fetching action with provided payload.
 *
 * @function
 * @param {PayloadActionCreator<void, string>} fetchingAction - Action to dispatch in order to fetch data.
 * @param {Object} actionPayload - Additional action payload.
 * @param {function} dataTransformFunc - Transformation function applied to fetched data in onSuccess callback. Receives previous state as second argument.
 * @param {array} deps - Dependencies controlling state refreshing.
 * @returns {FetchedState}
 */
export const useFetchedNullableState = (
  fetchingAction,
  actionPayload = {},
  dataTransformFunc = data => data,
  deps = []
) => useFetchedState(fetchingAction, actionPayload, dataTransformFunc, deps, null)
