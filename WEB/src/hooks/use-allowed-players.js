import { useMemo } from 'react'

import { GAME_TYPE } from 'src/constants'

/**
 * @global
 * @typedef {Object} AllowedPlayers
 * @property {number[]} allowedHomePlayers - players of the home team, allowed to participate in tje next game
 * @property {number[]} allowedAwayPlayers - players of the away team, allowed to participate in tje next game
 */

/**
 * Returns ids of allowed players for start formation/substitution of currently active game for both teams.
 *
 * @function
 * @param {MatchDetailPlayer[]} homePlayers - Home team players
 * @param {MatchDetailPlayer[]} awayPlayers - Away team players
 * @param {MatchDetailGame[]} matchGames - Games associated with the match
 * @returns {AllowedPlayers}
 */
export const useAllowedPlayers = (
  homePlayers,
  awayPlayers,
  matchGames
) => useMemo(() => ({
  allowedHomePlayers: getAllowedPlayers(homePlayers, matchGames, 'home'),
  allowedAwayPlayers: getAllowedPlayers(awayPlayers, matchGames, 'away'),
}), [homePlayers, awayPlayers, matchGames])

/**
 * Returns set of ids of players available for selection either for substitution or as game start formation.
 * This method aggregates team formations and substitutions from already played sets and filters team roaster
 * according to it based on futnet rules.
 *
 * @inner
 * @param {MatchDetailPlayer[]} teamPlayers - Team players participating in the match
 * @param {MatchDetailGame[]} matchGames - Games associated with the match
 * @param {'home' | 'away'} team - Examined team
 * @returns {number[]} - Ids of allowed players
 */
const getAllowedPlayers = (teamPlayers, matchGames, team) => {
  const activeGame = matchGames.find(game => game.sets.length !== 2)
  const { typeOrder, type } = activeGame
  const availablePlayers = teamPlayers.map(({ id }) => id)
  const [playersKey, substitutionsKey] = team === 'home'
    ? ['homePlayers', 'homeSubstitutions']
    : ['awayPlayers', 'awaySubstitutions']

  if (typeOrder === 1) {
    // Anyone can play in the first single/double/triple
    return availablePlayers
  } else if (typeOrder === 2 || (typeOrder === 3 && type === GAME_TYPE.DOUBLE)) {
    const prevGames = matchGames.filter(game => game.type === type && game.typeOrder < typeOrder)
    const prevGamePlayersSet = new Set([
      ...prevGames.flatMap(game => game[playersKey]),
      ...prevGames.flatMap(game => game.sets.flatMap(set => set[substitutionsKey]).flat()),
    ])

    // Only players that didn't participate in previous doubles/triples are allowed in the 2nd triple or 2nd and 3rd doubles
    return availablePlayers.filter(id => !prevGamePlayersSet.has(id))
  } else {
    const previousTypeOrder = shouldParticipateFirstGroup(team, type, typeOrder)
      ? 1
      : shouldParticipateSecondGroup(team, type, typeOrder)
        ? 2
        : null

    if (!previousTypeOrder) {
      // This is unknown state that should never happen, so we return an empty array of available players and log error
      console.error(`Reached unexpected state of the game playing ${type} number ${typeOrder}!`)
      return []
    }

    const prevGame = matchGames.find(game => game.type === type && game.typeOrder === previousTypeOrder)
    const prevGamePlayersSet = new Set([
      ...prevGame[playersKey],
      ...prevGame.sets.flatMap(set => set[substitutionsKey]).flat(),
    ])

    // Allow only players that participated in particular previous game based on the rules
    return availablePlayers.filter(id => prevGamePlayersSet.has(id))
  }
}

/**
 * Participants of the first double/triple must participate in 3rd triple and 4th double for the home team and
 * 4th triple and 5th double for the away team.
 *
 * @inner
 * @param {'home' | 'away'} team - Examined team
 * @param {number} type - Type of the game
 * @param {number} typeOrder - Order of this game within the games of the same type in the match
 * @returns {boolean}
 */
const shouldParticipateFirstGroup = (team, type, typeOrder) =>
  (team === 'home' && (
    (typeOrder === 3 && type === GAME_TYPE.TRIPLE) || (typeOrder === 4 && type === GAME_TYPE.DOUBLE)))
  || (team === 'away' && (
    (typeOrder === 4 && type === GAME_TYPE.TRIPLE) || (typeOrder === 5 && type === GAME_TYPE.DOUBLE)))

/**
 * Participants of the second double/triple must participate in 3rd triple and 4th double for the away team and
 * 4th triple and 5th double for the home team.
 *
 * @inner
 * @param {'home' | 'away'} team - Examined team
 * @param {number} type - Type of the game
 * @param {number} typeOrder - Order of this game within the games of the same type in the match
 * @returns {boolean}
 */
const shouldParticipateSecondGroup = (team, type, typeOrder) =>
  (team === 'away' && (
    (typeOrder === 3 && type === GAME_TYPE.TRIPLE) || (typeOrder === 4 && type === GAME_TYPE.DOUBLE)))
  || (team === 'home' && (
    (typeOrder === 4 && type === GAME_TYPE.TRIPLE) || (typeOrder === 5 && type === GAME_TYPE.DOUBLE)))
