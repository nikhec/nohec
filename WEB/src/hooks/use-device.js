import { useMemo } from 'react'

import { DEVICES, MOBILE_WIDTH, TABLET_WIDTH } from 'src/constants'

import { useWindowDimensions } from './use-window-dimensions'

/**
 * Checks whether is actual device width smaller than configured TABLET device width threshold.
 *
 * @function
 * @returns {Boolean}
 */
export const useIsTablet = () => {
  const { width: windowWidth } = useWindowDimensions()

  return useMemo(() => windowWidth <= TABLET_WIDTH && windowWidth >= MOBILE_WIDTH, [windowWidth])
}

/**
 * Checks whether is actual device width smaller than configured MOBILE device width threshold.
 *
 * @function
 * @returns {Boolean}
 */
export const useIsMobile = () => {
  const { width: windowWidth } = useWindowDimensions()

  return useMemo(() => windowWidth <= MOBILE_WIDTH, [windowWidth])
}

/**
 * Checks whether is actual device width bigger than configured TABLET device width threshold.
 *
 * @function
 * @returns {Boolean}
 */
export const useIsDesktop = () => {
  const { width: windowWidth } = useWindowDimensions()

  return useMemo(() => windowWidth > TABLET_WIDTH, [windowWidth])
}

/**
 * Returns string from enumeration of possible devices.
 *
 * @function
 * @returns {string} - One of DEVICES enumeration object
 */
export const useDevice = () => {
  const isDesktop = useIsDesktop()
  const isTablet = useIsTablet()

  return isDesktop
    ? DEVICES.DESKTOP
    : isTablet
      ? DEVICES.TABLET
      : DEVICES.MOBILE
}
