import { useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'

import { getImmediateScrollOffset } from '../utils/immediate-scroll-offset'
import { useWindowDimensions } from './use-window-dimensions'

/**
 * @global
 * @typedef {Object} SliderLogic
 * @property {React.MutableRefObject<HTMLDivElement>} selectedToggleRef
 * @property {Object | null} selectedToggleRect
 * @property {React.Dispatch<React.SetStateAction<Object | null>>} setToggleRect
 */

/**
 * Handles basic slider logic. Stores, manages and provides current slider position and size.
 *
 * @function
 * @param {any[]} deps - additional dependencies to hook slider position whose change should trigger refreshing effect.
 * @returns {SliderLogic}
 */
export const useSliderLogic = deps => {
  const locale = useSelector(({ customization }) => customization.locale)

  const selectedToggleRef = useRef(null)
  const [selectedToggleRect, setToggleRect] = useState(null)

  const { width: windowWidth } = useWindowDimensions()

  // Sliders are horizontal, we fix vertical coordinate after it is initialized and only update width and horizontal position
  useEffect(() => {
    if (selectedToggleRef.current) {
      const { left, top, width, height } = selectedToggleRef.current.getBoundingClientRect()
      setToggleRect(prevState => ({ left, width, top: prevState?.top ?? top + height }))
    } else {
      setToggleRect(null)
    }
  }, [selectedToggleRef.current, locale, windowWidth, ...deps])

  // The only case when we want to update vertical position is on window width change, we must accommodate scroll offset
  useEffect(() => {
    if (selectedToggleRef.current) {
      const { left, top, width, height } = selectedToggleRef.current.getBoundingClientRect()
      const scrollOffset = getImmediateScrollOffset()

      setToggleRect({ left, width, top: top + height + scrollOffset })
    } else {
      setToggleRect(null)
    }
  }, [windowWidth])

  return {
    selectedToggleRef,
    selectedToggleRect,
    setToggleRect,
  }
}
