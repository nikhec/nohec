import { useState, useEffect } from 'react'

/**
 * @global
 * @typedef {Object} WindowDimensions
 * @property {number} width
 * @property {height} height
 */

/**
 * Gets `innerWidth` and `innerHeight` properties of the `window` global.
 *
 * @returns {WindowDimensions}
 */
const getWindowDimensions = () => {
  const { innerWidth: width, innerHeight: height } = window
  return {
    width,
    height,
  }
}

/**
 * Internally stores window dimensions in the state and updates it on `resize` event triggered on the `window` global.
 *
 * @function
 * @returns {WindowDimensions}
 */
export const useWindowDimensions = () => {
  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions())

  useEffect(() => {
    const handleResize = () => setWindowDimensions(getWindowDimensions())

    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [])

  return windowDimensions
}
