/**
 * Groups commonly used constants.
 */

/**
 * Makes match detail accessible from user Account given number of milliseconds before its play time.
 *
 * @type {number}
 */
export const MATCH_PLAYABLE_BEFORE = 3600000

/**
 * Maximal MOBILE deice width.
 *
 * @type {number}
 */
export const MOBILE_WIDTH = 700

/**
 * Maximal TABLET deice width.
 *
 * @type {number}
 */
export const TABLET_WIDTH = 1000

/**
 * Minimal time interval between subsequent form submitting.
 *
 * @type {number}
 */
export const SUBMIT_TIMEOUT = 2000

/**
 * Enumeration of used devices.
 *
 * @type {Object}
 */
export const DEVICES = {
  MOBILE: 'MOBILE',
  TABLET: 'TABLET',
  DESKTOP: 'DESKTOP',
}

/**
 * Used for matching react-conduit's Inlets with single defined Outlet.
 *
 * @type {string}
 */
export const CONDUIT_LABEL = 'conduitLabel'

/**
 * Unique identifier for the scroll root DOM element.
 *
 * @type {string}
 */
export const SCROLL_ROOT = 'scrollRoot'
