/**
 * Groups config (environmental) variables and provides them with default values.
 *
 * @ignore
 */

/**
 * Port of the API server.
 *
 * @ignore
 * @type {number}
 */
export const SERVER_PORT = process.env.SERVER_PORT

/**
 * Port of the media server.
 *
 * @ignore
 * @type {number}
 */
export const MEDIA_PORT = process.env.MEDIA_PORT ?? SERVER_PORT
