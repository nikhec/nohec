/**
 * Groups various constants used for styling.
 */

import * as COLORS from './colors'
import { MOBILE_WIDTH } from './common'

/**
 * Binding between js constant and CSS variable.
 *
 * @type {string}
 */
export const BLEND_MODE = 'var(--blendMode)'

/**
 * Height of the standard variant of the navigation bar, in pixels.
 *
 * @type {number}
 */
export const NAVBAR_HEIGHT = 60

/**
 * Height of the scrolled variant of the navigation bar, in pixels.
 *
 * @type {number}
 */
export const NAVBAR_HEIGHT_SCROLLED = 48

/**
 * Length of the animation, in seconds.
 *
 * @type {number}
 */
export const LOCALE_ANIMATION_LENGTH = 0.5

/**
 * Length of the animation, in seconds.
 *
 * @type {number}
 */
export const THEME_ANIMATION_LENGTH = 0.5

/**
 * Style overrides for control element of react-select.
 *
 * @type {object}
 */
export const selectControlStyles = {
  height: '50px',
  lineHeight: '50px',
  fontSize: '14px',
  paddingLeft: '25px',
  border: 'none',
  outline: 'none',
  boxShadow: 'none',
  borderRadius: 0,
  fontWeight: 'normal',
  backgroundColor: COLORS.BACKGROUND,
  cursor: 'pointer',
  [`@media (max-width: ${MOBILE_WIDTH}px)`]: {
    paddingLeft: '5px',
    lineHeight: '38px',
    height: '38px',
  },
}

/**
 * Style overrides for menu list element of react-select.
 *
 * @type {object}
 */
export const selectMenuListStyles = {
  margin: 0,
  padding: 0,
}

/**
 * Style overrides for menu element of react-select.
 *
 * @type {object}
 */
export const selectMenuStyles = {
  margin: 0,
  padding: 0,
  borderRadius: 0,
}

/**
 * Style overrides for option element of react-select.
 *
 * @type {object}
 */
export const selectOptionStyles = {
  position: 'relative',
  height: '50px',
  lineHeight: '50px',
  fontSize: '14px',
  paddingLeft: '50px',
  borderBottom: `1px solid ${COLORS.BORDER}`,
  backgroundColor: COLORS.BACKGROUND,
  cursor: 'pointer',
  color: COLORS.RED_PRIMARY,
  '&:last-child': {
    borderBottom: 0,
  },
  '&:hover': {
    fontWeight: '500',
    '&:after': {
      content: '"+"',
      position: 'absolute',
      right: '25px',
      height: '50px',
      lineHeight: '44px',
      color: COLORS.BLUE_PRIMARY,
      fontSize: '22px',
      fontWeight: '500',
    },
  },
  [`@media (max-width: ${MOBILE_WIDTH}px)`]: {
    paddingLeft: '15px',
    lineHeight: '38px',
    height: '38px',
    '&:hover': {
      '&:after': {
        right: '10px',
        height: '38px',
        lineHeight: '34px',
        fontSize: '18px',
      },
    },
  },
}
