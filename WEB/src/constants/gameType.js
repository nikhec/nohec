/**
 * Defines game types with identifiers corresponding to those used on the API.
 */

/**
 * @type {number}
 */
export const SINGLE = 1

/**
 * @type {number}
 */
export const DOUBLE = 2

/**
 * @type {number}
 */
export const TRIPLE = 3
