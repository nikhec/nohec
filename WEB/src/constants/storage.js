/**
 * Groups constants for localStorage keys.
 */

/**
 * LocalStorage key for holding email and id of the logged user.
 *
 * @type {string}
 */
export const LOGGED_USER = 'loggedUser'

/**
 * LocalStorage key for holding currently selected locale identifier.
 *
 * @type {string}
 */
export const LOCALE = 'locale'

/**
 * LocalStorage key for holding currently selected theme identifier.
 *
 * @type {string}
 */
export const THEME = 'theme'

/**
 * LocalStorage key for holding progress of currently recorded game set.
 *
 * @type {string}
 */
export const GAME_SET_RECORD_PROGRESS = 'game-set-record-progress'
