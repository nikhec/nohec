/**
 * Defines internally used locale strings.
 */

/**
 * @type {string}
 */
export const CS = 'cs'

/**
 *
 * @type {string}
 */
export const EN = 'en'
