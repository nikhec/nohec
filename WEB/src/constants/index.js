import * as COLORS from './colors'
import * as CONFIG from './config'
import * as API from './routes'
import * as DESIGN from './design'
import * as TEXTS from './texts'
import * as LOCALES from './locales'
import * as STORAGE from './storage'
import * as MATCH_RECORD_STATE from './matchRecordState'
import * as GAME_TYPE from './gameType'

export {
  MOBILE_WIDTH, TABLET_WIDTH, SUBMIT_TIMEOUT, MATCH_PLAYABLE_BEFORE, DEVICES, CONDUIT_LABEL, SCROLL_ROOT,
} from './common'

export {
  COLORS, CONFIG, API, DESIGN, TEXTS, LOCALES, STORAGE, MATCH_RECORD_STATE, GAME_TYPE,
}
