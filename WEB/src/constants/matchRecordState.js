/**
 * Defines states the match recording can be in.
 */

/**
 * State of the finished match.
 *
 * @type {string}
 */
export const FINISHED = 'FINISHED'

/**
 * State of freshly started match that doesn't have captains associated yet.
 * Forces captain selection page to be rendered.
 *
 * @type {string}
 */
export const CAPTAIN_SELECTION = 'CAPTAIN_SELECTION'

/**
 * State of started match with already assigned captains.
 * Forces game overview page to be rendered.
 *
 * @type {string}
 */
export const STARTED = 'STARTED'

/**
 * State of started match, when user decides to fill particular game but the game doesn't have associated players yet.
 * Forces player selection page to be rendered.
 *
 * @type {string}
 */
export const PLAYER_SELECTION = 'PLAYER_SELECTION'

/**
 * State of started match, when particular game is filled.
 * Forced game page to be rendered.
 *
 * @type {string}
 */
export const GAME = 'GAME'

/**
 * State of started match that has reached required score.
 * Forces summation page to be rendered.
 *
 * @type {string}
 */
export const SUMMATION = 'SUMMATION'

/**
 * State of started match that has reached required score and was provided with optional summation comment.
 * Forces final overview page to be rendered.
 *
 * @type {string}
 */
export const OVERVIEW = 'OVERVIEW'

/**
 * State of started match that is missing only signing before finalizing.
 * Forces sign page to be rendered.
 *
 * @type {string}
 */
export const SIGN = 'SIGN'
