/**
 * Groups constants used as ordered collections of texts.
 */
import * as GAME_TYPE from './gameType'

/**
 * Array of ids corresponding to localized messages for months.
 *
 * @type {string[]}
 */
export const MONTHS = [
  'MONTHS.JANUARY',
  'MONTHS.FEBRUARY',
  'MONTHS.MARCH',
  'MONTHS.APRIL',
  'MONTHS.MAY',
  'MONTHS.JUNE',
  'MONTHS.JULY',
  'MONTHS.AUGUST',
  'MONTHS.SEPTEMBER',
  'MONTHS.OCTOBER',
  'MONTHS.NOVEMBER',
  'MONTHS.DECEMBER',
]

/**
 * Array of ids corresponding to localized messages for days.
 *
 * @type {string[]}
 */
export const DAYS = [
  'DAYS.MONDAY',
  'DAYS.TUESDAY',
  'DAYS.WEDNESDAY',
  'DAYS.THURSDAY',
  'DAYS.FRIDAY',
  'DAYS.SATURDAY',
  'DAYS.SUNDAY',
]

/**
 * Holds mappings from GameTypes to localized text ids.
 *
 * @type {Record<number, string>}
 */
export const GAME_TYPES = {
  [GAME_TYPE.SINGLE]: 'MATCH.SINGLE',
  [GAME_TYPE.DOUBLE]: 'MATCH.DOUBLE',
  [GAME_TYPE.TRIPLE]: 'MATCH.TRIPLE',
}
