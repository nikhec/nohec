/**
 * Defines bindings between color constants and appropriate color CSS variables.
 */

/**
 * Mapping to foreground color CSS variable.
 *
 * @type {string}
 */
export const FOREGROUND = 'var(--cForeground)'

/**
 * Mapping to background color CSS variable.
 *
 * @type {string}
 */
export const BACKGROUND = 'var(--cBackground)'

/**
 * Mapping to font color CSS variable.
 *
 * @type {string}
 */
export const FONT = 'var(--cFont)'

/**
 * Mapping to inverse font color CSS variable.
 *
 * @type {string}
 */
export const FONT_INVERSE = 'var(--cFontInverse)'

/**
 * Mapping to border color CSS variable.
 *
 * @type {string}
 */
export const BORDER = 'var(--cBorder)'

/**
 * Mapping to primary red color CSS variable.
 *
 * @type {string}
 */
export const RED_PRIMARY = 'var(--cRedPrimary)'

/**
 * Mapping to primary blue color CSS variable.
 *
 * @type {string}
 */
export const BLUE_PRIMARY = 'var(--cBluePrimary)'

/**
 * Mapping to light gray color CSS variable.
 *
 * @type {string}
 */
export const GRAY_LIGHT = 'var(--cGrayLight)'

/**
 * Mapping to dark gray color CSS variable.
 *
 * @type {string}
 */
export const GRAY_DARK = 'var(--cGrayDark)'
