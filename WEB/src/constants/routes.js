/**
 * Defines paths for React routing.
 */

/**
 * @type {string}
 */
export const RESULTS = 'results'

/**
 * @type {string}
 */
export const TEAMS = 'teams'

/**
 * @type {string}
 */
export const TEAM_DETAIL = 'team-detail'

/**
 * @type {string}
 */
export const CALENDAR = 'calendar'

/**
 * @type {string}
 */
export const MY_CALENDAR = 'my-calendar'

/**
 * @type {string}
 */
export const MY_ACCOUNT = 'my-account'

/**
 * @type {string}
 */
export const MATCH = 'match'

/**
 * @type {string}
 */
export const MATCH_RECORD = 'match-record'
