import React from 'react'
import createSagaMiddleware from 'redux-saga'
import { Provider } from 'react-redux'
import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import { ConnectedRouter } from 'connected-react-router'
import { Outlet, ConduitProvider } from 'react-conduit'
import { createBrowserHistory } from 'history'

import createRootReducer from './reducers/rootReducer'
import { rootSaga } from './sagas/rootSaga'
import PageTheme from './components/PageTheme'
import Footer from './components/Footer'
import Navbar from './components/navbar/Navbar'
import Router from './components/Router'
import I18nProvider from './components/I18nProvider'
import { CONDUIT_LABEL, SCROLL_ROOT } from './constants'
import { PageContent, PageWrapper } from './styles/blocks/page'

/**
 * React application root component.
 * Provides Redux store, setups saga middleware and browser history.
 *
 * @name Application
 * @component
 */
export default () => {
  const sagaMiddleware = createSagaMiddleware()
  const history = createBrowserHistory()

  const store = createStore(createRootReducer(history),
    compose(applyMiddleware(routerMiddleware(history), sagaMiddleware), window.__REDUX_DEVTOOLS_EXTENSION__
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : v => v))

  sagaMiddleware.run(rootSaga)

  return (
    <Provider store={store}>
      <I18nProvider>
        <ConduitProvider>
          <PageTheme>
            <PageWrapper id={SCROLL_ROOT}>
              <ConnectedRouter history={history}>
                <Navbar />
                <PageContent>
                  <Router />
                </PageContent>
              </ConnectedRouter>
              <Footer />
            </PageWrapper>
            <Outlet label={CONDUIT_LABEL} />
          </PageTheme>
        </ConduitProvider>
      </I18nProvider>
    </Provider>
  )
}
