import { CONFIG } from 'src/constants'

const port = CONFIG.SERVER_PORT ? `:${CONFIG.SERVER_PORT}` : ''

const mediaPort = CONFIG.MEDIA_PORT ? `:${CONFIG.MEDIA_PORT}` : ''

/**
 * Url prefix for all media content (images) that is stored on the server
 *
 * @type {string}
 */
export const MEDIA_URL = `${window.location.protocol}//${window.location.hostname}${mediaPort}`

/**
 * Exports axios configuration file.
 *
 * @type {Object}
 * @property {string} baseURL
 * @property {Record<string, string>} headers
 * @property {boolean} withCredentials
 * @property {string} credentials
 * @property {number} timeout
 */
export const config = {
  baseURL: `${window.location.protocol}//${window.location.hostname}${port}/api`,
  xsrfHeaderName: 'X-CSRFToken',
  xsrfCookieName: 'csrftoken',
  withCredentials: true,
  credentials: 'same-origin',
  timeout: 16000,
}
