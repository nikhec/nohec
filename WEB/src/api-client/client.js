import axios from 'axios'

import { config } from './config'

/**
 * Axios instance with configuration from `./config`.
 *
 * @type {AxiosInstance}
 */
export const apiClient = axios.create(config)
