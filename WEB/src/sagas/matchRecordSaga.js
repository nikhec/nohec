/* eslint-disable camelcase */
import { takeEvery, call, put, select } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import { API } from 'src/constants'
import { MatchRecordActions, MatchActions } from 'src/actions'
import { enterMatch } from 'src/effects/matchEffects'
import { selectCaptains, selectPlayers, submitSet, submitNote, submitSign } from 'src/effects/matchRecordEffects'
import { mapMatchEnterError, mapMatchRecordingError } from 'src/utils/error-mappers'

/**
 * Sends enter match request to the server and awaits its response.
 *
 * @param {Object} action - Action object
 * @param {number} action.payload - Payload passed to the action, corresponding to the Unique Identifier of the match to enter
 */
function* enterMatchSaga({ payload: matchId }) {
  try {
    const { data } = yield call(enterMatch, matchId)
    yield put(MatchRecordActions.recordStateLoadingSuccess({ ...data, id: matchId }))
  } catch (error) {
    if (error.response?.status && Number(error.response.status) < 500) {
      const errorMessage = mapMatchEnterError(error.response.status)
      yield put(MatchRecordActions.recordStateLoadingError(errorMessage))
    } else {
      const errorMessage = 'ERROR.FETCH_CONNECTION'

      console.error(errorMessage)
      yield put(MatchRecordActions.recordStateLoadingError(errorMessage))
    }
  }
}

/**
 * General persisting generator, calls given effect for provided matchId with supplied data.
 * Emits action for redux state update on success and calls onError callback on error.
 *
 * @param {function} effect - Effect function to call for data persisting
 * @param {number} matchId - Id of the match for the route construction
 * @param {Object} data - Data to be passed to persisting request
 * @param {function} onError - Callback called upon persisting error
 * @param {PayloadActionCreator<void, string>} successAction - Action to be dispatched upon successful persisting
 * @param {Object | undefined} actionPayload - Payload to provide to dispatched action, optional, defaults to provided data
 */
function* matchPersistingSaga(effect, matchId, data, onError, successAction, actionPayload) {
  try {
    yield call(effect, matchId, data)
    yield put(successAction(actionPayload ?? data))
  } catch (error) {
    const intl = yield select(({ app }) => app.intl)

    if (error.response?.status && Number(error.response.status) < 500) {
      const errorMessage = mapMatchRecordingError(error.response.status, error.response.data, intl)
      onError(errorMessage)
    } else {
      const errorMessage = intl?.formatMessage({ id: 'ERROR.PERSISTING_CONNECTION' })

      console.error(errorMessage)
      onError([errorMessage])
    }
  }
}

/**
 * Sends captain persisting request to the server.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {number} action.payload.matchId - Unique identifier of the persisted match
 * @param {Object} action.payload.captains - Captains object
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* selectCaptainsSaga({ payload: { matchId, captains, onError } }) {
  yield call(
    matchPersistingSaga,
    selectCaptains,
    matchId,
    captains,
    onError,
    MatchRecordActions.captainsSelected
  )
}

/**
 * Sends players persisting request to the server.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {number} action.payload.matchId - Unique identifier of the persisted match
 * @param {number} action.payload.gameId - Unique identifier of the persisted game
 * @param {number[]} action.payload.homePlayers - List of home player ids
 * @param {number[]} action.payload.awayPlayers - List of away player ids
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* selectPlayersSaga({ payload: { matchId, gameId, homePlayers, awayPlayers, onError } }) {
  yield call(
    matchPersistingSaga,
    selectPlayers,
    matchId,
    {
      id: gameId,
      home_players: homePlayers,
      away_players: awayPlayers,
    },
    onError,
    MatchRecordActions.playersSelected,
    { gameId, homePlayers, awayPlayers }
  )
}

/**
 * Sends set persisting request to the server.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {number} action.payload.matchId - Unique identifier of the persisted match
 * @param {number} action.payload.gameId - Unique identifier of the persisted game
 * @param {number[]} action.payload.result - Array of points of teams participating in the match
 * @param {number[]} action.payload.timeouts - Array of remaining timeouts for the teams participating in the match
 * @param {Object} action.payload.substitutions - Substitutions Object
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* submitSetSaga({ payload: { matchId, gameId, result, timeouts, substitutions, onError } }) {
  yield call(
    matchPersistingSaga,
    submitSet,
    matchId,
    {
      game: gameId,
      home_points: result[0],
      away_points: result[1],
      home_timeout: timeouts[0],
      away_timeout: timeouts[1],
      home_substitutions: substitutions.home.map(([outgoing, incoming]) => ({ outgoing, incoming })),
      away_substitutions: substitutions.away.map(([outgoing, incoming]) => ({ outgoing, incoming })),
    },
    onError,
    MatchRecordActions.setSubmitted,
    {
      gameId,
      result: {
        homePoints: result[0],
        homeSubstitutions: substitutions.home,
        awayPoints: result[1],
        awaySubstitutions: substitutions.away,
      },
    }
  )
}

/**
 * Sends note persisting request to the server.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {number} action.payload.matchId - Unique identifier of the persisted match
 * @param {string} action.payload.note - Match note
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* submitNoteSaga({ payload: { matchId, note, onError } }) {
  yield call(
    matchPersistingSaga,
    submitNote,
    matchId,
    { note },
    onError,
    MatchRecordActions.noteSubmitted
  )
}

/**
 * Sends match recording finalization request to the server.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {number} action.payload.matchId - Unique identifier of the persisted match
 * @param {string[]} action.payload.passwords - Array of match signatures
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* submitSignSaga({ payload: { matchId, passwords, onError } }) {
  const payload = {
    home_captain: passwords.home,
    away_captain: passwords.away,
  }

  if (passwords.referee) {
    payload.referee = passwords.referee
  }

  try {
    yield call(submitSign, matchId, payload)
    yield put(push(`${API.MATCH}?id=${matchId}`))
  } catch (error) {
    const intl = yield select(({ app }) => app.intl)

    if (error.response?.status && Number(error.response.status) < 500) {
      const errorMessage = mapMatchRecordingError(error.response.status, error.response.data, intl)
      onError(errorMessage)
    } else {
      const errorMessage = intl?.formatMessage({ id: 'ERROR.PERSISTING_CONNECTION' })

      console.error(errorMessage)
      onError([errorMessage])
    }
  }
}

/**
 * Handles effect dispatching upon MatchRecordActions.
 *
 * @generator
 */
export function* matchRecordSaga() {
  yield takeEvery(MatchActions.enterMatch.toString(), enterMatchSaga)
  yield takeEvery(MatchRecordActions.selectCaptains.toString(), selectCaptainsSaga)
  yield takeEvery(MatchRecordActions.selectPlayers.toString(), selectPlayersSaga)
  yield takeEvery(MatchRecordActions.submitSet.toString(), submitSetSaga)
  yield takeEvery(MatchRecordActions.submitNote.toString(), submitNoteSaga)
  yield takeEvery(MatchRecordActions.submitSign.toString(), submitSignSaga)
}
