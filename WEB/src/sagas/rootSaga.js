import { all, call, fork, put } from 'redux-saga/effects'

import { AuthActions } from 'src/actions'
import { STORAGE } from 'src/constants'
import { loggedTest } from 'src/effects/authEffects'

import { authSaga } from './authSaga'
import { matchesSaga } from './matchesSaga'
import { teamsSaga } from './teamsSaga'
import { panelSaga } from './panelSaga'
import { customizationSaga } from './customizationSaga'
import { matchRecordSaga } from './matchRecordSaga'

/**
 * Attempts to log in the user on page load/refresh with information stored in localStorage.
 */
function* refreshLogin() {
  const user = localStorage.getItem(STORAGE.LOGGED_USER)

  if (user) {
    try {
      const parsedUser = JSON.parse(user)

      if (parsedUser.email) {
        // React will manage to render the page in the meantime while we wait for loggedTest result
        // we expect session to be valid and log out user only when API says so
        yield put(AuthActions.logInSuccess(parsedUser))

        yield call(loggedTest)
      }
    } catch (ignore) {
      localStorage.removeItem(STORAGE.LOGGED_USER)
      yield put(AuthActions.logOutSuccess())
    }
  }
}

/**
 * Combines all sagas to the root saga that is then hooked into the saga middleware to enable action handling in sagas.
 *
 * @generator
 */
export function* rootSaga() {
  yield all([
    fork(authSaga),
    fork(matchesSaga),
    fork(teamsSaga),
    fork(panelSaga),
    fork(customizationSaga),
    fork(matchRecordSaga),
  ])

  yield call(refreshLogin)
}
