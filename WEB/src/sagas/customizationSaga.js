import { takeEvery, put, delay } from 'redux-saga/effects'

import { STORAGE, DESIGN } from 'src/constants'
import { CustomizationActions } from 'src/actions'

/**
 * Asynchronously turns off localization flag transition after given interval.
 *
 * @param {Object} action - Action object
 * @param {string} action.payload - Payload passed to the action, corresponding to set locale string
 */
function* changeLocaleSaga({ payload: locale }) {
  localStorage.setItem(STORAGE.LOCALE, locale)

  yield put(CustomizationActions.setLocaleChanging(true))
  // Stop locale transition effect
  yield delay(DESIGN.LOCALE_ANIMATION_LENGTH * 1000)
  yield put(CustomizationActions.setLocaleChanging(false))
}

/**
 * Asynchronously turns off theme icon transition after given interval.
 *
 * @param {Object} action - Action object
 * @param {string} action.payload - Payload passed to the action, corresponding to set theme
 */
function* changeThemeSaga({ payload: theme }) {
  localStorage.setItem(STORAGE.THEME, theme)

  yield put(CustomizationActions.setThemeChanging(true))
  // Stop theme transition effect
  yield delay(DESIGN.THEME_ANIMATION_LENGTH * 1000)
  yield put(CustomizationActions.setThemeChanging(false))
}

/**
 * Handles state changing upon Customization Actions.
 *
 * @generator
 */
export function* customizationSaga() {
  yield takeEvery(CustomizationActions.changeLocale.toString(), changeLocaleSaga)
  yield takeEvery(CustomizationActions.changeTheme.toString(), changeThemeSaga)
}
