/* eslint-disable camelcase */
import { takeEvery, call } from 'redux-saga/effects'

import { TeamActions } from 'src/actions'
import { loadTeams, loadTeamDetail, loadLeagueStandings, loadTeamStatistics } from 'src/effects/teamEffects'
import { handleFetchError } from 'src/utils/sagas'

/**
 * Sends team load request to the server and awaits its response.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {number | null} action.payload.seasonId - Unique id of the season to fetch data for
 * @param {Function} action.payload.onSuccess - Callback function called on success with the data fetched
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* loadTeamsSaga({ payload: { seasonId, onSuccess, onError } }) {
  try {
    const { data } = yield call(loadTeams, seasonId
      ? { league__season__id: seasonId }
      : { latest_season: true }
    )
    yield call(onSuccess, data)
  } catch (error) {
    handleFetchError(error, onError)
  }
}

/**
 * Sends load team detail request to the server and awaits its response.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {number | null} action.payload.teamId - Unique id of the team to fetch detail for
 * @param {Function} action.payload.onSuccess - Callback function called on success with the data fetched
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* loadTeamDetailSaga({ payload: { teamId, onSuccess, onError } }) {
  try {
    const { data } = yield call(loadTeamDetail, teamId)
    yield call(onSuccess, data)
  } catch (error) {
    handleFetchError(error, onError)
  }
}

/**
 * Sends load team standings request to the server and awaits its response.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {number | null} action.payload.leagueId - Unique id of the league to fetch standings for
 * @param {Function} action.payload.onSuccess - Callback function called on success with the data fetched
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* loadTeamStandingsSaga({ payload: { leagueId, onSuccess, onError } }) {
  try {
    const { data } = yield call(loadLeagueStandings, leagueId)
    yield call(onSuccess, data)
  } catch (error) {
    handleFetchError(error, onError)
  }
}

/**
 * Sends load team statistics load request to the server and awaits its response.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {number | null} action.payload.teamId - Unique id of the team to fetch the statistics for
 * @param {Function} action.payload.onSuccess - Callback function called on success with the data fetched
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* loadTeamStatisticsSaga({ payload: { teamId, onSuccess, onError } }) {
  try {
    const { data } = yield call(loadTeamStatistics, teamId)
    yield call(onSuccess, data)
  } catch (error) {
    handleFetchError(error, onError)
  }
}

/**
 * Handles effect calling upon Team Actions.
 *
 * @generator
 */
export function* teamsSaga() {
  yield takeEvery(TeamActions.loadTeams.toString(), loadTeamsSaga)
  yield takeEvery(TeamActions.loadTeamDetail.toString(), loadTeamDetailSaga)
  yield takeEvery(TeamActions.loadTeamStandings.toString(), loadTeamStandingsSaga)
  yield takeEvery(TeamActions.loadTeamStatistics.toString(), loadTeamStatisticsSaga)
}
