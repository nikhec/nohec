import { takeEvery, call, put } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import { AuthActions } from 'src/actions'
import { fetchSignature, logIn, logOut } from 'src/effects/authEffects'
import { handleFetchError } from 'src/utils/sagas'
import { API, STORAGE } from 'src/constants'

/**
 * Performs all actions necessary to stop identifying the current user as logged in.
 */
function* logOutSaga() {
  try {
    yield call(logOut)
    localStorage.removeItem(STORAGE.LOGGED_USER)
    yield put(AuthActions.logOutSuccess())
  } catch (e) {
    console.error(e)
  }
}

/**
 * Sends log in request to the server and awaits its response.
 * 4xx and 5xx responses throws an error and are therefore handled in catch block with passing error message to signIn component.
 * Any other status codes are interpreted as success and appropriate action is dispatched.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {Object} action.payload.inputs - Log in form input data
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* logInSaga({ payload: { inputs, onError } }) {
  try {
    const { data: { email, ...name } } = yield call(logIn, inputs)
    const userName = (name?.first_name && name?.last_name)
      ? `${name.first_name} ${name.last_name}`
      : null

    const user = { email, name: userName }
    localStorage.setItem(STORAGE.LOGGED_USER, JSON.stringify(user))

    yield put(AuthActions.logInSuccess(user))
    yield put(push(`${API.MY_ACCOUNT}`))
  } catch (error) {
    yield call(onError, error.response?.data?.message ?? 'FORM.LOG_IN_FAILED')
  }
}

/**
 * Sends fetch signature request to the server and awaits its response.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {Function} action.payload.onSuccess - Callback function called on success with the data fetched
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* fetchSignatureSaga({ payload: { onSuccess, onError } }) {
  try {
    const { data } = yield call(fetchSignature)
    yield call(onSuccess, data?.signature)
  } catch (error) {
    handleFetchError(error, onError)
  }
}

/**
 * Handles effect dispatching upon Authentication Actions.
 *
 * @generator
 */
export function* authSaga() {
  yield takeEvery(AuthActions.logOut.toString(), logOutSaga)
  yield takeEvery(AuthActions.logIn.toString(), logInSaga)
  yield takeEvery(AuthActions.fetchSignature.toString(), fetchSignatureSaga)
}
