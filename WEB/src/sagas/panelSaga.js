import { takeEvery, call } from 'redux-saga/effects'

import { PanelActions } from 'src/actions'
import { loadLeagues, loadSeasons } from 'src/effects/panelEffects'
import { handleFetchError } from 'src/utils/sagas'

/**
 * Sends load seasons request to the server using appropriate effect and awaits its response.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {Function} action.payload.onSuccess - Callback function called on success with the data fetched
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* loadSeasonsSaga({ payload: { onSuccess, onError } }) {
  try {
    const { data } = yield call(loadSeasons)
    yield call(onSuccess, data)
  } catch (error) {
    handleFetchError(error, onError)
  }
}

/**
 * Sends load seasons request to the server using appropriate effect and awaits its response.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {Function} action.payload.onSuccess - Callback function called on success with the data fetched
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* loadLeaguesSaga({ payload: { onSuccess, onError } }) {
  try {
    const { data } = yield call(loadLeagues)
    yield call(onSuccess, data)
  } catch (error) {
    handleFetchError(error, onError)
  }
}

/**
 * Handles effect calling upon Team Actions.
 *
 * @generator
 */
export function* panelSaga() {
  yield takeEvery(PanelActions.loadSeasons.toString(), loadSeasonsSaga)
  yield takeEvery(PanelActions.loadLeagues.toString(), loadLeaguesSaga)
}
