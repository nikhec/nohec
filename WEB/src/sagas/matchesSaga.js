/* eslint-disable camelcase */
import { takeEvery, call } from 'redux-saga/effects'

import { MatchActions } from 'src/actions'
import { handleFetchError } from 'src/utils/sagas'
import { loadMatches, loadMyMatches, loadMatchDetail } from 'src/effects/matchEffects'

/**
 * Triggers an effect for fetching my matches.
 *
 * @param {Object} action - Action object
 */
function* loadMyMatchesSaga(action) {
  yield call(getMatches, action, loadMyMatches)
}

/**
 * Triggers an effect for fetching matches.
 *
 * @param {Object} action - Action object
 */
function* loadMatchesSaga(action) {
  yield call(getMatches, action, loadMatches)
}

/**
 * Helper that sends load request to the server and awaits its response.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {Object} action.payload.filters - Filters object
 * @param {Function} action.payload.onSuccess - Callback function called on success with the data fetched
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 * @param {Function} loadingEffect - Effect function performing actual data fetching
 */
function* getMatches({ payload: { filters: { dateLt, dateGt, limit, leagueId }, onSuccess, onError } }, loadingEffect) {
  try {
    const { data } = yield call(
      loadingEffect,
      {
        date__lt: dateLt,
        date__gt: dateGt,
        limit,
        league__id: leagueId,
      })
    yield call(onSuccess, data)
  } catch (error) {
    handleFetchError(error, onError)
  }
}

/**
 * Sends load request to the server and awaits its response.
 *
 * @param {Object} action - Action object
 * @param {Object} action.payload - Payload passed to the action
 * @param {number} action.payload.matchId - Unique identifier of the match
 * @param {Function} action.payload.onSuccess - Callback function called on success with the data fetched
 * @param {Function} action.payload.onError - Callback function called with error message string on request failure
 */
function* loadMatchDetailSaga({ payload: { matchId, onSuccess, onError } }) {
  try {
    const { data } = yield call(loadMatchDetail, matchId)
    yield call(onSuccess, { ...data, id: matchId })
  } catch (error) {
    handleFetchError(error, onError)
  }
}

/**
 * Handles effect dispatching upon Matches Actions.
 *
 * @generator
 */
export function* matchesSaga() {
  yield takeEvery(MatchActions.loadMatches.toString(), loadMatchesSaga)
  yield takeEvery(MatchActions.loadMyMatches.toString(), loadMyMatchesSaga)
  yield takeEvery(MatchActions.loadMatchDetail.toString(), loadMatchDetailSaga)
}
