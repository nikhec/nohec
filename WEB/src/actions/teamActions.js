import { createAction } from '@reduxjs/toolkit'

/**
 * Triggers team loading effect.
 *
 * @function
 * @param {number | null} seasonId - Unique identifier of the season to be fetched for
 * @param {Function} onSuccess - Handles successful data fetching
 * @param {Function} onError - Callback for loading error handling
 */
export const loadTeams = createAction('LOAD_TEAMS')

/**
 * Triggers team loading effect.
 *
 * @function
 * @param {number} teamId - Unique identifier of the team to fetch detail for
 * @param {Function} onSuccess - Handles successful data fetching
 * @param {Function} onError - Callback for loading error handling
 */
export const loadTeamDetail = createAction('LOAD_TEAM_DETAIL')

/**
 * Triggers effect that loads standings. Fetches data needed to display standings table on the results page.
 *
 * @function
 * @param {number} leagueId - Unique identifier of the league
 * @param {Function} onSuccess - Handles successful data fetching
 * @param {Function} onError - Handles failed data fetching
 */
export const loadTeamStandings = createAction('LOAD_TEAM_STANDINGS')

/**
 * Triggers effect that loads team statistics. Fetches data needed to graphs on the team detail page.
 *
 * @function
 * @param {number} teamId - Unique identifier of the team
 * @param {Function} onSuccess - Handles successful data fetching
 * @param {Function} onError - Handles failed data fetching
 */
export const loadTeamStatistics = createAction('LOAD_TEAM_STATISTICS')
