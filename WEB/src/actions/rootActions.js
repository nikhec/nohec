import { createAction } from '@reduxjs/toolkit'

/**
 * Replaces current Intl object in store with the new one.
 *
 * @function
 * @param {Object} intl - New Intl object
 */
export const loadInternationalization = createAction('LOAD_INTERNATIONALIZATION')
