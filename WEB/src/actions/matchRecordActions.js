import { createAction } from '@reduxjs/toolkit'

/**
 * Persists fetched match recording state into redux state.
 *
 * @function
 * @param {Object} state - Fetched ApiMatchRecord
 */
export const recordStateLoadingSuccess = createAction('RECORD_STATE_LOADING_SUCCESS')

/**
 * Persists fetching error into redux state.
 *
 * @function
 * @param {string} error - Error message
 */
export const recordStateLoadingError = createAction('RECORD_STATE_LOADING_ERROR')

/**
 * Triggers persisting of selected captains during match recording.
 *
 * @function
 * @param {number} matchId - Id of the match to persist data for
 * @param {Object} captains - Captains object
 * @param {number} captains.home - Id of the selected captain for the home team
 * @param {number} captains.away - Id of the selected captain for the away team
 * @param {Function} onError - Callback handling failed data persisting
 */
export const selectCaptains = createAction('SELECT_CAPTAINS')

/**
 * Updates redux state with captain ids after they are persisted by the API.
 *
 * @function
 * @param {number} homeCaptain - Id of the selected captain for the home team
 * @param {number} awayCaptain - Id of the selected captain for the away team
 */
export const captainsSelected = createAction('CAPTAINS_SELECTED')

/**
 * Triggers persisting of selected players of partial game during match recording.
 *
 * @function
 * @param {number} matchId - Id of the match to persist data for
 * @param {number} gameId - Id of the partial game within the match to persist data for
 * @param {number[]} homePlayers - Array of ids of home players
 * @param {number[]} awayPlayers - Array of ids of away players
 * @param {Function} onError - Callback handling failed data persisting
 */
export const selectPlayers = createAction('SELECT_PLAYERS')

/**
 * Updates redux state with player ids after they are persisted by the API.
 *
 * @function
 * @param {number} gameId - Id of the partial game within the match to persist data for
 * @param {number[]} homePlayers - Array of ids of home players
 * @param {number[]} awayPlayers - Array of ids of away players
 */
export const playersSelected = createAction('PLAYERS_SELECTED')

/**
 * Triggers persisting of recorded set of partial game.
 *
 * @function
 * @param {number} matchId - Id of the match to persist data for
 * @param {number} gameId - Id of the partial game within the match to persist data for
 * @param {number[]} result - Final score of the set
 * @param {number[]} timeouts - Times remaining for each team within the set
 * @param {Object[]} substitutions - Substitutions made during the set
 * @param {number[]} substitutions.home - Substitutions of the home team
 * @param {number[]} substitutions.away - Substitutions of the away team
 * @param {Function} onError - Callback handling failed data persisting
 */
export const submitSet = createAction('SUBMIT_SET')

/**
 * Updates redux state with partial game's set after it has been persisted by the API.
 *
 * @function
 * @param {number} gameId - Id of the partial game within the match to persist data for
 * @param {number[]} result - Final score of the set
 * @param {number[]} times - Times remaining for each team within the set
 */
export const setSubmitted = createAction('SET_SUBMITTED')

/**
 * Triggers persisting of a final note for the match.
 *
 * @function
 * @param {number} matchId - Id of the match to persist data for
 * @param {string} note - Id of the partial game within the match to persist data for
 * @param {Function} onError - Callback handling failed data persisting
 */
export const submitNote = createAction('SUBMIT_NOTE')

/**
 * Updates redux state with match's note after it has been persisted by the API.
 *
 * @function
 * @param {string} note - Final score of the set
 */
export const noteSubmitted = createAction('NOTE_SUBMITTED')

/**
 * Updates redux state after the user has confirmed the match record overview.
 *
 * @function
 */
export const overviewConfirmed = createAction('OVERVIEW_CONFIRMED')

/**
 * Triggers finalization of the match recording with provided password.
 *
 * @function
 * @param {number} matchId - Id of the match to persist data for
 * @param {Object} passwords - Passwords used for signing of the match recording
 * @param {string} passwords.home - Home team captain's signature
 * @param {string} passwords.away - Away team captain's signature
 * @param {string} passwords.referee - Referee's signature or an empty string
 * @param {Function} onError - Callback handling failed data persisting
 */
export const submitSign = createAction('SUBMIT_SIGN')
