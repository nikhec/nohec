import { createAction } from '@reduxjs/toolkit'

/**
 * Triggers load matches effect. Accepts two dates as filters for time range specifying.
 *
 * @function
 * @param {Object} filters - Filters object
 * @param {number} filters.dateLt - Month to filter with
 * @param {number} filters.dateGt - Year to filter with
 * @param {number} filters.leagueId - Unique identifier of the league to fetch matches for
 * @param {Function} onSuccess - Handles successful data fetching
 * @param {Function} onError - Callback handling failed data fetching
 */
export const loadMatches = createAction('LOAD_MATCHES')

/**
 * Triggers load my matches effect. Accepts two dates as filters for time range specifying.
 *
 * @function
 * @param {Object} filters - Filters object
 * @param {number} filters.dateLt - Date used as bottom threshold for returned matches
 * @param {number} filters.dateGt - Date used as upper threshold for returned matches
 * @param {number} filters.limit - Number of results to return
 * @param {Function} onSuccess - Callback handling successful data fetching
 * @param {Function} onError - Callback handling failed data fetching
 */
export const loadMyMatches = createAction('LOAD_MY_MATCHES')

/**
 * Triggers effect that loads a single match detail. Fetches data needed to display detail page of a single match.
 *
 * @function
 * @param {number} matchId - Match's unique identifier
 * @param {Function} onSuccess - Callback handling successful data fetching
 * @param {Function} onError - Callback handling failed data fetching
 */
export const loadMatchDetail = createAction('LOAD_MATCH_DETAIL')

/**
 * Retrieves the current state of the match and starts it if it hasn't been started yet.
 * Fails in case some other user owns the lock.
 *
 * @function
 * @param {number} matchId - Month unique identifier
 */
export const enterMatch = createAction('ENTER_MATCH')
