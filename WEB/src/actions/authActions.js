import { createAction } from '@reduxjs/toolkit'

/**
 * Triggers log in effect.
 *
 * @function
 * @param {Object} inputs - Filters object
 * @param {string} inputs.email - User email
 * @param {string} inputs.password - user password
 * @param {Function} onError - Callback for log in error handling
 */
export const logIn = createAction('LOG_IN')

/**
 * Successful log in action is caught and processed by rootReducer.
 *
 * @function
 * @param {Object} user - User object
 * @param {string} user.email - Email of the user
 * @param {number | null} user.referee - Id of the associated referee object.
 */
export const logInSuccess = createAction('LOG_IN_SUCCESS')

/**
 * Triggers log out effect.
 *
 * @function
 */
export const logOut = createAction('LOG_OUT')

/**
 * Successful log out action is caught and processed by rootReducer.
 *
 * @function
 */
export const logOutSuccess = createAction('LOG_OUT_SUCCESS')

/**
 * Triggers signature fetching effect.
 *
 * @function
 */
export const fetchSignature = createAction('FETCH_SIGNATURE')
