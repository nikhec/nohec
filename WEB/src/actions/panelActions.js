import { createAction } from '@reduxjs/toolkit'

/**
 * Fetches list of all seasons from the API.
 *
 * @function
 * @param {Function} onSuccess - Handles successful data fetching
 * @param {Function} onError - Handles failed data fetching
 */
export const loadSeasons = createAction('LOAD_SEASONS')

/**
 * Fetches list of all leagues from the API.
 *
 * @function
 * @param {Function} onSuccess - Handles successful data fetching
 * @param {Function} onError - Handles failed data fetching
 */
export const loadLeagues = createAction('LOAD_LEAGUES')
