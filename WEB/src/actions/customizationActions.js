import { createAction } from '@reduxjs/toolkit'

/**
 * Triggers locale change.
 *
 * @function
 * @param {string} locale - New locale to switch to
 */
export const changeLocale = createAction('CHANGE_LOCALE')

/**
 * Set's new state of locale changing animation.
 *
 * @function
 * @param {boolean} state - New state of locale changing animation
 */
export const setLocaleChanging = createAction('SET_LOCALE_CHANGING')

/**
 * Triggers theme change.
 *
 * @function
 * @param {string} theme - New theme to switch to
 */
export const changeTheme = createAction('CHANGE_THEME')

/**
 * Set's new state of theme changing animation.
 *
 * @function
 * @param {boolean} state - New state of theme changing animation
 */
export const setThemeChanging = createAction('SET_THEME_CHANGING')
