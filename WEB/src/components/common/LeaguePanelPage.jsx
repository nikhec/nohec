import React, { useEffect, useMemo, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import BallImage from '~/assets/icons/icon-ball.png'

import { PanelActions } from 'src/actions'
import { useFetchedArrayState, useFetchedState } from 'src/hooks/use-fetched-state'
import { useIsDesktop, useIsMobile } from 'src/hooks/use-device'
import { selectLeagues } from 'src/selectors/panelSelectors'
import { ColumnView, ContentGroup } from 'src/styles/blocks/page'
import {
  LeaguePanelLeague, LeaguePanelLeftColumn, LeaguePanelRightColumn,
  LeaguePanelLeagueName, LeaguePanelLeagueImage, LeaguesPanelLeaguesBox,
} from 'src/styles/blocks/leagues'

import ErrorBanner from './ErrorBanner'
import LoadingBanner from './LoadingBanner'
import SeasonSelect from './SeasonSelect'

/**
 * Wrapper component for pages displaying league/season panel on the left.
 * Provides child components with currently selected league and season identifiers.
 *
 * @component
 */
const LeaguePanelPage = ({ ChildComponent }) => {
  const [selected, setSelected] = useState({ leagueId: null, seasonId: null })

  const {
    state: seasons,
    error: seasonsError,
    isFetching: seasonsFetching,
  } = useFetchedArrayState(PanelActions.loadSeasons)

  const seasonsFromLatest = useMemo(() => [...seasons]?.sort((sA, sB) => sB.year - sA.year), [seasons])

  const {
    state: leagues,
    error: leaguesError,
    isFetching: leaguesFetching,
  } = useFetchedState(PanelActions.loadLeagues, {}, selectLeagues)

  useEffect(() => {
    const firstSeasonId = seasonsFromLatest?.[0]?.id ?? null

    setSelected(prevState => ({
      leagueId: prevState.leagueId ?? (firstSeasonId && leagues?.[firstSeasonId]?.[0]?.id) ?? null,
      seasonId: prevState.seasonId ?? firstSeasonId,
    }))
  }, [leagues, seasonsFromLatest])

  const isDesktop = useIsDesktop()
  const isMobile = useIsMobile()

  const isFetching = seasonsFetching || leaguesFetching
  const error = seasonsError ?? leaguesError

  const { leagueId, seasonId } = selected
  const currentLeagues = useMemo(() => leagues[seasonId] ?? [], [leagues, seasonId])

  return !isFetching && !error
    ? (
      <ColumnView>
        <LeaguePanelLeftColumn>
          <h1><FormattedMessage id="COMMON.LEAGUES" /></h1>
          <LeaguesPanelLeaguesBox>
            {currentLeagues.sort(({ id: idA }, { id: idB }) => idA - idB).map(({ name, code, id }) => (
              <LeaguePanelLeague
                key={`league-${id}`}
                onClick={() => setSelected(prevState => ({ ...prevState, leagueId: id }))}
              >
                {!isMobile && (
                  <LeaguePanelLeagueImage selected={leagueId === id}>
                    <img alt="" src={BallImage} />
                  </LeaguePanelLeagueImage>
                )}
                <LeaguePanelLeagueName selected={leagueId === id}>{isDesktop ? name : code}</LeaguePanelLeagueName>
              </LeaguePanelLeague>
            ))}
          </LeaguesPanelLeaguesBox>

          <div>
            <h2><FormattedMessage id="COMMON.SEASON" /></h2>
            {seasonId && <SeasonSelect
              seasons={isDesktop ? seasons : seasonsFromLatest}
              selectedSeasonId={seasonId}
              onSelection={seasonId => setSelected({
                leagueId: leagues?.[seasonId]?.[0]?.id ?? null,
                seasonId,
              })}
            />}
          </div>
        </LeaguePanelLeftColumn>

        <LeaguePanelRightColumn>
          <ChildComponent leagueId={leagueId} seasonId={seasonId} />
        </LeaguePanelRightColumn>
      </ColumnView>
    ) : error
      ? <ContentGroup omitBottomMargin><ErrorBanner messageId={error} /></ContentGroup>
      : <LoadingBanner />
}

LeaguePanelPage.propTypes = {
  /**
   * Content for the page displayed on the right from the panel
   */
  ChildComponent: PropTypes.func.isRequired,
}

export default LeaguePanelPage
