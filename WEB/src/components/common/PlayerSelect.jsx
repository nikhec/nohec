import React, { useMemo, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import Select, { components } from 'react-select'

import { COLORS, DESIGN } from 'src/constants'
import { useIsMobile } from 'src/hooks/use-device'
import { MatchDetailPlayerPropType } from 'src/utils/prop-types'
import { MatchBodyListSelectArrow, MatchPlayersList } from 'src/styles/blocks/matches'

/**
 * Player selector input component.
 *
 * @component
 */
const PlayerSelect = ({
  players,
  allowedPlayers,
  selectedPlayerId,
  onSelection,
  placeholder,
  fullOverflow,
  canRemove,
}) => {
  const [expanded, setExpanded] = useState(false)
  const selectRef = useRef(null)

  const isMobile = useIsMobile()

  const Control = ({ children, ...rest }) => (
    <components.Control {...rest}>
      {children}
      <MatchBodyListSelectArrow
        hasValue={rest.hasValue}
        showRemove={canRemove && rest.hasValue}
        onMouseDown={e => {
          if (canRemove && rest.hasValue) {
            e.stopPropagation()
            onSelection(null)
          }
        }}
      >
        {expanded && (!isMobile || !rest.hasValue) ? '–' : '+'}
      </MatchBodyListSelectArrow>
    </components.Control>
  )

  // This ordering simply pushes disallowed players to the back
  const orderedPlayers = useMemo(() => allowedPlayers
    ? [...players].sort((pA, pB) => allowedPlayers.includes(pA.id)
      ? -1
      : allowedPlayers.includes(pB.id) ? 1 : pA.id - pB.id)
    : players,
  [players, allowedPlayers])

  return (
    <MatchPlayersList
      fullOverflow={fullOverflow}
      hasError={allowedPlayers && selectedPlayerId && !allowedPlayers.includes(selectedPlayerId)}
      onFocus={() => setExpanded(true)}
      onBlur={() => setExpanded(false)}
    >
      <Select
        ref={selectRef}
        options={orderedPlayers.map(player => ({ label: player.name, value: player.id }))}
        value={selectedPlayerId
          ? { label: orderedPlayers.find(({ id }) => id === selectedPlayerId).name, value: selectedPlayerId }
          : null}
        onChange={({ value }) => {
          selectRef.current.blur()
          onSelection(Number(value))
        }}
        placeholder={placeholder}
        isSearchable={false}
        components={{ Control }}
        styles={{
          singleValue: provided => ({
            ...provided,
            color: COLORS.FONT,
          }),
          valueContainer: provided => ({
            ...provided,
            color: COLORS.FONT,
            position: 'static',
          }),
          control: (provided, { hasValue }) => ({
            ...provided,
            ...DESIGN.selectControlStyles,
            fontWeight: hasValue ? 'normal' : 'bold',
            backgroundColor: hasValue ? COLORS.BACKGROUND : COLORS.FOREGROUND,
          }),
          menu: provider => ({
            ...provider,
            ...DESIGN.selectMenuStyles,
          }),
          menuList: provider => ({
            ...provider,
            ...DESIGN.selectMenuListStyles,
          }),
          indicatorSeparator: () => ({ display: 'none' }),
          dropdownIndicator: () => ({ display: 'none' }),
          option: (_, { isSelected, value }) => ({
            ...DESIGN.selectOptionStyles,
            display: isSelected ? 'none' : 'inherit',
            ...allowedPlayers && !allowedPlayers.includes(value)
              ? {
                opacity: 0.5,
                color: COLORS.FONT,
                '&:hover': {
                  ...DESIGN.selectOptionStyles['&:hover'],
                  '&:after': {
                    ...DESIGN.selectOptionStyles['&:hover']?.['&:after'],
                    content: '"!"',
                    color: COLORS.RED_PRIMARY,
                  },
                },
              } : {},
          }),
        }}
      />
    </MatchPlayersList>
  )
}

PlayerSelect.propTypes = {
  /**
   * List of players
   */
  players: PropTypes.arrayOf(MatchDetailPlayerPropType).isRequired,
  /**
   * List of player ids that should be specially marked as 'allowed' (all are 'allowed' if absent)
   */
  allowedPlayers: PropTypes.arrayOf(PropTypes.number),
  /**
   * Identifier of currently selected player
   */
  selectedPlayerId: PropTypes.number,
  /**
   * Selection callback
   */
  onSelection: PropTypes.func.isRequired,
  /**
   * Default localized text to show when no value is selected
   */
  placeholder: PropTypes.string.isRequired,
  /**
   * Indicates that the list should overflow on both sides.
   */
  fullOverflow: PropTypes.bool,
  /**
   * Indicates whether the select should offer an option to unselect selected value.
   */
  canRemove: PropTypes.bool,
}

export default PlayerSelect
