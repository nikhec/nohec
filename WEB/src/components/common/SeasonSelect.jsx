import React, { useRef } from 'react'
import Select from 'react-select'
import PropTypes from 'prop-types'

import { COLORS, DESIGN, TABLET_WIDTH } from 'src/constants'
import { useIsDesktop } from 'src/hooks/use-device'
import { SeasonSelectBox } from 'src/styles/blocks/leagues'

/**
 * Seasons selector input component.
 *
 * @component
 */
const SeasonSelect = ({ seasons, selectedSeasonId, onSelection }) => {
  const selectRef = useRef(null)
  const isDesktop = useIsDesktop()

  const hasMoreSeasons = seasons.length > 1

  return (
    <SeasonSelectBox isSelectable={hasMoreSeasons}>
      <Select
        ref={selectRef}
        options={seasons.map(({ id, year }) => ({ value: id, label: year }))}
        value={{ label: seasons.find(({ id }) => id === selectedSeasonId).year, value: selectedSeasonId }}
        menuPlacement={isDesktop ? 'top' : 'bottom'}
        onChange={({ value }) => {
          selectRef.current.blur()
          onSelection(Number(value))
        }}
        isSearchable={false}
        styles={{
          singleValue: provided => ({
            ...provided,
            color: COLORS.FONT,
            [`@media (max-width: ${TABLET_WIDTH}px)`]: hasMoreSeasons
              ? {}
              : {
                width: 'calc(100% - 29px)',
                textAlign: 'center',
              },
          }),
          control: provided => ({
            ...provided,
            ...DESIGN.selectControlStyles,
            paddingLeft: '10px !important',
            fontSize: '20px',
            fontWeight: '500',
            cursor: hasMoreSeasons ? 'pointer' : 'default',
            lineHeight: '38px',
            height: '38px',
          }),
          indicatorSeparator: () => ({ display: 'none' }),
          dropdownIndicator: (provided, { isFocused }) => hasMoreSeasons
            ? {
              ...provided,
              transform: isFocused ? 'rotate(180deg)' : 'none',
            } : { display: 'none' },
          menu: provider => ({
            ...provider,
            ...DESIGN.selectMenuStyles,
            zIndex: 2,
          }),
          menuList: provider => ({
            ...provider,
            ...DESIGN.selectMenuListStyles,
          }),
          option: (_, state) => ({
            ...DESIGN.selectOptionStyles,
            display: state.isSelected ? 'none' : 'inherit',
            paddingLeft: '30px',
            lineHeight: '38px',
            height: '38px',
            fontWeight: 400,
            color: COLORS.FONT,
            '&:hover': {
              color: COLORS.BLUE_PRIMARY,
              ...DESIGN.selectOptionStyles['&:hover'],
              '&:after': {
                ...DESIGN.selectOptionStyles['&:hover']?.['&:after'],
                right: '12px',
                height: '38px',
                lineHeight: '34px',
                fontSize: '18px',
              },
            },
          }),
        }}
      />
    </SeasonSelectBox>
  )
}

SeasonSelect.propTypes = {
  /**
   * List of selectable seasons
   */
  seasons: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    year: PropTypes.number.isRequired,
  })).isRequired,
  /**
   * Id of currently selected season
   */
  selectedSeasonId: PropTypes.number.isRequired,
  /**
   * Selection callback
   */
  onSelection: PropTypes.func.isRequired,
}

export default SeasonSelect
