import React, { forwardRef, useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'

import { ButtonBox, FormButton } from 'src/styles/blocks/form'
import { COLORS, SUBMIT_TIMEOUT } from 'src/constants'

/**
 * Renders primary button with BLUE_PRIMARY background and FONT_INVERSE font color.
 *
 * @component
 */
const PrimaryButton = forwardRef(({ children, onClick, disabled, higher, hasSubmitTimeout, textColor, backgroundColor }, ref) => {
  const [isSubmitted, setIsSubmitted] = useState(false)
  const timer = useRef(null)

  useEffect(() => () => clearTimeout(timer.current), [])

  const handleSubmitClick = e => {
    if (hasSubmitTimeout) {
      setIsSubmitted(true)
      timer.current = setTimeout(() => setIsSubmitted(false), SUBMIT_TIMEOUT)
    }

    onClick(e)
  }

  return (
    <ButtonBox disabled={disabled || isSubmitted} backgroundColor={backgroundColor}>
      <FormButton
        ref={ref}
        onClick={handleSubmitClick}
        disabled={disabled || isSubmitted}
        higher={higher}
        textColor={textColor}
      >
        {children}
      </FormButton>
    </ButtonBox>
  )
})

PrimaryButton.defaultProps = {
  textColor: COLORS.FONT_INVERSE,
  backgroundColor: COLORS.BLUE_PRIMARY,
}

PrimaryButton.propTypes = {
  /**
   * Indicates whether is button clickable
   */
  disabled: PropTypes.bool,
  /**
   * Indicates whether to render higher variant of the button
   */
  higher: PropTypes.bool,
  /**
   * Content of the button, either string or React element
   */
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]),
  /**
   * Click handler of the button
   */
  onClick: PropTypes.func,
  /**
   * Indicates whether should the button automatically disable onClick for SUBMIT_TIMEOUT
   */
  hasSubmitTimeout: PropTypes.bool,
  /**
   * Override for the default ont color
   */
  textColor: PropTypes.string,
  /**
   * Override for the default background color
   */
  backgroundColor: PropTypes.string,
}

export default PrimaryButton
