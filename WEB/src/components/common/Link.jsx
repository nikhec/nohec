import React from 'react'
import PropTypes from 'prop-types'

import { LinkWrapper } from 'src/styles/blocks/page'

/**
 * Wraps content with anchor element that makes links recognizable by browser but disables default click handler
 *
 * @component
 */
const Link = ({ route, children }) => (
  <LinkWrapper href={route} onClick={e => e.preventDefault()}>
    {children}
  </LinkWrapper>
)

Link.propTypes = {
  /**
   * Route to open on new tab on middle mouse button click
   */
  route: PropTypes.string.isRequired,
  /**
   * Content to be wrapped by the link
   */
  children: PropTypes.node.isRequired,
}

export default Link
