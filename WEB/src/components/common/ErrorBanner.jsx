import React from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import { ErrorBox } from 'src/styles/blocks/page'

/**
 * Component with error message displayed on failed data fetching.
 *
 * @component
 */
const ErrorBanner = ({ messageId }) => (
  <ErrorBox>
    <FormattedMessage id={messageId} />
  </ErrorBox>
)

ErrorBanner.propTypes = {
  /**
   * Id of localized string for the error.
   */
  messageId: PropTypes.string.isRequired,
}

export default ErrorBanner
