import React, { useEffect } from 'react'
import { Inlet } from 'react-conduit'
import PropTypes from 'prop-types'

import { CONDUIT_LABEL, SCROLL_ROOT } from 'src/constants'
import { Backdrop, ModalBody } from 'src/styles/blocks/page'

/**
 * Component rendering modal window.
 *
 * @component
 */
const Modal = ({ onBackdropClick, children }) => {
  const scrollRoot = document.getElementById(SCROLL_ROOT)

  useEffect(() => {
    scrollRoot.style.overflow = 'hidden'

    return () => { scrollRoot.style.overflow = 'auto' }
  }, [])

  return (
    <Inlet label={CONDUIT_LABEL}>
      <Backdrop onClick={onBackdropClick}>
        <ModalBody onClick={e => e.stopPropagation()}>
          {children}
        </ModalBody>
      </Backdrop>
    </Inlet>
  )
}

Modal.propTypes = {
  /**
   * Handler of click events fired by the Backdrop.
   */
  onBackdropClick: PropTypes.func,
  /**
   * Content of the modal
   */
  children: PropTypes.node.isRequired,
}

export default Modal
