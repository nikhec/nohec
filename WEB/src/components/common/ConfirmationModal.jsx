import React from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import { COLORS } from 'src/constants'
import { ConfirmationModalBody, ConfirmationModalIcon } from 'src/styles/blocks/page'

import Modal from './Modal'
import PrimaryButton from './PrimaryButton'

/**
 * Displays confirmation dialog, alternative to window's confirm
 *
 * @component
 */
const ConfirmationModal = ({ confirmationMessage, severityColor, onConfirm, onCancel }) => (
  <Modal>
    <ConfirmationModalBody>
      <ConfirmationModalIcon iconColor={severityColor}>!</ConfirmationModalIcon>
      <h1>{confirmationMessage}</h1>

      <PrimaryButton higher onClick={onConfirm} backgroundColor={severityColor}>
        <FormattedMessage id="COMMON.CONFIRM" />
      </PrimaryButton>

      <PrimaryButton higher onClick={onCancel} textColor={COLORS.FONT} backgroundColor={COLORS.GRAY_DARK}>
        <FormattedMessage id="COMMON.CANCEL" />
      </PrimaryButton>
    </ConfirmationModalBody>
  </Modal>
)

ConfirmationModal.propTypes = {
  /**
   * Localized confirmation question
   */
  confirmationMessage: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
  /**
   * Color used to indicate message severity
   */
  severityColor: PropTypes.string.isRequired,
  /**
   * Handles click event of the confirm button.
   */
  onConfirm: PropTypes.func.isRequired,
  /**
   * Handles click event of the cancel button.
   */
  onCancel: PropTypes.func.isRequired,
}

export default ConfirmationModal
