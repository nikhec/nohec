import React, { useCallback, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import {
  InputRow, TextInputField, InputWrapper, TextInputBox, TextInputPlaceholder,
} from 'src/styles/blocks/form'

/**
 * Renders component for an input field with the email type.
 *
 * @component
 */
const EmailInput = ({ name, placeholderId, reference, hasError, isFilled, onUpdate }) => {
  const [state, setState] = useState({
    inputTextLength: 0,
    inputActive: false,
  })

  const handleInputChange = useCallback(({ target }) => {
    setState(prevState => ({
      ...prevState,
      inputTextLength: target.value.length,
    }))

    onUpdate(name, target.value)
  }, [name, onUpdate])

  const { inputActive } = state

  return (
    <InputRow>
      <InputWrapper active={inputActive} hasError={hasError}>
        <TextInputBox htmlFor={name}>
          {placeholderId && (
            <TextInputPlaceholder isFilled={isFilled}>
              <FormattedMessage id={placeholderId} />
            </TextInputPlaceholder>
          )}

          <TextInputField
            name={name}
            type="email"
            ref={reference}
            onChange={handleInputChange}
            onFocus={() => setState(prevState => ({ ...prevState, inputActive: true }))}
            onBlur={() => setState(prevState => ({ ...prevState, inputActive: false }))}
            isFilled={isFilled}
            noValidate
          />
        </TextInputBox>
      </InputWrapper>
    </InputRow>
  )
}

EmailInput.propTypes = {
  /**
   * Input name
   */
  name: PropTypes.string.isRequired,
  /**
   * Id of the localized message to be displayed in the input when it is empty
   */
  placeholderId: PropTypes.string,
  /**
   * useRef object, element requests focus upon rendering if provided
   */
  reference: PropTypes.object,
  /**
   * Indicates whether to shrink field's placeholder
   */
  isFilled: PropTypes.bool,
  /**
   * Indicates that input has an invalid value
   */
  hasError: PropTypes.bool,
  /**
   * Callback function that is called on each input update with input's name and current value as parameters
   */
  onUpdate: PropTypes.func,
}

export default EmailInput
