import React from 'react'

import BallIcon from '~/assets/icons/icon-ball.png'

import { BannerBox, BannerImage } from 'src/styles/blocks/page'

/**
 * Component with animated icon displayed during data fetching from server.
 *
 * @component
 */
const LoadingBanner = () => (
  <BannerBox>
    <BannerImage alt="loading icon" src={BallIcon} />
  </BannerBox>
)

export default LoadingBanner
