import React, { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import csFlag from '~/assets/flags/cs.png'
import enFlag from '~/assets/flags/en.png'

import { LOCALES } from 'src/constants'
import { LocaleFlag, LocaleFlagBox } from 'src/styles/blocks/footer'
import { CustomizationActions } from 'src/actions'

const flags = [[LOCALES.CS, csFlag], [LOCALES.EN, enFlag]]

/**
 * Displays locale selector element.
 *
 * @component
 */
const LocaleSelector = () => {
  const [locale, localeChanging] = useSelector(({ customization }) => [customization.locale, customization.localeChanging])

  const dispatch = useDispatch()
  const changeLocale = useCallback(flagIndex => {
    const [flagLocale] = flags[flagIndex]
    dispatch(CustomizationActions.changeLocale(flagLocale))
  }, [dispatch])

  const selectedIndex = flags.findIndex(([flagLocale]) => flagLocale === locale)

  return (
    <LocaleFlagBox>
      {flags.map(([flagLocale, flagIcon], index) => {
        const selected = index === selectedIndex
        const prevSelected = localeChanging && index === (selectedIndex + flags.length - 1) % flags.length
        const nextToSelect = (selectedIndex + 1) % flags.length

        return (selected || prevSelected) && (
          <LocaleFlag
            key={flagLocale}
            alt="locale-flag"
            src={flagIcon}
            onClick={() => changeLocale(nextToSelect)}
            selected={selected}
            localeChanging={localeChanging}
          />
        )
      })}
    </LocaleFlagBox>
  )
}

export default LocaleSelector
