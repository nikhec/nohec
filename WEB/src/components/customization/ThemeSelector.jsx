import React, { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { CustomizationActions } from 'src/actions'
import { COLORS } from 'src/constants'
import { IconButton, ThemeIcon, ThemeIconLines } from 'src/styles/blocks/footer'

const properties = {
  light: {
    transformCenter: 'scale(1)',
    transformContainer: 'rotate(40deg)',
    transformMask: 'scale(0.1)',
    opacity: 1,
  },
  dark: {
    transformCenter: 'scale(1.7) translate(-4.9px, -5px)',
    transformContainer: 'rotate(90deg)',
    transformMask: 'scale(1)',
    opacity: 0,
  },
}

/**
 * Displays theme selector element.
 *
 * @component
 */
const ThemeSelector = () => {
  const theme = useSelector(({ customization: { theme } }) => theme)
  const dispatch = useDispatch()
  const changeTheme = useCallback(newTheme => dispatch(CustomizationActions.changeTheme(newTheme)), [dispatch])

  const { transformCenter, transformContainer, transformMask, opacity } = properties[theme]
  const svgContainerProps = { transform: transformContainer }
  const centerCircleProps = { transform: transformCenter }
  const maskedCircleProps = { transform: transformMask }
  const linesProps = { opacity }

  return (
    <IconButton onClick={() => changeTheme(theme === 'dark' ? 'light' : 'dark')}>
      <ThemeIcon
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        style={svgContainerProps}
      >
        <circle fill={COLORS.FONT} cx="12" cy="12" r="5" style={centerCircleProps} />
        <circle fill={COLORS.FOREGROUND} cx="6" cy="8" r="9" stroke="none" style={maskedCircleProps} />
        <ThemeIconLines stroke={COLORS.FONT} lineProps={linesProps}>
          <line x1="12" y1="21" x2="12" y2="23" />
          <line x1="4.22" y1="19.78" x2="5.64" y2="18.36" />
          <line x1="1" y1="12" x2="3" y2="12" />
          <line x1="4.22" y1="4.22" x2="5.64" y2="5.64" />
          <line x1="12" y1="1" x2="12" y2="3" />
          <line x1="18.36" y1="5.64" x2="19.78" y2="4.22" />
          <line x1="21" y1="12" x2="23" y2="12" />
          <line x1="18.36" y1="18.36" x2="19.78" y2="19.78" />
        </ThemeIconLines>
      </ThemeIcon>
    </IconButton>
  )
}

export default ThemeSelector
