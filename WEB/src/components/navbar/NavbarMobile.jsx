import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import SvgMoreVert from '~/assets/svg/more-vert.svg'
import NohecIcon from '~/assets/icons/icon-192.png'
import NohecIconDark from '~/assets/icons/icon-192-dark.png'

import { API } from 'src/constants'
import {
  NavbarHamburgerMenu, NavbarLogoBox, NavbarMenuSlider, NavbarMobileAnchor, NavbarPopUpBox,
  NavbarMobileToggle, NavbarRightTogglesBox, NavbarTitle, NavbarTitleBox, NavbarTitleWrapper,
} from 'src/styles/blocks/navbar'

import { Link } from '../common'

/**
 * Content of the navigation bar displayed on the MOBILE device.
 *
 * @component
 */
const NavbarMobile = ({ onLogIn, onLogOut, isScrolled, isUserLogged, isThemeDark }) => {
  const router = useSelector(({ router }) => router)
  const history = useHistory()

  const location = router.location.pathname.substring(1)

  const [expanded, setExpanded] = useState(false)
  const [clicking, setClicking] = useState(false)
  const [selectedToggleRect, setToggleRect] = useState(null)

  const toggleBoxRef = useRef()
  const selectedToggleRef = useRef(null)

  const handleNavigation = (element, newLocation) => {
    history.push(newLocation)
    if (element) {
      const { top, height, width } = element.getBoundingClientRect()
      setToggleRect({
        top: top - height / 2,
        right: width,
      })
    } else {
      setToggleRect(null)
    }
  }

  const handleExpanding = useCallback(() => {
    setExpanded(prevState => !prevState)
    toggleBoxRef.current.focus()
    // not expanded here means we just expanded the navbar, padding-top is going to animate to 15 but is 0 at the moment
    if (!expanded && selectedToggleRef.current) {
      const { top, height, width } = selectedToggleRef.current.getBoundingClientRect()
      setToggleRect({
        top: top + 15 - height / 2,
        right: width,
      })
    }
  }, [expanded])

  useEffect(() => {
    if (selectedToggleRef.current) {
      const { top, height, width } = selectedToggleRef.current.getBoundingClientRect()
      setToggleRect({
        top: top - height / 2,
        right: width,
      })
    } else {
      setToggleRect(null)
    }
  }, [isUserLogged])

  useEffect(() => {
    if (selectedToggleRef.current) {
      const { top, height, width } = selectedToggleRef.current.getBoundingClientRect()
      // navbar height is yet to be animated, we need to set top manually to the value navbar will reach after animation
      setToggleRect({
        top: top + (isScrolled ? -12 : 12) - height / 2,
        right: width,
      })
    }
  }, [isScrolled])

  const mapToMobileToggle = (([messageId, newLocation]) => (
    <Link route={newLocation} key={messageId}>
      <NavbarMobileToggle
        ref={location === newLocation ? selectedToggleRef : null}
        onClick={e => handleNavigation(e.target, newLocation)}
      >
        <FormattedMessage id={messageId} />
      </NavbarMobileToggle>
    </Link>
  ))

  return (
    <>
      <NavbarTitleWrapper>
        <NavbarTitleBox onClick={() => handleNavigation(null, '/')}>
          <NavbarLogoBox>
            <img src={isThemeDark ? NohecIconDark : NohecIcon} alt="logo" />
          </NavbarLogoBox>
          <NavbarTitle>Nohec</NavbarTitle>
        </NavbarTitleBox>
      </NavbarTitleWrapper>

      <NavbarRightTogglesBox>
        <NavbarHamburgerMenu
          onClick={handleExpanding}
          onMouseDown={() => setClicking(true)}
          onMouseUp={() => setClicking(false)}
          expanded={expanded}
        >
          <SvgMoreVert />
        </NavbarHamburgerMenu>

        <NavbarPopUpBox
          expanded={expanded}
          ref={toggleBoxRef}
          tabIndex={-1}
          onBlur={() => !clicking && setExpanded(false)}
        >
          <NavbarMobileAnchor>
            {[['NAVBAR.RESULTS', API.RESULTS], ['NAVBAR.TEAMS', API.TEAMS], ['NAVBAR.CALENDAR', API.CALENDAR]].map(mapToMobileToggle)}

            {isUserLogged && [['NAVBAR.MY_CALENDAR', API.MY_CALENDAR], ['NAVBAR.MY_ACCOUNT', API.MY_ACCOUNT]].map(mapToMobileToggle)}
            {
              isUserLogged
                ? (
                  <NavbarMobileToggle accent onClick={onLogOut}>
                    <FormattedMessage id="NAVBAR.LOG_OUT" />
                  </NavbarMobileToggle>
                ) : (
                  <NavbarMobileToggle accent onClick={onLogIn}>
                    <FormattedMessage id="NAVBAR.LOG_IN" />
                  </NavbarMobileToggle>
                )
            }
            {selectedToggleRect && <NavbarMenuSlider {...selectedToggleRect} expanded={expanded} scrolled={isScrolled} />}
          </NavbarMobileAnchor>
        </NavbarPopUpBox>
      </NavbarRightTogglesBox>
    </>
  )
}

NavbarMobile.propTypes = {
  /**
   * Callback for handling click on sign in button on navigation bar
   */
  onLogIn: PropTypes.func.isRequired,
  /**
   * Callback for handling click on sign out button on navigation bar
   */
  onLogOut: PropTypes.func.isRequired,
  /**
   * Indicates whether is page content scrolled
   */
  isScrolled: PropTypes.bool,
  /**
   * Indicates whether is user logged in
   */
  isUserLogged: PropTypes.bool,
  /**
   * Indicates whether is user using the dark theme or not
   */
  isThemeDark: PropTypes.bool,
}

export default NavbarMobile
