import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import SvgLogOut from '~/assets/svg/log-out.svg'
import NohecIcon from '~/assets/icons/icon-192.png'
import NohecIconDark from '~/assets/icons/icon-192-dark.png'

import { API } from 'src/constants'
import { useSliderLogic } from 'src/hooks/use-slider-logic'
import { useIsTablet } from 'src/hooks/use-device'
import {
  NavbarLogoBox, NavbarMiddleTogglesBox, NavbarRightTogglesBox,
  NavbarSlider, NavbarTitle, NavbarTitleBox, NavbarTitleWrapper, NavbarToggle,
} from 'src/styles/blocks/navbar'
import { Link } from 'src/components/common'

/**
 * Content of the navigation bar displayed on the DESKTOP/TABLET device.
 *
 * @component
 */
const NavbarDesktop = ({ onLogIn, onLogOut, isScrolled, isUserLogged, isThemeDark }) => {
  const router = useSelector(({ router }) => router)
  const location = router.location.pathname.substring(1)

  const history = useHistory()
  const isTablet = useIsTablet()

  const {
    selectedToggleRef,
    selectedToggleRect,
    setToggleRect,
  } = useSliderLogic([isUserLogged, location])

  // Tablet view changes margin, which is animated so we need to correct slider after animation is done
  useEffect(() => {
    const timeout = setTimeout(() => {
      if (selectedToggleRef.current) {
        const { left, top, width, height } = selectedToggleRef.current.getBoundingClientRect()
        setToggleRect({ left, width, top: top + height })
      }
    }, 400)

    return () => clearTimeout(timeout)
  }, [isTablet])

  useEffect(() => {
    if (selectedToggleRef.current && isScrolled != null) {
      const { top, height } = selectedToggleRef.current.getBoundingClientRect()
      // We hide slider and later correct it and make it reappear after navbar is animated
      // The top coordinate correction is here to fix visual bug sometimes visible tick before slider's disappearance
      setToggleRect(prevState => ({
        ...prevState,
        top: top + height,
        opacity: 0,
        width: 0,
      }))

      // Timeout to correct slider's position after navbar is animated
      const timeout = setTimeout(() => {
        if (selectedToggleRef.current) {
          const { left, top, width, height } = selectedToggleRef.current.getBoundingClientRect()
          setToggleRect({ left, width, top: top + height, opacity: 1 })
        }
      }, 400)

      return () => clearTimeout(timeout)
    }
    return undefined
  }, [isScrolled])

  const handleNavigation = (element, newLocation) => {
    history.push(newLocation)
    if (element) {
      const { left, top, width, height } = element.getBoundingClientRect()
      setToggleRect({ left, width, top: top + height })
    } else {
      setToggleRect(null)
    }
  }

  const mapToToggle = ([messageId, newLocation]) => (
    <Link route={newLocation} key={messageId}>
      <NavbarToggle
        ref={location === newLocation ? selectedToggleRef : null}
        onClick={e => handleNavigation(e.target, newLocation)}
      >
        <FormattedMessage id={messageId} />
      </NavbarToggle>
    </Link>
  )

  return (
    <>
      <NavbarTitleWrapper>
        <Link route="/">
          <NavbarTitleBox onClick={() => handleNavigation(null, '/')}>
            <NavbarLogoBox>
              <img src={isThemeDark ? NohecIconDark : NohecIcon} alt="logo" />
            </NavbarLogoBox>
            {!isTablet && <NavbarTitle>Nohec</NavbarTitle>}
          </NavbarTitleBox>
        </Link>
      </NavbarTitleWrapper>

      <NavbarMiddleTogglesBox>
        {[['NAVBAR.RESULTS', API.RESULTS], ['NAVBAR.TEAMS', API.TEAMS], ['NAVBAR.CALENDAR', API.CALENDAR]].map(mapToToggle)}
      </NavbarMiddleTogglesBox>

      <NavbarRightTogglesBox>
        {
          isUserLogged
            ? (
              <>
                {[['NAVBAR.MY_CALENDAR', API.MY_CALENDAR], ['NAVBAR.MY_ACCOUNT', API.MY_ACCOUNT]].map(mapToToggle)}
                <NavbarToggle accent onClick={onLogOut} title="log out">
                  <SvgLogOut />
                </NavbarToggle>
              </>
            ) : <NavbarToggle accent onClick={onLogIn}><FormattedMessage id="NAVBAR.LOG_IN" /></NavbarToggle>
        }
      </NavbarRightTogglesBox>

      {selectedToggleRect && <NavbarSlider {...selectedToggleRect} scrolled={isScrolled} />}
    </>
  )
}

NavbarDesktop.propTypes = {
  /**
   * Callback for handling click on sign in button on navigation bar
   */
  onLogIn: PropTypes.func.isRequired,
  /**
   * Callback for handling click on sign out button on navigation bar
   */
  onLogOut: PropTypes.func.isRequired,
  /**
   * Indicates whether is page content scrolled
   */
  isScrolled: PropTypes.bool,
  /**
   * Indicates whether is user logged in
   */
  isUserLogged: PropTypes.bool,
  /**
   * Indicates whether is user using the dark theme or not
   */
  isThemeDark: PropTypes.bool,
}

export default NavbarDesktop
