import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { AuthActions } from 'src/actions'
import { useIsMobile } from 'src/hooks/use-device'
import { useIsScrolled } from 'src/hooks/use-scroll-offsets'
import { PageNavbar } from 'src/styles/blocks/navbar'

import NavbarDesktop from './NavbarDesktop'
import NavbarMobile from './NavbarMobile'
import SignInPopUp from './SingInPupUp'

/**
 * Page navigation bar component.
 * Toggles between MOBILE and DESKTOP/TABLET view components.
 *
 * @component
 */
const Navbar = () => {
  const isMobile = useIsMobile()
  const isScrolled = useIsScrolled()
  const dispatch = useDispatch()

  const [signingIn, setSigningIn] = useState(false)

  const isUserLogged = useSelector(({ app: { user } }) => Boolean(user))
  const isThemeDark = useSelector(({ customization: { theme } }) => theme === 'dark')

  useEffect(() => {
    if (isUserLogged) {
      setSigningIn(false)
    }
  }, [isUserLogged])

  const emailFieldRef = useRef()

  const handleLogIn = () => {
    setSigningIn(true)
    // Focusing directly will disturb animation and force the focused input to jump to view
    setTimeout(() => emailFieldRef.current.focus(), [400])
  }

  const handleLogOut = useCallback(() => dispatch(AuthActions.logOut()), [dispatch])

  return (
    <PageNavbar scrolled={isScrolled}>
      {
        isMobile
          ? <NavbarMobile
              onLogIn={handleLogIn}
              onLogOut={handleLogOut}
              isUserLogged={isUserLogged}
              isScrolled={isScrolled}
              isThemeDark={isThemeDark}
          />
          : <NavbarDesktop
              onLogIn={handleLogIn}
              onLogOut={handleLogOut}
              isUserLogged={isUserLogged}
              isScrolled={isScrolled}
              isThemeDark={isThemeDark}
          />
      }

      <SignInPopUp signingIn={signingIn} setSigningIn={setSigningIn} emailReference={emailFieldRef} />
    </PageNavbar>
  )
}

export default Navbar
