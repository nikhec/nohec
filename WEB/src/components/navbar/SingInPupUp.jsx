import React, { useCallback, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { FormattedMessage } from 'react-intl'

import { NavbarPopUpBox, SignInFormBox, SignInFormError } from 'src/styles/blocks/navbar'
import { AuthActions } from 'src/actions'

import { PassInput, PrimaryButton, EmailInput } from '../common'

const EMAIL_VALIDATION_REGEX = /^[^\s@]+@[^\s@]+\.[^\s@]+$/

/**
 * Component displaying sign in form under the navigation bar.
 *
 * @component
 */
const SignInPopUp = ({ signingIn, setSigningIn, emailReference }) => {
  const dispatch = useDispatch()

  const [formState, setFormState] = useState({
    inputs: {
      email: null,
      password: null,
    },
    errors: [],
  })

  const [, errorMessageId] = formState.errors[0] ?? [null, null]
  // We consider sign in form valid when there are no error entities associated with any input name
  // Error messages without associated input are considered server-side and should not require any change before resubmitting
  const isSignInValid = !formState.errors.find(([name]) => name)
    && formState.inputs.email
    && formState.inputs.password

  const handleInputUpdate = (name, newValue) => {
    if (name !== 'email' && name !== 'password') {
      console.error('Unknown input name')
      return
    }

    const valid = name !== 'email' || EMAIL_VALIDATION_REGEX.test(String(newValue).toLowerCase())
    const messageId = valid ? null : 'FORM.EMAIL_INVALID'

    setFormState(prevState => {
      const newErrors = prevState.errors.filter(([inputName]) => inputName && inputName !== name)
      if (!valid) {
        newErrors.push([name, messageId])
      }

      return {
        ...prevState,
        inputs: {
          ...prevState.inputs,
          [name]: newValue,
        },
        errors: newErrors,
      }
    })
  }

  const handleSubmitError = useCallback(message => {
    setFormState(prevState => ({
      ...prevState,
      errors: [...prevState.errors, [null, message]],
    }))
  }, [setFormState])

  const handleFormSubmit = useCallback(() => {
    popUpRef.current.focus()
    dispatch(AuthActions.logIn({
      inputs: formState.inputs,
      onError: handleSubmitError,
    }))
  }, [formState, setFormState])

  const popUpRef = useRef()

  const handleBlur = e => {
    if (!e.relatedTarget) {
      setSigningIn(false)
      setFormState({
        inputs: {
          email: null,
          password: null,
        },
        errors: [],
      })
      // Clears all inputs
      Array.from(popUpRef.current.querySelectorAll('input')).forEach(input => input.value = '')
    }
  }

  return (
    <NavbarPopUpBox
      expanded={signingIn}
      tabIndex={-1}
      ref={popUpRef}
      onBlur={handleBlur}
    >
      <SignInFormBox>
        <EmailInput
          name="email"
          placeholderId="FORM.EMAIL"
          isFilled={formState.inputs.email?.length > 0}
          hasError={!!formState.errors.find(([name]) => name === 'email')}
          reference={emailReference}
          onUpdate={handleInputUpdate}
        />
        <PassInput
          name="password"
          placeholderId="FORM.PASSWORD"
          isFilled={formState.inputs.password?.length > 0}
          hasError={!!formState.errors.find(([name]) => name === 'password')}
          onUpdate={handleInputUpdate}
        />
        <PrimaryButton hasSubmitTimeout disabled={!isSignInValid} onClick={handleFormSubmit}>
          <FormattedMessage id="FORM.LOG_IN" />
        </PrimaryButton>

        {errorMessageId && (
          <SignInFormError>
            <FormattedMessage id={errorMessageId} />
          </SignInFormError>
        )}
      </SignInFormBox>
    </NavbarPopUpBox>
  )
}

SignInPopUp.propTypes = {
  /**
   * Indicates whether is user awaiting log in response from the server
   */
  signingIn: PropTypes.bool.isRequired,
  /**
   * Setter for the indicator
   */
  setSigningIn: PropTypes.func.isRequired,
  /**
   * Reference object for the email field of the popUp
   */
  emailReference: PropTypes.object,
}

export default SignInPopUp
