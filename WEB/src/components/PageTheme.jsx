import React from 'react'
import { PropTypes } from 'prop-types'
import { useSelector } from 'react-redux'

import { ThemeWrapper } from 'src/styles/blocks/page'

/**
 * Provides theming wrapper component.
 *
 * @name PageTheme
 * @component
 */
const PageTheme = ({ children }) => {
  const [theme, themeChanging] = useSelector(({ customization }) => [customization.theme, customization.themeChanging])

  return (
    <ThemeWrapper className={theme} themeChanging={themeChanging}>
      {children}
    </ThemeWrapper>
  )
}

PageTheme.propTypes = {
  /**
   * Content rendered as child components
   */
  children: PropTypes.node,
}

export default PageTheme
