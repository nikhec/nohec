import React from 'react'
import { useSelector } from 'react-redux'
import { Route, Redirect, Switch } from 'react-router-dom'

import { API } from 'src/constants'

import {
  HomePage, ResultsPage, CalendarPage, TeamsPage, TeamDetailPage, MatchDetailPage, MatchRecordPage, AccountPage,
} from './pages'
import { LeaguePanelPage } from './common'

/**
 * Tries to parse id out of search query of the location string and supply it as property to given component.
 * Returns redirection to Home page in case no id is found.
 *
 * @memberof Router
 * @inner
 * @function
 * @param {JSX.Element} Component
 * @param {Object} props
 * @returns {function({ location: { search: string } }): JSX.Element}
 */
const getRouteIdProvider = (Component, props = {}) => ({ location: { search } }) => {
  const id = search.match(/^(.*id=(\d+))$/)?.[2]

  return id
    ? <Component {...props} id={(Number(id))} />
    : <Redirect to={{ pathname: '/' }} />
}

/**
 * Wrapper component around the part of router enabling route for logged in user.
 * This way, we ensure that the other part of router won't be re-rendered after initial logged-in test is performed.
 *
 * @memberof Router
 * @inner
 */
const LoggedUserRouter = () => {
  const isUserLogged = useSelector(({ app: { user } }) => Boolean(user))

  return isUserLogged
    ? (
      <Switch>
        <Route path={`/${API.MY_ACCOUNT}`} component={AccountPage} />
        <Route path={`/${API.MY_CALENDAR}`} render={props => <CalendarPage myCalendar {...props} />} />

        <Route path={`/${API.MATCH_RECORD}`} component={getRouteIdProvider(MatchRecordPage)} />

        <Route path="/*" component={() => <Redirect to={{ pathname: '/' }} />} />
      </Switch>
    ) : <Route path="/*" component={() => <Redirect to={{ pathname: '/' }} />} />
}

/**
 * Provides Web Application with FE routing in terms of conditional components rendering.
 *
 * @name Router
 * @component
 */
const Router = () => (
  <Switch>
    <Route exact path="/" render={() => <LeaguePanelPage ChildComponent={HomePage} />} />
    <Route path={`/${API.RESULTS}`} render={() => <LeaguePanelPage ChildComponent={ResultsPage} />} />
    <Route path={`/${API.TEAMS}`} render={() => <LeaguePanelPage ChildComponent={TeamsPage} />} />
    <Route path={`/${API.CALENDAR}`} component={CalendarPage} />

    <Route path={`/${API.TEAM_DETAIL}`} component={getRouteIdProvider(TeamDetailPage)} />
    <Route path={`/${API.MATCH}`} component={getRouteIdProvider(MatchDetailPage)} />

    <LoggedUserRouter />
  </Switch>
)

export default Router
