import React, { useMemo } from 'react'
import { createIntl, createIntlCache, RawIntlProvider } from 'react-intl'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types'

import csCZ from '~/languages/cs-CZ'
import enUS from '~/languages/en-US'

import { RootActions } from 'src/actions'
import { LOCALES } from 'src/constants'

const messagesMap = {
  [LOCALES.CS]: csCZ,
  [LOCALES.EN]: enUS,
}

const cache = createIntlCache()

/**
 * Provides context for internationalization with loaded messages for currently active locale.
 *
 * @name I18nProvider
 * @component
 */
const I18nProvider = ({ children }) => {
  const locale = useSelector(({ customization }) => customization.locale)
  const messages = useMemo(() => messagesMap[locale] ?? csCZ, [locale])

  const intl = createIntl({
    locale,
    messages,
  }, cache)

  const dispatch = useDispatch()
  dispatch(RootActions.loadInternationalization(intl))

  return (
    <RawIntlProvider value={intl}>
      {children}
    </RawIntlProvider>
  )
}

I18nProvider.propTypes = {
  /**
   * Content rendered as child components
   */
  children: PropTypes.node,
}

export default I18nProvider
