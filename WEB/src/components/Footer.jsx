import React from 'react'

import { PageFooter, FooterWrapper, FooterRow } from 'src/styles/blocks/footer'

import { ThemeSelector, LocaleSelector } from './customization'

/**
 * Page footer.
 *
 * @name Footer
 * @component
 */
export default () => (
  <PageFooter>
    <FooterWrapper>
      <FooterRow>
        <ThemeSelector />
        <LocaleSelector />
      </FooterRow>
      <FooterRow>
        © 2021 Nohec Team
      </FooterRow>
    </FooterWrapper>
  </PageFooter>
)
