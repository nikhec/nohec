import React from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import { API } from 'src/constants'
import { MatchPropType } from 'src/utils/prop-types'
import { MatchesTableNode, MatchesTableTeam, MatchesTableTeamLogo, AccountTableRow } from 'src/styles/blocks/matches'

import { Link } from '../common'

/**
 * Single row within matches list table on logged user's dashboard page.
 *
 * @component
 */
const AccountMatchesTableRow = ({ match, hasDetail, isMobile, onDetailClick }) => (
  <AccountTableRow hasDetail={hasDetail}>
    <MatchesTableNode fixedSize={80}>
      {match.date}
    </MatchesTableNode>
    <MatchesTableNode fixedSize={50}>
      {match.time}
    </MatchesTableNode>
    <MatchesTableNode fixedSize={300}>
      <MatchesTableTeam fixedWidth>
        <MatchesTableTeamLogo logo={match.homeTeam.logo} title={match.homeTeam.name} />
        {!isMobile && match.homeTeam.name}
      </MatchesTableTeam>
    </MatchesTableNode>
    <MatchesTableNode accent fixedSize={180}>
      {match.isFinished ? match.score.join(':') : '/'}
    </MatchesTableNode>
    <MatchesTableNode fixedSize={300}>
      <MatchesTableTeam fixedWidth>
        <MatchesTableTeamLogo logo={match.awayTeam.logo} title={match.awayTeam.name} />
        {!isMobile && match.awayTeam.name}
      </MatchesTableTeam>
    </MatchesTableNode>
    <MatchesTableNode
      accent
      fixedSize={isMobile ? 30 : 80}
      isClickable={hasDetail}
      onClick={hasDetail ? () => onDetailClick(match.id) : null}
    >
      <Link route={`${API.MATCH}?id=${match.id}`}>
        {hasDetail && (isMobile
          ? '...'
          : <FormattedMessage id="ACCOUNT.SHOW_MATCH" />
        )}
      </Link>
    </MatchesTableNode>
  </AccountTableRow>
)

AccountMatchesTableRow.propTypes = {
  /**
   * Match associated with given row.
   */
  match: MatchPropType.isRequired,
  /**
   * Indicates whether the match should display link to match page.
   */
  hasDetail: PropTypes.bool,
  /**
   * Indicates whether the view is in mobile mode.
   */
  isMobile: PropTypes.bool,
  /**
   * CLick handler for the match detail page link.
   */
  onDetailClick: PropTypes.func,
}

export default AccountMatchesTableRow
