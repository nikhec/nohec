import React from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import { MatchTeamPropType } from 'src/utils/prop-types'
import {
  MatchHeaderBox, MatchHeaderHeadline, MatchHeaderLogo, MatchHeaderLogoBox, MatchHeaderLogoHint, MatchHeaderTeamName,
} from 'src/styles/blocks/matches'

/**
 * Component displaying match detail headline, with two logos and some content between them.
 *
 * @component
 */
const MatchHeader = ({
  homeTeam,
  homeTeamCol,
  awayTeam,
  awayTeamCol,
  headline,
  subContent,
  imageSize,
  isSingleLine,
  topAlignment,
}) => (
  <MatchHeaderBox isSingleLine={isSingleLine}>
    <MatchHeaderLogoBox isSingleLine={isSingleLine} topAlignment={topAlignment}>
      {!isSingleLine && <MatchHeaderLogoHint><FormattedMessage id="MATCH.HOME_TEAM" /></MatchHeaderLogoHint>}
      <MatchHeaderLogo logo={homeTeam.logo} isSingleLine={isSingleLine} imageSize={imageSize} />
      <MatchHeaderTeamName>{homeTeam.name}</MatchHeaderTeamName>
      {homeTeamCol}
    </MatchHeaderLogoBox>

    <MatchHeaderHeadline isSingleLine={isSingleLine}>
      {headline}
      {!isSingleLine && subContent}
    </MatchHeaderHeadline>

    <MatchHeaderLogoBox isSingleLine={isSingleLine} topAlignment={topAlignment}>
      {!isSingleLine && <MatchHeaderLogoHint isRight><FormattedMessage id="MATCH.AWAY_TEAM" /></MatchHeaderLogoHint>}
      <MatchHeaderLogo logo={awayTeam.logo} isSingleLine={isSingleLine} imageSize={imageSize} />
      <MatchHeaderTeamName>{awayTeam.name}</MatchHeaderTeamName>
      {awayTeamCol}
    </MatchHeaderLogoBox>
  </MatchHeaderBox>
)

MatchHeader.defaultProps = {
  imageSize: 220,
}

MatchHeader.propTypes = {
  /**
   * Home team is always displayed in the left.
   */
  homeTeam: MatchTeamPropType,
  /**
   * Away team is always displayed in the right.
   */
  awayTeam: MatchTeamPropType,
  /**
   * Content to be displayed under the homeTeam logo, in the same column.
   */
  homeTeamCol: PropTypes.node,
  /**
   * Content to be displayed under the awayTeam logo, in the same column.
   */
  awayTeamCol: PropTypes.node,
  /**
   * This is the main content displayed between two logos. Vertically centered.
   */
  headline: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  /**
   * This is content displayed under the headline.
   */
  subContent: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  /**
   * Size of displayed logo image of team.
   */
  imageSize: PropTypes.number,
  /**
   * Indicates whether to display team logos inline with their names.
   */
  isSingleLine: PropTypes.bool,
  /**
   * Indicates whether to align logo columns to the top of the container box or to the bottom. Defaults to the bottom.
   */
  topAlignment: PropTypes.bool,
}

export default MatchHeader
