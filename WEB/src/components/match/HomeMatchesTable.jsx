import React from 'react'
import { FormattedMessage } from 'react-intl'
import { useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'

import { API } from 'src/constants'
import { useIsMobile } from 'src/hooks/use-device'
import { MatchPropType } from 'src/utils/prop-types'
import { ContentGroup, ContentGroupHeadline, ContentTableBox, ShowMoreBox } from 'src/styles/blocks/page'
import { MatchesTable } from 'src/styles/blocks/matches'

import { Link } from '../common'
import MatchResultTableRow from './MatchResultTableRow'

/**
 * Home page's single matches table component.
 *
 * @component
 */
const HomeMatchesTable = ({ headlineMessageId, emptyMessageId, matches, resultsRoute }) => {
  const history = useHistory()
  const browseMatchDetail = matchId => history.push(`${API.MATCH}?id=${matchId}`)

  const isMobile = useIsMobile()

  return (
    <>
      <ContentGroupHeadline isUpperCase>
        <FormattedMessage id={headlineMessageId} />
      </ContentGroupHeadline>

      <ContentGroup>
        {matches.length > 0
          ? (
            <ContentTableBox>
              <ShowMoreBox onClick={() => history.push(resultsRoute)}>
                <Link route={resultsRoute}>
                  <FormattedMessage id="COMMON.SHOW_MORE" />
                </Link>
              </ShowMoreBox>

              <MatchesTable>
                <tbody>
                  {matches.map((match, index) => (
                    <MatchResultTableRow
                      key={`row-${index}`}
                      match={match}
                      rowIndex={index}
                      isMobile={isMobile}
                      onDetailClick={browseMatchDetail}
                    />
                  ))}
                </tbody>
              </MatchesTable>
            </ContentTableBox>
          ) : <FormattedMessage id={emptyMessageId} />}
      </ContentGroup>
    </>
  )
}

HomeMatchesTable.propTypes = {
  /**
   * Message Id of the headline of the table
   */
  headlineMessageId: PropTypes.string.isRequired,
  /**
   * Id of the message to show on empty matches list
   */
  emptyMessageId: PropTypes.string.isRequired,
  /**
   * Matches to be displayed in the table
   */
  matches: PropTypes.arrayOf(MatchPropType).isRequired,
  /**
   * Route to redirect to upon clicking on show more button
   */
  resultsRoute: PropTypes.string.isRequired,
}

export default HomeMatchesTable
