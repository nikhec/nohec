import React from 'react'
import PropTypes from 'prop-types'

import { API } from 'src/constants'
import { MatchPropType } from 'src/utils/prop-types'
import { MatchesTableNode, MatchesTableRow, MatchesTableTeam, MatchesTableTeamLogo } from 'src/styles/blocks/matches'

import { Link } from '../common'

/**
 * Single row within match results table on guest user's pages.
 *
 * @component
 */
const MatchResultTableRow = ({ match, rowIndex, isMobile, onDetailClick }) => (
  <MatchesTableRow spansLink={!isMobile} rowIndex={rowIndex} onClick={() => !isMobile && onDetailClick(match.id)}>
    <MatchesTableNode fixedSize={80} spansLink>
      {isMobile
        ? match.date
        : <Link route={`${API.MATCH}?id=${match.id}`}>{match.date}</Link>}
    </MatchesTableNode>
    <MatchesTableNode fixedSize={50}>
      {match.time}
    </MatchesTableNode>
    <MatchesTableNode fixedSize={50}>
      {match.round}.
    </MatchesTableNode>
    <MatchesTableNode fixedSize={250}>
      <MatchesTableTeam fixedWidth>
        <MatchesTableTeamLogo logo={match.homeTeam.logo} title={match.homeTeam.name} />
        {!isMobile && match.homeTeam.name}
      </MatchesTableTeam>
    </MatchesTableNode>
    <MatchesTableNode accent fixedSize={170}>
      {match.isFinished ? match.score.join(':') : '/'}
    </MatchesTableNode>
    <MatchesTableNode fixedSize={250}>
      <MatchesTableTeam fixedWidth>
        <MatchesTableTeamLogo logo={match.awayTeam.logo} title={match.awayTeam.name} />
        {!isMobile && match.awayTeam.name}
      </MatchesTableTeam>
    </MatchesTableNode>
    {isMobile
      ? (
        <MatchesTableNode
          accent
          fixedSize={30}
          isClickable
          onClick={() => onDetailClick(match.id)}
        >
          <Link route={`${API.MATCH}?id=${match.id}`}>...</Link>
        </MatchesTableNode>
      ) : null}
  </MatchesTableRow>
)

MatchResultTableRow.propTypes = {
  /**
   * Match associated with given row.
   */
  match: MatchPropType.isRequired,
  /**
   * Index of the row within the table.
   */
  rowIndex: PropTypes.number.isRequired,
  /**
   * Indicates whether the view is in mobile mode.
   */
  isMobile: PropTypes.bool,
  /**
   * CLick handler for the match detail page link.
   */
  onDetailClick: PropTypes.func,
}

export default MatchResultTableRow
