import React from 'react'
import PropTypes from 'prop-types'

import { useIsDesktop } from 'src/hooks/use-device'
import { ColumnContentBox, ContentGroup, ContentGroupHeadline } from 'src/styles/blocks/page'
import { MatchDetailRowBox, MatchHeaderHeadline, MatchDetailRowColumn } from 'src/styles/blocks/matches'

/**
 * Component displaying a single row of columned contents within match detail.
 *
 * @component
 */
const MatchContentRow = ({ headline, leftContent, centerContent, rightContent, omitBottomMargin, forceColumn }) => {
  const isDesktop = useIsDesktop()

  return (
    <ColumnContentBox>
      {headline && <ContentGroupHeadline>{headline}</ContentGroupHeadline>}
      <ContentGroup omitBottomMargin={omitBottomMargin}>
        {!isDesktop && (
          <MatchHeaderHeadline isSingleLine forceColumn={forceColumn}>
            {centerContent}
          </MatchHeaderHeadline>
        )}
        <MatchDetailRowBox>
          <MatchDetailRowColumn>
            {leftContent}
          </MatchDetailRowColumn>

          <MatchHeaderHeadline isSingleLine>
            {isDesktop && centerContent}
          </MatchHeaderHeadline>

          <MatchDetailRowColumn>
            {rightContent}
          </MatchDetailRowColumn>
        </MatchDetailRowBox>
      </ContentGroup>
    </ColumnContentBox>
  )
}

MatchContentRow.propTypes = {
  /**
   * This is the headline displayed above the left column content.
   */
  headline: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  /**
   * This is the content for the left column.
   */
  leftContent: PropTypes.node,
  /**
   * This is the content for the center column.
   */
  centerContent: PropTypes.node,
  /**
   * This is the content for the right column.
   */
  rightContent: PropTypes.node,
  /**
   * Indicates whether the omit the bottom margin.
   */
  omitBottomMargin: PropTypes.bool,
  /**
   * Indicates whether to force column alignment of content column even on smaller devices.
   */
  forceColumn: PropTypes.bool,
}

export default MatchContentRow
