import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'
import { isEmpty } from 'lodash'

import { API } from 'src/constants'
import { TeamActions } from 'src/actions'
import { useFetchedState } from 'src/hooks/use-fetched-state'
import { selectTeamList } from 'src/selectors/teamSelectors'
import { ContentGroup } from 'src/styles/blocks/page'
import { TeamsListBox, TeamsListNodeBox, TeamsListNode, TeamsListNodeName } from 'src/styles/blocks/teams'

import { Link, ErrorBanner, LoadingBanner } from '../common'

/**
 * Teams page component.
 *
 * @component
 */
const TeamsPage = ({ leagueId, seasonId }) => {
  const {
    isFetching,
    state,
    error,
  } = useFetchedState(TeamActions.loadTeams, { seasonId }, selectTeamList, [seasonId])

  const visibleTeams = useMemo(() => {
    if (isEmpty(state)) {
      return []
    }

    const getLowestIdInGroup = obj => Object.keys(obj)
      .find((key, _, arr) => arr.every(otherKey => otherKey >= key))

    const visibleSeason = seasonId ?? getLowestIdInGroup(state)
    const visibleLeague = leagueId ?? getLowestIdInGroup(state[visibleSeason])
    return state[visibleSeason][visibleLeague]
  }, [state, leagueId])

  const history = useHistory()
  const handleTeamNodeClick = teamId => history.push(`${API.TEAM_DETAIL}?id=${teamId}`)

  return (
    <TeamsListBox>
      {!error && (visibleTeams.length > 0 || !isFetching)
        ? visibleTeams.map(team => (
          <TeamsListNodeBox
            key={`team-list-node-${team.id}`}
            onClick={() => handleTeamNodeClick(team.id)}
          >
            <Link route={`${API.TEAM_DETAIL}?id=${team.id}`}>
              <TeamsListNode teamLogo={team.logo}>
                <div>
                  <TeamsListNodeName>{team.name}</TeamsListNodeName>
                </div>
              </TeamsListNode>
            </Link>
          </TeamsListNodeBox>
        )) : error
          ? <ContentGroup omitBottomMargin><ErrorBanner messageId={error} /></ContentGroup>
          : <LoadingBanner />}
    </TeamsListBox>
  )
}

TeamsPage.propTypes = {
  /**
   * Id of the currently selected league.
   */
  leagueId: PropTypes.number,
  /**
   * Id of the currently selected season.
   */
  seasonId: PropTypes.number,
}

export default TeamsPage
