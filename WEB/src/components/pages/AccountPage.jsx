import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'

import SvgEyeVisible from '~/assets/svg/eye-visible.svg'
import SvgEyeHidden from '~/assets/svg/eye-hidden.svg'
import SvgCopy from '~/assets/svg/copy.svg'

import { AuthActions, MatchActions } from 'src/actions'
import { useIsMobile } from 'src/hooks/use-device'
import { useFetchedNullableState } from 'src/hooks/use-fetched-state'
import { API, MATCH_PLAYABLE_BEFORE } from 'src/constants'
import { selectMatches } from 'src/selectors/matchSelectors'
import { MatchesTable } from 'src/styles/blocks/matches'
import { ColumnContentBox, ContentGroup, ContentGroupHeadline } from 'src/styles/blocks/page'
import {
  AccountHeader, AccountMoreButton, SignatureBox, SignatureHeadline, SignatureIcon, SignatureKey,
} from 'src/styles/blocks/account'

import { LoadingBanner, ErrorBanner } from '../common'
import { AccountMatchesTableRow } from '../match'

const MATCHES_PER_PAGE = 10

/**
 * User account page component.
 *
 * @component
 */
const AccountPage = () => {
  const user = useSelector(({ app }) => app.user)
  const isMobile = useIsMobile()

  const [matchesState, setMatchesState] = useState({
    matches: [],
    error: null,
  })
  const [showSignature, setShowSignature] = useState(false)
  const { state: signature } = useFetchedNullableState(AuthActions.fetchSignature)
  const [page, setPage] = useState(1)
  const [isFetching, setIsFetching] = useState(true)

  const copyIcon = useRef(null)
  const timer = useRef(null)

  useEffect(() => () => clearTimeout(timer.current), [])

  const copyToClipboard = useCallback(async () => {
    try {
      await navigator.clipboard.writeText(signature)
      if (copyIcon.current) {
        copyIcon.current.style.opacity = 0.3
        copyIcon.current.style.pointerEvents = 'none'

        timer.current = setTimeout(() => {
          copyIcon.current.style.opacity = 0.7
          copyIcon.current.style.pointerEvents = 'all'
        }, 1500)
      }
    } catch (e) {
      console.error('Copying to clipboard failed')
    }
  }, [signature, copyIcon, timer])

  const date = new Date()
  const matchHasDetail = ({ dateTime, result }) => {
    const matchDate = new Date(dateTime)
    return Boolean(result || matchDate <= date || (matchDate - date < MATCH_PLAYABLE_BEFORE))
  }

  const dispatch = useDispatch()
  useEffect(() => {
    setIsFetching(true)

    const successHandler = (matches, mergeMatches) => {
      const selectedMatches = selectMatches(matches)
      setIsFetching(false)
      setMatchesState(prevState => ({
        matches: mergeMatches(prevState.matches, selectedMatches),
        error: null,
      }))
    }

    const errorhandler = error => {
      setIsFetching(false)
      setMatchesState(prevState => ({
        matches: prevState.matches,
        error,
      }))
    }

    const dateTime = new Date().toISOString()
    dispatch(MatchActions.loadMyMatches({
      filters: { dateGt: dateTime, limit: 4 },
      onSuccess: matches => successHandler(matches, (prev, current) => {
        const newWithDetails = current.slice(0).reverse().findIndex(match => !matchHasDetail(match))

        return [...current.slice(-(3 + newWithDetails)), ...prev]
      }),
      onError: errorhandler,
    }))

    dispatch(MatchActions.loadMyMatches({
      filters: { dateLt: dateTime },
      onSuccess: matches => successHandler(matches, (prev, current) => [...prev, ...current]),
      onError: errorhandler,
    }))
  }, [])

  const history = useHistory()
  const browseMatchDetail = matchId => history.push(`${API.MATCH}?id=${matchId}`)

  const { matches, error } = matchesState

  return (
    <ColumnContentBox>
      <AccountHeader>
        <FormattedMessage id="ACCOUNT.WELCOME" />, <b>{user.name ?? user.email}</b>!
      </AccountHeader>

      {signature && (
        <SignatureBox>
          <SignatureHeadline><FormattedMessage id="COMMON.SIGNATURE" />:</SignatureHeadline>
          <div>
            <SignatureKey>
              {showSignature ? signature : '●●●●'}
            </SignatureKey>
            <SignatureIcon
              title={showSignature ? 'hide' : 'show'}
              onClick={() => setShowSignature(prev => !prev)}
            >
              {showSignature ? <SvgEyeVisible /> : <SvgEyeHidden />}
            </SignatureIcon>
            <SignatureIcon title="copy" onClick={copyToClipboard} ref={copyIcon}>
              <SvgCopy />
            </SignatureIcon>
          </div>
        </SignatureBox>
      )}

      {!isFetching && !error ? (
        <>
          <ContentGroupHeadline isUpperCase>
            <FormattedMessage id="COMMON.MATCHES" />
          </ContentGroupHeadline>
          <ContentGroup omitBottomMargin>
            {matches.length === 0 ? (
              <FormattedMessage id="CALENDAR.NO_NEXT_MATCH" />
            ) : (
              <>
                <MatchesTable>
                  <tbody>
                    {matches.slice(0, 4 + page * MATCHES_PER_PAGE).map((match, index) => (
                      <AccountMatchesTableRow
                        key={`match-row-${index}`}
                        match={match}
                        hasDetail={matchHasDetail(match)}
                        isMobile={isMobile}
                        onDetailClick={browseMatchDetail}
                      />
                    ))}
                  </tbody>
                </MatchesTable>

                {matches.length > 4 + page * MATCHES_PER_PAGE && (
                  <AccountMoreButton onClick={() => setPage(prevPage => prevPage + 1)}>+</AccountMoreButton>
                )}
              </>
            )}
          </ContentGroup>
        </>
      ) : error
        ? <ContentGroup omitBottomMargin><ErrorBanner messageId={error} /></ContentGroup>
        : <LoadingBanner />}
    </ColumnContentBox>
  )
}

export default AccountPage
