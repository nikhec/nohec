import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useDispatch } from 'react-redux'
import { debounce } from 'lodash'
import PropTypes from 'prop-types'

import { MatchActions } from 'src/actions'
import { selectMatchesByDate, selectMatches, selectNextMatch } from 'src/selectors/matchSelectors'
import { ContentGroup } from 'src/styles/blocks/page'
import {
  CalendarViewBox, CalendarViewColGroup, CalendarViewColLeft, CalendarViewColRight,
} from 'src/styles/blocks/calendar'

import { ErrorBanner, LoadingBanner } from '../common'
import { CalendarTable, CalendarControls, CalendarDetail, CalendarNextMatch } from '../calendar'

/**
 * Calendar page component. Used by both Calendar and My Calendar pages.
 *
 * @component
 */
const CalendarPage = ({ myCalendar }) => {
  const currentDate = new Date()
  const [selectedDate, selectDate] = useState({
    date: currentDate.getDate(),
    month: currentDate.getMonth(),
    year: currentDate.getFullYear(),
  })

  const [matchesState, setMatchesState] = useState({
    matches: [],
    error: null,
  })
  const [isFetching, setIsFetching] = useState(true)

  const { date, month, year } = selectedDate

  const handleChangeDate = useCallback((newDay, newMonth) => {
    selectDate(({ date, month, year }) => ({
      date: newDay ?? date,
      month: newMonth ?? month,
      year: newMonth === 0 && month === 11
        ? year + 1
        : newMonth === 11 && month === 0
          ? year - 1
          : year,
    }))
  }, [month])

  const dispatch = useDispatch()
  const fetchMatches = useCallback(debounce((month, year) => {
    const action = myCalendar ? MatchActions.loadMyMatches : MatchActions.loadMatches
    const dateLt = new Date(month + 2 < 12 ? year : year + 1, (month + 2) % 12, 1)
    const dateGt = new Date(year, (month - 1) % 12, 1)

    dispatch(action({
      filters: { dateLt: dateLt.toISOString(), dateGt: dateGt.toISOString() },
      onSuccess: matches => {
        const selectedMatches = selectMatches(matches)

        setIsFetching(false)
        setMatchesState(prevState => ({
          matches: Object.values({
            ...Object.fromEntries(prevState.matches.map(match => [match.id, match])),
            ...Object.fromEntries(selectedMatches.map(match => [match.id, match])),
          }),
          error: null,
        }))
      },
      onError: error => {
        setIsFetching(false)
        setMatchesState(prevState => ({
          matches: prevState.matches,
          error,
        }))
      },
    }))
  }, 250), [myCalendar, dispatch])

  useEffect(() => fetchMatches(month, year), [month, year])

  const { matches: reshapedMatches, error } = matchesState
  const matches = useMemo(() => selectMatchesByDate(reshapedMatches), [reshapedMatches])
  const nextMatch = myCalendar
    ? useMemo(() => selectNextMatch(reshapedMatches, null), [reshapedMatches])
    : null

  const detailedMatches = matches[year]?.[month]?.[date] ?? []

  return !isFetching && !error
    ? (
      <CalendarViewBox>
        <CalendarViewColLeft>
          <CalendarTable selectedDate={selectedDate} onChangeDate={handleChangeDate} matches={matches} />
        </CalendarViewColLeft>
        <CalendarViewColRight>
          <CalendarViewColGroup>
            <CalendarControls
              selectedMonth={month}
              selectedYear={year}
              onChangeDate={handleChangeDate}
            />
            <CalendarDetail matches={detailedMatches} hasNextMatch={myCalendar} />
          </CalendarViewColGroup>
          {myCalendar && <CalendarNextMatch nextMatch={nextMatch} />}
        </CalendarViewColRight>
      </CalendarViewBox>
    ) : error
      ? <ContentGroup omitBottomMargin><ErrorBanner messageId={error} /></ContentGroup>
      : <LoadingBanner />
}

CalendarPage.propTypes = {
  /**
   * Used to distinguish between ordinary calendar and my calendar of logged user.
   */
  myCalendar: PropTypes.bool,
}

export default CalendarPage
