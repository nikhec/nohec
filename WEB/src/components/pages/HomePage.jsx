import React, { useMemo } from 'react'
import PropTypes from 'prop-types'

import { API } from 'src/constants'
import { MatchActions } from 'src/actions'
import { useFetchedState } from 'src/hooks/use-fetched-state'
import { selectLeagueMatches } from 'src/selectors/matchSelectors'
import { ContentGroup } from 'src/styles/blocks/page'
import { MatchDetailBox } from 'src/styles/blocks/matches'

import { ErrorBanner, LoadingBanner } from '../common'
import { HomeMatchesTable } from '../match'

/**
 * Home page component.
 *
 * @component
 */
const HomePage = ({ leagueId }) => {
  const {
    isFetching,
    state: matches,
    error,
  } = useFetchedState(
    MatchActions.loadMatches,
    { filters: { leagueId } },
    (data, prevState) => selectLeagueMatches(data, prevState, leagueId),
    [leagueId]
  )

  const { prev, next } = useMemo(() => matches?.[leagueId] ?? { prev: null, next: null }, [matches, leagueId])

  return !error && (prev?.length > 0 || next?.length > 0 || !isFetching)
    ? (
      <MatchDetailBox>
        {prev && <HomeMatchesTable
          headlineMessageId="COMMON.LATEST_RESULTS"
          emptyMessageId="COMMON.NO_LEAGUE_MATCHES"
          matches={prev.slice(0, 3)}
          resultsRoute={`${API.RESULTS}?tab=0#results`}
        />}

        {next && <HomeMatchesTable
          headlineMessageId="COMMON.UPCOMING_MATCHES"
          emptyMessageId="COMMON.NO_UPCOMING_MATCHES"
          matches={next.slice(0, 3)}
          resultsRoute={`${API.RESULTS}?tab=1#results`}
        />}
      </MatchDetailBox>
    ) : error
      ? <ContentGroup omitBottomMargin><ErrorBanner messageId={error} /></ContentGroup>
      : <LoadingBanner />
}

HomePage.propTypes = {
  /**
   * Id of the currently selected league.
   */
  leagueId: PropTypes.number,
}

export default HomePage
