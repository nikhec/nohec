import React, { useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types'

import { MatchActions } from 'src/actions'
import { MATCH_RECORD_STATE } from 'src/constants'

import { LoadingBanner } from '../common'
import {
  MatchRecordSign, MatchRecordSummation, MatchRecordGame, MatchRecordCaptainSelection,
  MatchRecordOverview, MatchRecordPlayerSelection, MatchEnterError,
} from '../match-record'

/**
 * Wrapper component for the whole match recording process.
 *
 * @component
 */
const MatchRecordPage = ({ id }) => {
  const { state: matchRecord, error: matchStateError } = useSelector(({ matchRecord }) => matchRecord)

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(MatchActions.enterMatch(id))
  }, [dispatch])

  const currentScreen = useMemo(() => {
    switch (matchRecord?.state) {
      case MATCH_RECORD_STATE.SIGN:
        return <MatchRecordSign matchRecord={matchRecord} />
      case MATCH_RECORD_STATE.SUMMATION:
        return <MatchRecordSummation matchRecord={matchRecord} />
      case MATCH_RECORD_STATE.OVERVIEW:
      case MATCH_RECORD_STATE.STARTED:
        return <MatchRecordOverview matchRecord={matchRecord} />
      case MATCH_RECORD_STATE.GAME: {
        const activeGame = matchRecord.games.find(game => game.sets.length !== 2)

        return (
          <MatchRecordGame
            key={`game-${activeGame.id}-set-${activeGame.sets.length + 1}`}
            matchRecord={matchRecord}
            activeGame={activeGame}
          />
        )
      }
      case MATCH_RECORD_STATE.CAPTAIN_SELECTION:
        return <MatchRecordCaptainSelection matchRecord={matchRecord} />
      case MATCH_RECORD_STATE.PLAYER_SELECTION: {
        const activeGame = matchRecord.games.find(game => !game.awayPlayers.length || !game.homePlayers.length)

        return <MatchRecordPlayerSelection matchRecord={matchRecord} activeGame={activeGame} />
      }
      default:
        return null
    }
  }, [matchRecord])

  return matchRecord
    ? matchStateError
      ? <MatchEnterError id={id} message={matchStateError} />
      : currentScreen
    : <LoadingBanner />
}

MatchRecordPage.propTypes = {
  /**
   * Id of the match we display detail for.
   */
  id: PropTypes.number.isRequired,
}

export default MatchRecordPage
