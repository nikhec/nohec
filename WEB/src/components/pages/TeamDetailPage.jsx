import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { useHistory } from 'react-router-dom'
import { isEmpty } from 'lodash'
import PropTypes from 'prop-types'

import { API } from 'src/constants'
import { TeamActions } from 'src/actions'
import { useSliderLogic } from 'src/hooks/use-slider-logic'
import { useFetchedState } from 'src/hooks/use-fetched-state'
import { useIsMobile } from 'src/hooks/use-device'
import { selectTeamDetail } from 'src/selectors/teamSelectors'
import { Slider, DetailSliderBox, ContentGroup, ContentTableBox } from 'src/styles/blocks/page'
import { NavbarToggle } from 'src/styles/blocks/navbar'
import { TeamDetailBodyBox, TeamDetailBox, TeamDetailLogo, TeamDetailLogoBox } from 'src/styles/blocks/teams'
import { MatchesTable } from 'src/styles/blocks/matches'

import { TeamDetailStatistics, TeamDetailPlayersRow, TeamDetailPlayersHeader } from '../team'
import { MatchResultTableRow } from '../match'
import { ErrorBanner, LoadingBanner } from '../common'

/**
 * Team detail page component.
 *
 * @component
 */
const TeamDetailPage = ({ id }) => {
  const [caret, setCaret] = useState(0)
  const [hoveredStat, setHoveredStat] = useState(null)

  const {
    state,
    isFetching,
    error,
  } = useFetchedState(TeamActions.loadTeamDetail, { teamId: id }, selectTeamDetail)

  const {
    selectedToggleRef,
    selectedToggleRect,
  } = useSliderLogic([caret, isFetching])

  const isMobile = useIsMobile()
  const history = useHistory()
  const browseMatchDetail = matchId => history.push(`${API.MATCH}?id=${matchId}`)

  const { name, logo, currentMembers, matches } = state

  return !isFetching && !error && !isEmpty(state)
    ? (
      <>
        <TeamDetailBox>
          <TeamDetailLogoBox>
            <TeamDetailLogo logo={logo} />
            <h2>{name}</h2>
          </TeamDetailLogoBox>
          {!isMobile && <TeamDetailStatistics teamId={id} />}
        </TeamDetailBox>

        <DetailSliderBox>
          {['COMMON.ROASTER', 'COMMON.MATCHES'].map((messageId, index) => (
            <NavbarToggle
              key={`detail-caret-${index}`}
              ref={index === caret ? selectedToggleRef : null}
              onClick={() => setCaret(index)}
            >
              <FormattedMessage id={messageId} />
            </NavbarToggle>
          ))}

          {selectedToggleRect && <Slider {...selectedToggleRect} />}
        </DetailSliderBox>

        <TeamDetailBodyBox>
          <ContentGroup>
            {caret === 0
              ? (
                <MatchesTable>
                  <TeamDetailPlayersHeader hoveredColumn={hoveredStat} />
                  <tbody>
                    {currentMembers.map((player, index) => (
                      <TeamDetailPlayersRow
                        key={`home-player-row-${index}`}
                        playerName={player.name}
                        stats={player.stats}
                        hoveredColumn={hoveredStat}
                        onStatHover={columnId => setHoveredStat(columnId)}
                      />
                    ))}
                  </tbody>
                </MatchesTable>
              ) : (
                <ContentTableBox>
                  <MatchesTable>
                    <tbody>
                      {matches.map((match, index) => (
                        <MatchResultTableRow
                          key={`match-row-${index}`}
                          match={match}
                          rowIndex={index}
                          isMobile={isMobile}
                          onDetailClick={browseMatchDetail}
                        />
                      ))}
                    </tbody>
                  </MatchesTable>
                </ContentTableBox>
              )}
          </ContentGroup>
        </TeamDetailBodyBox>
      </>
    ) : error
      ? <ContentGroup omitBottomMargin><ErrorBanner messageId={error} /></ContentGroup>
      : <LoadingBanner />
}

TeamDetailPage.propTypes = {
  /**
   * Unique identifier of the team.
   */
  id: PropTypes.number.isRequired,
}

export default TeamDetailPage
