import React from 'react'
import PropTypes from 'prop-types'

import { MatchActions } from 'src/actions'
import { useFetchedState } from 'src/hooks/use-fetched-state'
import { selectMatchDetail } from 'src/selectors/matchDetailSelectors'
import { ContentGroup } from 'src/styles/blocks/page'

import { ErrorBanner, LoadingBanner } from '../common'
import { FinishedMatchDetail, NewMatchDetail } from '../match-detail'

/**
 * Single match detail page component.
 *
 * @component
 */
const MatchDetailPage = ({ id }) => {
  const {
    isFetching,
    state: match,
    error,
  } = useFetchedState(MatchActions.loadMatchDetail, { matchId: id }, selectMatchDetail)

  return !isFetching
    ? match.isFinished
      ? <FinishedMatchDetail match={match} />
      : <NewMatchDetail match={match} />
    : error
      ? <ContentGroup omitBottomMargin><ErrorBanner messageId={error} /></ContentGroup>
      : <LoadingBanner />
}

MatchDetailPage.propTypes = {
  /**
   * Id of the match we display detail for.
   */
  id: PropTypes.number.isRequired,
}

export default MatchDetailPage
