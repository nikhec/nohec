import HomePage from './HomePage'
import TeamsPage from './TeamsPage'
import TeamDetailPage from './TeamDetailPage'
import CalendarPage from './CalendarPage'
import ResultsPage from './ResultsPage'
import MatchDetailPage from './MatchDetailPage'
import AccountPage from './AccountPage'
import MatchRecordPage from './MatchRecordPage'

export { HomePage, TeamsPage, TeamDetailPage, ResultsPage, CalendarPage, MatchDetailPage, MatchRecordPage, AccountPage }
