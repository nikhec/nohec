import React, { useEffect, useLayoutEffect, useMemo, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import { API } from 'src/constants'
import { MatchActions, TeamActions } from 'src/actions'
import { useSliderLogic } from 'src/hooks/use-slider-logic'
import { useFetchedState } from 'src/hooks/use-fetched-state'
import { useIsMobile } from 'src/hooks/use-device'
import { getImmediateScrollOffset } from 'src/utils/immediate-scroll-offset'
import { selectLeagueMatches } from 'src/selectors/matchSelectors'
import { selectLeagueStandings } from 'src/selectors/teamSelectors'
import {
  ColumnContentBox, ContentGroup, ContentGroupHeadline, ContentTableBox, DetailSliderBox, Slider,
} from 'src/styles/blocks/page'
import { MatchesTable } from 'src/styles/blocks/matches'
import { NavbarToggle } from 'src/styles/blocks/navbar'

import { ErrorBanner, LoadingBanner } from '../common'
import { MatchResultTableRow } from '../match'
import { TeamStandingsTableRow } from '../team'

/**
 * Results page component.
 *
 * @component
 */
const ResultsPage = ({ leagueId }) => {
  const selectorWithLeagueId = selectorFunc => (data, prevState) => selectorFunc(data, prevState, leagueId)
  const {
    isFetching: matchesFetching,
    state: matches,
    error: matchesError,
  } = useFetchedState(
    MatchActions.loadMatches,
    { filters: { leagueId } },
    selectorWithLeagueId(selectLeagueMatches),
    [leagueId]
  )

  const {
    isFetching: standingsFetching,
    state: standings,
    error: standingsError,
  } = useFetchedState(
    TeamActions.loadTeamStandings,
    { leagueId },
    selectorWithLeagueId(selectLeagueStandings),
    [leagueId]
  )

  const history = useHistory()
  const browseMatchDetail = matchId => history.push(`${API.MATCH}?id=${matchId}`)
  const browseTeamDetail = teamId => history.push(`${API.TEAM_DETAIL}?id=${teamId}`)

  const router = useSelector(({ router }) => router)
  const [tab, setTab] = useState(router.location.query.tab == 1 ? 1 : 0)

  const {
    selectedToggleRef,
    selectedToggleRect,
    setToggleRect,
  } = useSliderLogic([tab])

  useLayoutEffect(() => {
    if (selectedToggleRef.current) {
      const { top, height } = selectedToggleRef.current.getBoundingClientRect()
      const scrollOffset = getImmediateScrollOffset()

      setToggleRect(prevState => ({ ...prevState, top: top + height + scrollOffset }))
    }
  }, [leagueId])

  const isMobile = useIsMobile()

  const resultsRef = useRef(null)
  useEffect(() => {
    if (resultsRef.current && router.location.hash === '#results') {
      history.push({ search: `?tab=${tab}`, hash: '' })
      resultsRef.current.scrollIntoView({ behavior: 'smooth' })
    }
  }, [resultsRef.current])

  const isFetching = matchesFetching || standingsFetching
  const error = matchesError ?? standingsError

  const visibleStandings = useMemo(() => standings?.[leagueId] ?? [], [standings, leagueId])
  const visibleMatches = useMemo(() => (tab === 0
    ? matches?.[leagueId]?.prev
    : matches?.[leagueId]?.next
  ) ?? null, [matches, tab, leagueId])

  return !error && (visibleMatches || !isFetching)
    ? (
      <ColumnContentBox>
        <ContentGroupHeadline isUpperCase>
          <FormattedMessage id="COMMON.STANDINGS" />
        </ContentGroupHeadline>
        <ContentGroup>
          <ContentTableBox>
            <MatchesTable>
              <tbody>
                {visibleStandings.map((standing, index) => (
                  <TeamStandingsTableRow
                    key={`standing-row-${index}`}
                    standing={standing}
                    rowIndex={index}
                    isMobile={isMobile}
                    onDetailClick={browseTeamDetail}
                  />
                ))}
              </tbody>
            </MatchesTable>
          </ContentTableBox>
        </ContentGroup>

        <ContentGroupHeadline isUpperCase id="results" ref={resultsRef}>
          <FormattedMessage id="COMMON.MATCHES" />
        </ContentGroupHeadline>

        <DetailSliderBox>
          {['COMMON.PREVIOUS', 'COMMON.UPCOMING'].map((messageId, index) => (
            <NavbarToggle
              key={`results-tab-${index}`}
              ref={index === tab ? selectedToggleRef : null}
              onClick={() => {
                setTab(index)
                history.push({ search: `?tab=${index}` })
              }}
            >
              <FormattedMessage id={messageId} />
            </NavbarToggle>
          ))}

          {selectedToggleRect && <Slider {...selectedToggleRect} />}
        </DetailSliderBox>

        <ContentGroup omitBottomMargin>
          {!(visibleMatches?.length > 0) && !isFetching ? (
            tab === 0
              ? <FormattedMessage id="COMMON.NO_LEAGUE_MATCHES" />
              : <FormattedMessage id="COMMON.NO_UPCOMING_MATCHES" />
          ) : (
            <ContentTableBox>
              <MatchesTable>
                <tbody>
                  {visibleMatches?.map((match, index) => (
                    <MatchResultTableRow
                      key={`match-row-${index}`}
                      match={match}
                      rowIndex={index}
                      isMobile={isMobile}
                      onDetailClick={browseMatchDetail}
                    />
                  ))}
                </tbody>
              </MatchesTable>
            </ContentTableBox>
          )}
        </ContentGroup>
      </ColumnContentBox>
    ) : error
      ? <ContentGroup omitBottomMargin><ErrorBanner messageId={error} /></ContentGroup>
      : <LoadingBanner />
}

ResultsPage.propTypes = {
  /**
   * Id of the currently selected league.
   */
  leagueId: PropTypes.number,
}

export default ResultsPage
