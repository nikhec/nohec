import FinishedMatchDetail from './FinishedMatchDetail'
import NewMatchDetail from './NewMatchDetail'

export { FinishedMatchDetail, NewMatchDetail }
