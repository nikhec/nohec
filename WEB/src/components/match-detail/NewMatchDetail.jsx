import React, { useMemo } from 'react'
import { FormattedMessage } from 'react-intl'
import { useHistory } from 'react-router-dom'
import { Map, MarkerLayer, Marker, MouseControl, ZoomControl, CompassControl } from 'react-mapycz'

import { API, MATCH_PLAYABLE_BEFORE } from 'src/constants'
import { MatchDetailPropType } from 'src/utils/prop-types'
import { useIsDesktop } from 'src/hooks/use-device'
import {
  MatchDetailBox, MatchDetailButtonBox, MatchDetailPlaceBox, MatchDetailPlaceInfo, MatchDetailPlaceMap,
} from 'src/styles/blocks/matches'

import { PrimaryButton, Link } from '../common'
import { MatchHeader } from '../match'

/**
 * Component for displaying detail of not yet played/finished match.
 * Is also used as an entry point for logged and assigned referee user to start filling match details.
 *
 * @component
 */
const NewMatchDetail = ({ match }) => {
  const canUserStartMatch = useMemo(() => {
    const date = new Date()
    const matchDate = new Date(match.dateTime)

    return !match.isFinished && match.id && match.canModify
      && (matchDate <= date || (matchDate - date < MATCH_PLAYABLE_BEFORE))
  }, [match])

  const isDesktop = useIsDesktop()
  const history = useHistory()
  const handleMatchStartClick = () => history.push(`${API.MATCH_RECORD}?id=${match.id}`)

  return (
    <MatchDetailBox>
      <MatchHeader
        homeTeam={match.homeTeam}
        awayTeam={match.awayTeam}
        headline={<h1><FormattedMessage id="MATCH.VERSUS" /></h1>}
        subContent={
          <>
            <h3>{match.time}</h3>
            <h2>{match.date}</h2>
          </>
        }
      />

      {canUserStartMatch && (
        <MatchDetailButtonBox>
          <Link route={`${API.MATCH_RECORD}?id=${match.id}`}>
            <PrimaryButton higher onClick={handleMatchStartClick}>
              <FormattedMessage id="MATCH.START" />
            </PrimaryButton>
          </Link>
        </MatchDetailButtonBox>
      )}

      {match.place && (
        <MatchDetailPlaceBox>
          <MatchDetailPlaceInfo>
            {match.place.address}
          </MatchDetailPlaceInfo>
          <MatchDetailPlaceMap>
            <Map height="100%" width="100%" center={{ lat: match.place.latitude, lng: match.place.longitude }}>
              <ZoomControl />
              <MouseControl zoom pan wheel={isDesktop} />
              ${!isDesktop && <CompassControl right={10} top={50} />}
              <MarkerLayer>
                <Marker coords={{ lat: match.place.latitude, lng: match.place.longitude }} />
              </MarkerLayer>
            </Map>
          </MatchDetailPlaceMap>
        </MatchDetailPlaceBox>
      )}
    </MatchDetailBox>
  )
}

NewMatchDetail.propTypes = {
  /**
   * Information about currently displayed match
   */
  match: MatchDetailPropType.isRequired,
}

export default NewMatchDetail
