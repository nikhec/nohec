import React, { useMemo, useState } from 'react'
import { FormattedMessage } from 'react-intl'

import { useSliderLogic } from 'src/hooks/use-slider-logic'
import { useDevice } from 'src/hooks/use-device'
import { MatchDetailPropType } from 'src/utils/prop-types'
import { NavbarToggle } from 'src/styles/blocks/navbar'
import { Slider, DetailSliderBox } from 'src/styles/blocks/page'

import MatchDetailPlayers from './MatchDetailPlayers'
import MatchDetailSets from './MatchDetailSets'
import MatchDetailGeneral from './MatchDetailGeneral'

/**
 * Component for displaying detail of already finished match.
 *
 * @component
 */
const FinishedMatchDetail = ({ match }) => {
  const [caret, setCaret] = useState(0)

  const {
    selectedToggleRef,
    selectedToggleRect,
  } = useSliderLogic([caret])

  const device = useDevice()
  const detailBody = useMemo(() => {
    switch (caret) {
      case 1: return <MatchDetailPlayers homeTeam={match.homeTeam} awayTeam={match.awayTeam} />
      case 2: return <MatchDetailSets match={match} device={device} />
      default: return <MatchDetailGeneral match={match} />
    }
  }, [caret, device, match])

  return (
    <>
      <DetailSliderBox>
        {['MATCH.GENERAL', 'MATCH.PLAYERS', 'MATCH.SETS'].map((messageId, index) => (
          <NavbarToggle
            key={`detail-caret-${index}`}
            ref={index === caret ? selectedToggleRef : null}
            onClick={() => setCaret(index)}
          >
            <FormattedMessage id={messageId} />
          </NavbarToggle>
        ))}

        {selectedToggleRect && <Slider {...selectedToggleRect} />}
      </DetailSliderBox>

      {detailBody}
    </>
  )
}

FinishedMatchDetail.propTypes = {
  /**
   * Information about currently displayed match
   */
  match: MatchDetailPropType.isRequired,
}

export default FinishedMatchDetail
