import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'

import { DEVICES, TEXTS } from 'src/constants'
import { MatchDetailPropType } from 'src/utils/prop-types'
import { MatchBodyListNode, MatchDetailBodyBox, MatchPlayersList } from 'src/styles/blocks/matches'

import { MatchHeader, MatchContentRow } from '../match'

/**
 * Component for displaying sets description of the match.
 *
 * @component
 */
const MatchDetailSets = ({ match, device }) => {
  const getSideContent = (teamPlayers, playerIds) => (
    <MatchPlayersList>
      {teamPlayers.filter(({ id }) => playerIds.includes(id)).map(({ name }, index) => (
        <MatchBodyListNode key={`player-${index}`} accent>
          {name}
        </MatchBodyListNode>
      ))}
    </MatchPlayersList>
  )

  const setsToContent = sets => {
    const outcome = sets.reduce(([accHome, accAway], { homePoints, awayPoints }) => homePoints > awayPoints
      ? [accHome + 1, accAway]
      : [accHome, accAway + 1],
    [0, 0])

    return device === DEVICES.DESKTOP
      ? (
        <>
          <h1>{outcome.join(' : ')}</h1>
          {sets.map(({ homePoints, awayPoints }, index) => (
            <h2 key={`match-set-${index}`}>
              {homePoints < 10 ? <>&nbsp;{homePoints}&nbsp;</> : homePoints}
              &nbsp;:&nbsp;
              {awayPoints < 10 ? <>&nbsp;{awayPoints}&nbsp;</> : awayPoints}
            </h2>
          ))}
        </>
      ) : (
        <>
          <h1>{outcome.join(' : ')} [ </h1>
          <h2>{sets.map(({ homePoints, awayPoints }) => `${homePoints} : ${awayPoints}`).join(', ')}</h2>
          <h1> ]</h1>
        </>
      )
  }

  return (
    <MatchDetailBodyBox isColumn>
      <MatchHeader
        homeTeam={match.homeTeam}
        awayTeam={match.awayTeam}
        headline={<h1>{match.score.join(' : ')}</h1>}
        imageSize={100}
        isSingleLine
      />

      {match.games.map((game, index) => (
        <MatchContentRow
          key={`detail-content-row-${index}`}
          leftContent={getSideContent(match.homeTeam.players, game.homePlayers)}
          rightContent={getSideContent(match.awayTeam.players, game.awayPlayers)}
          centerContent={setsToContent(game.sets)}
          headline={<>{game.typeOrder}. <FormattedMessage id={TEXTS.GAME_TYPES[game.type]} /></>}
        />
      ))}
    </MatchDetailBodyBox>
  )
}

MatchDetailSets.propTypes = {
  /**
   * Information about currently displayed match
   */
  match: MatchDetailPropType.isRequired,
  /**
   * Indicates what device is the view displayed on.
   */
  device: PropTypes.string.isRequired,
}

export default MatchDetailSets
