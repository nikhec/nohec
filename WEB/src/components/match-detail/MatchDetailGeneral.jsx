import React from 'react'

import { MatchDetailPropType } from 'src/utils/prop-types'
import { MatchBodyListNodeCaptain, MatchBodyListNode, MatchPlayersList } from 'src/styles/blocks/matches'

import { MatchHeader } from '../match'

/**
 * Component for displaying match detail for general caret.
 *
 * @component
 */
const MatchDetailGeneral = ({ match }) => {
  const getPlayerList = players => (
    <MatchPlayersList fullOverflow>
      {players.map(({ name, isCaptain }, index) => (
        <MatchBodyListNode key={`home-player-${index}`} accent>
          {name} {isCaptain && <MatchBodyListNodeCaptain>C</MatchBodyListNodeCaptain>}
        </MatchBodyListNode>
      ))}
    </MatchPlayersList>
  )

  return (
    <MatchHeader
      topAlignment
      homeTeam={match.homeTeam}
      awayTeam={match.awayTeam}
      homeTeamCol={getPlayerList(match.homeTeam.players)}
      awayTeamCol={getPlayerList(match.awayTeam.players)}
      headline={<h1>{match.score.join(' : ')}</h1>}
      subContent={
        <>
          <h3>{match.time}</h3>
          <h2>{match.date}</h2>
        </>
      }
    />
  )
}

MatchDetailGeneral.propTypes = {
  /**
   * Information about currently displayed match
   */
  match: MatchDetailPropType.isRequired,
}

export default MatchDetailGeneral
