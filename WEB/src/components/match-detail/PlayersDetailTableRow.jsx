import React from 'react'
import PropTypes from 'prop-types'

import { COLORS } from 'src/constants'
import { HighlightedNodeContent, MatchesTableNode, MatchesTableRow } from 'src/styles/blocks/matches'

/**
 * Single row within players detail table on match detail or club detail pages.
 *
 * @component
 */
const PlayersDetailTableRow = ({ playerName, values }) => (
  <MatchesTableRow>
    <MatchesTableNode textColor={COLORS.RED_PRIMARY} leftAligned>
      <HighlightedNodeContent>
        {playerName.split(' ')[0]}
      </HighlightedNodeContent>
      &nbsp;{playerName.split(' ').slice(1).join(' ')}
    </MatchesTableNode>

    {values.map((value, index) => (
      <MatchesTableNode key={`node-${index}`}>
        {value}
      </MatchesTableNode>
    ))}
  </MatchesTableRow>
)

PlayersDetailTableRow.propTypes = {
  /**
   * Player name associated with given row.
   */
  playerName: PropTypes.string.isRequired,
  /**
   * Array of values to display in remaining columns.
   */
  values: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.node])
  ),
}

export default PlayersDetailTableRow
