import React, { useState } from 'react'

import { MatchDetailTeamPropType } from 'src/utils/prop-types'
import { ColumnContentBox, ContentGroup, ContentGroupHeadline } from 'src/styles/blocks/page'
import { MatchDetailBodyBox, MatchesTable } from 'src/styles/blocks/matches'

import { TeamDetailPlayersHeader, TeamDetailPlayersRow } from '../team'

/**
 * Component for displaying detailed players description of the match.
 *
 * @component
 */
const MatchDetailPlayers = ({ homeTeam, awayTeam }) => {
  const [hoveredHomeStat, setHoveredHomeStat] = useState(null)
  const [hoveredTeamStat, setHoveredTeamStat] = useState(null)

  return (
    <MatchDetailBodyBox isColumn>
      <ColumnContentBox>
        <ContentGroupHeadline>{homeTeam.name}</ContentGroupHeadline>
        <ContentGroup>
          <MatchesTable>
            <TeamDetailPlayersHeader hoveredColumn={hoveredHomeStat} />
            <tbody>
              {homeTeam.players.map((player, index) => (
                <TeamDetailPlayersRow
                  key={`home-team-row-${index}`}
                  playerName={player.name}
                  stats={player.stats}
                  hoveredColumn={hoveredHomeStat}
                  onStatHover={columnId => setHoveredHomeStat(columnId)}
                />
              ))}
            </tbody>
          </MatchesTable>
        </ContentGroup>
      </ColumnContentBox>

      <ColumnContentBox>
        <ContentGroupHeadline>{awayTeam.name}</ContentGroupHeadline>
        <ContentGroup>
          <MatchesTable>
            <TeamDetailPlayersHeader hoveredColumn={hoveredTeamStat} />
            <tbody>
              {awayTeam.players.map((player, index) => (
                <TeamDetailPlayersRow
                  key={`away-team-row-${index}`}
                  playerName={player.name}
                  stats={player.stats}
                  hoveredColumn={hoveredTeamStat}
                  onStatHover={columnId => setHoveredTeamStat(columnId)}
                />
              ))}
            </tbody>
          </MatchesTable>
        </ContentGroup>
      </ColumnContentBox>
    </MatchDetailBodyBox>
  )
}

MatchDetailPlayers.propTypes = {
  /**
   * Information for the players table about the home team
   */
  homeTeam: MatchDetailTeamPropType.isRequired,
  /**
   * Information for the players table about the away team
   */
  awayTeam: MatchDetailTeamPropType.isRequired,
}

export default MatchDetailPlayers
