import React, { useMemo } from 'react'
import { useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'

import { API } from 'src/constants'
import { MatchPropType } from 'src/utils/prop-types'
import {
  CalendarDetailLeagueBox, CalendarDetailLeagueLabel, CalendarDetailMatch, CalendarDetailMatchLeft,
  CalendarDetailMatchLogo, CalendarDetailMatchName, CalendarDetailMatchPlace,
  CalendarDetailMatchRight, CalendarDetailMatchRow, CalendarDetailMatchSeparator,
} from 'src/styles/blocks/calendar'

import { Link } from '../common'

/**
 * Displays given list of matches.
 *
 * @component
 */
const CalendarDetailList = ({ matches }) => {
  const groupedMatches = useMemo(() => matches
    .sort(({ league: lA }, { league: lB }) => lA.id - lB.id)
    .reduce((acc, match) => {
      if (!acc[match.league]) {
        acc[match.league] = []
      }
      acc[match.league].push(match)

      return acc
    }, {}), [matches])

  const history = useHistory()
  const browseMatchDetail = matchId => history.push(`${API.MATCH}?id=${matchId}`)

  return (
    <>
      {Object.entries(groupedMatches).map(([league, leagueMatches], index) => (
        <CalendarDetailLeagueBox key={`league-detail-${index}`}>
          <CalendarDetailLeagueLabel>
            {league}
          </CalendarDetailLeagueLabel>

          {leagueMatches.map((match, index) => (
            <CalendarDetailMatchRow
              key={`match-detail-${index}`}
              onClick={() => browseMatchDetail(match.id)}
            >
              <Link route={`${API.MATCH}?id=${match.id}`}>
                <CalendarDetailMatch>
                  <CalendarDetailMatchLeft>
                    <CalendarDetailMatchName>{match.homeTeam.name}</CalendarDetailMatchName>
                    <CalendarDetailMatchLogo logo={match.homeTeam.logo} />
                  </CalendarDetailMatchLeft>

                  <CalendarDetailMatchSeparator>
                    {match.isFinished ? match.score.join(':') : '/'}
                  </CalendarDetailMatchSeparator>

                  <CalendarDetailMatchRight>
                    <CalendarDetailMatchLogo logo={match.awayTeam.logo} />
                    <CalendarDetailMatchName>{match.awayTeam.name}</CalendarDetailMatchName>
                  </CalendarDetailMatchRight>
                </CalendarDetailMatch>

                <CalendarDetailMatchPlace>
                  {match.place} - {match.time}
                </CalendarDetailMatchPlace>
              </Link>
            </CalendarDetailMatchRow>
          ))}
        </CalendarDetailLeagueBox>
      ))}
    </>
  )
}

CalendarDetailList.propTypes = {
  /**
   * Scheduled matches.
   */
  matches: PropTypes.arrayOf(MatchPropType).isRequired,
}

export default CalendarDetailList
