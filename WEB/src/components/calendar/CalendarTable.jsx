import React from 'react'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'

import { TEXTS } from 'src/constants'
import { MatchPropType } from 'src/utils/prop-types'
import { CalendarBox, CalendarHeaderNode } from 'src/styles/blocks/calendar'

import CalendarTableNode from './CalendarTableNode'

/**
 * Represents calendar table displayed on the Calendar page.
 *
 * @component
 */
const CalendarTable = ({ matches, selectedDate: { date, year, month }, onChangeDate }) => {
  const firstDay = new Date(year, month, 1)
  const lastDay = month === 11
    ? new Date(year + 1, 0, 0)
    : new Date(year, month + 1, 0)
  const lastDayLastMonth = new Date(year, month, 0)

  // getDay returns 0 for sunday, we need to shift it to the last index
  let firstDayIndex = (firstDay.getDay() + 7) % 8
  // In case the month starts with Monday, we need to forcibly dedicate the first row to the previous month
  firstDayIndex += firstDayIndex < 3 ? 7 : 0
  const lastDayIndex = lastDay.getDate() - 1 + firstDayIndex
  const lastDateLastMonth = lastDayLastMonth.getDate()

  const intl = useSelector(({ app }) => app.intl)

  return (
    <CalendarBox>
      <thead>
        <tr>
          {TEXTS.DAYS.map((dayId, index) => (
            <CalendarHeaderNode key={`day-${index}`} isWeekend={index > 4}>
              {intl?.formatMessage({ id: dayId }).substring(0, 2)}
            </CalendarHeaderNode>
          ))}
        </tr>
      </thead>
      <tbody>
        {Array.from(Array(6)).map((_, rowNum) => (
          <tr key={`calendar-row-${rowNum}`}>
            {Array.from(Array(7)).map((_, colNum) => {
              const dateIndex = (rowNum * 7) + colNum
              // Calculates day, month and year of associated calendar node
              const [day, dayMonth, dayYear] = dateIndex < firstDayIndex
                ? [
                  lastDateLastMonth - firstDayIndex + dateIndex + 1,
                  (month - 1) % 12,
                  month === 0 ? year - 1 : year,
                ]
                : dateIndex > lastDayIndex
                  ? [
                    dateIndex - lastDayIndex,
                    (month + 1) % 12,
                    month === 11 ? year + 1 : year,
                  ]
                  : [dateIndex - firstDayIndex + 1, month, year]

              return (
                <CalendarTableNode
                  key={`calendar-col-${colNum}`}
                  day={day}
                  matches={matches[dayYear]?.[dayMonth]?.[day] ?? []}
                  isSelected={date === day && month === dayMonth}
                  isActiveMonth={dayMonth === month}
                  isWeekend={colNum > 4}
                  onClick={() => onChangeDate(day, dayMonth)}
                />
              )
            })}
          </tr>
        ))}
      </tbody>
    </CalendarBox>
  )
}

CalendarTable.propTypes = {
  /**
   * Map of matches, indexed by years > months > days.
   */
  matches: PropTypes.objectOf(
    PropTypes.objectOf(
      PropTypes.objectOf(
        PropTypes.arrayOf(MatchPropType)
      )
    )
  ),
  /**
   * Represents currently selected date.
   */
  selectedDate: PropTypes.shape({
    date: PropTypes.number.isRequired,
    month: PropTypes.number.isRequired,
    year: PropTypes.number.isRequired,
  }).isRequired,
  /**
   * Callback for handling selection of different date.
   */
  onChangeDate: PropTypes.func.isRequired,
}

export default CalendarTable
