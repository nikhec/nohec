import React from 'react'
import { FormattedMessage } from 'react-intl'

import { MatchPropType } from 'src/utils/prop-types'
import { CalendarNextMatchBox, CalendarNextMatchHeader } from 'src/styles/blocks/calendar'

import CalendarDetailList from './CalendarDetailList'

/**
 * Displays Next Match row on the bottom of the detail for currently selected day (located on top for other devices).
 *
 * @component
 */
const CalendarNextMatch = ({ nextMatch }) => (
  <CalendarNextMatchBox>
    <CalendarNextMatchHeader>
      <FormattedMessage id="CALENDAR.NEXT_MATCH" />
    </CalendarNextMatchHeader>

    {nextMatch
      ? <CalendarDetailList matches={[nextMatch]} />
      : <FormattedMessage id="CALENDAR.NO_NEXT_MATCH" />}
  </CalendarNextMatchBox>
)

CalendarNextMatch.propTypes = {
  /**
   * Information about the first upcoming match.
   */
  nextMatch: MatchPropType,
}

export default CalendarNextMatch
