import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'

import { MatchPropType } from 'src/utils/prop-types'
import { CalendarDetailBox, CalendarDetailWrapper } from 'src/styles/blocks/calendar'

import CalendarDetailList from './CalendarDetailList'

/**
 * Displays detail with list of matches for selected day.
 *
 * @component
 */
const CalendarDetail = ({ matches, hasNextMatch }) => (
  <CalendarDetailWrapper>
    <CalendarDetailBox>
      {matches.length
        ? <CalendarDetailList matches={matches} />
        : hasNextMatch
          ? <FormattedMessage id="CALENDAR.NO_MY_MATCHES" />
          : <FormattedMessage id="CALENDAR.NO_MATCHES" />}
    </CalendarDetailBox>
  </CalendarDetailWrapper>
)

CalendarDetail.propTypes = {
  /**
   * Matches that are scheduled for this date.
   *
   */
  matches: PropTypes.arrayOf(MatchPropType).isRequired,
  /**
   * Indicates whether Next Match row is displayed under the detail column or not.
   */
  hasNextMatch: PropTypes.bool,
}

export default CalendarDetail
