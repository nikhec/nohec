import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'

import SvgArrowUp from '~/assets/svg/arrow-up.svg'
import SvgArrowDown from '~/assets/svg/arrow-down.svg'
import SvgArrowLeft from '~/assets/svg/arrow-left.svg'
import SvgArrowRight from '~/assets/svg/arrow-right.svg'

import { TEXTS } from 'src/constants'
import { useIsMobile } from 'src/hooks/use-device'
import {
  CalendarControlLeft, CalendarControlMonth, CalendarControlRight, CalendarControlsBox,
  CalendarControlToday, CalendarControlVertical, CalendarControlYear,
} from 'src/styles/blocks/calendar'

import { PrimaryButton } from '../common'

/**
 * Displays controls for month toggling.
 *
 * @component
 */
const CalendarControls = ({ selectedMonth, selectedYear, onChangeDate }) => {
  const isMobile = useIsMobile()
  const today = new Date()

  const handleNextMonthCLick = () => onChangeDate(null, (selectedMonth + 11) % 12)
  const handlePrevMonthCLick = () => onChangeDate(null, (selectedMonth + 13) % 12)

  return (
    <CalendarControlsBox>
      {isMobile ? (
        <CalendarControlVertical>
          <SvgArrowUp onClick={handleNextMonthCLick} />
        </CalendarControlVertical>
      ) : (
        <CalendarControlLeft>
          <SvgArrowLeft onClick={handleNextMonthCLick} />
        </CalendarControlLeft>
      )}

      <CalendarControlMonth>
        <FormattedMessage id={TEXTS.MONTHS[selectedMonth]} />
      </CalendarControlMonth>
      {selectedYear !== today.getFullYear() && (
        <CalendarControlYear>{selectedYear}</CalendarControlYear>
      )}

      {isMobile ? (
        <CalendarControlVertical>
          <SvgArrowDown onClick={handlePrevMonthCLick} />
        </CalendarControlVertical>
      ) : (
        <CalendarControlRight>
          <SvgArrowRight onClick={handlePrevMonthCLick} />
        </CalendarControlRight>
      )}

      <CalendarControlToday>
        <PrimaryButton onClick={() => onChangeDate(today.getDate(), today.getMonth())}>
          <FormattedMessage id="CALENDAR.TODAY" />
        </PrimaryButton>
      </CalendarControlToday>
    </CalendarControlsBox>
  )
}

CalendarControls.propTypes = {
  /**
   * Number of currently selected month. Indexed from 0.
   */
  selectedMonth: PropTypes.number.isRequired,
  /**
   * Selected year.
   */
  selectedYear: PropTypes.number.isRequired,
  /**
   * Callback for handling control actions.
   */
  onChangeDate: PropTypes.func.isRequired,
}

export default CalendarControls
