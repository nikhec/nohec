import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'

import { MatchPropType } from 'src/utils/prop-types'
import { useIsMobile } from 'src/hooks/use-device'
import {
  CalendarBodyNodeDay, CalendarBodyNode, CalendarBodyNodeMatch, CalendarBodyNodeMore,
  CalendarBodyNodeLogo, CalendarBodyNodeResult,
} from 'src/styles/blocks/calendar'

/**
 * Represents a single node (box) within calendar table.
 *
 * @component
 */
const CalendarTableNode = ({ day, matches, isSelected, isActiveMonth, isWeekend, onClick }) => {
  const isMobile = useIsMobile()
  const matchBodyPlaceholderId = matches.length < 2
    ? 'CALENDAR.MATCH_SINGLE'
    : 'CALENDAR.MATCH_MANY'

  return (
    <CalendarBodyNode
      isSelected={isSelected}
      isActiveMonth={isActiveMonth}
      isWeekend={isWeekend}
      onClick={onClick}
    >
      <CalendarBodyNodeDay>{day}</CalendarBodyNodeDay>
      {matches.length
        ? isMobile
          ? (
            <CalendarBodyNodeMatch>
              <CalendarBodyNodeResult>
                {matches.length} <FormattedMessage id={matchBodyPlaceholderId} />
              </CalendarBodyNodeResult>
            </CalendarBodyNodeMatch>
          ) : (
            <CalendarBodyNodeMatch>
              <CalendarBodyNodeLogo logo={matches[0].homeTeam.logo} title={matches[0].homeTeam.name} />
              <CalendarBodyNodeResult>
                {matches[0].isFinished ? matches[0].score.join(':') : 'x'}
              </CalendarBodyNodeResult>
              <CalendarBodyNodeLogo logo={matches[0].awayTeam.logo} title={matches[0].awayTeam.name} />
            </CalendarBodyNodeMatch>
          )
        : null}
      {matches.length > 1 && !isMobile && (
        <CalendarBodyNodeMore>
          +{matches.length - 1} <FormattedMessage id="CALENDAR.MORE" />
        </CalendarBodyNodeMore>
      )}
    </CalendarBodyNode>
  )
}

CalendarBodyNode.defaultProps = {
  isActiveMonth: true,
}

CalendarTableNode.propTypes = {
  /**
   * Day of the month the node represents.
   */
  day: PropTypes.number.isRequired,
  /**
   * Array of matches that are played on this date.
   */
  matches: PropTypes.arrayOf(MatchPropType).isRequired,
  /**
   * Indicates whether is the node currently selected.
   */
  isSelected: PropTypes.bool,
  /**
   * Indicates whether the node belongs to currently selected month.
   */
  isActiveMonth: PropTypes.bool,
  /**
   * Indicates whether is the day of the node weekend.
   */
  isWeekend: PropTypes.bool,
  /**
   * Callback handling click event on the node.
   */
  onClick: PropTypes.func.isRequired,
}

export default CalendarTableNode
