import React from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import SvgTimer from '~/assets/svg/timer.svg'
import SvgSubstitution from '~/assets/svg/substitution.svg'

import {
  MatchRecordActionButton, MatchRecordActionButtonIcon, MatchRecordActionButtonsBox,
} from 'src/styles/blocks/match-record'

/**
 * Component rendering game action buttons such as timeout or card buttons.
 *
 * @component
 */
const ActionButtons = ({ timeoutDisabled, substitutionsDisabled, reversed, onTimeoutClick, onSubstitutionClick }) => {
  const timeoutButton = (
    <MatchRecordActionButton key="timeout-button" onClick={onTimeoutClick} disabled={timeoutDisabled}>
      <MatchRecordActionButtonIcon>
        <SvgTimer />
      </MatchRecordActionButtonIcon>
      <span>
        <FormattedMessage id="MATCH_RECORD.TIMEOUT" />
      </span>
    </MatchRecordActionButton>
  )

  const substitutionButton = (
    <MatchRecordActionButton key="substitution-button" onClick={onSubstitutionClick} disabled={substitutionsDisabled}>
      <MatchRecordActionButtonIcon>
        <SvgSubstitution />
      </MatchRecordActionButtonIcon>
      <span>
        <FormattedMessage id="MATCH_RECORD.SUBSTITUTION" />
      </span>
    </MatchRecordActionButton>
  )

  const buttons = [timeoutButton, substitutionButton]

  return (
    <MatchRecordActionButtonsBox reversed={reversed}>
      <div>
        {reversed
          ? buttons.reverse()
          : buttons}
      </div>
    </MatchRecordActionButtonsBox>
  )
}

ActionButtons.propTypes = {
  /**
   * Indicates whether is the timeout button disabled or enabled.
   */
  timeoutDisabled: PropTypes.bool.isRequired,
  /**
   * Indicates whether is the substitutions button disabled or enabled.
   */
  substitutionsDisabled: PropTypes.bool.isRequired,
  /**
   * Indicates whether to render action buttons reversed.
   */
  reversed: PropTypes.bool,
  /**
   * Handler for the timeout button click event.
   */
  onSubstitutionClick: PropTypes.func.isRequired,
  /**
   * Handler for the substitutions button click event.
   */
  onTimeoutClick: PropTypes.func.isRequired,
}

export default ActionButtons
