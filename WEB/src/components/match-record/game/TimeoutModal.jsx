import React, { useEffect, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import SvgTimer from '~/assets/svg/timer.svg'

import { TimeoutRemainingIndicator } from 'src/styles/blocks/match-record'

import { Modal, PrimaryButton } from '../../common'

/**
 * Component rendering a modal window with timeout timer.
 *
 * @component
 */
const TimeoutModal = ({ startTime, timeoutLength, onFinishClick }) => {
  const [remainingTime, setRemainingTime] = useState(timeoutLength)

  useEffect(() => {
    const interval = setInterval(() => {
      const now = Date.now()
      const delta = ((timeoutLength * 1000) - (now - startTime)) / 1000
      // We keep the modal open for 3 seconds after the time has ran out
      if (delta < -3) {
        onFinishClick()
      }
      setRemainingTime(Math.max(delta, 0))
    }, 50)

    return () => clearInterval(interval)
  }, [])

  return (
    <Modal>
      <h1><FormattedMessage id="MATCH_RECORD.TIMEOUT" /></h1>

      <TimeoutRemainingIndicator
        highlighted={remainingTime < 10}
        progress={remainingTime / (timeoutLength * 1000)}
        ended={remainingTime < 0.1}
      >
        <span>{remainingTime.toFixed(1)}</span>
        <SvgTimer />
      </TimeoutRemainingIndicator>

      <PrimaryButton higher onClick={onFinishClick}>
        <FormattedMessage id="COMMON.FINISH" />
      </PrimaryButton>
    </Modal>
  )
}

TimeoutModal.defaultProps = {
  timeoutLength: 30,
}

TimeoutModal.propTypes = {
  /**
   * Unix timestamp in milliseconds identifying the time when the timeout was triggered.
   */
  startTime: PropTypes.number.isRequired,
  /**
   * Handles click event of the finish button.
   */
  onFinishClick: PropTypes.func.isRequired,
  /**
   * Length of the timeout in seconds.
   */
  timeoutLength: PropTypes.number,
}

export default TimeoutModal
