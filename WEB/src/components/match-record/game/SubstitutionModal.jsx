import React, { useMemo, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'

import SvgSubstitution from '~/assets/svg/substitution.svg'

import { COLORS } from 'src/constants'
import { MatchDetailPlayerPropType } from 'src/utils/prop-types'
import {
  GameSubstitutionBox, GameSubstitutionColumnBox, GameSubstitutionColumnNode,
  GameSubstitutionIcon, MatchRecordError,
} from 'src/styles/blocks/match-record'

import ContinueButton from '../ContinueButton'
import { Modal } from '../../common'

/**
 * Component rendering modal window with substitution options.
 *
 * @component
 */
const SubstitutionModal = ({ players, playersOnField, allowedPlayers, onFinishClick, onBackdropClick }) => {
  // Substitution is held in an array, 0 = outgoing, 1 = incoming
  const [substitution, setSubstitution] = useState([null, null])

  const outgoingPlayers = players.filter(({ id }) => playersOnField.includes(id))
  const incomingPlayers = players.filter(({ id }) => !playersOnField.includes(id))
  const orderedIncomingPlayers = allowedPlayers
    ? [...incomingPlayers].sort((pA, pB) => allowedPlayers.includes(pA.id)
      ? -1
      : allowedPlayers.includes(pB.id) ? 1 : pA.id - pB.id)
    : incomingPlayers

  const intl = useSelector(({ app }) => app.intl)
  const invalidSubstitution = useMemo(() =>
    intl?.formatMessage({ id: 'ERROR.INVALID_SUBSTITUTION' }), [intl])

  const confirmationMessage = useMemo(() =>
    intl?.formatMessage({ id: 'ERROR.CONFIRMATION' }), [intl])

  const isInvalidSubstitution = substitution[1] && !allowedPlayers.includes(substitution[1])

  const handleSubstituteClick = () => {
    onFinishClick(substitution)
    onBackdropClick()
  }

  return (
    <Modal onBackdropClick={onBackdropClick}>
      <h1><FormattedMessage id="MATCH_RECORD.SUBSTITUTION" /></h1>

      <GameSubstitutionBox>
        <GameSubstitutionColumnBox>
          {outgoingPlayers.map(({ name, id }, index) => (
            <GameSubstitutionColumnNode
              key={`outgoing-player-${index}`}
              accent={substitution[0] === id}
              accentColor={COLORS.RED_PRIMARY}
              onClick={() => setSubstitution(prevState => [id, prevState[1]])}
            >
              {name}{substitution[0] === id && ' 🠞'}
            </GameSubstitutionColumnNode>
          ))}
        </GameSubstitutionColumnBox>

        <GameSubstitutionIcon>
          <SvgSubstitution />
        </GameSubstitutionIcon>

        <GameSubstitutionColumnBox>
          {orderedIncomingPlayers.map(({ name, id }, index) => {
            const isInvalid = allowedPlayers && !allowedPlayers.includes(id)

            return (
              <GameSubstitutionColumnNode
                key={`incoming-player-${index}`}
                invalid={isInvalid}
                accent={substitution[1] === id}
                accentColor={COLORS.BLUE_PRIMARY}
                onClick={() => setSubstitution(prevState => [prevState[0], id])}
              >
                {substitution[1] === id && '🠜 '}{name}
              </GameSubstitutionColumnNode>
            )
          })}
        </GameSubstitutionColumnBox>
      </GameSubstitutionBox>

      <MatchRecordError errorLines={isInvalidSubstitution}>
        {isInvalidSubstitution && `${invalidSubstitution}!`}
      </MatchRecordError>

      <ContinueButton
        onClick={handleSubstituteClick}
        confirmationMessage={isInvalidSubstitution ? `${invalidSubstitution}, ${confirmationMessage}` : null}
        disabled={substitution.some(val => val === null)}
        messageId="MATCH_RECORD.SUBSTITUTE"
      />
    </Modal>
  )
}

SubstitutionModal.propTypes = {
  /**
   * Players available for this match as substitutions.
   */
  players: PropTypes.arrayOf(MatchDetailPlayerPropType).isRequired,
  /**
   * Players currently playing on the field.
   */
  playersOnField: PropTypes.arrayOf(PropTypes.number).isRequired,
  /**
   * List of player ids that should be specially marked as 'allowed' (all are 'allowed' if absent)
   */
  allowedPlayers: PropTypes.arrayOf(PropTypes.number),
  /**
   * Handles click event of the finish button.
   */
  onFinishClick: PropTypes.func.isRequired,
  /**
   * Handles click event on the backdrop.
   */
  onBackdropClick: PropTypes.func.isRequired,
}

export default SubstitutionModal
