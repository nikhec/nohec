import React from 'react'
import PropTypes from 'prop-types'

import SvgArrowUp from '~/assets/svg/arrow-up.svg'
import SvgArrowDown from '~/assets/svg/arrow-down.svg'

import {
  MatchRecordGameControlsArrow, MatchRecordGameControlsCol, MatchRecordPointsBox,
} from 'src/styles/blocks/match-record'

/**
 * Component rendering a single score box within match game recording.
 *
 * @component
 */
const ScoreBox = ({ scoreIndex, gameScore, onScoreChange }) => (
  <MatchRecordGameControlsCol>
    <MatchRecordGameControlsArrow isVisible={gameScore[scoreIndex] < 10}>
      <SvgArrowUp onClick={() => onScoreChange(scoreIndex, 1)} />
    </MatchRecordGameControlsArrow>
    <MatchRecordPointsBox>{gameScore[scoreIndex]}</MatchRecordPointsBox>
    <MatchRecordGameControlsArrow isVisible={gameScore[scoreIndex] > 0}>
      <SvgArrowDown onClick={() => onScoreChange(scoreIndex, -1)} />
    </MatchRecordGameControlsArrow>
  </MatchRecordGameControlsCol>
)

ScoreBox.propTypes = {
  /**
   * Index of the team the scoreBox is associated with, either 1 or 0
   */
  scoreIndex: PropTypes.number.isRequired,
  /**
   * Array of scores for the team
   */
  gameScore: PropTypes.arrayOf(PropTypes.number).isRequired,
  /**
   * Handler fof the score change events
   */
  onScoreChange: PropTypes.func.isRequired,
}

export default ScoreBox
