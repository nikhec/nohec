import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useDispatch } from 'react-redux'
import { FormattedMessage } from 'react-intl'

import { TEXTS, GAME_TYPE, STORAGE } from 'src/constants'
import { MatchRecordActions } from 'src/actions'
import { useAllowedPlayers } from 'src/hooks/use-allowed-players'
import { MatchGamePropType, MatchRecordPropType } from 'src/utils/prop-types'
import { MatchContentRowMatchType, MatchDetailBox } from 'src/styles/blocks/matches'
import { MatchRecordError, MatchRecordGameControls } from 'src/styles/blocks/match-record'

import ContinueButton from './ContinueButton'
import { ScoreBox, ActionButtons, TimeoutModal, SubstitutionModal } from './game'
import { MatchContentRow, MatchHeader } from '../match'

/**
 * Helper hook that abstracts component's state logic initialization and localStorage usage.
 *
 * @memberof MatchRecordGame
 * @inner
 * @param {MatchDetailPlayer[]} homePlayers - Selected home players for the game
 * @param {MatchDetailPlayer[]} awayPlayers - Selected away players for the game
 * @param {MatchDetailGame} activeGame - Current game in progress
 * @returns {MatchRecordGameState}
 */
const useMatchRecordGameState = (homePlayers, awayPlayers, activeGame) => {
  const [gameScore, setGameScore] = useState([0, 0])
  const [gameTimeouts, setGameTimeouts] = useState([1, 1])
  const [gameSubstitutions, setGameSubstitutions] = useState({ home: [], away: [] })

  useEffect(() => {
    const gameSetRecord = localStorage.getItem(STORAGE.GAME_SET_RECORD_PROGRESS)
    if (gameSetRecord) {
      const { gameId, setNumber, gameScore, gameTimeouts, gameSubstitutions } = JSON.parse(gameSetRecord)

      if (gameId !== activeGame.id || setNumber !== activeGame.sets.length + 1) {
        localStorage.removeItem(STORAGE.GAME_SET_RECORD_PROGRESS)
        return
      }

      if (gameScore && gameScore.length === 2) {
        setGameScore([
          Math.max(0, Math.min(10, gameScore[0])),
          Math.max(0, Math.min(10, gameScore[1])),
        ])
      }

      if (gameTimeouts && gameTimeouts.length === 2) {
        setGameTimeouts([
          Math.max(0, Math.min(1, gameTimeouts[0])),
          Math.max(0, Math.min(1, gameTimeouts[1])),
        ])
      }

      const homePlayersMap = homePlayers.map(({ id }) => id)
      const awayPlayersMap = awayPlayers.map(({ id }) => id)

      const getTeamSubstitutions = (teamPlayersMap, teamSubstitutions) => {
        const substitutionsValid = teamSubstitutions.length !== 0 && teamSubstitutions.every(
          ([outgoing, incoming]) => teamPlayersMap.includes(incoming) && teamPlayersMap.includes(outgoing)
        )

        return substitutionsValid
          ? teamSubstitutions
          : []
      }

      if (gameSubstitutions) {
        setGameSubstitutions({
          home: getTeamSubstitutions(homePlayersMap, gameSubstitutions.home ?? []),
          away: getTeamSubstitutions(awayPlayersMap, gameSubstitutions.away ?? []),
        })
      }
    }
  }, [])

  useEffect(() => {
    localStorage.setItem(STORAGE.GAME_SET_RECORD_PROGRESS, JSON.stringify({
      gameId: activeGame.id,
      setNumber: activeGame.sets.length + 1,
      gameScore,
      gameTimeouts,
      gameSubstitutions,
    }))
  }, [gameScore, gameTimeouts, gameSubstitutions])

  return {
    gameScore,
    setGameScore,
    gameTimeouts,
    setGameTimeouts,
    gameSubstitutions,
    setGameSubstitutions,
  }
}

/**
 * Component displaying a single set recording within a partial game.
 *
 * @component
 */
const MatchRecordGame = ({ matchRecord: { id: matchId, score, homeTeam, awayTeam, games }, activeGame }) => {
  const {
    gameScore,
    gameTimeouts,
    gameSubstitutions,
    setGameScore,
    setGameTimeouts,
    setGameSubstitutions,
  } = useMatchRecordGameState(homeTeam.players, awayTeam.players, activeGame)

  const [apiErrors, setApiErrors] = useState([])
  const [timeoutStart, setTimeoutStart] = useState(null)
  const [substitutionProps, setSubstitutionProps] = useState(null)

  const handleTimeoutClick = teamIndex => {
    setGameTimeouts(prevState => {
      const newState = [...prevState]
      newState[teamIndex] = 0
      return newState
    })
    setTimeoutStart(Date.now())
  }

  const dispatch = useDispatch()
  const handleSetSubmission = useCallback(() => {
    setApiErrors([])

    dispatch(MatchRecordActions.submitSet({
      matchId,
      gameId: activeGame.id,
      result: gameScore,
      timeouts: gameTimeouts,
      substitutions: gameSubstitutions,
      onError: setApiErrors,
    }))
  }, [gameScore, gameTimeouts, gameSubstitutions, dispatch])

  const handleScoreChange = (index, delta) => setGameScore(prevState => {
    const newState = [...prevState]
    newState[index] = Math.max(0, Math.min(10, prevState[index] + delta))
    return newState
  })

  const {
    allowedHomePlayers,
    allowedAwayPlayers,
  } = useAllowedPlayers(homeTeam.players, awayTeam.players, games)

  const handleSubstitutionChange = (newSubstitution, teamIndex) =>
    setGameSubstitutions(({ home, away }) => ({
      away: teamIndex === 1 ? [...away, newSubstitution] : away,
      home: teamIndex === 0 ? [...home, newSubstitution] : home,
    }))

  const buttonDisabled = useMemo(() =>
    !gameScore.some(val => val === 10) || gameScore[0] === gameScore[1], [gameScore])

  const playersOnField = useMemo(() => {
    const reduceFunc = (substitutions, players) => substitutions.reduce((acc, [outgoing, incoming]) => {
      const index = acc.findIndex(p => p === outgoing)
      if (index !== -1) {
        acc[index] = incoming
      } else {
        console.error('Invalid substitution encountered!')
      }
      return acc
    }, players.slice(0))

    const [homePlayers, awayPlayers] = activeGame.sets.length === 1
      ? [
        reduceFunc(activeGame.sets[0].homeSubstitutions, activeGame.homePlayers),
        reduceFunc(activeGame.sets[0].awaySubstitutions, activeGame.awayPlayers),
      ] : [
        activeGame.homePlayers,
        activeGame.awayPlayers,
      ]

    return [
      reduceFunc(gameSubstitutions.home, homePlayers),
      reduceFunc(gameSubstitutions.away, awayPlayers),
    ]
  }, [gameSubstitutions, activeGame])

  const getActionButtons = (teamIndex, players) => (
    <ActionButtons
      timeoutDisabled={gameTimeouts[teamIndex] === 0}
      onTimeoutClick={() => handleTimeoutClick(teamIndex)}
      reversed={teamIndex === 0}
      substitutionsDisabled={activeGame.type === GAME_TYPE.SINGLE}
      onSubstitutionClick={() => setSubstitutionProps({
        players,
        playersOnField: playersOnField[teamIndex],
        allowedPlayers: teamIndex === 0 ? allowedHomePlayers : allowedAwayPlayers,
        onFinishClick: substitution => handleSubstitutionChange(substitution, teamIndex),
      })}
    />
  )

  return (
    <MatchDetailBox>
      {timeoutStart && <TimeoutModal startTime={timeoutStart} onFinishClick={() => setTimeoutStart(null)} />}
      {substitutionProps && (
        <SubstitutionModal
          {...substitutionProps}
          onBackdropClick={() => setSubstitutionProps(null)}
        />
      )}

      <MatchHeader
        homeTeam={homeTeam}
        awayTeam={awayTeam}
        headline={<h1>{score.join(' : ')}</h1>}
        imageSize={100}
        isSingleLine
      />

      <MatchContentRow
        forceColumn
        omitBottomMargin
        leftContent={getActionButtons(0, homeTeam.players)}
        rightContent={getActionButtons(1, awayTeam.players)}
        centerContent={
          <>
            <h1>{activeGame.sets.length + 1}. <FormattedMessage id="MATCH_RECORD.SET" /></h1>
            <MatchContentRowMatchType>
              {activeGame.typeOrder}. <FormattedMessage id={TEXTS.GAME_TYPES[activeGame.type]} />
            </MatchContentRowMatchType>

            <MatchRecordGameControls>
              <ScoreBox scoreIndex={0} gameScore={gameScore} onScoreChange={handleScoreChange} />
              <h1>:</h1>
              <ScoreBox scoreIndex={1} gameScore={gameScore} onScoreChange={handleScoreChange} />
            </MatchRecordGameControls>
          </>
        }
      />

      <MatchRecordError errorLines={apiErrors.length}>
        {apiErrors.join('\n')}
      </MatchRecordError>

      <ContinueButton
        onClick={handleSetSubmission}
        disabled={buttonDisabled}
        messageId="MATCH_RECORD.FINISH_SET"
      />
    </MatchDetailBox>
  )
}

MatchRecordGame.propTypes = {
  /**
   * Information about the match recording.
   */
  matchRecord: MatchRecordPropType.isRequired,
  /**
   * Information about match record's active game.
   */
  activeGame: MatchGamePropType.isRequired,
}

export default MatchRecordGame
