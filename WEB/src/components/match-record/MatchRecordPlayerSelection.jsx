import React, { useCallback, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage } from 'react-intl'

import { TEXTS } from 'src/constants'
import { MatchRecordActions } from 'src/actions'
import { useAllowedPlayers } from 'src/hooks/use-allowed-players'
import { MatchRecordPropType, MatchGamePropType } from 'src/utils/prop-types'
import { MatchContentRowMatchType, MatchDetailBox } from 'src/styles/blocks/matches'
import { MatchRecordError } from 'src/styles/blocks/match-record'

import ContinueButton from './ContinueButton'
import { MatchContentRow, MatchHeader } from '../match'
import { PlayerSelect } from '../common'

/**
 * Component displaying player selection screen within match recording.
 *
 * @component
 */
const MatchRecordPlayerSelection = ({ matchRecord: { id: matchId, score, homeTeam, awayTeam, games }, activeGame }) => {
  const requiredPlayerCount = activeGame.type

  const [apiErrors, setApiErrors] = useState([])
  const [homePlayers, setHomePlayers] = useState(Array(requiredPlayerCount).fill(null))
  const [awayPlayers, setAwayPlayers] = useState(Array(requiredPlayerCount).fill(null))

  const buttonDisabled = useMemo(() =>
    homePlayers.length !== requiredPlayerCount || homePlayers.some(player => player === null)
    || awayPlayers.length !== requiredPlayerCount || awayPlayers.some(player => player === null),
  [homePlayers, awayPlayers]
  )

  const intl = useSelector(({ app }) => app.intl)
  const selectPlayerPlaceholder = useMemo(() =>
    intl?.formatMessage({ id: 'MATCH_RECORD.SELECT_PLAYER' }), [intl])

  const invalidComposition = useMemo(() =>
    intl?.formatMessage({ id: 'ERROR.INVALID_COMPOSITION' }), [intl])

  const confirmationMessage = useMemo(() =>
    intl?.formatMessage({ id: 'ERROR.CONFIRMATION' }), [intl])

  const dispatch = useDispatch()
  const handleTeamSubmission = useCallback(() => {
    setApiErrors([])

    dispatch(MatchRecordActions.selectPlayers({
      matchId,
      gameId: activeGame.id,
      homePlayers,
      awayPlayers,
      onError: setApiErrors,
    }))
  }, [dispatch, homePlayers, awayPlayers])

  const getSelectionHandler = (setState, index) => newSelection => setState(prevState => {
    const newState = prevState.map(id => id === newSelection ? null : id)
    newState[index] = newSelection
    return newState
  })

  const {
    allowedHomePlayers,
    allowedAwayPlayers,
  } = useAllowedPlayers(homeTeam.players, awayTeam.players, games)

  const getTeamColumn = (players, teamPlayers, allowedPlayers, setState) => players.map((player, index) => (
    <PlayerSelect
      canRemove
      key={`team-players-${index}`}
      players={teamPlayers}
      allowedPlayers={allowedPlayers}
      selectedPlayerId={player}
      onSelection={getSelectionHandler(setState, index)}
      placeholder={selectPlayerPlaceholder}
    />
  ))

  const isInvalidComposition = useMemo(() => (
    homePlayers.some(id => id && !allowedHomePlayers.includes(id))
    || awayPlayers.some(id => id && !allowedAwayPlayers.includes(id))
  ), [homePlayers, awayPlayers, allowedHomePlayers, allowedAwayPlayers])

  return (
    <MatchDetailBox>
      <MatchHeader
        homeTeam={homeTeam}
        awayTeam={awayTeam}
        headline={<h1>{score.join(' : ')}</h1>}
        imageSize={100}
        isSingleLine
      />

      <MatchContentRow
        forceColumn
        leftContent={getTeamColumn(homePlayers, homeTeam.players, allowedHomePlayers, setHomePlayers)}
        rightContent={getTeamColumn(awayPlayers, awayTeam.players, allowedAwayPlayers, setAwayPlayers)}
        centerContent={
          <>
            <h1><FormattedMessage id="MATCH_RECORD.TEAM_COMPOSITION" /></h1>
            <MatchContentRowMatchType>
              {activeGame.typeOrder}. <FormattedMessage id={TEXTS.GAME_TYPES[activeGame.type]} />
            </MatchContentRowMatchType>
          </>
        }
      />

      <MatchRecordError errorLines={isInvalidComposition ? 1 : apiErrors.length}>
        {(isInvalidComposition && `${invalidComposition}!`) || apiErrors.join('\n')}
      </MatchRecordError>

      <ContinueButton
        disabled={buttonDisabled}
        messageId="COMMON.CONTINUE"
        confirmationMessage={isInvalidComposition ? `${invalidComposition}, ${confirmationMessage}` : null}
        onClick={handleTeamSubmission}
      />
    </MatchDetailBox>
  )
}

MatchRecordPlayerSelection.propTypes = {
  /**
   * Information about the match recording.
   */
  matchRecord: MatchRecordPropType.isRequired,
  /**
   * Information about match record's active game.
   */
  activeGame: MatchGamePropType.isRequired,
}

export default MatchRecordPlayerSelection
