import React from 'react'
import { FormattedMessage } from 'react-intl'
import { useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'

import { API } from 'src/constants'
import { MatchDetailButtonBox } from 'src/styles/blocks/matches'
import { MatchEnterErrorBox } from 'src/styles/blocks/match-record'

import { PrimaryButton } from '../common'

/**
 * Component rendering errors on failed attempt to enter match recording.
 *
 * @component
 */
const MatchEnterError = ({ id, message }) => {
  const history = useHistory()
  const handleReturnBackClick = () => history.push(`${API.MATCH}?id=${id}`)

  return (
    <MatchEnterErrorBox>
      <h1><FormattedMessage id="MATCH_RECORD.ATTEMPT_FAILED" /></h1>
      <h2><FormattedMessage id={message} /></h2>
      <h4><FormattedMessage id="MATCH_RECORD.OR" /></h4>

      <MatchDetailButtonBox>
        <PrimaryButton higher onClick={handleReturnBackClick}>
          <FormattedMessage id="MATCH_RECORD.RETURN_BACK" />
        </PrimaryButton>
      </MatchDetailButtonBox>
    </MatchEnterErrorBox>
  )
}

MatchEnterError.propTypes = {
  /**
   * Unique identifier of the match.
   */
  id: PropTypes.number.isRequired,
  /**
   * Error message.
   */
  message: PropTypes.string.isRequired,
}

export default MatchEnterError
