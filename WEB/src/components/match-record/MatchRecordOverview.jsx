import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { FormattedMessage } from 'react-intl'

import { TEXTS } from 'src/constants'
import { MatchRecordActions } from 'src/actions'
import { MatchRecordPropType } from 'src/utils/prop-types'
import { MatchContentRowMatchType, MatchDetailBox, MatchDetailButtonBox } from 'src/styles/blocks/matches'
import { MatchOverviewScoreField, MatchRecordOverviewBox } from 'src/styles/blocks/match-record'

import { MatchContentRow, MatchHeader } from '../match'
import { PrimaryButton } from '../common'

/**
 * Component displaying overview of the match recording.
 *
 * @component
 */
const MatchRecordOverview = ({ matchRecord: { games, score, homeTeam, awayTeam } }) => {
  const dispatch = useDispatch()
  const handleOverviewConfirmation = useCallback(() => {
    dispatch(MatchRecordActions.overviewConfirmed())
  }, [dispatch])

  return (
    <MatchDetailBox>
      <MatchHeader
        homeTeam={homeTeam}
        awayTeam={awayTeam}
        headline={
          <>
            <h1>{score.join(' : ')}</h1>
            <h1><FormattedMessage id="MATCH_RECORD.OVERVIEW" /></h1>
          </>
        }
      />

      <MatchContentRow
        forceColumn
        omitBottomMargin
        centerContent={
          <MatchRecordOverviewBox>
            {games.map((game, index) => (
              <MatchContentRowMatchType extraMargin key={`overview-game-${index}`}>
                {game.typeOrder}. <FormattedMessage id={TEXTS.GAME_TYPES[game.type]} />
                <MatchOverviewScoreField>
                  {game.sets.reduce((acc, { homePoints, awayPoints }) => [
                    acc[0] + Math.max(0, homePoints - 9),
                    acc[1] + Math.max(0, awayPoints - 9),
                  ], [0, 0]).join(' : ')}
                </MatchOverviewScoreField>
              </MatchContentRowMatchType>
            ))}
          </MatchRecordOverviewBox>
        }
      />

      <MatchDetailButtonBox>
        <PrimaryButton higher hasSubmitTimeout onClick={handleOverviewConfirmation}>
          <FormattedMessage id="COMMON.CONFIRM" />
        </PrimaryButton>
      </MatchDetailButtonBox>
    </MatchDetailBox>
  )
}

MatchRecordOverview.propTypes = {
  /**
   * Information about the match recording.
   */
  matchRecord: MatchRecordPropType.isRequired,
}

export default MatchRecordOverview
