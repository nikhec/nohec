import React, { useCallback, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage } from 'react-intl'

import { MatchRecordPropType } from 'src/utils/prop-types'
import { MatchDetailBox } from 'src/styles/blocks/matches'
import { MatchRecordError, MatchRecordNote } from 'src/styles/blocks/match-record'
import { MatchRecordActions } from 'src/actions'

import ContinueButton from './ContinueButton'
import { MatchHeader } from '../match'

/**
 * Component displaying a block text field for game note.
 *
 * @component
 */
const MatchRecordSummation = ({ matchRecord: { id: matchId, score, homeTeam, awayTeam } }) => {
  const [apiErrors, setApiErrors] = useState([])
  const [note, setNote] = useState('')

  const dispatch = useDispatch()
  const handleNoteSubmission = useCallback(() => {
    setApiErrors([])

    dispatch(MatchRecordActions.submitNote({
      matchId,
      note,
      onError: setApiErrors,
    }))
  }, [note, dispatch])

  const intl = useSelector(({ app }) => app.intl)
  const noteMessage = useMemo(() => intl.formatMessage({ id: 'MATCH_RECORD.NOTE' }), [intl])

  return (
    <MatchDetailBox>
      <MatchHeader
        homeTeam={homeTeam}
        awayTeam={awayTeam}
        headline={
          <>
            <h1>{score.join(' : ')}</h1>
            <h1><FormattedMessage id="MATCH_RECORD.SUMMATION" /></h1>
          </>
        }
      />

      <MatchRecordNote
        placeholder={noteMessage}
        maxlength={1000}
        onChange={({ target: { value } }) => setNote(value)}
      />

      <MatchRecordError errorLines={apiErrors.length}>
        {apiErrors.join('\n')}
      </MatchRecordError>

      <ContinueButton onClick={handleNoteSubmission} messageId="COMMON.CONTINUE" />
    </MatchDetailBox>
  )
}

MatchRecordSummation.propTypes = {
  /**
   * Information about the match recording.
   */
  matchRecord: MatchRecordPropType.isRequired,
}

export default MatchRecordSummation
