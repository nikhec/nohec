import React, { useCallback, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage } from 'react-intl'

import { MatchRecordActions } from 'src/actions'
import { MatchRecordPropType } from 'src/utils/prop-types'
import { MatchDetailBox } from 'src/styles/blocks/matches'
import { MatchRecordError } from 'src/styles/blocks/match-record'

import ContinueButton from './ContinueButton'
import { PlayerSelect } from '../common'
import { MatchHeader } from '../match'

/**
 * Component displaying captain selection screen within match recording.
 *
 * @component
 */
const MatchRecordCaptainSelection = ({ matchRecord: { id: matchId, homeCaptain, awayCaptain, homeTeam, awayTeam } }) => {
  const getDefaultCptValue = (players, selectedId) => players.find(player => player.id === selectedId)?.id ?? null

  const [apiErrors, setApiErrors] = useState([])
  const [homeCpt, setHomeCpt] = useState(getDefaultCptValue(homeTeam.players, homeCaptain))
  const [awayCpt, setAwayCpt] = useState(getDefaultCptValue(awayTeam.players, awayCaptain))

  const intl = useSelector(({ app }) => app.intl)
  const selectPlaceholderText = useMemo(() =>
    intl?.formatMessage({ id: 'MATCH_RECORD.SELECT_CAPTAIN' }), [intl])

  const dispatch = useDispatch()
  const handleCaptainSubmission = useCallback(() => {
    setApiErrors([])

    dispatch(MatchRecordActions.selectCaptains({
      matchId,
      captains: { home: homeCpt, away: awayCpt },
      onError: setApiErrors,
    }))
  }, [homeCpt, awayCpt, dispatch])

  const buttonDisabled = awayCpt === null || homeCpt === null

  return (
    <MatchDetailBox>
      <MatchHeader
        homeTeam={homeTeam}
        awayTeam={awayTeam}
        homeTeamCol={
          <PlayerSelect
            players={homeTeam.players}
            selectedPlayerId={homeCpt}
            onSelection={newSelection => setHomeCpt(newSelection)}
            placeholder={selectPlaceholderText}
            fullOverflow
          />
        }
        awayTeamCol={
          <PlayerSelect
            players={awayTeam.players}
            selectedPlayerId={awayCpt}
            onSelection={newSelection => setAwayCpt(newSelection)}
            placeholder={selectPlaceholderText}
            fullOverflow
          />
        }
        headline={<h1><FormattedMessage id="MATCH_RECORD.CAPTAIN_SELECTION" /></h1>}
      />

      <MatchRecordError errorLines={apiErrors.length}>
        {apiErrors.join('\n')}
      </MatchRecordError>

      <ContinueButton
        onClick={handleCaptainSubmission}
        disabled={buttonDisabled}
        messageId="COMMON.CONTINUE"
      />
    </MatchDetailBox>
  )
}

MatchRecordCaptainSelection.propTypes = {
  /**
   * Information about the match recording.
   */
  matchRecord: MatchRecordPropType.isRequired,
}

export default MatchRecordCaptainSelection
