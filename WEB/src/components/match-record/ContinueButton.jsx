import React, { forwardRef, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import { COLORS } from 'src/constants'
import { MatchDetailButtonBox } from 'src/styles/blocks/matches'

import { PrimaryButton, ConfirmationModal } from '../common'

/**
 * Block displaying continue to the next step button at the bottom of each match recording step.
 *
 * @component
 */
const ContinueButton = forwardRef(({ disabled, messageId, confirmationMessage, onClick }, ref) => {
  const [showConfirmation, setShowConfirmation] = useState(false)

  const handleClick = () => {
    if (confirmationMessage) {
      setShowConfirmation(true)
    } else {
      onClick()
    }
  }

  const handleConfirmation = () => {
    setShowConfirmation(false)
    onClick()
  }

  return (
    <MatchDetailButtonBox higher>
      {showConfirmation && (
        <ConfirmationModal
          confirmationMessage={confirmationMessage}
          severityColor={COLORS.RED_PRIMARY}
          onConfirm={handleConfirmation}
          onCancel={() => setShowConfirmation(false)}
        />
      )}

      <PrimaryButton
        higher
        hasSubmitTimeout
        disabled={disabled}
        ref={ref}
        onClick={handleClick}
      >
        <FormattedMessage id={messageId} />
      </PrimaryButton>
    </MatchDetailButtonBox>
  )
})

ContinueButton.propTypes = {
  /**
   * Indicates whether is the button disabled or not
   */
  disabled: PropTypes.bool,
  /**
   * Id of the localized message to display within the button
   */
  messageId: PropTypes.string.isRequired,
  /**
   * Localized message, if present, shows confirmation dialog with the message before triggering provided click handler
   */
  confirmationMessage: PropTypes.string,
  /**
   * Click handler
   */
  onClick: PropTypes.func.isRequired,
}

export default ContinueButton
