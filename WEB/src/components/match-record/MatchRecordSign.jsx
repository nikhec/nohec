import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { useDispatch } from 'react-redux'
import { FormattedMessage } from 'react-intl'

import { MatchActions, MatchRecordActions } from 'src/actions'
import { MatchRecordPropType } from 'src/utils/prop-types'
import { useFetchedNullableState } from 'src/hooks/use-fetched-state'
import { selectMatchRefereeCount } from 'src/selectors/matchDetailSelectors'
import { ContentGroup } from 'src/styles/blocks/page'
import { MatchDetailBox } from 'src/styles/blocks/matches'
import {
  MatchRecordError, MatchRecordSignFields, MatchRecordSignFieldsBox, MatchRecordSignHeadline, MatchRecordSignContent,
} from 'src/styles/blocks/match-record'

import ContinueButton from './ContinueButton'
import { MatchHeader } from '../match'
import { ErrorBanner, LoadingBanner } from '../common'

/**
 * Component displaying a page dedicated to signing and closing match recording.
 *
 * @component
 */
const MatchRecordSign = ({ matchRecord: { id: matchId, score, time, date, homeTeam, awayTeam } }) => {
  const {
    isFetching,
    state: refereeCount,
    error,
  } = useFetchedNullableState(MatchActions.loadMatchDetail, { matchId }, selectMatchRefereeCount)

  const hasReferee = useMemo(() => refereeCount && refereeCount > 0, [refereeCount])

  const [apiErrors, setApiErrors] = useState([])
  const [passwords, setPasswords] = useState([[], [], []])
  const [buttonDisabled, setButtonDisabled] = useState(true)
  const [activeBlock, setActiveBlock] = useState(0)

  const fieldRefs = useRef([[], [], []])
  const buttonRef = useRef(null)

  const dispatch = useDispatch()
  const handleSignSubmission = useCallback(() => {
    setApiErrors([])

    const passwordStrings = passwords.map(pass => pass.join(''))
    dispatch(MatchRecordActions.submitSign({
      matchId,
      passwords: {
        home: passwordStrings[0],
        away: passwordStrings[1],
        referee: passwordStrings[2],
      },
      onError: setApiErrors,
    }))
  }, [passwords, dispatch])

  useEffect(() => {
    if (fieldRefs.current[0][0]) {
      fieldRefs.current[0][0].focus()
    }
  }, [fieldRefs.current])

  const handleSignFieldChange = useCallback(({ target: { value } }, index, blockIndex) => {
    setPasswords(prevState => {
      const newState = [...prevState]
      newState[blockIndex][index] = value.toUpperCase()

      const allPasswords = newState.slice(0, hasReferee ? 3 : 2)

      if (allPasswords.some(pass => pass.length !== 4 || pass.some(char => char === '' || char == null))) {
        setButtonDisabled(true)
      } else {
        setButtonDisabled(false)
      }
      return newState
    })

    if (!value) {
      return
    }

    if (index < 3) {
      const element = fieldRefs.current[blockIndex][index + 1]
      element.focus()
      element.select()
    } else if ((hasReferee && blockIndex < 2) || (!hasReferee && blockIndex < 1)) {
      const element = fieldRefs.current[blockIndex + 1][0]
      element.focus()
      element.select()
    }
  }, [passwords, hasReferee, fieldRefs.current, buttonRef.current])

  useEffect(() => {
    if (buttonDisabled === false) {
      buttonRef.current.focus()
    }
  }, [buttonDisabled, buttonRef.current])

  const handleSignFieldKeyDown = useCallback(({ keyCode }, index, blockIndex) => {
    if (keyCode === 8 && !passwords[blockIndex][index]) {
      if (index > 0) {
        fieldRefs.current[blockIndex][index - 1].focus()
      } else if (blockIndex > 0) {
        fieldRefs.current[blockIndex - 1][3].focus()
      }
    }
  }, [passwords, fieldRefs.current])

  const handleBlockPaste = async (e, blockIndex) => {
    e.preventDefault()

    try {
      const text = await navigator.clipboard.readText()
      const chars = text.padEnd(4).toUpperCase().split('').slice(0, 4)

      setPasswords(prevState => {
        const newState = [...prevState]
        newState[blockIndex] = chars
        return newState
      })
      handleSignFieldChange({ target: { value: chars[3] } }, 3, blockIndex)
    } catch (e) {
      console.error('Could not read clipboard content')
    }
  }

  const getSignFields = (headlineId, blockIndex) => (
    <div onPaste={e => handleBlockPaste(e, blockIndex)}>
      <MatchRecordSignHeadline onClick={() => setActiveBlock(blockIndex)} active={activeBlock === blockIndex}>
        <FormattedMessage id={headlineId} />
      </MatchRecordSignHeadline>

      <MatchRecordSignFieldsBox>
        {Array(4).fill(null).map((_, index) => (
          <MatchRecordSignFields
            key={`sign-box-${index}`}
            maxLength={1}
            value={passwords[blockIndex][index] ?? ''}
            onKeyDown={e => handleSignFieldKeyDown(e, index, blockIndex)}
            onChange={e => handleSignFieldChange(e, index, blockIndex)}
            onFocus={() => setActiveBlock(blockIndex)}
            ref={element => fieldRefs.current[blockIndex][index] = element}
          />
        ))}
      </MatchRecordSignFieldsBox>
    </div>
  )

  return (
    <MatchDetailBox>
      <MatchHeader
        homeTeam={homeTeam}
        awayTeam={awayTeam}
        headline={
          <>
            <h1>{score.join(' : ')}</h1>
            <h3>{time}</h3>
            <h2>{date}</h2>
          </>
        }
      />

      {!isFetching && !error ? (
        <>
          <MatchRecordSignContent>
            {getSignFields('MATCH_RECORD.HOME_CAPTAIN', 0)}
            {getSignFields('MATCH_RECORD.AWAY_CAPTAIN', 1)}
            {hasReferee ? getSignFields('MATCH_RECORD.REFEREE', 2) : null}
          </MatchRecordSignContent>

          <MatchRecordError errorLines={apiErrors.length}>
            {apiErrors.join('\n')}
          </MatchRecordError>
        </>
      ) : error
        ? <ContentGroup omitBottomMargin><ErrorBanner messageId={error} /></ContentGroup>
        : <LoadingBanner />}

      <ContinueButton
        onClick={handleSignSubmission}
        disabled={buttonDisabled}
        ref={buttonRef}
        messageId="MATCH_RECORD.SIGN"
      />
    </MatchDetailBox>
  )
}

MatchRecordSign.propTypes = {
  /**
   * Information about the match recording.
   */
  matchRecord: MatchRecordPropType.isRequired,
}

export default MatchRecordSign
