import React, { memo, useEffect, useMemo, useRef, useState } from 'react'
import { XYPlot, Hint, ArcSeries, LineSeries, HorizontalGridLines, YAxis, Crosshair } from 'react-vis'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import { COLORS } from 'src/constants'
import { TeamActions } from 'src/actions'
import { selectTeamStatistics } from 'src/selectors/teamSelectors'
import { useFetchedState } from 'src/hooks/use-fetched-state'
import {
  StaticsLabel, StatisticsColumn, StatisticsCrosshairLine,
  StatisticsCrosshairText, StatisticsGraphName, StatisticsHint,
} from 'src/styles/blocks/statistics'

/**
 * Team detail page's statistics graphs.
 *
 * @component
 */
const TeamDetailStatistics = memo(({ teamId }) => {
  const { state } = useFetchedState(TeamActions.loadTeamStatistics, { teamId }, selectTeamStatistics)

  const { roundStats, lastFiveMatches } = state

  const lastMatchesRef = useRef(null)
  const [lastMatchesHint, setLastMatchesHint] = useState(null)
  const [lastMatchesGraph, setLastMatchesGraph] = useState([
    { color: COLORS.BLUE_PRIMARY, radius0: 50, radius: 75, angle: 0, angle0: 0 },
    { color: COLORS.GRAY_DARK, radius0: 50, radius: 75, angle: (2 * Math.PI) / 3, angle0: (4 * Math.PI) / 3 },
    { color: COLORS.RED_PRIMARY, radius0: 50, radius: 75, angle: (4 * Math.PI) / 3, angle0: 2 * Math.PI },
  ])

  const [roundStatsCrosshair, setRoundStatsCrosshair] = useState(null)
  const [roundStatsGraph, setRoundStatsGraph] = useState({
    wins: Array(14).fill(0).map((_, index) => ({ x: index + 1, y: 4 })),
    draws: Array(14).fill(0).map((_, index) => ({ x: index + 1, y: 0 })),
    losses: Array(14).fill(0).map((_, index) => ({ x: index + 1, y: 2 })),
  })

  const matchesCount = lastFiveMatches?.draws + lastFiveMatches?.losses + lastFiveMatches?.wins

  useEffect(() => {
    if (lastFiveMatches) {
      const { wins, losses, draws } = lastFiveMatches

      const pointSectionSize = (2 * Math.PI) / (wins + losses + draws)
      setLastMatchesGraph([
        { color: COLORS.BLUE_PRIMARY, radius0: 50, radius: 75, angle: 0, angle0: wins * pointSectionSize },
        { color: COLORS.GRAY_DARK, radius0: 50, radius: 75, angle: wins * pointSectionSize, angle0: (wins + draws) * pointSectionSize },
        { color: COLORS.RED_PRIMARY, radius0: 50, radius: 75, angle: (wins + draws) * pointSectionSize, angle0: 2 * Math.PI },
        {
          color: 'transparent',
          radius0: 0,
          radius: 80,
          angle: 0,
          angle0: wins * pointSectionSize,
          id: 0,
          messageId: 'MATCH.WINS',
          value: wins,
        },
        {
          color: 'transparent',
          radius0: 0,
          radius: 80,
          angle: wins * pointSectionSize,
          angle0: (wins + draws) * pointSectionSize,
          id: 1,
          messageId: 'MATCH.DRAWS',
          value: draws,
        },
        {
          color: 'transparent',
          radius0: 0,
          radius: 80,
          angle: (wins + draws) * pointSectionSize,
          angle0: 2 * Math.PI,
          id: 2,
          messageId: 'MATCH.LOSSES',
          value: losses,
        },
      ])
    }

    if (roundStats) {
      const accumulateValues = (values, key) => values.reduce((acc, value, index) => {
        acc.push({ x: index + 1, y: value[key] + (acc[index - 1]?.y ?? 0) })
        return acc
      }, [])

      setRoundStatsGraph({
        wins: accumulateValues(roundStats, 'wins'),
        draws: accumulateValues(roundStats, 'draws'),
        losses: accumulateValues(roundStats, 'losses'),
      })
    }
  }, [state])

  const roundStatsYTicks = useMemo(() => {
    const { wins, losses, draws } = roundStatsGraph
    const maxValue = Math.max(wins[wins.length - 1]?.y, losses[losses.length - 1]?.y, draws[draws.length - 1]?.y) || 0

    if (maxValue < 7) {
      return Array(maxValue + 1)
        .fill(0).map((_, index) => index)
    } else {
      return Array((((maxValue % 2 === 0) ? maxValue : maxValue + 1) / 2) + 1)
        .fill(0).map((_, index) => index * 2)
    }
  }, [roundStatsGraph])

  const handleArcOver = (value, isEntering, target) => {
    const { id, y } = value
    if (!(lastMatchesHint?.id === id && isEntering)
      && (!target || !lastMatchesRef.current?.contains(target))
    ) {
      setLastMatchesHint(isEntering ? { ...value, y: 120 - y } : null)

      const radiusDelta = isEntering ? 5 : -5
      setLastMatchesGraph(prevState =>
        prevState.map((val, index) => ({
          ...val,
          radius0: index === id ? val.radius0 + radiusDelta : val.radius0,
          radius: index === id ? val.radius + radiusDelta : val.radius,
        }))
      )
    }
  }

  return (
    <>
      <StatisticsColumn>
        <XYPlot width={165} height={165} xDomain={[0, 16]} yDomain={[0, 25]}>
          <StaticsLabel>
            <span>{matchesCount || ''}</span>
          </StaticsLabel>
          <ArcSeries
            animation="noWobble"
            colorType="literal"
            radiusType="literal"
            center={{ x: 6, y: 9 }}
            padAngle={Math.PI / 40}
            data={lastMatchesGraph}
            onValueMouseOver={value => handleArcOver(value, true)}
            onValueMouseOut={(value, e) => handleArcOver(value, false, e.event.relatedTarget)}
          />

          {lastMatchesHint && (
            <Hint value={lastMatchesHint} x={lastMatchesHint.x} y={lastMatchesHint.y}>
              <StatisticsHint ref={lastMatchesRef} onMouseLeave={e => handleArcOver(lastMatchesHint, false, e.relatedTarget)}>
                <h3><FormattedMessage id={lastMatchesHint.messageId} /></h3>
                <span>
                  {lastMatchesHint.value} ({Math.round((lastMatchesHint.value / matchesCount) * 100)}%)
                </span>
              </StatisticsHint>
            </Hint>
          )}
        </XYPlot>

        <StatisticsGraphName>
          <FormattedMessage id="MATCH.LAST_MATCHES" />
        </StatisticsGraphName>
      </StatisticsColumn>

      <StatisticsColumn>
        <XYPlot
          width={300}
          height={165}
          margin={{ right: 40, left: 10, bottom: 10 }}
          onMouseLeave={() => setRoundStatsCrosshair(null)}
        >
          <HorizontalGridLines tickTotal={roundStatsYTicks.length} />
          <YAxis
            hideLine
            tickValues={roundStatsYTicks}
            tickFormat={val => Math.round(val)}
            orientation="right"
            style={{ line: { stroke: 'none' } }}
          />
          <LineSeries animation="noWobble" data={roundStatsGraph.draws} color={COLORS.GRAY_DARK} />
          <LineSeries animation="noWobble" data={roundStatsGraph.wins} color={COLORS.BLUE_PRIMARY} />
          <LineSeries
            animation="noWobble"
            data={roundStatsGraph.losses}
            color={COLORS.RED_PRIMARY}
            onNearestX={({ x }) => {
              if (x !== roundStatsCrosshair?.[0]?.x) {
                setRoundStatsCrosshair([
                  roundStatsGraph.wins[x - 1],
                  roundStatsGraph.draws[x - 1],
                  roundStatsGraph.losses[x - 1],
                ])
              }
            }}
          />
          {roundStatsCrosshair && (
            <Crosshair className="absolute" values={roundStatsCrosshair}>
              <StatisticsCrosshairLine />
              <StatisticsCrosshairText>
                <h3>{roundStatsCrosshair[0].x}. <FormattedMessage id="MATCH.ROUND" /></h3>
              </StatisticsCrosshairText>
            </Crosshair>
          )}
        </XYPlot>

        <StatisticsGraphName>
          <FormattedMessage id="MATCH.SEASON_MATCHES" />
        </StatisticsGraphName>
      </StatisticsColumn>
    </>
  )
})

TeamDetailStatistics.propTypes = {
  /**
   * Unique identifier of the team
   */
  teamId: PropTypes.number.isRequired,
}

export default TeamDetailStatistics
