import React, { useMemo } from 'react'
import PropTypes from 'prop-types'

import { COLORS } from 'src/constants'
import { useIsMobile } from 'src/hooks/use-device'
import { TeamDetailPlayerStats } from 'src/utils/prop-types'
import { HighlightedNodeContent, MatchesTableNode, MatchesTableRow } from 'src/styles/blocks/matches'

/**
 * Single row within players detail table on team detail page.
 *
 * @component
 */
const TeamDetailPlayersRow = ({ playerName, stats, onStatHover, hoveredColumn }) => {
  const isMobile = useIsMobile()

  const values = useMemo(() => {
    const getGroupColumns = group => {
      const sum = group.wins + group.draws + group.losses

      return [
        <b>{sum}</b>,
        group.wins,
        group.losses,
        group.draws,
        sum > 0 ? `${Math.round((group.wins / sum) * 100)}%` : '--',
      ]
    }

    const columns = getGroupColumns(stats.games)

    if (!isMobile) {
      columns.push(
        ...getGroupColumns(stats.triples),
        ...getGroupColumns(stats.doubles),
        ...getGroupColumns(stats.singles),
      )
    }

    return columns
  }, [stats, isMobile])

  return (
    <MatchesTableRow>
      <MatchesTableNode textColor={COLORS.RED_PRIMARY} leftAligned>
        <HighlightedNodeContent>
          {playerName.split(' ')[0]}
        </HighlightedNodeContent>
        &nbsp;{playerName.split(' ').slice(1).join(' ')}
      </MatchesTableNode>

      {values.map((value, index) => (
        <MatchesTableNode
          key={`node-${index}`}
          textColor={hoveredColumn === index + 1 ? COLORS.RED_PRIMARY : undefined}
          onMouseEnter={() => onStatHover(index + 1)}
          onMouseLeave={() => onStatHover(null)}
        >
          {value}
        </MatchesTableNode>
      ))}
    </MatchesTableRow>
  )
}

TeamDetailPlayersRow.propTypes = {
  /**
   * Player name associated with given row.
   */
  playerName: PropTypes.string.isRequired,
  /**
   * Player statistics associated with given row.
   */
  stats: TeamDetailPlayerStats.isRequired,
  /**
   * Index of the column that is currently hovered.
   */
  hoveredColumn: PropTypes.number,
  /**
   * OnMouseEnter/OnMouseLeave handler for the stats nodes.
   */
  onStatHover: PropTypes.func.isRequired,
}

export default TeamDetailPlayersRow
