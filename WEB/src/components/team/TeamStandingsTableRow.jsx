import React from 'react'
import PropTypes from 'prop-types'

import { API } from 'src/constants'
import { StandingsPropType } from 'src/utils/prop-types'
import { MatchesTableNode, MatchesTableRow, MatchesTableTeam, MatchesTableTeamLogo } from 'src/styles/blocks/matches'

import { Link } from '../common'

/**
 * Single row within team standings table on results pages.
 *
 * @component
 */
const TeamStandingsTableRow = ({ standing, rowIndex, isMobile, onDetailClick }) => (
  <MatchesTableRow spansLink rowIndex={rowIndex} onClick={() => onDetailClick(standing.team.id)}>
    <MatchesTableNode fixedSize={100} spansLink>
      <Link route={`${API.TEAM_DETAIL}?id=${standing.team.id}`}>{standing.position}.</Link>
    </MatchesTableNode>
    <MatchesTableNode leftAligned>
      <MatchesTableTeam>
        <MatchesTableTeamLogo logo={standing.team.logo} title={standing.team.name} />
        {standing.team.name}
      </MatchesTableTeam>
    </MatchesTableNode>
    {!isMobile
      ? (
        <>
          <MatchesTableNode fixedSize={60}>
            {standing.matches}
          </MatchesTableNode>
          <MatchesTableNode fixedSize={60}>
            {standing.wins}
          </MatchesTableNode>
          <MatchesTableNode fixedSize={60}>
            {standing.losses}
          </MatchesTableNode>
          <MatchesTableNode fixedSize={60}>
            {standing.draws}
          </MatchesTableNode>
        </>
      ) : null}
    <MatchesTableNode fixedSize={70} accent>
      {standing.points}
    </MatchesTableNode>
    <MatchesTableNode fixedSize={120}>
      {standing.setsWon}:{standing.setsLost}
    </MatchesTableNode>
  </MatchesTableRow>
)

TeamStandingsTableRow.propTypes = {
  /**
   * Team standing associated with given row.
   */
  standing: StandingsPropType.isRequired,
  /**
   * Index of the row within the table.
   */
  rowIndex: PropTypes.number.isRequired,
  /**
   * Indicates whether the view is in mobile mode.
   */
  isMobile: PropTypes.bool,
  /**
   * CLick handler for the team detail page link.
   */
  onDetailClick: PropTypes.func,
}

export default TeamStandingsTableRow
