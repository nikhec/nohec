import TeamDetailStatistics from './TeamDetailStatistics'
import TeamDetailPlayersRow from './TeamDetailPlayersRow'
import TeamStandingsTableRow from './TeamStandingsTableRow'
import TeamDetailPlayersHeader from './TeamDetailPlayersHeader'

export { TeamDetailStatistics, TeamDetailPlayersRow, TeamStandingsTableRow, TeamDetailPlayersHeader }
