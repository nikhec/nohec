import React from 'react'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import { useIsMobile } from 'src/hooks/use-device'
import {
  MatchesTableHeaderNode, MatchesTableTooltipLegend, MatchesTableHeaderRow, MatchesTableHeaderTooltip,
} from 'src/styles/blocks/matches'

const GROUPS = ['STATS.MATCHES', 'STATS.TRIPLES', 'STATS.DOUBLES', 'STATS.SINGLES']
const GROUP_COLUMNS = ['STATS.SUM', 'STATS.WINS', 'STATS.LOSSES', 'STATS.DRAWS', 'STATS.WINS_PERCENT']
const GROUP_SIZE = GROUP_COLUMNS.length

/**
 * Header row on top o team detail players table showing explanation for column vaules.
 *
 * @component
 */
const TeamDetailPlayersHeader = ({ hoveredColumn }) => {
  const isMobile = useIsMobile()

  return (
    <thead style={{ height: 0 }}>
      <MatchesTableHeaderRow>
        <MatchesTableHeaderNode />
        <MatchesTableHeaderNode colSpan={GROUP_SIZE}>
          <MatchesTableHeaderTooltip visible={hoveredColumn >= 1 && hoveredColumn <= GROUP_SIZE}>
            <b><FormattedMessage id={GROUPS[0]} /></b>
          </MatchesTableHeaderTooltip>
        </MatchesTableHeaderNode>

        {!isMobile && GROUPS.slice(1).map((message, index) => (
          <MatchesTableHeaderNode colSpan={GROUP_SIZE} key={`players-header-tooltip-${index + 1}`}>
            <MatchesTableHeaderTooltip
              visible={hoveredColumn > (index + 1) * GROUP_SIZE && hoveredColumn <= (index + 2) * GROUP_SIZE}
            >
              <b><FormattedMessage id={message} /></b>
            </MatchesTableHeaderTooltip>
          </MatchesTableHeaderNode>
        ))}
      </MatchesTableHeaderRow>

      <MatchesTableHeaderRow>
        <MatchesTableHeaderNode />
        {Array(isMobile ? 1 : 4).fill(0).map((_, groupIndex) =>
          GROUP_COLUMNS.map((message, index) => {
            const columnIndex = groupIndex * GROUP_SIZE + index + 1
            return (
              <MatchesTableHeaderNode key={`players-header-group-${groupIndex}-${index}`}>
                <MatchesTableTooltipLegend
                  visible={hoveredColumn > groupIndex * GROUP_SIZE && hoveredColumn <= (groupIndex + 1) * GROUP_SIZE}
                  hovered={hoveredColumn === columnIndex}
                >
                  <b><FormattedMessage id={message} /></b>
                </MatchesTableTooltipLegend>
              </MatchesTableHeaderNode>
            )
          })
        )}
      </MatchesTableHeaderRow>
    </thead>
  )
}

TeamDetailPlayersHeader.propTypes = {
  /**
   * Index of the column that is currently hovered.
   */
  hoveredColumn: PropTypes.number,
}

export default TeamDetailPlayersHeader
