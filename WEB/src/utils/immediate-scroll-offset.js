import { SCROLL_ROOT } from 'src/constants'

let scrollOffset = null
const element = document.getElementById(SCROLL_ROOT)
if (element) {
  const handleScroll = () => {
    const { scrollTop } = element
    scrollOffset = scrollTop - element.offsetTop
  }

  element.addEventListener('scroll', handleScroll)
}

/**
 * Returns current scroll offset on demand.
 *
 * @function
 * @returns {number} - Scroll offset size in pixels
 */
export const getImmediateScrollOffset = () => scrollOffset
