/**
 * Defines helper utilities for Redux sagas.
 */

/**
 * Either calls the callback provided or logs the error to the console.
 *
 * @function
 * @param {Object} error - Error object as returned from Axios call
 * @param {function | undefined} onError - Callback function for error handling
 */
export const handleFetchError = (error, onError) => {
  if (onError) {
    onError(error.message ?? 'ERROR.FETCH_CONNECTION')
  } else {
    console.error(error.message ?? 'Data fetching failed. Please, check your internet connection or try again later.')
  }
}
