/**
 * Defines helper utilities for API errors to localized message IDs mapping.
 */
import { isEmpty } from 'lodash'

/**
 * Error mapper for match recording enter endpoint
 *
 * @function
 * @param {number} status - Response status code
 * @returns {string} - Localization message id
 */
export const mapMatchEnterError = status => {
  switch (status) {
    case 403:
      return 'ERROR.MATCH_PERMISSIONS'
    case 404:
      return 'ERROR.MATCH_NOT_FOUND'
    case 409:
      return 'ERROR.MATCH_LOCKED'
    case 410:
      return 'ERROR.MATCH_FINISHED'
    default:
      return 'ERROR.LOAD_CONNECTION'
  }
}

/**
 * Error mapper for match recording progress endpoints
 *
 * @function
 * @param {number} status - Response status code
 * @param {object} body - Response body
 * @param {object | undefined} intl - Intl internationalization object for current locale
 * @returns {string[]} - Array of serialized error messages
 */
export const mapMatchRecordingError = (status, body, intl) => {
  if (status !== 400 || isEmpty(body)) {
    return [intl?.formatMessage({ id: mapMatchEnterError(status) })]
  }

  const serializedErrors = Object.entries(body)
    .map(([field, errorMessage]) => {
      if (field !== 'non_field_errors' && field !== 'detail') {
        return `${field} - ${errorMessage}`
      }
      return null
    })
    .filter(Boolean)

  if (body.non_field_errors) {
    serializedErrors.unshift(body.non_field_errors)
  }

  if (body.detail) {
    serializedErrors.unshift(body.detail)
  }

  return serializedErrors
}
