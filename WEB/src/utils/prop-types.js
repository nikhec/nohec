/**
 * Defines complex commonly used PropTypes.
 */
import PropTypes from 'prop-types'

/**
 * Match player object shape
 */
export const MatchPlayerPropType = PropTypes.shape({
  /**
   * Unique identifier of the player
   * @ignore
   */
  id: PropTypes.number.isRequired,
  /**
   * Name of the player
   * @ignore
   */
  name: PropTypes.string.isRequired,
})

/**
 * Match object's team shape
 */
export const MatchTeamPropType = PropTypes.shape({
  /**
   * Team unique identifier
   * @ignore
   */
  id: PropTypes.number.isRequired,
  /**
   * Team logo
   * @ignore
   */
  logo: PropTypes.string.isRequired,
  /**
   * Team name
   * @ignore
   */
  name: PropTypes.string.isRequired,
  /**
   * Team players
   * @ignore
   */
  players: PropTypes.arrayOf(MatchPlayerPropType),
})

/**
 * Match object shape
 */
export const MatchPropType = PropTypes.shape({
  /**
   * Unique identifier of the match
   * @ignore
   */
  id: PropTypes.number.isRequired,
  /**
   * Place associated with the match
   * @ignore
   */
  league: PropTypes.string.isRequired,
  /**
   * Number of the round the match was played in
   * @ignore
   */
  round: PropTypes.number.isRequired,
  /**
   * Date string associated with the match
   * @ignore
   */
  date: PropTypes.string.isRequired,
  /**
   * Place associated with the match
   * @ignore
   */
  place: PropTypes.string,
  /**
   * Time string associated with the match
   * @ignore
   */
  time: PropTypes.string.isRequired,
  /**
   * Match score
   * @ignore
   */
  score: PropTypes.arrayOf(PropTypes.number),
  /**
   * Object of the home playing team
   * @ignore
   */
  homeTeam: MatchTeamPropType.isRequired,
  /**
   * Object of the away playing team
   * @ignore
   */
  awayTeam: MatchTeamPropType.isRequired,
})

/**
 * Match detail's player object shape
 */
export const MatchDetailPlayerPropType = PropTypes.shape({
  /**
   * Unique identifier of the player
   * @ignore
   */
  id: PropTypes.number.isRequired,
  /**
   * Name of the player
   * @ignore
   */
  name: PropTypes.string.isRequired,
  /**
   * Indicates whether was the player selected as the team captain for the match.
   * @ignore
   */
  isCaptain: PropTypes.bool,
})

/**
 * Match detail's team object shape
 */
export const MatchDetailTeamPropType = PropTypes.shape({
  /**
   * Team unique identifier
   * @ignore
   */
  id: PropTypes.number.isRequired,
  /**
   * Team name
   * @ignore
   */
  name: PropTypes.string.isRequired,
  /**
   * Team logo
   * @ignore
   */
  logo: PropTypes.string.isRequired,
  /**
   * Current team players
   * @ignore
   */
  players: PropTypes.arrayOf(MatchDetailPlayerPropType).isRequired,
})

/**
 * Set object shape within a single match detail's game
 */
export const MatchGameSetPropType = PropTypes.shape({
  /**
   * Points of the home team in given set
   * @ignore
   */
  homePoints: PropTypes.number,
  /**
   * Home team substitutions
   * @ignore
   */
  homeSubstitutions: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number).isRequired),
  /**
   * Points of the away team in given set
   * @ignore
   */
  awayPoints: PropTypes.number,
  /**
   * Away team substitutions
   * @ignore
   */
  awaySubstitutions: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number).isRequired),
})

/**
 * Match detail's game object shape
 */
export const MatchGamePropType = PropTypes.shape({
  /**
   * Unique identifier of the match game
   * @ignore
   */
  id: PropTypes.number.isRequired,
  /**
   * Type of the match game, either 1 (Single), 2 (Double) or 3 (Triple)
   * @ignore
   */
  type: PropTypes.number.isRequired,
  /**
   * Order of this game type within the match (first, second, third, ...)
   * @ignore
   */
  typeOrder: PropTypes.number.isRequired,
  /**
   * Players of the away team
   * @ignore
   */
  awayPlayers: PropTypes.arrayOf(PropTypes.number).isRequired,
  /**
   * Players of the home team
   * @ignore
   */
  homePlayers: PropTypes.arrayOf(PropTypes.number).isRequired,
  /**
   * List of sets played within this game
   * @ignore
   */
  sets: PropTypes.arrayOf(MatchGameSetPropType).isRequired,
})

/**
 * Match court info object shape
 */
export const MatchCourtPropType = PropTypes.shape({
  /**
   * Unique identifier of the court
   * @ignore
   */
  id: PropTypes.number.isRequired,
  /**
   * Address of the court
   * @ignore
   */
  address: PropTypes.string.isRequired,
  /**
   * Court's longitude
   * @ignore
   */
  longitude: PropTypes.number.isRequired,
  /**
   * Court's latitude
   * @ignore
   */
  latitude: PropTypes.number.isRequired,
})

/**
 * Match detail info object shape
 */
export const MatchDetailPropType = PropTypes.shape({
  /**
   * Unique identifier of the match
   * @ignore
   */
  id: PropTypes.number.isRequired,
  /**
   * Date string associated with the match
   * @ignore
   */
  date: PropTypes.string.isRequired,
  /**
   * Time string associated with the match
   * @ignore
   */
  time: PropTypes.string.isRequired,
  /**
   * ISO datetime string associated with the match
   * @ignore
   */
  dateTime: PropTypes.string.isRequired,
  /**
   * Place associated with the match
   * @ignore
   */
  place: MatchCourtPropType.isRequired,
  /**
   * Match score
   * @ignore
   */
  score: PropTypes.arrayOf(PropTypes.number),
  /**
   * List of unique identifiers of users assigned to the match of referees
   * @ignore
   */
  referees: PropTypes.arrayOf(PropTypes.number).isRequired,
  /**
   * Array of games played within the match
   * @ignore
   */
  games: PropTypes.arrayOf(MatchGamePropType).isRequired,
  /**
   * Object of the away playing team
   * @ignore
   */
  awayTeam: MatchDetailTeamPropType.isRequired,
  /**
   * Object of the home playing team
   * @ignore
   */
  homeTeam: MatchDetailTeamPropType.isRequired,
})

/**
 * Match record object shape
 */
export const MatchRecordPropType = PropTypes.shape({
  /**
   * Unique identifier of the match
   * @ignore
   */
  id: PropTypes.number.isRequired,
  /**
   * Match score
   * @ignore
   */
  score: PropTypes.arrayOf(PropTypes.number).isRequired,
  /**
   * Note associated with the match
   * @ignore
   */
  note: PropTypes.string,
  /**
   * Describes current state of the match - one of MATCH_RECORD_STATE constants.
   * @ignore
   */
  state: PropTypes.string.isRequired,
  /**
   * Array of games played within the match
   * @ignore
   */
  games: PropTypes.arrayOf(MatchGamePropType).isRequired,
  /**
   * Object of the home playing team
   * @ignore
   */
  homeTeam: MatchTeamPropType.isRequired,
  /**
   * Unique identifier of the player selected as home team's captain
   * @ignore
   */
  homeCaptain: PropTypes.number,
  /**
   * Object of the away playing team
   * @ignore
   */
  awayTeam: MatchTeamPropType.isRequired,
  /**
   * Unique identifier of the player selected as away team's captain
   * @ignore
   */
  awayCaptain: PropTypes.number,
})

/**
 * Single team object shape
 */
export const TeamPropType = PropTypes.shape({
  /**
   * Unique identifier of the team
   * @ignore
   */
  id: PropTypes.number.isRequired,
  /**
   * Name of the team
   * @ignore
   */
  name: PropTypes.string.isRequired,
  /**
   * Logo of the team
   * @ignore
   */
  logo: PropTypes.string.isRequired,
})

/**
 * Single team standings object shape
 */
export const StandingsPropType = PropTypes.shape({
  /**
   * Team associated with the standing object
   * @ignore
   */
  team: TeamPropType.isRequired,
  /**
   * Position of the team within the league's ladder
   * @ignore
   */
  position: PropTypes.number.isRequired,
  /**
   * Number of matches so far played by the team within the league
   * @ignore
   */
  matches: PropTypes.number.isRequired,
  /**
   * Number of won matches of the team within the league
   * @ignore
   */
  wins: PropTypes.number.isRequired,
  /**
   * Number of tied matches of the team within the league
   * @ignore
   */
  draws: PropTypes.number.isRequired,
  /**
   * Number of lost matches of the team within the league
   * @ignore
   */
  losses: PropTypes.number.isRequired,
  /**
   * Number of won sets of the team within the league
   * @ignore
   */
  setsWon: PropTypes.number.isRequired,
  /**
   * Number of lost sets of the team within the league
   * @ignore
   */
  setsLost: PropTypes.number.isRequired,
  /**
   * Number of points of the team within the league
   * @ignore
   */
  points: PropTypes.number.isRequired,
})

/**
 * Single group of statistic's results object shape
 */
export const TeamDetailPlayerStatResults = PropTypes.shape({
  /**
   * Games from this group the player won
   * @ignore
   */
  wins: PropTypes.number.isRequired,
  /**
   * Games from this group the player lost
   * @ignore
   */
  losses: PropTypes.number.isRequired,
  /**
   * Games from this group the player tied
   * @ignore
   */
  draws: PropTypes.number.isRequired,
})

/**
 * Single player's statistics object shape from all his matches
 */
export const TeamDetailPlayerStats = PropTypes.shape({
  /**
   * Actual statistics of the player from all games
   * @ignore
   */
  games: TeamDetailPlayerStatResults.isRequired,
  /**
   * Actual statistics of the player from singles games
   * @ignore
   */
  singles: TeamDetailPlayerStatResults.isRequired,
  /**
   * Actual statistics of the player from doubles games
   * @ignore
   */
  doubles: TeamDetailPlayerStatResults.isRequired,
  /**
   * Actual statistics of the player from triples games
   *
   */
  triples: TeamDetailPlayerStatResults.isRequired,
})
