/**
 * Provides styled components used on the Account page.
 */
import styled from 'styled-components'

import { COLORS } from 'src/constants'

import { ContentGroupHeadline } from './page'

/**
 * Field for account user name headline.
 */
export const AccountHeader = styled.h1`
  font-size: 2em;
  margin: 60px 0;
  text-align: center;
  font-weight: normal;
`

/**
 * Box for user's signature key on the Account page.
 */
export const SignatureBox = styled.div`
  width: 350px;
  display: flex;
  flex-flow: row;
  justify-content: space-around;
  margin: 0 auto 25px;
  
  > div {
    display: flex;
    flex-flow: row;
  }
`

export const SignatureHeadline = styled(ContentGroupHeadline)`
  line-height: 30px;
  margin: 0;
`

/**
 * Styled container for the signature key.
 */
export const SignatureKey = styled(SignatureHeadline)`
  color: ${COLORS.FONT};
  text-transform: uppercase;
  margin-right: 5px;
`

/**
 * Styled container for the action icon within the user's signature box.
 */
export const SignatureIcon = styled.div`
  width: 30px;
  height: 30px;
  margin: -5px 0 0 5px;
  padding: 5px;
  opacity: 0.7;
  user-select: none;
  
  :hover {
    background-color: ${COLORS.GRAY_LIGHT};
    cursor: pointer;
    opacity: 1;
  }
  
  > svg {
    width: 30px;
    height: 30px;
  }
`

/**
 * Styled component for rounded button displayed at the bottom of account matches table.
 */
export const AccountMoreButton = styled.button`
  display: block;
  width: 40px;
  height: 40px;
  color: ${COLORS.BLUE_PRIMARY};
  background-color: ${COLORS.FOREGROUND};
  opacity: 0.7;
  font-size: 1.8em;
  font-weight: 600;
  border: 1px solid ${COLORS.BORDER};
  border-radius: 50%;
  margin: 15px auto 0;
  
  :hover {
    opacity: 1;
    cursor: pointer;
    background-color: ${COLORS.BACKGROUND};
  }
  
  :active, :focus {
    outline: none;
  }
`
