/**
 * Provides styled components for website's statistics and graphs.
 */
import styled from 'styled-components'

import { COLORS, TABLET_WIDTH } from 'src/constants'

/**
 * Styled component wrapper for a set of statistic values or a single graph.
 *
 * @property {number} fixedHeight - Fixes height of the column to given value of pixels.
 */
export const StatisticsColumn = styled.div`
  display: flex;
  margin: 0 20px;
  flex-flow: column;
  justify-content: center;
  text-align: center;
  height: ${props => `${props.fixedHeight}px` ?? 'auto'};
  
  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 10px;
  }
  
  > div {
    position: relative;
  }
  
  > * svg {
    position: absolute;
    z-index: 1;
    left: 0;
    top: 0;
  }
  
  > * line {
    stroke: ${COLORS.BORDER};
    stroke-width: 0.6px;
  }
  
  > * path {
    fill: none;
    stroke-width: 1.5px;
  }
`

/**
 * Container for the label in the center of a statistics graph.
 */
export const StaticsLabel = styled.div`
  position: absolute;
  order: -1;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  font-size: 36px;
  font-weight: 700;
  
  > span, > div {
    display: flex;
    height: 100%;
    align-items: center;
    justify-content: center;
  }
`

/**
 * Container for the name of the graph situated underneath it.
 */
export const StatisticsGraphName = styled.div`
  margin: 10px 0;
  font-size: 19px;
`

/**
 * Container for the floating tooltip information for given graph component.
 */
export const StatisticsHint = styled.div`
  position: absolute;
  z-index: 2;
  width: max-content;
  background-color: ${COLORS.FOREGROUND};
  border: 1px solid ${COLORS.BORDER};
  text-align: center;
  padding: 10px;
  
  > h3 {
    margin: 0 0 10px;
    font-size: 24px
    font-weight: 600;
  }
  
  > span {
    font-size: 18px;
  }
`

/**
 * Vertical line placed inside the graph crosshair.
 */
export const StatisticsCrosshairLine = styled.div`
  position: absolute;
  top: 0;
  left: -1px;
  width: 2px;
  height: 100%;
  background-color: ${COLORS.GRAY_LIGHT};
`

/**
 * Tooltip placed on top of graph's crosshair.
 */
export const StatisticsCrosshairText = styled.div`
  position: absolute;
  top: -35px;
  left: -35px;
  width: 70px;
  z-index: 2;
  background-color: ${COLORS.FOREGROUND};
  border: 1px solid ${COLORS.BORDER};
  text-align: center;
  padding: 5px;
  
  > h3 {
    margin: 0;
    font-size: 15px;
    font-weight: 600;
  }
`
