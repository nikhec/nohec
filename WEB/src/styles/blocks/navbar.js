/**
 * Provides styles for application's navigation bar components.
 */
import styled from 'styled-components'

import { COLORS, DESIGN, TABLET_WIDTH } from 'src/constants'
import { Slider } from 'src/styles/blocks/page'

/**
 * Defines styling for page's navigation bar.
 *
 * @property {boolean} [scrolled=false] - Indicates whether is application's content scrolled to transform navbar.
 */
export const PageNavbar = styled.nav`
  position: fixed;
  z-index: 15;
  height: ${props => props.scrolled ? DESIGN.NAVBAR_HEIGHT_SCROLLED : DESIGN.NAVBAR_HEIGHT}px;
  width: fill-available;
  margin: ${props => props.scrolled ? '0 20px' : '0 80px'};
  display: flex;
  flex-flow: row;
  align-items: center;
  justify-content: space-between;
  background-color: ${COLORS.BACKGROUND};
  border-bottom: 1px solid ${COLORS.BORDER};
  transition: height 0.4s, margin 0.4s;

  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 0 10px;
    padding: 0 10px;
  } 
`

/**
 * Block on the left of navigation bar, pushing its siblings to the right.
 */
export const NavbarTitleWrapper = styled.span`
  height: 50px;
  display: flex;
  flex-grow: 5;
  user-select: none;
`

/**
 * Clickable box tightly wrapping logo and title.
 */
export const NavbarTitleBox = styled.span`
  height: 100%;
  display: flex;
  flex-flow: row;
  align-items: center;
  
  :hover {
    cursor: pointer;
  }
`

/**
 * Wrapper for the application logo image.
 */
export const NavbarLogoBox = styled.span`
  height: 40px;
  width: 40px;
  
  & > img {
    width: 100%;
    height: 100%;
    border-radius: 50%;
  }
`

/**
 * Wrapper for the application title text.
 */
export const NavbarTitle = styled.h2`
  padding-left: 10px;
  text-transform: uppercase;
`

// Defines reusable styles used in both groups of navigation bar toggles.
const NavbarTogglesBox = styled.span`
  height: 50px;
  display: flex;
  flex-direction: row;
  align-items: center;
`

/**
 * Navigation bar's middle group of toggles.
 */
export const NavbarMiddleTogglesBox = styled(NavbarTogglesBox)`
  justify-content: space-around;
  flex-grow: 1;
`

/**
 * Navigation bar's right group of toggles.
 */
export const NavbarRightTogglesBox = styled(NavbarTogglesBox)`
  justify-content: flex-end;
  flex-grow: 3;
  
  > h2 {
    margin: 0 6px;
  }
`

/**
 * Clickable toggle for DESKTOP/TABLET navigation bar.
 *
 * @property {boolean} [accent=false] - Toggles with accent are bolder and have BLUE_PRIMARY font color.
 */
export const NavbarToggle = styled.h2`
  display: flex;
  align-content: center;
  padding: 5px 10px;
  margin: 0;
  line-height: 1.2em;
  font-weight: ${props => props.accent ? 500 : 450};
  color: ${props => props.accent ? COLORS.BLUE_PRIMARY : COLORS.FONT};
  text-transform: capitalize;
  box-sizing: border-box;
  user-select: none;
  
  :hover {
    background-color: ${COLORS.GRAY_LIGHT};
    cursor: pointer;

    > svg {
      opacity: 1;
    }
  }
  
  > svg {
    width: 30px;
    height: 30px;
    opacity: 0.7;
  }
`

/**
 * DESKTOP/TABLET navigation bar's slider that moves under toggles. Is positioned absolutely.
 *
 * @property {boolean} [scrolled=false] - Indicates whether is application's content scrolled to transform navbar.
 * @property {number} left - Slider's left offset.
 * @property {number} width - Slider's width.
 * @property {number} [top] - Slider's top offset, used only when `scrolled` is `false`.
 * @property {number} [opacity=1] - Is used to temporary hide slider during navigation bar's transition due to content scrolling.
 */
export const NavbarSlider = styled(Slider)`
  position: fixed;
  top: ${props => props.scrolled ? DESIGN.NAVBAR_HEIGHT_SCROLLED : props.top}px;
  opacity: ${props => props.opacity ?? 1};
`

/**
 * Wrapper for the menu icon on MOBILE devices.
 *
 * @property {boolean} [expanded=false] - Indicates whether is MOBILE menu expanded or not.
 */
export const NavbarHamburgerMenu = styled.div`
  display: flex;
  align-content: center;
  padding: 5px 10px;
  margin: 0;
  line-height: 1.2em;
  transform: rotate(${props => props.expanded ? 90 : 0}deg);
  transition: transform 0.4s;

  & > svg {
    width: 30px;
    height: 30px;
    opacity: 0.7;

    :hover {
      opacity: 1;
      cursor: pointer;
    }
  }
`

/**
 * Provides floating and expandable pop-up component.
 *
 * @property {boolean} [expanded=false] - Indicates whether is Pop-up expanded or not.
 */
export const NavbarPopUpBox = styled.div`
  position: absolute;
  top: 100%;
  right: 10px;
  display: flex;
  flex-direction: column;
  height: auto;
  background-color: ${COLORS.FOREGROUND};
  overflow: hidden;
  transition: max-height 0.4s, padding-top 0.45s, border 0.4s;

  ${props => props.expanded ? `
    padding-top: 15px;
    border: 1px solid ${COLORS.BORDER};
    max-height: 250px;
  ` : `
    padding-top: 0;
    max-height: 0;
    border: 0 solid ${COLORS.BORDER};
  `}

  :focus, :active {
    outline: none;
  }
`

/**
 * Defines padding for the sign-in form inside navigation bar pop-up.
 */
export const SignInFormBox = styled.div`
  margin: 0 15px;
`

/**
 * Wrapper for sign-in form's error message at the bottom of the pop-up.
 */
export const SignInFormError = styled.div`
  width: 300px;
  text-align: center;
  margin-bottom: 15px;
  font-weight: 500;
  color: ${COLORS.RED_PRIMARY};
`

/**
 * Anchor element for absolutely positioned MOBILE navigation bar's menu.
 */
export const NavbarMobileAnchor = styled.div`
  position: relative;
  padding: 0 15px 0 34px;
`

/**
 * Clickable toggle for MOBILE navigation bar.
 *
 * @property {boolean} [accent=false] - Toggles with accent are bolder and have BLUE_PRIMARY font color.
 */
export const NavbarMobileToggle = styled.h3`
  height: 18px;
  margin: 0 0 20px;
  font-weight: ${props => props.accent ? 500 : 450};
  color: ${props => props.accent ? COLORS.BLUE_PRIMARY : COLORS.FONT};
  text-transform: capitalize;
  user-select: none;

  :hover {
    cursor: pointer;
  }
`

/**
 * MOBILE navigation bar's slider that moves on the left of toggles. Is positioned absolutely.
 *
 * @property {boolean} [scrolled=false] - Indicates whether is application's content scrolled to transform navbar.
 * @property {number} right - Slider's right offset.
 * @property {number} top - Slider's top offset.
 */
export const NavbarMenuSlider = styled.div`
  position: absolute;
  // We are subtracting navbar's height + drawer menu's top adding + half of slider's height
  top: 0;
  right: 0;
  transform: ${props => `translate(-${props.right + 26}px, ${props.top - 1 - (props.scrolled ? DESIGN.NAVBAR_HEIGHT_SCROLLED : DESIGN.NAVBAR_HEIGHT)}px)`}; 
  height: 10px;
  width: 10px;
  border-radius: 50%;
  background-color: ${COLORS.BLUE_PRIMARY};
  transition: transform 0.4s;
`
