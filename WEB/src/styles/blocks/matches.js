/**
 * Provides styled components for matches.
 */
import styled from 'styled-components'

import { COLORS, TABLET_WIDTH, MOBILE_WIDTH } from 'src/constants'

/**
 * Defines table component for matches list.
 */
export const MatchesTable = styled.table`
  width: 100%;
  table-layout: auto;
  border-collapse: collapse;
  box-sizing: border-box;
  
  > tbody {
    border: 1px solid ${COLORS.BORDER};
    box-shadow: 0 0 5px 0 ${COLORS.BORDER};
  }
`

/**
 * Defines row component for the matches list table.
 *
 * @property { number } rowIndex - Index of the row within the table.
 * @property { boolean } [spansLink=false] - Indicates whether should this box span containing link over the whole row.
 */
export const MatchesTableRow = styled.tr`
  height: 50px;
  border: 1px solid ${COLORS.BORDER};
  opacity: 1;
  transition: box-shadow ease-out 0.3s, background-color 0.3s;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    height: 35px;
    line-height: 36px;
    
     & div {
      height: 35px;
     }
  }
  
  & div {
    max-height: 50px;
  }
    
  ${props => props.spansLink && `
    :hover {
      cursor: pointer;
      background-color: ${COLORS.FOREGROUND};
    }
    
    > td > a:before {
      content: '';
      position: absolute;
      left: 0;
      right: 0;
      top: ${props.rowIndex * 50}px;
      height: 50px;
      
      @media (max-width: ${MOBILE_WIDTH}px) {
        top: ${props.rowIndex * 39}px;
        height: 39px;
      }
    }
  `}
`

/**
 * Defines row component for the header of matches list table.
 */
export const MatchesTableHeaderRow = styled.tr`
  height: 0;
  border: none;
`

/**
 * Defines node component for matches list table.
 *
 * @property { number } fixedSize - Fixes width of table node to given number of pixels.
 * @property { string } textColor - Used to overwrite text color in the node.
 * @property { boolean } [accent=false] - Indicates whether should be the node's value highlighted.
 * @property { boolean } [leftAligned=false] - Indicates whether to overwrite default center text alignment.
 * @property { boolean } [isClickable=false] - Checks whether has the node associated click handler.
 */
export const MatchesTableNode = styled.td`
  overflow: hidden;
  text-align: ${props => props.leftAligned ? 'left' : 'center'};
  ${props => props.fixedSize != null && `width: ${props.fixedSize}px`};
  padding-right: 5px;
  transition: color 0.2s;
  user-select: none;
  
  ${props => props.accent && `
    color: ${COLORS.BLUE_PRIMARY};
    font-size: 17px;
    line-height: 19px;
    font-weight: 600;
  `}
  ${props => props.textColor && `color: ${props.textColor};`}
  
  ${props => props.isClickable && `
    opacity: 0.8;

    :hover {
      cursor: pointer;
      opacity: 1;
    }
  `}
`

/**
 * Defines header node component for the matches list table.
 */
export const MatchesTableHeaderNode = styled.td`
  position: relative;
  padding: 0 5px 0 0;
  text-align: center;
  font-size: 16px;
  line-height: 32px;
`

/**
 * Defines header tooltip component for the matches list table.
 *
 * @property {boolean} [visible=false] - Indicates whether is the current tooltip visible.
 */
export const MatchesTableHeaderTooltip = styled.div`
  position: absolute;
  top: -60px;
  left: -2px;
  right: -2px;
  overflow: hidden;
  background-color: ${COLORS.FOREGROUND};
  transition: opacity 0.4s;
  
  ${props => props.visible ? `
    bottom: 2px;
    opacity: 1;
    border: 1px solid ${COLORS.BORDER};
  ` : `
    bottom: 60px;
    height: 0;
    opacity: 0;
    border: none;
  `}
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    position: relative;
    opacity: 1;
    top: auto;
    height: auto;
    bottom: auto;
    border: none;
    background-color: inherit;
  }
`

/**
 * Defines header legend for columns inside header tooltip of the matches list table.
 *
 * @property {boolean} [visible=false] - Indicates whether is the current tooltip's legend visible.
 * @property {boolean} [hovered=false] - Indicates whether is the current tooltip's legend hovered.
 */
export const MatchesTableTooltipLegend = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: -34px;
  opacity: ${props => props.visible ? 1 : 0};
  visibility: ${props => props.visible ? 'visible' : 'hidden'};
  color: ${props => props.hovered ? COLORS.RED_PRIMARY : COLORS.FONT};
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    position: relative;
    opacity: 1;
    visibility: visible;
    top: auto;
  }
`

/**
 * Wrapper around match table node with highlighted content
 */
export const HighlightedNodeContent = styled.span`
  margin-left: 20px;
  font-weight: 600;
`

/**
 * Styled wrapper component for team description.
 *
 * @property {boolean} fixedWidth - fixes width of the box to achieve center + left alignment
 */
export const MatchesTableTeam = styled.div`
  display: flex;
  flex-flow: row;
  align-items: center;
  overflow: hidden;
  color: ${COLORS.RED_PRIMARY};
  font-weight: 500;
  white-space: nowrap;

  ${props => props.fixedWidth && `
    max-width: 260px;
    margin-left: auto;
    
    @media (max-width: ${MOBILE_WIDTH}px) {
      max-width: 50px;
    }
  `}
`

/**
 * Styled image component for team logo.
 *
 * @property {string} logo - Logo of the team
 */
export const MatchesTableTeamLogo = styled.div`
  height: 25px;
  width: 25px;
  min-width: 25px;
  margin: 0 5px;
  background: url(${props => props.logo}) no-repeat center;
  background-size: contain;
`

/**
 * Styled wrapper component for detail page, placing multiple content components in column.
 */
export const MatchDetailBox = styled.div`
  margin: 5px 0;
  display: flex;
  height: 100%;
  flex-flow: column;
  justify-content: center;
`

/**
 * Wrapper styled component for match detail header consisting of logos, names and additional content.
 *
 * @property { boolean } [isSingleLine=false] - Indicates how to align team logo and name.
 */
export const MatchHeaderBox = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: space-between;
  width: ${props => props.isSingleLine ? 90 : 80}%;
  margin: ${props => props.isSingleLine ? '10px auto 10px' : '0 auto'};
  padding-bottom: 20px;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    width: 80%;
  }
`

/**
 * Wrapper styled component for match detail content column.
 */
export const MatchDetailRowBox = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: space-between;
  padding-bottom: 20px;
`

/**
 * Wrapper styled component for a single column within content row on detail page.
 */
export const MatchDetailRowColumn = styled.div`
  position: relative;
  display: flex;
  flex: 1;
  flex-flow: column;
  height: fit-content;
  align-items: center;
  align-self: center;
  text-align: center;
  box-sizing: bounding-box;
`

/**
 * Wrapper styled component for single team column within the header holding team logo and name.
 *
 * @property { boolean } [isSingleLine=false] - Indicates how to align team logo and name.
 * @property { boolean } [topAlignment=false] - Indicates how to pin the whole column.
 */
export const MatchHeaderLogoBox = styled(MatchDetailRowColumn)`
  flex-flow: ${props => props.isSingleLine ? 'row' : 'column'};
  align-self: ${props => props.topAlignment ? 'flex-start' : 'flex-end'};
  
  :last-child {
    flex-flow: ${props => props.isSingleLine ? 'row-reverse' : 'column'};
  }
  
  > * {
    transition: width 0.5s, left 0.5s, right 0.5s;
  }
    
  @media (max-width: ${TABLET_WIDTH}px) {
    font-size: 18px;
  }

  @media (max-width: ${MOBILE_WIDTH}px) {
    font-size: 15px;
    flex-flow: column !important;
  }
`

/**
 * Component container for home/away team hint displayed above logo images in the header.
 *
 * @property { boolean } [isRight=false] - Indicates how to align hint horizontally.
 */
export const MatchHeaderLogoHint = styled.span`
  position: absolute;
  top: -20px;
  ${props => props.isRight ? 'right' : 'left'}: calc(50% - 135px);
  color: ${COLORS.FONT};
  font-size: 16px;
  font-weight: 700;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    ${props => props.isRight ? 'right' : 'left'}: calc(50% - 125px);
  }

  @media (max-width: ${MOBILE_WIDTH}px) {
    font-size: 13px;
    ${props => props.isRight ? 'right' : 'left'}: calc(50% + 30px);
  }
`

/**
 * Styled team logo for the match header component
 *
 * @property {string} logo - Team logo
 * @property { boolean } imageSize - Controls size of the logo image.
 * @property { boolean } [isSingleLine=false] - Indicates how to align team logo and name.
 */
export const MatchHeaderLogo = styled.div`
  width: ${props => props.imageSize}px;
  height: ${props => props.imageSize}px;
  margin: ${props => props.isSingleLine ? '0 10px' : '0 0 15px'};
  background: url(${props => props.logo}) no-repeat center;
  background-size: contain;

  @media (max-width: ${TABLET_WIDTH}px) {
    width: ${props => props.imageSize - 20}px;
    height: ${props => props.imageSize - 20}px;
  }

  @media (max-width: ${MOBILE_WIDTH}px) {
    width: 80px;
    height: 80px;
    margin-bottom: 10px;
  }
`

/**
 * Styled container for the team name
 */
export const MatchHeaderTeamName = styled.div`
  font-size: 1.7vw;
  line-height: 1.7vw;
  color: ${COLORS.BLUE_PRIMARY};
  font-weight: 500;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    font-size: 15px;
    line-height: 16px;
  }
`

/**
 * Styled wrapper for content between two logos on match detail page.
 *
 * @property { boolean } [isSingleLine=false] - Indicates how to align team logo and name.
 * @property { boolean } [forceColumn=false] - Forces column alignment of the content even on smaller devices.
 */
export const MatchHeaderHeadline = styled.div`
  display: flex;
  flex-flow: column;
  margin: 0 ${props => props.isSingleLine ? 40 : 20}px;
  height: ${props => props.isSingleLine ? 'auto' : '260px'};
  justify-content: center;
  align-items: center;
  
  > * {
    margin-top: 0;
  }
  
  > h1 {
    font-size: 60px;
    line-height: 55px;
    font-weight: 700;
    text-align: center;
    margin: 20px 0;
  }
  
  > h3 {
    font-size: 15px;
    line-height: 14px;
    margin-bottom: 10px;
  }
  
  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 0 20px;
    
    > h1 {
      font-size: 2.5em;
      line-height: 45px;
    }
    
    ${props => props.isSingleLine && !props.forceColumn && `
      flex-flow: row;
    
      > h2, > h3 {
        margin: 23px 0 17px;
        line-height: 55px;
        display: inline-block;
      }
    `}
  }

  @media (max-width: ${MOBILE_WIDTH}px) {
    height: auto;

    > h1 {
      font-size: 40px;
      line-height: 35px;
    }
    
    > h3 {
      font-size: 12px;
      line-height: 10px;
    }
    
    > h2 {
      font-size: 16px;
      line-height: 14px;
    }
  }
`

/**
 * Styled box for button block at the bottom of the detail page.
 *
 * @property {boolean} [higher=false] - Indicates the containing button is higher variant
 */
export const MatchDetailButtonBox = styled.div`
  margin: 15px 0;
  ${props => props.higher && '& button { margin: 0; }'}
`

/**
 * Styled component box for basic detail page's body.
 *
 * @property { boolean } [isColumn=false] - Indicates whether to align items into column or default to row.
 */
export const MatchDetailBodyBox = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-around;
  flex-flow: ${props => props.isColumn ? 'column' : 'row'};
`

/**
 * Styled container for place section on new match detail page.
 */
export const MatchDetailPlaceBox = styled.div`
  width: 80%;
  max-width: 1000px;
  margin: 25px auto 0;
`

/**
 * Styled container for place information.
 */
export const MatchDetailPlaceInfo = styled.div`
  font-size: 17px;
  font-weight: 500;
  text-align: center;
  margin-bottom: 10px;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    font-size: 15px;
  }
`

/**
 * Wrapper for the mapy.cz Map
 */
export const MatchDetailPlaceMap = styled.div`
  height: 450px; 
  transition: height 0.3s;
  border: 1px solid ${COLORS.BORDER};
  box-shadow: 0 0 5px 0 ${COLORS.BORDER};
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    height: 300px;
  }
`

/**
 * Styled wrapper component for a simple list of players.
 *
 * @property { boolean } [fullOverflow=false] - Indicates that the list should overflow on both sides.
 * @property { boolean } [hasError=false] - Indicates whether the box contains an invalid value.
 */
export const MatchPlayersList = styled.div`
  display: flex;
  flex-flow: column;
  flex: 1;
  margin-top: 20px;
  height: fit-content;
  width: fill-available;
  max-width: 400px;
  text-align: left;
  border: 1px solid ${props => props.hasError ? COLORS.RED_PRIMARY : COLORS.BORDER};
  border-collapse: collapse;
  box-sizing: border-box;
  box-shadow: 0 0 5px 0 ${COLORS.BORDER};
  position: relative;
  margin: ${props => props.fullOverflow
    ? '40px -30% 0'
    : '40px 0 0'};
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    max-width: 150px;
    margin-top: 20px;
  }
`

/**
 * Node within a simple single column list of item.
 *
 * @property { boolean } [accent=false] - Accented values are displayed in red.
 */
export const MatchBodyListNode = styled.div`
  position: relative;
  height: 50px;
  line-height: 50px;
  font-size: 14px;
  padding-left: 50px;
  border-bottom: 1px solid ${COLORS.BORDER};
  color: ${props => props.accent ? COLORS.RED_PRIMARY : COLORS.FONT};
  
  :last-child {
    border-bottom: 0;
  }
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    height: 35px;
    line-height: 36px;
    padding-left: 25px;
  }
`

/**
 * Box for arrow in the select node.
 *
 * @property {boolean} [showRemove=false] - Indicates whether the arrow represents a remove button for selected value.
 * @property {boolean} [hasValue=false] - Indicates whether option has selected value.
 */
export const MatchBodyListSelectArrow = styled.div`
  width: 24px;
  height: 24px;
  margin: 13px 14px;
  float: right;
  text-align: center;
  font-size: 24px;
  line-height: 22px;
  font-weight: 500;
  user-select: none;
  transition: transform 0.3s, color 0.3s;
  border-radius: 50%;
  
  ${props => props.showRemove && `    
    :hover {
      color: ${COLORS.RED_PRIMARY};
      transform: rotate(45deg);
      cursor: pointer;
      font-size: 30px;
      line-height: 21px;
    }
  `}
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    margin: 9px 7px;
    font-size: 20px;
    line-height: 18px;
    
    ${props => props.hasValue && `
      color: ${COLORS.RED_PRIMARY};
      transform: rotate(45deg);
      cursor: pointer;
      font-size: 22px;
      line-height: 16px;
    `}
  }
`

/**
 * Wrapper of the C letter indicating the player is captain of the team.
 */
export const MatchBodyListNodeCaptain = styled.div`
  position: absolute;
  right: 20px;
  top: 0;
  font-weight: 600;
  font-size: 15px;
  user-select: none;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    font-size: 12px;
  }
`

/**
 * Wrapper for the match type label within match's content row.
 *
 * @property { boolean } [extraMargin=false] - Increases bottom margin of the headline.
 */
export const MatchContentRowMatchType = styled.h2`
  color: ${COLORS.BLUE_PRIMARY};
  text-transform: uppercase;
  margin-bottom: ${props => props.extraMargin ? '30px' : 'default'};
`

/**
 * Styled row component within Account matches table.
 *
 * @property { boolean } [hasDetail=false] - Indicates whether the match row shows link to detail page.
 */
export const AccountTableRow = styled(MatchesTableRow)`
  ${props => !props.hasDetail && `
    :nth-child(1) {
      opacity: 0.2;
    }
    
    :nth-child(2) {
      opacity: 0.4;
    }
    
    :nth-child(3) {
      opacity: 0.6;
    }
  `}
`
