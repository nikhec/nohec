/**
 * Provides styled components for calendars.
 */
import styled from 'styled-components'

import { COLORS, MOBILE_WIDTH, TABLET_WIDTH } from 'src/constants'
import { ColumnView } from 'src/styles/blocks/page'

/**
 * Outermost wrapper for the calendar view.
 */
export const CalendarViewBox = styled(ColumnView)`
  @media (max-width: ${TABLET_WIDTH}px) {
    flex-flow: column-reverse;
    justify-content: flex-end;
  } 
`

/**
 * Left column on the calendar view. Shifts to top on other devices.
 */
export const CalendarViewColLeft = styled.div`
  width: 60%;
  margin: 0 0 5px;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    width: 100%;
    margin: 0;
  } 
`

/**
 * Right column on the calendar view. Shifts to bottom on other devices.
 */
export const CalendarViewColRight = styled.div`
  width: 40%;
  display: flex;
  flex-flow: column;
  justify-content: center;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    flex-flow: column-reverse;
    width: 100%;
  }
`

/**
 * Groups together control and detail boxes because those two are in single row on other devices than desktop.
 *
 * @property { boolean } [hasNextMatch=false] - Indicates whether the Next Match row is displayed under the group.
 */
export const CalendarViewColGroup = styled.div`
  width: 100%;
  height: ${props => `calc(100% - ${props.hasNextMatch ? 120 : 0}px)`};
  display: flex;
  flex-flow: column;
  justify-content: center;

  @media (max-width: ${TABLET_WIDTH}px) {
    flex-flow: row;
    height: 140px;
  }
`

/**
 * Wrapper for the calendar table.
 */
export const CalendarBox = styled.table`
  width: 100%;
  height: 100%;
  table-layout: fixed;
  border: 1px solid ${COLORS.BORDER};
  border-collapse: collapse;
  box-sizing: border-box;
  box-shadow: 0 0 5px 0 ${COLORS.BORDER};
  user-select: none;
`

/**
 * Node of the calendar table header.
 *
 * @property { boolean } [isWeekend=false] - Indicates whether the node belongs to the weekend.
 */
export const CalendarHeaderNode = styled.th`
  height: 22px;
  width: calc(100% / 7);
  color: ${props => props.isWeekend ? COLORS.RED_PRIMARY : COLORS.BLUE_PRIMARY};
  text-align: center;
  font-weight: 500;
  font-size: 1.2em;
  text-transform: capitalize;
  border: 1px solid ${COLORS.BORDER};
`

/**
 * Node of the calendar table body.
 *
 * @property { boolean } [isWeekend=false] - Indicates whether the node belongs to the weekend.
 * @property { boolean } [isSelected=false] - Indicates whether the node belongs currently selected date.
 * @property { boolean } [isActiveMonth=false] - Indicates whether the node belongs currently selected month.
 */
export const CalendarBodyNode = styled.td`
  height: 88px;
  max-width: calc(100% / 7);
  overflow: hidden;
  position: relative;
  border: 1px solid ${COLORS.BORDER};
  background-color: ${props => props.isSelected ? COLORS.BORDER : 'inherit'};
  color: ${props =>
    props.isWeekend ? COLORS.RED_PRIMARY : COLORS.FONT};
  opacity: ${props => props.isActiveMonth ? 1 : 0.5};
  transition: box-shadow ease-out 0.3s;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    height: 65px;
  } 
  
  :hover {
    box-shadow: inset 0 0 0 2px ${COLORS.BLUE_PRIMARY};
    opacity: 1;
    cursor: pointer;
    
    > div {
      color: ${COLORS.BLUE_PRIMARY};
      opacity: 1;
    }
  }
`

/**
 * Field for the calendar body node's date.
 */
export const CalendarBodyNodeDay = styled.div`
  position: absolute;
  top: 2px;
  right: 5px;
  z-index: 1;
  font-weight: 500;
  font-size: 1.1em;
`

/**
 * Field for the calendar body node's content.
 */
export const CalendarBodyNodeMatch = styled.div`
  position: absolute;
  top: 28px;
  height: 100%;
  width: 100%;
  display: flex;
  flex-flow: row;
  justify-content: center;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    position: relative;
    top: 0;
    height: auto;
    text-align: center;
  } 
`

/**
 * Field for the calendar body node content's logo of participating team.
 *
 * @property {string} logo - Team logo
 */
export const CalendarBodyNodeLogo = styled.div`
  width: 25px;
  height: 25px;
  background: url(${props => props.logo}) no-repeat center;
  background-size: contain;
`

/**
 * Field for the calendar body node content's result of the match.
 */
export const CalendarBodyNodeResult = styled.div`
  color: ${COLORS.FONT};
  font-weight: 700;
  line-height: 25px;
  margin: 0 5px;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    line-height: 20px;
  } 
`

/**
 * Field for the calendar body node content's note about more matches played on that particular date.
 */
export const CalendarBodyNodeMore = styled.div`
  position: absolute;
  bottom: 2px;
  width: 100%;
  text-align: center;
  color: ${COLORS.FONT};
  margin: 0 auto;
  font-weight: 600;
`

/**
 * Wrapper for calendar controls.
 */
export const CalendarControlsBox = styled.div`
  position: relative;
  width: 200px;
  min-width: 200px;
  height: 120px;
  min-height: 120px;
  margin: 10px auto 10px calc(50% - 80px);
  
  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 0 0 20px 0;
    align-self: center;
  }
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    display: flex;
    flex-flow: column;
    align-items: center;
    width: 160px;
    min-width: 160px;
    
    & * > button {
      margin: 10px 0 0;
    }
  } 
`

const CalendarControlHorizontal = styled.div`
  position: absolute;
  top: 0;
  opacity: 0.6;
  width: 40px;
  height: 40px;
  overflow: hidden;
  
  :hover {
    opacity: 1;
    color: ${COLORS.BLUE_PRIMARY};
    cursor: pointer;
  }
  
  & > svg {
    margin: -25% 0 0 -25%;
    width: 150%;
    height: 150%;
  }
`

/**
 * Calendar controls' left arrow wrapper.
 */
export const CalendarControlLeft = styled(CalendarControlHorizontal)`
  left: 0;
`

/**
 * Calendar controls' right arrow wrapper.
 */
export const CalendarControlRight = styled(CalendarControlHorizontal)`
  right: 0;
`

/**
 * Calendar controls' arrow wrapper for displaying in the vertical view on other devices.
 */
export const CalendarControlVertical = styled(CalendarControlHorizontal)`
  position: relative;
  width: 30px;
  height: 30px;
  
  & > svg {
    margin: -25% 0 0 -50%;
    width: 200%;
    height: 200%;
  }
`

/**
 * Calendar controls' field with currently selected month.
 */
export const CalendarControlMonth = styled.h2`
  align-self: center;
  color: ${COLORS.BLUE_PRIMARY};
  text-align: center;
  font-weight: 600;
  font-size: 1.8em;
  line-height: 40px;
  margin: 0;
`

/**
 * Calendar controls' field with currently selected year.
 */
export const CalendarControlYear = styled.h3`
  align-self: center;
  color: ${COLORS.FONT};
  text-align: center;
  font-weight: 500;
  font-size: 1.1em;
  margin: 0;
`

/**
 * Calendar controls button's wrapper.
 */
export const CalendarControlToday = styled.div`
  position: absolute;
  bottom: 0;
  width: calc(100% - 60px);
  margin: 0 30px;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    position: relative;
  } 
`

/**
 * Wrapper for calendar controls view with matches detail for selected date. Creates the wrapper matches scroll within.
 */
export const CalendarDetailWrapper = styled.div`
  height: calc(100% - 160px);
  width: calc(100% - 20px);
  display: table;

  @media (max-width: ${TABLET_WIDTH}px) {
    height: 100%;
    max-width: 60%;
    overflow-x: hidden;
    display: inherit;
  }

  @media (max-width: ${MOBILE_WIDTH}px) {
    max-width: 70%;
  }
`

/**
 * Wraps around matches detail for selected date.
 */
export const CalendarDetailBox = styled.div`
  display: flex;
  flex-flow: column;
  height: calc(100% - 10px);
  width: 100%;
  margin: 10px 0 0 20px;
  overflow-y: auto;
  text-align: center;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 0 0 20px 20px;
    max-height: 120px;
  }
`

/**
 * Wrapper for the name of league matches belong to.
 */
export const CalendarDetailLeagueBox = styled.div`
  display: flex;
  flex-flow: column;
  text-align: left;
`

/**
 * Field for the name of league matches belong to.
 */
export const CalendarDetailLeagueLabel = styled.span`
  font-weight: 500;
  margin: 0 0 5px 15px;
  font-size: 1.1em;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 0 0 5px;
  }
`

/**
 * Defines a row for a single match detail.
 *
 * @property { object } onClick - Indicates the row has associated click handler to display match detail.
 */
export const CalendarDetailMatchRow = styled.div`
  padding: 5px 0 5px 10px;
  transition: background-color 0.5s ease-in-out;
  
  ${props => props.onClick && `
    :hover {
      cursor: pointer;
      background-color: ${COLORS.FOREGROUND};
    }
  `}
`

/**
 * Wrapper for a single match detail.
 */
export const CalendarDetailMatch = styled.div`
  display: flex;
  flex-flow: row;
  width: 100%;
  justify-content: center;
  line-height: 30px;
`

/**
 * Styled column container for a team within match detail on calendar page
 *
 * @property {boolean} [isNextMatch = false] - Indicates whether the column belongs to the next match section
 */
const CalendarDetailMatchCol = styled.div`
  display: flex;
  flex-flow: row;
  width: 45%;
  align-items: center;
  
  ${props => !props.isNextMatch && `
    @media (max-width: ${MOBILE_WIDTH}px) {
      flex-flow: column;
      text-align: center;
    } 
  `}
`

/**
 * Column for home team.
 */
export const CalendarDetailMatchLeft = styled(CalendarDetailMatchCol)`
  justify-content: flex-end;
  margin-right: 8px;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    flex-flow: column-reverse;
  } 
`

/**
 * Column for visitor team.
 */
export const CalendarDetailMatchRight = styled(CalendarDetailMatchCol)`
  margin-left: 8px;
`

/**
 * Middle column between the team displaying match result or slash if the match is yet to be played.
 */
export const CalendarDetailMatchSeparator = styled.span`
  font-size: 1.2em;
  font-weight: 600;
`

/**
 * Field for team's logo within the match detail.
 *
 * @property {string} logo - Team logo
 */
export const CalendarDetailMatchLogo = styled.div`
  width: 30px;
  height: 30px;
  background: url(${props => props.logo}) no-repeat center;
  background-size: contain;
  margin: 0 5px;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    margin-bottom: 5px;
  } 
`

/**
 * Field for team's name within the match detail.
 */
export const CalendarDetailMatchName = styled.span`
  height: max-content;
  color: ${COLORS.RED_PRIMARY};
  font-size: 0.9vw;
  font-weight: 500;
  line-height: 1.1vw;
  letter-spacing: -0.7px;

  @media (max-width: ${TABLET_WIDTH}px) {
    font-size: 1.4vw;
    line-height: 1.5vw;
  }
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    font-size: 0.8em;
    line-height: 1em;
  }
`

/**
 * Field for place the match is played on within the match detail.
 */
export const CalendarDetailMatchPlace = styled.div`
  width: 100%;
  text-align: center;
  margin-top: 5px;
  font-size: 13px;
  font-weight: 500;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    font-size: 12px;
  }
`

/**
 * Wrapper for the Next Match row.
 */
export const CalendarNextMatchBox = styled.div`
  height: 120px;
  margin-left: 30px;
  text-align: center;

  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 0 auto 20px;
    width: fill-available;
    max-width: 500px;
    height: auto;
  }
`

/**
 * Field for the Next Match row headline.
 */
export const CalendarNextMatchHeader = styled.h2`
  color: ${COLORS.BLUE_PRIMARY};
  margin: 10px 0;
  text-align: center;
  text-transform: uppercase;
`
