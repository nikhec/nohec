/**
 * Provides styled components for team view.
 */
import styled from 'styled-components'

import { COLORS, TABLET_WIDTH, MOBILE_WIDTH } from 'src/constants'

import { MatchHeaderLogoBox } from './matches'

/**
 * Wrapper for the team list.
 */
export const TeamsListBox = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  flex-flow: row;
  flex-wrap: wrap;
  align-content: center;
  overflow-y: auto;
  overflow-x: hidden;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    justify-content: center;
  }
`

/**
 * Wrapper for a single node with a team view within a team list.
 */
export const TeamsListNodeBox = styled.div`
  position: relative;
  width: 25%;
  max-width: 270px;
  min-width: 150px;
  
  :before {
    content: "";
    display: block;
    padding-top: 100%;
  }
`

/**
 * Single node with a team view within a team list.
 *
 * @property {string} teamLogo - logo of the team.
 */
export const TeamsListNode = styled.div`
  position: absolute;
  left: 20px;
  top: 20px;
  right: 20px;
  bottom: 20px;
  border: 1px solid ${COLORS.BORDER};
  box-shadow: 0 0 5px 0 ${COLORS.BORDER};
  background: ${COLORS.FOREGROUND} url(${props => props.teamLogo}) no-repeat center;
  background-size: calc(100% - 15px) auto;
  transition: left 0.3s, top 0.3s, right 0.3s, bottom 0.3s;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    left: 10px;
    top: 10px;
    right: 10px;
    bottom: 10px;
  }
  
  > div {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    display: flex;
    align-items: center;
    height: 0;
    width: 100%;
    background-color: rgba(255, 255, 255, 0.85);
    transition: height 0.3s;
    overflow: hidden;
    
    @media (max-width: ${TABLET_WIDTH}px) {
      height: 60px;
    }
  }
  
  :hover {
    cursor: pointer;

    > div {
      height: 60px;
    }
  }
`

/**
 * Holds name of a team within a team list node.
 */
export const TeamsListNodeName = styled.div`
  width: 100%;
  color: ${COLORS.BLUE_PRIMARY};
  text-transform: uppercase;
  font-size: 1.2rem;
  font-weight: 600;
  line-break: auto;
  word-break: break-word;
  line-height: normal;
  text-align: center;
  line-height: normal;
  user-select: none;
`

/**
 * Box for the team logo and statistics on the team detail page.
 */
export const TeamDetailBox = styled.div`
  height: 270px;
  display: flex;
  flex-flow: row;
  justify-content: center;
  margin-bottom: 40px;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    height: 220px;
  }
`

/**
 * Box for the team logo and name on the team detail page.
 */
export const TeamDetailLogoBox = styled(MatchHeaderLogoBox)`
  flex: none;
  margin-right: 140px;
  height: 100%;
  
  > h2 {
    color: ${COLORS.BLUE_PRIMARY};
    margin: 0;
    font-size: 24px;
  }

  @media (max-width: ${TABLET_WIDTH}px) {
    margin-right: 15px;
  }
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    margin-right: 0;
  }
`

/**
 * Square wrapper for the team logo image.
 *
 * @property {string} logo - url of the image to use as container's background
 */
export const TeamDetailLogo = styled.div`
  width: 220px;
  height: 220px;
  background: url(${props => props.logo}) no-repeat center;
  background-size: contain;
  margin-bottom: 15px;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    width: 200px;
    height: 200px;
  }
`

/**
 * Wrapper for the body of team detail page.
 */
export const TeamDetailBodyBox = styled.div`
  max-width: 1200px;
  margin: 30px auto 0;
`
