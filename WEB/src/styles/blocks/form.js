/**
 * Provides styled components for inputs.
 */
import styled, { css } from 'styled-components'

import { COLORS } from 'src/constants'

/**
 * Outermost wrapper for the input, defines height, margin and absolute position anchor.
 */
export const InputRow = styled.div`
  margin: 0 auto 6px;
  position: relative;
  min-height: 38px;
`

/**
 * Provides input with general styling.
 *
 * @property { boolean } [active=false] - Indicates whether the input has focus or not.
 * @property { boolean } [hasError=false] - Indicates whether the input has error.
 */
export const InputWrapper = styled.div`
  background-color: ${COLORS.BACKGROUND};
  align-items: center;
  border: 1px solid ${props => props.hasError ? COLORS.RED_PRIMARY : COLORS.BORDER};
  border-radius: 3px; 
  box-sizing: border-box;
  color: ${props => props.active ? COLORS.FONT : COLORS.GRAY_DARK};
  font-size: 14px;
  position: relative;
  width: 300px;
  display: inline-flex;
  flex-direction: row;
`

/**
 * Defines positioning of input's placeholder and value.
 */
export const TextInputBox = styled.label`
  height: 36px;
  padding: 0;
  position: relative;
  margin: 0;
  min-width: 0;
  font-weight: 400;
  flex: 1 0 0;
  display: flex;
  overflow: hidden;
`

/**
 * Holds input placeholder text that shrinks and moves on input updating.
 *
 * @property {boolean} [isFilled=false] - Indicates whether input field has any value, thus signalizing placeholder to shrink.
 */
export const TextInputPlaceholder = styled.span`
  position: absolute;
  left: 8px;
  right: 0;
  height: 36px;
  line-height: 36px;
  font-size: 12px;
  overflow: hidden;
  z-index: 5;
  user-select: none;
  pointer-events: none;
  transition: transform ease-out .1s, -webkit-transform ease-out .1s;
  transform-origin: left;
  
  ${props => props.isFilled && {
    transform: 'scale(.83333) translateY(-13px)',
  }}
`

/**
 * Holds input value.
 *
 * @property {boolean} [isFilled=false] - Indicates whether input field has any value, thus signalizing value to slightly translate.
 */
export const TextInputField = styled.input`
  width: 110%;
  background-color: ${COLORS.BACKGROUND};
  color: ${COLORS.FONT};
  overflow: hidden;
  padding: 16px 8px 9px;
  margin-top: -5px;
  text-overflow: ellipsis;
  border: 0;
  transition: transform ease-out .1s, -webkit-transform ease-out .1s;

  ${props => props.isFilled && {
    transform: 'translateY(2px)',
  }}
  
  &:active, &:focus {
    outline: 0;
  }
`

/**
 * Wrapper box for `show`/`hide` password display toggle.
 */
export const ShowPassBox = styled.div`
  height: 100%;
  margin-right: 8px;
  vertical-align: middle;
  display: flex;
  margin-left: 8px;
  align-items: stretch;
`

/**
 * Styles for button that toggles password display between plaintext and dots.
 */
export const ShowPassButton = styled.button`
  outline: 0;
  border: 0;
  background-color: transparent;
  color: ${COLORS.FONT};
  display: inline;
  padding: 0;
  position: relative;
  font-weight: 600;
  cursor: pointer;
  text-align: center;
  user-select: none;
  width: auto;
  
  &:active {
    opacity: .7;
  };
`

// Defines reusable block of css styles that can be later used along with `opacity` to darken element's background
// without affecting color of its inner text.
const buttonDarkener = css`
  z-index: auto;
  position: absolute;
  top: 0;
  left: 0;
  content: '';
  width: 100%;
  height: 100%;
  background-color: black;
`

/**
 * Wrapper box for form's submit button.
 *
 * @property {string} [backgroundColor=BLUE_PRIMARY] - Button's background color.
 * @property {boolean} [disabled=false] - Indicates whether should the button be rendered as `disabled`.
 */
export const ButtonBox = styled.div`
  position: relative;
  display: block;
  min-width: 100px;
  max-width: 300px;
  margin: 0 auto;
  align-content: stretch;
  background-color: ${props => props.backgroundColor};

  ${props => props.disabled && `
    opacity: .4;
    pointer-events: none;
  `}

  :hover:before {
    ${buttonDarkener};
    opacity: 0.2;
  }

  :active:before {
    ${buttonDarkener};
    opacity: 0.4;
  }
`

/**
 * Provides styled form button.
 *
 * @property {string} [textColor=FONT_INVERSE] - Button's font color.
 * @property {boolean} higher - Indicates whether to render higher variant of the button or standard
 */
export const FormButton = styled.button`
  width: fill-available;
  height: ${props => props.higher ? 42 : 32}px;
  border: none;
  border-radius: 2px;
  background-color: transparent;
  color: ${props => props.textColor};
  position: relative;
  box-sizing: border-box;
  cursor: pointer;
  display: block;
  font-size: ${props => props.higher ? 1.4 : 1.2}em;
  font-weight: 600;
  line-height: 1.3em;
  padding: 5px 9px;
  margin: 15px 0;
  text-align: center;
  user-select: none;

  :focus {
    outline: 0;
  }
`
