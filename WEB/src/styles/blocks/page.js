/**
 * Provides general styles for the application that are to be inherited by default and styles for general components.
 */
import styled from 'styled-components'

import { COLORS, DESIGN, MOBILE_WIDTH, TABLET_WIDTH } from 'src/constants'

/**
 * Defines transition effects during theme changing.
 */
export const ThemeWrapper = styled.div`
  height: 100%;
  ${props => props.themeChanging && `
    transition: all ${DESIGN.THEME_ANIMATION_LENGTH}s 0s;

    * {
      transition: all ${DESIGN.THEME_ANIMATION_LENGTH}s 0s !important;
    }
  `}
  
  * {
    scrollbar-color: ${COLORS.GRAY_DARK} ${COLORS.GRAY_LIGHT};
    scrollbar-width: thin;
  }
  
  * ::-webkit-scrollbar {
    appearance: none;
    width: 4px;
    height: 4px;
  }

  * ::-webkit-scrollbar-thumb {
    appearance: none;
    background-color: ${COLORS.GRAY_DARK};
    border-radius: 5px;
  }

  * ::-webkit-scrollbar-track {
    background-color: ${COLORS.GRAY_LIGHT};
  }
  
  * input:-webkit-autofill {
    background-color: inherit;
  }
`

/**
 * Defines page background, font color, content positioning and restyles page scrollbars.
 */
export const PageWrapper = styled.section`
  position: relative;
  background-color: ${COLORS.BACKGROUND};
  color: ${COLORS.FONT};
  height: 100%;
  overflow: auto;
  display: flex;
  flex-direction: column;
  
  scrollbar-color: ${COLORS.GRAY_DARK} ${COLORS.GRAY_LIGHT};
  scrollbar-width: thin;
  
  ::-webkit-scrollbar {
    appearance: none;
    width: 4px;
    height: 4px;
  }

  ::-webkit-scrollbar-thumb {
    appearance: none;
    background-color: ${COLORS.GRAY_DARK};
    border-radius: 5px;
  }

  ::-webkit-scrollbar-track {
    background-color: ${COLORS.GRAY_LIGHT};
  }
`

/**
 * Provides page content with margin and make it always fill-up all free space/
 */
export const PageContent = styled.main`
  flex-grow: 1;
  margin: 85px 80px 20px;
  transition: margin 0.4s;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 85px 10px 20px;
  } 
`

/**
 * Non-styled anchor element
 */
export const LinkWrapper = styled.a`
  color: inherit;
  cursor: inherit;
  text-decoration: inherit;
  outline: inherit;
  outline-offset: inherit;
`

/**
 * Error message box.
 */
export const ErrorBox = styled.div`
  color: ${COLORS.RED_PRIMARY};
  font-size: 1.2em;
`

/**
 * Animated loading icon wrapper. Defines icon size.
 */
export const BannerBox = styled.div`
  position: relative;
  width: 50px;
  height: 75px;
  margin: auto;
`

/**
 * Animated loading icon styled component.
 */
export const BannerImage = styled.img`
  position: absolute;
  bottom: 0;
  width: 50px;
  height: calc(100% - 25px);
  animation: appear 0.5s linear, rotate 1.5s cubic-bezier(.57,.2,.46,.88) infinite, jump 1.5s ease-in-out infinite;
  
  @keyframes appear {
    from { opacity: 0; }
    to { opacity: 0.7; }
  }
  
  @keyframes rotate {
    from { transform: rotate(0deg); }
    to { transform: rotate(360deg); }
  }
  
  @keyframes jump {
    0% { bottom: 0; }
    50% { bottom: 25px; }
    100% { bottom: 0; }
  }
`

/**
 * Styled Slider component.
 *
 * @property {number} left - Slider's left offset.
 * @property {number} width - Slider's width.
 * @property {number} top - Slider's top offset.
 */
export const Slider = styled.div`
  position: absolute;
  left: 0;
  top: ${props => props.top}px;
  transform: translateX(${props => props.left - 5}px);
  transform-origin: center;
  height: 2px;
  width: ${props => props.width + 10}px;
  background-color: ${COLORS.BLUE_PRIMARY};
  transition: transform 0.4s, top 0s, width 0.4s;
  transition-delay: 0s, 0.2s;
`

/**
 * Styled wrapper box for horizontal slider carets on detailpages.
 */
export const DetailSliderBox = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: space-between;
  margin: 15px auto 20px;
  width: min-content;
  
  > h2 {
    margin: 0 15px;
    font-weight: 600;
  }
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    margin: 5px auto 30px;

    > h2 {
      margin: 0 10px;
  }
`

/**
 * Clickable background for Modal.
 */
export const Backdrop = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  z-index: 20;
  background-color: rgba(0, 0, 0, 0.4);
  animation: appear 0.3s linear;
  
  @keyframes appear {
    from { background-color: rgba(0, 0, 0, 0); }
    to { background-color: rgba(0, 0, 0, 0.4); }
  }
`

/**
 * Wrapper for the Modal content.
 */
export const ModalBody = styled.div`
  background-color: ${COLORS.BACKGROUND};
  text-align: center;
  padding: 25px 50px;
  color: ${COLORS.FONT};
  
  > h1 {
    font-size: 42px;
    line-height: 42px;
  }
`

/**
 * Container for the confirmation modal body
 */
export const ConfirmationModalBody = styled.div`
  max-width: 550px;
  
  > h1 {
    line-height: 32px;
    font-size: 24px;
  }
  
  > div {
    display: inline-block;
    margin: 10px;
    min-width: 175px;
    
    > button {
      margin: 0;
    }
  }
`

/**
 * Container for the confirmation modal warning icon
 *
 * @property {string} iconColor - defines color of the icon
 */
export const ConfirmationModalIcon = styled.span`
  display: block;
  width: 28px;
  height: 28px;
  margin: 0 auto 15px;
  font-size: 27px;
  font-weight: 600;
  line-height: 25px;
  color: ${props => props.iconColor};
  border-radius: 50%;
  border: 2.6px solid ${props => props.iconColor};
`

/**
 * Wrapper providing column organization of child elements.
 */
export const ColumnView = styled.div`
  display: flex;
  flex-flow: row;
  height: 100%;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    flex-flow: column;
  }
`

/**
 * Wrapper component for column content view.
 */
export const ColumnContentBox = styled.div`
  display: flex;
  flex-flow: column;
  margin: 0 25px;
  transition: margin 0.5s;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 0 15px;
  }
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    margin: 0 5px;
  } 
`

/**
 * Styled component for content group.
 *
 * @property { boolean } [omitBottomMargin=false] - Indicates whether the omit the bottom margin.
 */
export const ContentGroup = styled.div`
  margin: 10px 25px ${props => props.omitBottomMargin ? 0 : 40}px;
  background-color: ${COLORS.BACKGROUND};
  z-index: 1;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    margin: 10px 0 ${props => props.omitBottomMargin ? 0 : 40}px;
  }
`

/**
 * Field for content group headline.
 *
 * @property { boolean } [isUpperCase=false] - Indicates whether to uppercase all characters in the headline.
 */
export const ContentGroupHeadline = styled.h2`
  color: ${COLORS.BLUE_PRIMARY};
  margin: 0 0 15px 0;
  text-transform: ${props => props.isUpperCase ? 'uppercase' : 'none'};
`

/**
 * Provides table with relative positioned parent to enable absolute positioning within the table.
 */
export const ContentTableBox = styled.div`
  position: relative;
`

/**
 * Wrapper for show more label on the Home screen
 */
export const ShowMoreBox = styled.div`
  position: absolute;
  right: -10px;
  top: -30px;
  font-size: 17px;
  font-weight: 600;
  text-transform: uppercase;
  user-select: none;
  color: ${COLORS.BLUE_PRIMARY};
  
  :hover {
    cursor: pointer;
  }
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    font-size: 14px;
    right: 0;
    top: -22px;
  } 
`
