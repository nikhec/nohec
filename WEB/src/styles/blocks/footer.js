/**
 * Provides styles for application's footer components.
 */
import styled from 'styled-components'

import { COLORS, TABLET_WIDTH, DESIGN } from 'src/constants'

/**
 * Styled `footer` element with margins and top border.
 */
export const PageFooter = styled.footer`
  order: 5;
  margin: 0 80px;
  border-top: 1px solid ${COLORS.BORDER};
  transition: margin 0.4s;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 0 10px;
  } 
`

/**
 * Defines styling for footer content like font color, size, text alignment, etc.
 */
export const FooterWrapper = styled.div`
  width: 100%;
  padding: 10px 0 5px;
  margin: 0 auto;
  text-align: center;
  color: ${COLORS.GRAY_DARK};
  font-size: 12px;
  font-weight: 600;
  text-transform: uppercase;
`

/**
 * Footer's row component.
 */
export const FooterRow = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: center;
  align-content: center;
  padding-bottom: 5px;
`

/**
 * Locale flag wrapper.
 */
export const LocaleFlagBox = styled.div`
  width: 28px;
  height: 28px;
  border: 1px solid ${COLORS.BORDER};
  border-radius: 50%;
  opacity: 0.6;
  position: relative;
  overflow: hidden;
  
  :hover {
    cursor: pointer;
    opacity: 1;
  }
`

/**
 * Locale flag image component.
 */
export const LocaleFlag = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  ${props => props.localeChanging && `
    transition: opacity ${DESIGN.LOCALE_ANIMATION_LENGTH}s;
    animation: spin ${DESIGN.LOCALE_ANIMATION_LENGTH}s cubic-bezier(0.25, 0.1, 0.25, 1);
    @keyframes spin { 100% { transform: rotate(-360deg); } }
  `}
  
  ${props => props.selected
    ? 'z-index: 10;'
    : `
      z-index: 11;
      opacity: 0;
    `
}
`

/**
 * Styled theme icon's button component.
 */
export const IconButton = styled.button`
  display: inline-block;
  position: relative;
  width: 30px;
  height: 30px;
  padding: 8px;
  margin-right: 10px;
  background-color: ${COLORS.FOREGROUND};
  border: 1px solid ${COLORS.BORDER};
  border-radius: 50%;
  
  &:hover {
    cursor: pointer;
    background-color: ${COLORS.BACKGROUND};
  }
  
  &:active, &:focus {
    outline: none;
  }
`

/**
 * Styled theme icon svg component.
 */
export const ThemeIcon = styled.svg`
  position: absolute;
  top: 4px;
  left: 4px;
  width: 20px;
  height: 20px;
  fill: none;
  stroke: ${COLORS.FONT};
  stroke-width: 2;
  stroke-linecap: round;
  stroke-linejoin: round;
  transform: rotate(40deg);
  transition: transform ${DESIGN.THEME_ANIMATION_LENGTH}s !important;
  
  * {
    transition: transform ${DESIGN.THEME_ANIMATION_LENGTH}s, fill ${DESIGN.THEME_ANIMATION_LENGTH}s !important;
  }
`

/**
 * Styled theme icon's line group component.
 * Sets opacity transition delays for each child line so they animate consecutively.
 */
export const ThemeIconLines = styled.g`
  > line {
    ${props => props.lineProps};
    ${Array(8).fill(1).map((_, index) => `
      &:nth-child(${index}) {
        transition: opacity ${DESIGN.THEME_ANIMATION_LENGTH / 10}s ${index * (DESIGN.THEME_ANIMATION_LENGTH / 10)}s !important;
       }
    `).join('\n')}
  }
`
