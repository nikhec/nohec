/**
 * Provides styled components used exclusively during match recording.
 */
import styled, { css } from 'styled-components'

import { COLORS, MOBILE_WIDTH } from 'src/constants'
import { MatchBodyListNode, MatchContentRowMatchType } from 'src/styles/blocks/matches'

/**
 * Outermost wrapper for the MatchEnterError component.
 */
export const MatchEnterErrorBox = styled.div`
  display: flex;
  height: 75%;
  flex-flow: column;
  justify-content: center;
  align-content: center;
  text-align: center;
  
  > h1 {
    font-size: 2em;
    line-height: 1.4em;
    margin-top: 0;
    color: ${COLORS.RED_PRIMARY};
  }
  
  > h2 {
    line-size: 1.5;
    line-height: 1.3em;
  }
  
  > h4 {
    font-size: 1.2em;
    margin-bottom: 0;
  }
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    height: 100%;
  }
`

/**
 * Container for both action buttons of game set recording
 *
 * @property {boolean} [reversed=false] - Indicates whether to reverse content of the button on wrap
 */
export const MatchRecordActionButtonsBox = styled.div`
  width: 100%;
  min-width: 100%;
  display: block;
  
  > div {
    display: flex;
    flex-flow: ${props => props.reversed ? 'wrap-reverse' : 'wrap'};
    justify-content: center;
  }
`

/**
 * Styled component for an action button of game set recording.
 *
 * @property {boolean} [disabled=false] - Indicates whether to disable pointer events on the button
 */
export const MatchRecordActionButton = styled.div`
  position: relative;
  margin: 10px;
  width: 150px;
  height: 150px;
  transition: width 0.3s, height 0.3s, background-color 0.3s;
  
  ${props => props.disabled && `
    pointer-events: none;
    opacity: 0.4;
  `}
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    width: 100px;
    height: 100px;
  }
  
  > span {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    text-align: center;
    font-size: 18px;
    font-weight: 600;
    margin: 6px 0;
  }
  
  :hover {
    cursor: pointer;
    background-color: ${COLORS.FOREGROUND};
  }
`

/**
 * Container for the actual action button icon.
 */
export const MatchRecordActionButtonIcon = styled.div`
  width: calc(100% - 35px);
  height: calc(100% - 35px);
  margin: 5px auto;
  transition: fill 0.3s;
  
  > svg {
    width: 100%;
    height: 100%;
    
    #red {
      fill: ${COLORS.RED_PRIMARY}
    }
    
    #blue {
      fill: ${COLORS.BLUE_PRIMARY}
    }
  ]
`

/**
 * Wrapper for partial game controls during match recording.
 */
export const MatchRecordGameControls = styled.div`
  margin-top: 15px;
  display: flex;
  flex-flow: row;
  align-items: center;
  user-select: none;
  
  > h1 {
    font-size: 42px;
    line-height: 42px;
    margin-top: 24px;
  }
`

/**
 * Controls column.
 */
export const MatchRecordGameControlsCol = styled.div`
  display: flex;
  flex-flow: column;
  margin: 0 10px;
`

/**
 * Wrapper for controls arrow.
 *
 * @property {boolean} [isVisible=false] - toggles visibility of the awwor.
 */
export const MatchRecordGameControlsArrow = styled.div`
  width: 62px;
  height: 40px;
  overflow: hidden;
  visibility: ${props => props.isVisible ? 'visible' : 'hidden'};
  color: ${COLORS.GRAY_DARK};
  
  :hover {
    opacity: 1;
    color: ${COLORS.BLUE_PRIMARY};
    cursor: pointer;
  }
  
  & > svg {
    margin: -25% 0 0 -25%;
    width: 150%;
    height: 200%;
  }
`

const recordBox = css`
  background: ${COLORS.FOREGROUND};
  width: 62px;
  height: 70px;
  user-select: none;
  font-size: 42px;
  line-height: 70px;
  font-weight: 700;
  text-align: center;
  border: 1px solid ${COLORS.BORDER};
  box-shadow: 0 0 5px 0 ${COLORS.BORDER};
`

/**
 * Points box within game controls.
 */
export const MatchRecordPointsBox = styled.div`
  ${recordBox};
`

/**
 * Final match note box.
 */
export const MatchRecordNote = styled.textarea`
  background: ${COLORS.FOREGROUND};
  width: 75%;
  min-width: 75%;
  max-width: 75%;
  height: 300px;
  min-height: 75px;
  margin: 15px auto;
  padding: 10px;
  font-size: 18px;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif;
  color: ${COLORS.FONT};
  border: 1px solid ${COLORS.BORDER};
  box-shadow: 0 0 5px 0 ${COLORS.BORDER};
  
  :focus, :active {
    outline: none;
  }
`

/**
 * Wrapper for the content of the overview view.
 */
export const MatchRecordOverviewBox = styled.div`
  width: 210px;
`

/**
 * Field in the match record overview displaying score for each game of the match.
 */
export const MatchOverviewScoreField = styled.span`
  float: right;
`

/**
 * Container for match record passwords section
 */
export const MatchRecordSignContent = styled.div`
  display: flex;
  flex-flow: wrap;
  justify-content: space-around;
  margin: 10px 25px 40px;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    margin: 10px 0 40px;
  }
`

/**
 * Wrapper for sign password boxes.
 */
export const MatchRecordSignFieldsBox = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: center;
  margin: 10px auto 0;
`

/**
 *
 * @property {boolean} [active=false] - Indicates whether the associated block is the one active.
 */
export const MatchRecordSignHeadline = styled(MatchContentRowMatchType)`
  text-align: center;
  color: ${props => props.active ? COLORS.BLUE_PRIMARY : COLORS.GRAY_DARK};
  
  :hover {
    cursor: pointer;
  }
`

/**
 * Sign password box.
 */
export const MatchRecordSignFields = styled.input`
  ${recordBox};
  margin: 0 10px;
  text-transform: uppercase;
  
  :focus, :active {
    outline: none;
    border: 1px solid ${COLORS.FONT};
  }
`

/**
 * Container for any error message that is to be displayed during match recording
 *
 * @property {number} errorLines - Number of lines of errors to be displayed
 */
export const MatchRecordError = styled.div`
  color: ${COLORS.RED_PRIMARY};
  text-align: center;
  margin-bottom: -5px;
  font-size: 16px;
  font-weight: 600;
  height: ${props => props.errorLines * 20}px;
  opacity: ${props => props.errorLines > 0 ? 1 : 0};
  transition: height 0.3s, opacity 0.3s;
  overflow: hidden;
  white-space: pre-wrap;
`

/**
 * Remaining time indicator for match recording's game timeout.
 *
 * @property {boolean} [highlighted=false] - Indicates the time should be highlighted in red.
 * @property {number} progress - Indicates the percentual progress of the timeout.
 * @property {boolean} [ended=false] - Indicates whether the time has run out completely.
 */
export const TimeoutRemainingIndicator = styled.div`
  position: relative;
  height: 350px;
  width: 350px;
  display: flex;
  justify-content: center;
  align-items: center;
  
  > span {
    font-size: 46px;
    padding-top: 15px;
    font-weight: 700;
    color: ${props => props.highlighted ? COLORS.RED_PRIMARY : COLORS.FONT};
    ${props => props.ended && `
      animation: blink 1s steps(4) infinite;
  
      @keyframes blink {
        24% { color: ${COLORS.RED_PRIMARY}; }
        25% { color: ${COLORS.FONT}; }
        74% { color: ${COLORS.FONT}; }
        75% { color: ${COLORS.RED_PRIMARY}; }
      }
    `}
  }
  
  > svg {
    position: absolute;
    opacity: 0.15;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }
`

/**
 * Wrapper For the substitution options.
 */
export const GameSubstitutionBox = styled.div`
  display: flex;
  flex-flow: row;
  width: 500px;
  margin: 35px 0;
  justify-content: space-between;
  align-items: center;
  
  @media (max-width: ${MOBILE_WIDTH}px) {
    width: 350px;
  }
`

/**
 * Wrapper For a single substitution column.
 */
export const GameSubstitutionColumnBox = styled.div`
  display: flex;
  flex-flow: column;
  height: fit-content;
  max-height: 400px;
  width: 200px;
  overflow-y: auto;
  overflow-x: hidden;
  border: 1px solid ${COLORS.BORDER};
  box-shadow: 0 0 5px 0 ${COLORS.BORDER};
`

/**
 * Holds name of the player within a substitution column.
 *
 * @property {string} accentColor - Overwrites default primary blue accent color.
 * @property {boolean} [accent=false] - Indicates whether the item should be accented.
 * @property {boolean} [selected=false] - Indicates whether the item is selected.
 * @property {boolean} [invalid=false] - Indicates whether is selecting this player invalid
 */
export const GameSubstitutionColumnNode = styled(MatchBodyListNode)`
  padding-left: 10px;
  text-align: left;
  user-select: none;
  transition: color 0.3s;
  position: relative;
  
  ${props => props.accent && `
    color: ${props.accentColor ?? COLORS.BLUE_PRIMARY};
    font-weight: 500;
    
    ${props.invalid && `
      :after {
        content: "!";
        position: absolute;
        font-size: 18px;
        right: 15px;
        color: ${COLORS.RED_PRIMARY}; 
      }
    `}
  `}
  
  opacity: ${props => props.invalid ? 0.5 : 1};
  
  :hover {
    cursor: pointer;
    color: ${props => props.accent ? (props.accentColor ?? COLORS.BLUE_PRIMARY) : COLORS.FONT};
    font-weight: 500;
    
    ${props => props.invalid && `
      :after {
        content: "!";
        position: absolute;
        font-size: 18px;
        right: 15px;
        color: ${COLORS.RED_PRIMARY}; 
      }
    `}
  }
`

/**
 * Container for the icon displayed between substitution columns in the modal.
 */
export const GameSubstitutionIcon = styled(MatchRecordActionButtonIcon)`
  width: 40px;
  height: 40px;
  margin: 0 10px;
`
