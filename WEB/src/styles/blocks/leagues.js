/**
 * Provides styled components for leagues panel.
 */
import styled from 'styled-components'

import { COLORS, DESIGN, TABLET_WIDTH } from 'src/constants'

/**
 * Left column box of the league panel.
 */
export const LeaguePanelLeftColumn = styled.div`
  position: fixed;
  display: flex;
  flex-flow: column;
  width: 250px;
  background-color: ${COLORS.BACKGROUND};
  height: calc(100vh - 175px);
  overflow: auto;
  
  & h1 {
    margin-top: 5px;
    font-size: 32px;
  }
  
  & h2 {
    margin-top: 0;
  }
  
  @media (max-width: ${TABLET_WIDTH}px) {
    position: inherit;
    width: 100%;
    height: auto;
    margin-bottom: 20px;
    overflow: visible;
    
    & h1, & h2 {
      text-align: center;
    }
  }
`

/**
 * Box for leagues selector on league panel.
 */
export const LeaguesPanelLeaguesBox = styled.div`
  display: flex;
  flex-flow: column;
  
  @media (max-width: ${TABLET_WIDTH}px) {
    flex-flow: row;
    justify-content: space-evenly;
    width: 100%;
  }
`

/**
 * Line holding a single league on the left leagues panel.
 *
 * @property {boolean} [selected=false] - Indicates whether is the given league currently selected.
 */
export const LeaguePanelLeague = styled.div`
  margin:  0 0 25px;
  display: flex;
  flex-flow: row;
  align-content: center;
  
  :hover {
    cursor: pointer;
    
    > span {
      color: ${COLORS.FONT};
    }
  }
`

/**
 * Box holding image for a single league on the left leagues panel.
 *
 * @property {boolean} [selected=false] - Indicates whether is the given league currently selected.
 */
export const LeaguePanelLeagueImage = styled.span`
  position: relative;
  width: 32px;
  height: 32px;
  
  > img {
    width: 100%;
    height: 100%;
  }
    
  :after {
    position: absolute;
    content: '';
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background-color: ${props => props.selected ? COLORS.BLUE_PRIMARY : COLORS.GRAY_DARK};
    transition: background-color 0.3s;
    mix-blend-mode: ${DESIGN.BLEND_MODE};
  }
`

/**
 * Box holding name of a single league on the left leagues panel.
 *
 * @property {boolean} [selected=false] - Indicates whether is the given league currently selected.
 */
export const LeaguePanelLeagueName = styled.span`
  position: relative;
  padding: 0 10px;
  font-size: 22px;
  line-height: 30px;
  font-weight: 500;
  border-width: 0;
  border-color: ${COLORS.BLUE_PRIMARY};
  border-style: solid;
  transition: color 0.3s;    
  color: ${props => props.selected ? COLORS.FONT : COLORS.GRAY_DARK};
  
  :after {
    content: '';
    width: ${props => props.selected ? '100%' : 0};
    height: 2px;
    background-color: ${COLORS.BLUE_PRIMARY};
    position: absolute;
    transition: width 0.3s; 
    left: 0;
    bottom: -4px;
  }
`

/**
 * Box providing styles for underlying season select.
 *
 * @property {boolean} [isSelectable=false] - Indicates whether the select provides any options to select from.
 */
export const SeasonSelectBox = styled.div`
  margin: 0 5px;
  width: 175px;

  ${props => props.isSelectable
    ? `border: 1px solid ${COLORS.BORDER};`
    : `& * {
      pointer-events: none;
    }`}
  
  @media (max-width: ${TABLET_WIDTH}px) {
    margin: 0 auto;
  }
`

/**
 * Right column box of the league panel.
 */
export const LeaguePanelRightColumn = styled.div`
  margin-left: 265px;
  width: calc(100% - 250px);
  
  @media (max-width: ${TABLET_WIDTH}px) {
    width: 100%;
    margin-left: 0;
  }
`
