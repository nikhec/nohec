import { apiClient } from 'src/api-client/client'

/**
 * Loads all seasons from an appropriate API endpoint.
 *
 * @function
 * @returns {Promise}
 */
export const loadSeasons = () =>
  apiClient.get('seasons/')

/**
 * Loads all leagues from an appropriate API endpoint.
 *
 * @function
 * @returns {Promise}
 */
export const loadLeagues = () =>
  apiClient.get('leagues/')
