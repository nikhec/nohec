import { apiClient } from 'src/api-client/client'

/**
 * Sends log in request to an appropriate API endpoint.
 *
 * @function
 * @param {Object} data - Log in input form data
 * @returns {Promise}
 */
export const logIn = data =>
  apiClient.post('users/login/', data)

/**
 * Sends log out request to an appropriate API endpoint.
 *
 * @function
 * @returns {Promise}
 */
export const logOut = () =>
  apiClient.post('users/logout/')

/**
 * Sends request to an appropriate API endpoint to test validity of auth session.
 *
 * @function
 * @returns {Promise}
 */
export const loggedTest = () =>
  apiClient.get('users/logged-in-test/')

/**
 * Sends request to an appropriate API endpoint to fetch user's signature for the current day.
 *
 * @function
 * @returns {Promise}
 */
export const fetchSignature = () =>
  apiClient.get('users/signature/')
