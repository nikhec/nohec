import { apiClient } from 'src/api-client/client'

/**
 * Sends load matches request to the appropriate API endpoint with optional filter arguments.
 *
 * @function
 * @param {Object} filters - Filters used for data querying
 * @returns {Promise}
 */
export const loadMatches = filters =>
  apiClient.get('matches/', { params: filters })

/**
 * Sends load my matches request to the appropriate API endpoint with optional filter arguments.
 *
 * @function
 * @param {Object} filters - Filters used for data querying
 * @returns {Promise}
 */
export const loadMyMatches = filters =>
  apiClient.get('my-matches/', { params: filters })

/**
 * Sends load match detail request to the appropriate API endpoint.
 *
 * @function
 * @param {number} matchId - Unique identifier of the match to fetch detail for
 * @returns {Promise}
 */
export const loadMatchDetail = matchId =>
  apiClient.get(`matches/${matchId}/detail/`)

/**
 * Retrieves the current state of the match and stars it if it hasn't been started yet.
 * Fails in case some other user owns the lock.
 *
 * @function
 * @param {number} matchId - Id of the match to enter recording for
 * @returns {Promise}
 */
export const enterMatch = matchId =>
  apiClient.post(`matches/${matchId}/enter/`)
