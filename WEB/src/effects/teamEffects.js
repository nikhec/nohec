import { apiClient } from 'src/api-client/client'

/**
 * Loads team for currently selected season from an appropriate API endpoint.
 *
 * @function
 * @param {Object} data - Log in input form data
 * @returns {Promise}
 */
export const loadTeams = data =>
  apiClient.get('teams/', data)

/**
 * Loads team detail for given team id from an appropriate API endpoint.
 *
 * @function
 * @param {number} teamId - Unique identifier of the team
 * @returns {Promise}
 */
export const loadTeamDetail = teamId =>
  apiClient.get(`teams/${teamId}/detail/`)

/**
 * Loads team standings for currently selected league from an appropriate API endpoint.
 *
 * @function
 * @param {number} leagueId - Unique identifier of the league
 * @returns {Promise}
 */
export const loadLeagueStandings = leagueId =>
  apiClient.get(`leagues/${leagueId}/standings/`)

/**
 * Loads team statistics for currently selected team detail from an appropriate API endpoint.
 *
 * @function
 * @param {number} teamId - Unique identifier of the team
 * @returns {Promise}
 */
export const loadTeamStatistics = teamId =>
  apiClient.get(`teams/${teamId}/statistics/`)
