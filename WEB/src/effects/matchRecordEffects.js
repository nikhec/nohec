import { apiClient } from 'src/api-client/client'

/**
 * Sends persisting request for selected captains during match recording.
 *
 * @function
 * @param {number} matchId - Id of the match being recorded
 * @param {Object} data - Captains object
 * @returns {Promise}
 */
export const selectCaptains = (matchId, data) =>
  apiClient.post(`matches/${matchId}/captains/`, data)

/**
 * Sends persisting request for selected players of partial game during match recording.
 *
 * @function
 * @param {number} matchId - Id of the match being recorded
 * @param {object} data - Data to be persisted
 * @returns {Promise}
 */
export const selectPlayers = (matchId, data) =>
  apiClient.post(`matches/${matchId}/game/`, data)

/**
 * Sends persisting request for selected set of a partial game during match recording.
 *
 * @function
 * @param {number} matchId - Id of the match being recorded
 * @param {object} data - Data to be persisted
 * @returns {Promise}
 */
export const submitSet = (matchId, data) =>
  apiClient.post(`/matches/${matchId}/set/`, data)

/**
 * Sends persisting request for a final note for the match.
 *
 * @function
 * @param {number} matchId - Id of the match being recorded
 * @param {object} data - Data to be persisted
 * @returns {Promise}
 */
export const submitNote = (matchId, data) =>
  apiClient.post(`/matches/${matchId}/note/`, data)

/**
 * Sends finalization request for the match recording with provided password.
 *
 * @function
 * @param {number} matchId - Id of the match being recorded
 * @param {object} data - Data to be persisted
 * @returns {Promise}
 */
export const submitSign = (matchId, data) =>
  apiClient.post(`/matches/${matchId}/sign/`, data)
