This document describes the **Nohec - Web Application** project by providing:
 - overview of the stack used
 - overview of the CLI scripts available
 - instructions on how to set-up and run the project
 - high level description of the architecture 
 - documentation for the internal structure

## Stack
Only technologies with significant effect on the project's architecture are listed, logically grouped together.
  - [React](https://reactjs.org/)
  - [react-router-dom](https://www.npmjs.com/package/react-router-dom) + [history](https://www.npmjs.com/package/history) + [react-router-redux](https://www.npmjs.com/package/react-router-redux) + [connected-react-router](https://github.com/supasate/connected-react-router)
  - [redux](https://redux.js.org/) + [redux-saga](https://redux-saga.js.org/)
  - [prop-types](https://www.npmjs.com/package/prop-types)
  - [axios](https://www.npmjs.com/package/axios)
  - [styled-components](https://styled-components.com/)
  - [babel](https://babeljs.io/) + [webpack](https://webpack.js.org/)

## CLI scripts
The project uses `npm` package manager. Even though the scripts alone can be executed
using `yarn` package manager as well, their correct functioning cannot be guaranteed because
`yarn` uses different techniques of package version resolving than `npm` and only `package-lock.json` is provided.

**Start the proxy and the development servers**
```
npm start # proxy defaults to port 8080, dev to 8081
```

**Start only the development server**
```
npm start-dev # dev server defaults to port 8081
```

**Start only the proxy server**
```
npm start-proxy # proxy defaults to port 8080
```

**Transpile and bundle the application**
```
npm run build # outputs to the /dist directory in the root
```

**Build the latest WEB documentation from JSDocs + generate docs entrypoint**
```
npm run docs # outputs to the /WEB/docs directory
```

**Performs eslint code style linting**
```
npm run lint # outputs indentified errors and warnings to the console
```

**Performs eslint code style linting and attempts to fix the issues** 
```
npm run lint-fix # outputs unresolvable issues to the console
```

**Installs git hooks used during development**
```
npm run configure
```

#### Available environmental variables
 - `SERVER_PORT` - **Important** (in case it is not left blank on purpose) API server port
 - `ENVIRONMENT` - **Important** (any other value than `production` is treated as dev)
 - `PROXY_PORT` - Port dev proxy starts on, defaults to `8080`
 - `DEV_PORT` - Port dev server start on, defaults to `8081`
 - `MEDIA_PORT` - Media server port, defaults to the value of `SERVER_PORT`

#### Running the Web application locally
In the `/WEB` directory:
  1. execute `npm install` to download packages from the npm repository
  1. execute `npm start` to start webpack dev server on its default port `8081`

## Architecture
As usual for React applications, it injects itself into predefined `<div>` element
with id attribute set to `root` in the `assets/index.html` file. This file is server by the API along with produced
JS bundles and styles.

The application operates as a SPA (single page application), powered by the `<Router>` component which
decides which page to render based on the current value of the `location` object. Each page has its
corresponding component located in the `components/pages` directory. Page components then usually compose
the UI from other, smaller, components, which then consist from `styled-components` located inside
the `styles/blocks` directory. Shared functionality of the components is usually extracted into
either `utils` or `hooks`, based on the nature of the extracted functionality.

#### Store
The backbone of the application is global Redux store, accessible from all components via select hook.
This store holds and provides general information about the state of the WEB application.
The store object has following properties:
 - `user` - used to determine whether and which user is currently logged in
 - `intl` - internationalization object with loaded localized messages for currently selected locale
 - `router` - object from `connected-react-router` providing access to current `location` and `history` objects
 - `customization` - object holding information about currently selected `locale` and `theme`
 - `matchRecord` - object holding information about currently recorded match, this is used only during match recording process

#### Communication
Communication of the WEB application with the API server is handled by the `axios` client, which manages setting of
correct headers for the requests and parsing of the API responses into objects. During the development, the axios client
can, be made to communicate with an included proxy server, 
which then performs actual communication with the API server and handles correct preflight request responding to avoid
CORS issues.

Actual requests sent by the WEB client are defined under the `effects` directory. These effects are triggered
from sagas, functions found in the `sagas` directory, as usual reaction to action dispatching. Occasionally, an appropriate
action handler may only trigger `localStorage` changes and/or dispatch other actions. Actions are defined under the
`actions` directory and can be viewed as means of messaging between the React UI, Redux sagas and functions that update
underlying Redux store, reducers. When React component or function within a Redux saga dispatches an action, it can be
handled either by some other Redux saga, or Redux store management functions called reducers. Reducers are simple
functions defined in the `reducers` directory that perform Redux store updates based on the payload provided to
the action, and the previous value of the fragment of the store it manages. Finally, to abstract mapping of API
responses to shapes and structures used by the WEB application, a set of functions called selectors, located in the
`selectors` directory, are used. These functions perform nothing more but a simple mapping, reshaping and grouping.
