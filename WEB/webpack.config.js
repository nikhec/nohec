const path = require('path')
const webpack = require('webpack')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
require('dotenv').config()

const publicPath = process.env.ENVIRONMENT === 'production' ? '/static/frontend/' : '/'

module.exports = {
  // Enable sourcemaps for debugging webpack's output.
  devtool: process.env.ENVIRONMENT === 'production' ? undefined : 'source-map',

  context: path.resolve(__dirname, '.'),
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    alias: {
      '~': path.resolve(__dirname, '.'),
      'src': path.resolve(__dirname, './src'),
      'assets': path.resolve(__dirname, './assets'),
    },
  },
  output: {
    filename: 'main.[contenthash].js',
    chunkFilename: '[name].[contenthash].js',
    path: path.resolve(__dirname, './dist'),
    publicPath,
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      minSize: 20000,
      maxInitialRequests: Infinity,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: module =>
            `npm.${module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1].replace('@', '')}`,
        },
      },
    },
  },
  entry: {
    js: ['babel-polyfill', './src/index.jsx'],
  },
  devServer: {
    historyApiFallback: true,
    port: process.env.DEV_PORT ?? 8081,
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'file-loader',
      },
      // Enables inputting svg files (.svgr) as components to further style them
      // without blocking standard way of .svg files importing
      {
        test: /\.svg$/,
        use: [
          {
            loader: '@svgr/webpack',
            options: {
              svgo: false,
            },
          },
        ],
      },
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      process.env.ENVIRONMENT === 'production'
        ? {}
        : {
          enforce: 'pre',
          test: /\.js$/,
          loader: 'source-map-loader',
        },
    ],
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: path.resolve(__dirname, './assets/index.html'),
      publicPath,
    }),
    new CopyPlugin({
      patterns: [
        { from: './assets/manifest.json' },
        { from: './assets/favicon.ico' },
        { from: './assets/icons' },
      ],
      options: {
        concurrency: 100,
      },
    }),
    new webpack.DefinePlugin({
      'process.env.SERVER_PORT': (process.env.ENVIRONMENT !== 'production' && process.env.PROXY_PORT) || process.env.SERVER_PORT,
      'process.env.MEDIA_PORT': process.env.SERVER_PORT,
    }),
  ],
}
